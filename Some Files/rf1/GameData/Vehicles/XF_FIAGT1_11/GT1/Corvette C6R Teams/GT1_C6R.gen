// This file specifices how to generate the vehicle graphics.
// It is pointed to by one or more *.veh files.
//
// All information except tokens are copied directly.
// Tokens have the format "<value>" where value is one of the following:
//   ID        - this is replaced by the proper slot identification (000, 001, etc.), or
//   1,2,3,etc - this is replaced by the first 1,2,3 characters of the
//               vehicle filename
//   VEHDIR    - this is replaced with the proper vehicles directory (e.g. E:\Data\season01\vehicles)
//   TEAMDIR   - this is replaced with the proper team directory (e.g. season01\vehicles\williams)
//   SPIN      - for the vehicle spinner in the options only
//   NOTSPIN   - for everything EXCEPT the vehicle spinner
//   LOW       - low detail only
//   MED       - medium detail only
//   HIGH      - high detail only
//   MAX       - maximum detail only
//   DASHLOW   - low detail dash
//   DASHHIGH  - high detail dash
//
// Example:
//   For slot #2 using the vehicle file "nicecar.veh", the line
//   "Instance=SLOT<ID>   MeshFile=<123>gva.gmt" would be replaced with
//   "Instance=SLOT002   MeshFile=nicgva.gmt"


//----------------------------------------

SearchPath=<TEAMDIR>..\
SearchPath=<TEAMDIR>
SearchPath=<VEHDIR>

SearchPath=<VEHDIR>\GT1\
SearchPath=<VEHDIR>\GT1\Corvette C6R Teams\
//SearchPath=<VEHDIR>\GT1\Corvette C6R Teams\MAS\
//SearchPath=<VEHDIR>\GT1\Corvette C6R Teams\TEX\
SearchPath=<VEHDIR>\Stex\
SearchPath=<VEHDIR>\Talent\

MASFile=GT1_C6R.mas
//MASFile=GT1_C6RCOM.mas
MASFILE=STEX.MAS
MASFILE=TALENT.MAS

//----------------------------------------

Actor=VEHICLE

Instance=SLOT<ID>
{
  Moveable=True
//------------------------------------MAX CAR SETTINGS---------------------------------------------
<MAX> MeshFile=c6r_body_a.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(30.0) ShadowReceiver=True
<MAX> MeshFile=c6r_body_b.gmt CollTarget=False HATTarget=False LODIn=(30.0) LODOut=(60.0) ShadowReceiver=True
<MAX> MeshFile=c6r_body_c.gmt CollTarget=False HATTarget=False LODIn=(60.0) LODOut=(100.0) ShadowReceiver=True
<MAX> MeshFile=c6r_body_d.gmt CollTarget=False HATTarget=False LODIn=(100.0) LODOut=(350.0) ShadowReceiver=True
//------------------------------------HIGH CAR SETTINGS--------------------------------------------
<HIGH> MeshFile=c6r_body_a.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(20.0) ShadowReceiver=True
<HIGH> MeshFile=c6r_body_b.gmt CollTarget=False HATTarget=False LODIn=(20.0) LODOut=(40.0) ShadowReceiver=True
<HIGH> MeshFile=c6r_body_c.gmt CollTarget=False HATTarget=False LODIn=(40.0) LODOut=(70.0) ShadowReceiver=True
<HIGH> MeshFile=c6r_body_d.gmt CollTarget=False HATTarget=False LODIn=(70.0) LODOut=(350.0) ShadowReceiver=True
//------------------------------------MED CAR SETTINGS---------------------------------------------
<MED> MeshFile=c6r_body_b.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(40.0) ShadowReceiver=True
<MED> MeshFile=c6r_body_c.gmt CollTarget=False HATTarget=False LODIn=(40.0) LODOut=(80.0) ShadowReceiver=True
<MED> MeshFile=c6r_body_d.gmt CollTarget=False HATTarget=False LODIn=(80.0) LODOut=(300.0) ShadowReceiver=True
//------------------------------------LOW CAR SETTINGS---------------------------------------------
<LOW> MeshFile=c6r_body_b.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(20.0) ShadowReceiver=True
<LOW> MeshFile=c6r_body_c.gmt CollTarget=False HATTarget=False LODIn=(20.0) LODOut=(50.0) ShadowReceiver=True
<LOW> MeshFile=c6r_body_d.gmt CollTarget=False HATTarget=False LODIn=(50.0) LODOut=(300.0) ShadowReceiver=True

//------------------------------------SHADOW AND COLLISION OBJECTS---------------------------------
MeshFile=c6r_shadow.gmt CollTarget=False HATTarget=False ShadowObject=(True, Solid, 256, 256, AMBIENTSHADA.DDS) LODIn=(0.0) LODOut=(200.0)
MeshFile=c6r_collision.gmt Render=False CollTarget=True HATTarget=False LODIn=(0.0) LODOut=(200.0)

Actor=VEHICLE

Instance=COCKPIT
{
  Moveable=True
  MeshFile=c6r_cpit.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(3.0) ShadowReceiver=True
  MeshFile=c6r_cpit_body.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(3.0) ShadowReceiver=True
  MeshFile=c6r_cpit_grill.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(3.0) ShadowReceiver=True
  MeshFile=c6r_hood.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(3.0) ShadowReceiver=True
  MeshFile=c6r_cpit_TV.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(3.0) ShadowReceiver=False
  MeshFile=c6r_cpit_windows.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(3.0) ShadowReceiver=False
  MeshFile=c6r_motec.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(3.0) ShadowReceiver=True
  MeshFile=c6r_mirror_ds.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(3.0) ShadowReceiver=False
  MeshFile=c6r_mirror_ps.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(3.0) ShadowReceiver=False
  <DIRTEXISTS> MeshFile=C6R_CPIT_dirtscreen.GMT CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(3.0) ShadowReceiver=False
}
Instance=WINDOWS
{
  Moveable=True
  MeshFile=c6r_window.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(350.0) ShadowReceiver=True
}
////////////////////////////////////Steering Wheel/////////////////
Instance=WHEEL
{
  Moveable=True
  MeshFile=c6r_s_wheel.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(10.0) ShadowReceiver=True
}
//------------------------------------Headlights and Tail Lights------------------------------------------------//
//////////////////////////////////////Headlight Projectors/////////////////////////////////////////////
  Projector=Headlight
  {
   Moveable=True Dyn=True Active=False Pos=(0.205, 18.0, 15.0) Dir=(0.0, -.26, -.6) Radius=(10.0) Blend=(InvDstColor, One) Texmap=headlight1.tga
  }

//  Projector=Headlight
//  {
//        Moveable=True Active=False Pos=(0.0, 0.10, -75.6) Dir=(0.0, -0.5, -1.0) Radius=(28.0) LODOut=(350.0) Blend=(InvDstColor, One) Dyn=True Texmap=HEADLIGHT_GT1.tga
//  }
////////////////////////////////////////////////////////////////////////////////////////////////////////
  Instance=HlightDS   //Headlights -Driver Side
  {
    Moveable=True 
    MeshFile=hlglo_DS.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(350.0) 
  }
  Instance=HlightPS   //Headlights -Passenger Side
  {
    Moveable=True
    MeshFile=hlglo_PS.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(350.0) 
  }
  Instance=BLIGHTDS   //Brakelights -Driver Side
  {
    Moveable=True
    MeshFile=blglo_DS.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(350.0) 
  }
  Instance=BLIGHTPS   //Brakelights -Passenger Side
  {
    Moveable=True
    MeshFile=blglo_PS.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(350.0) 
  }
  Instance=RAINLIGHT // pluie ou limiteur
  {
    Moveable=True
    MeshFile=C6R_TLIGHT.GMT CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(200.0)
  }
//----------------------------------END OF HEADLIGHTS AND TAILLIGHTS------------------------------//
/////////////////////////////////////////////////TIRES////////////////////////////////////////////////////////
/////////////////////////////////LEFT FRONT TIRE AND SPINDLE//////////////////////////////////////////////////
  Instance=LFTIRE
  {
    Moveable=True
    <MAX> MeshFile=c6r_tlfa_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(350.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <MAX> MeshFile=c6r_rlfa_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(50.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <MAX> MeshFile=c6r_rlfb_<1>.gmt CollTarget=False HATTarget=False LODIn=(50.00) LODOut=(350.00) ShadowReceiver=True 
    <HIGH> MeshFile=c6r_tlfa_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(350.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <HIGH> MeshFile=c6r_rlfa_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(30.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <HIGH> MeshFile=c6r_rlfb_<1>.gmt CollTarget=False HATTarget=False LODIn=(30.00) LODOut=(350.00) ShadowReceiver=True 
    <MED> MeshFile=c6r_tlfa_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(300.00) ShadowReceiver=True 
    <MED> MeshFile=c6r_rlfa_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(20.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <MED> MeshFile=c6r_rlfb_<1>.gmt CollTarget=False HATTarget=False LODIn=(20.00) LODOut=(300.00) ShadowReceiver=True 
    <LOW> MeshFile=c6r_tlfa_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(300.00) ShadowReceiver=True 
    <LOW> MeshFile=c6r_rlfb_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(300.00) ShadowReceiver=True
    MeshFile=c6r_front_brake_l.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(50.00) ShadowReceiver=True

  }
  Instance=LFSpindle
  {
    Moveable=True
    MeshFile=c6r_front_caliper_l.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(10.00) ShadowReceiver=True
    MeshFile=C6R_bdglfa.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(100.00)
  }
/////////////////////////////////RIGHT FRONT TIRE AND SPINDLE/////////////////////////////////////////////////
  Instance=RFTIRE
  {
    Moveable=True
    <MAX> MeshFile=c6r_trfa_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(350.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <MAX> MeshFile=c6r_rrfa_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(50.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <MAX> MeshFile=c6r_rrfb_<1>.gmt CollTarget=False HATTarget=False LODIn=(50.00) LODOut=(350.00) ShadowReceiver=True 
    <HIGH> MeshFile=c6r_trfa_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(350.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <HIGH> MeshFile=c6r_rrfa_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(30.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <HIGH> MeshFile=c6r_rrfb_<1>.gmt CollTarget=False HATTarget=False LODIn=(30.00) LODOut=(350.00) ShadowReceiver=True 
    <MED> MeshFile=c6r_trfa_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(300.00) ShadowReceiver=True 
    <MED> MeshFile=c6r_rrfa_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(20.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <MED> MeshFile=c6r_rrfb_<1>.gmt CollTarget=False HATTarget=False LODIn=(20.00) LODOut=(300.00) ShadowReceiver=True 
    <LOW> MeshFile=c6r_trfa_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(300.00) ShadowReceiver=True 
    <LOW> MeshFile=c6r_rrfb_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(300.00) ShadowReceiver=True
    MeshFile=c6r_front_brake_r.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(50.00) ShadowReceiver=True
  }
  Instance=RFSpindle
  {
    Moveable=True
    MeshFile=c6r_front_caliper_r.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(10.00) ShadowReceiver=True
    MeshFile=C6R_bdgrfa.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(100.00)
  }
/////////////////////////////////LEFT REAR TIRE//////////////////////////////////////////////////////////////
  Instance=LRTIRE
  {
    Moveable=True
    <MAX> MeshFile=c6r_tlra_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(350.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <MAX> MeshFile=c6r_rlra_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(50.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <MAX> MeshFile=c6r_rlrb_<1>.gmt CollTarget=False HATTarget=False LODIn=(50.00) LODOut=(350.00) ShadowReceiver=True 
    <HIGH> MeshFile=c6r_tlra_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(350.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <HIGH> MeshFile=c6r_rlra_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(30.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <HIGH> MeshFile=c6r_rlrb_<1>.gmt CollTarget=False HATTarget=False LODIn=(30.00) LODOut=(350.00) ShadowReceiver=True 
    <MED> MeshFile=c6r_tlra_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(300.00) ShadowReceiver=True 
    <MED> MeshFile=c6r_rlra_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(20.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <MED> MeshFile=c6r_rlrb_<1>.gmt CollTarget=False HATTarget=False LODIn=(20.00) LODOut=(300.00) ShadowReceiver=True 
    <LOW> MeshFile=c6r_tlra_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(300.00) ShadowReceiver=True 
    <LOW> MeshFile=c6r_rlrb_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(300.00) ShadowReceiver=True
    MeshFile=c6r_rear_brake_l.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(50.00) ShadowReceiver=True
  }
  Instance=LRSpindle
  {
    Moveable=True
    MeshFile=c6r_rear_caliper_l.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(10.00) ShadowReceiver=True
    MeshFile=C6R_bdglra.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(100.00)
  }
/////////////////////////////////RIGHT REAR TIRE/////////////////////////////////////////////////////////////
  Instance=RRTIRE
  {
    Moveable=True
    <MAX> MeshFile=c6r_trra_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(350.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <MAX> MeshFile=c6r_rrra_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(50.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <MAX> MeshFile=c6r_rrrb_<1>.gmt CollTarget=False HATTarget=False LODIn=(50.00) LODOut=(350.00) ShadowReceiver=True 
    <HIGH> MeshFile=c6r_trra_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(350.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <HIGH> MeshFile=c6r_rrra_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(30.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <HIGH> MeshFile=c6r_rrrb_<1>.gmt CollTarget=False HATTarget=False LODIn=(30.00) LODOut=(350.00) ShadowReceiver=True 
    <MED> MeshFile=c6r_trra_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(300.00) ShadowReceiver=True 
    <MED> MeshFile=c6r_rrra_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(20.00) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
    <MED> MeshFile=c6r_rrrb_<1>.gmt CollTarget=False HATTarget=False LODIn=(20.00) LODOut=(300.00) ShadowReceiver=True 
    <LOW> MeshFile=c6r_trra_<2>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(300.00) ShadowReceiver=True 
    <LOW> MeshFile=c6r_rrrb_<1>.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(300.00) ShadowReceiver=True
    MeshFile=c6r_rear_brake_r.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(50.00) ShadowReceiver=True
  }
  Instance=RRSpindle
  {
    Moveable=True
    MeshFile=c6r_rear_caliper_r.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(10.00) ShadowReceiver=True
    MeshFile=C6R_bdgrra.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(100.00)
  }
///////////////////////////////////SPOILER/REARWING//////////////////////////
  Instance=RWING
  {
    Moveable=True
    MeshFile=c6r_wing.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(350.0) ShadowCaster=(True, Solid, 512, 512) ShadowReceiver=True
  }
////////////////////////////////////FRONT WING////////////////////
  Instance=FWING
  {
    Moveable=True
    MeshFile=c6r_front_blade.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(100.0) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
  }
//////////////////////////////////// DEBRIS ////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////HOOD/////////////////
  Instance=DEBRIS0
  {
    Moveable=True
    MeshFile=c6r_hood.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(350.0) ShadowReceiver=True
  }
///////////////////////////////////DIFFUSER//////////////////////////
  Instance=DEBRIS1
  {
    Moveable=True
    MeshFile=c6r_rear_difuser.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(200.0) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
  }
///////////////////////////////////GRILL//////////////////////////
  Instance=DEBRIS2
  {
    Moveable=True
    MeshFile=c6r_body_grill.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(200.0) ShadowReceiver=True ShadowCaster=(True, Solid, 128, 128)
  }
//////////////////////////////////// DRIVER ////////////////////////////////////////////////////////////////////////////////
  Instance=Driver
  {
    Moveable=True
    MeshFile=c6r_driver.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(50.0) ShadowReceiver=True
  }
  Instance=Helmet
  {
    Moveable=True
    MeshFile=c6r_helmet.gmt CollTarget=False HATTarget=False LODIn=(0.0) LODOut=(100.0) ShadowReceiver=True
  }
//////////////////////////////////// EXHAUST BACKFIRES///////////////////////////////////////////////////////////////////////
  Instance=BACKFIRE
  {
    Moveable=True
    MeshFile=c6r_backfire.gmt CollTarget=False HATTarget=False LODIn=(0.00) LODOut=(100.0) ShadowReceiver=False
  }
}