float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
//float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float  Fog    : FOG;

   float4 Tex    : TEXCOORD0; // base=xy, spec=zw
   float2 Bump   : TEXCOORD1; // bump map
  
   float3 Light  : TEXCOORD2;
   float3 EyeVec : TEXCOORD3;
   
   float3 Basis1 : TEXCOORD4; // 3x3 matrix
   float3 Basis2 : TEXCOORD5; // 3x3 matrix
   float3 Basis3 : TEXCOORD6; // 3x3 matrix
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);
sampler2D sSpec : register (s1);
sampler2D sBump : register (s2);

PS_OUTPUT MAINFUNCTION (PS_INPUT Input) 
{
  PS_OUTPUT Out;
  
  // sample the textures
  float4 tex = tex2D (sTex0, Input.Tex.xy);
  float4 spec = tex2D (sSpec, Input.Tex.zw) * SPECULARSCALE + float4(1.0, 1.0, 1.0, 1.0) * SPECULARBIAS;
  float3 bump = (tex2D (sBump, Input.Bump.xy).rgb - float3(0.5, 0.5, 0.5)) * 2.0;

  // put the normal into eye space using transpose texture basis
  float3 viewBump;
  viewBump.x = dot (Input.Basis1, bump);
  viewBump.y = dot (Input.Basis2, bump);
  viewBump.z = dot (Input.Basis3, bump);

  float3 normLight = normalize (Input.Light);
  float3 normEyeVec = normalize (Input.EyeVec);

  // compute specular color 
  float3 halfway = normalize (normLight - normEyeVec); 
  
  half4 specular;
  specular.rgb = saturate (pow (dot( halfway, viewBump ), SPECULARPOWER)) * float3(2.5, 2.4, 2.3);
  specular.rgb *= spec.rgb * diffuseColor.rgb;
  specular.a = 0;

  // compute diffuse color
  half4 diffuse = saturate(dot (normLight, viewBump)) * diffuseColor + ambientColor * AMBIENTSCALE;
  diffuse = saturate (diffuse);

  // Calculate fresnel effect for better reflections
  float dotEyeVecNormal = abs(dot(normEyeVec, viewBump));
  float fresnel = SKYFRESNELBASE + SKYFRESNELSCALE * pow(1 - dotEyeVecNormal, SKYFRESNELPOWER);
  float fresnel2 = SUNFRESNELBASE + SUNFRESNELSCALE * pow(1 - dotEyeVecNormal, SUNFRESNELPOWER);
  
  // Final color
  Out.Color.rgb = (tex.rgb * diffuse.rgb) + specular.rgb * fresnel2 + float3(0.6, 0.7, 0.8) * spec.rgb * fresnel;
  Out.Color.a = 1.0;

  return (Out);
}
