float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 dirLight : register (c14);
float4 specularColor : register (c3); //power in c3.a
//float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
   float4 Texc0  : TEXCOORD0;
   float4 Texc1  : TEXCOORD1;
   float4 Texc2  : TEXCOORD2;
   float4 EyeVec : TEXCOORD3;
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sTex0 : register (s0);


PS_OUTPUT SRPL_CLOUDS (PS_INPUT Input) 
{
  PS_OUTPUT Out;

  float day = max(diffuseColor.r + diffuseColor.g + diffuseColor.b - 0.6, 0.0) / 2.4;

  // Sample the textures
  float tex1 = tex2D (sTex0, Input.Texc0.xy).r;
  float tex2 = tex2D (sTex0, Input.Texc0.zw).r;
  float tex3 = tex2D (sTex0, Input.Texc1.xy).r;

  float value = tex1 * 0.42 + tex2 * 0.26 + tex3 * 0.15;
  value -= 0.2 * pow(Input.EyeVec.w, 0.5);
  value += (day - 0.5) * 0.12;

  if (value < 0.38)
  {
    discard;
  }

  float tex4 = tex2D (sTex0, Input.Texc1.zw).r;
  float tex5 = tex2D (sTex0, Input.Texc2.xy).r;
  float tex6 = tex2D (sTex0, Input.Texc2.zw).r;

  value +=  tex4 * 0.08 + tex5 * 0.06 + tex6 * 0.03;

  value = max(value, 0.55);

  float3 normEyeVec = normalize (Input.EyeVec.xyz);
  float3 LightVec = normalize(worldLight.xyz);
  float dusk = 1.0 - min(1.0, abs(day - 0.4) / 0.4);
  float sun = max(dot(LightVec, normEyeVec), 0.0);

  value = pow(max((0.75 - value) * 5.0, 0.0), 2.0);


float3 sunColor = float3(1.0, 0.96, 0.90) * day;
float3 cloudColor = float3(0.01, 0.02, 0.03)
 + float3(0.99, 0.98, 0.97) * day
 + (  float3(0.4, 0.1, 0.0) * sun
    + float3(0.2, 0.2, 0.2)
    + float3(0.3, 0.4, 0.1) * sun * value) * dusk;

  Out.Color.rgb =
   cloudColor * (0.7 + 0.4 * value)
   + sunColor * pow(sun, 3.0) * (0.1 + 0.25 * value);

  Out.Color.a = min((2.0 - value * 2.0), 1.0) * Input.Color.a
    * 10.0 * min(0.1, Input.EyeVec.w);

  return (Out);
}
