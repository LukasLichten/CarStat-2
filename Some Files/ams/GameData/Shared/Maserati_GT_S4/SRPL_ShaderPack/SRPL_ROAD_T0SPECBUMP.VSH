#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4 worldEye : register (c8);
float4 fogData : register (c9);
float4 dirLight : register (c14);
float4 worldLight    : register (c2);
float4x4 viewMatrix : register (c16);
float4x4 invViewMatrix : register (c20);

int4 numOmniLight : register (i0); // (count, init, stride, 0)
//int4 numSpotLight : register (i1); // (count, init, stride, 0)

float4 omniPosRad[64]   : register (c72);
float4 omniRGBDelta[64] : register (c136);

float4 spotPosRad[64]   : register (c72);
float4 spotDirCone[64]  : register (c136);
float4 spotRGBDelta[64] : register (c200);




struct VS_OUTPUT
{
   float4 Pos    : POSITION;
   float4 Diff   : COLOR0;
   float  Fog   : FOG;

   float4 Tex    : TEXCOORD0; // base=xy, spec=zw
   float2 Bump   : TEXCOORD1; // bump=xy
  
   float3 Light  : TEXCOORD2; // tangent space light
   float3 EyeVec : TEXCOORD3; // tangent space eye vec

   float3 Basis1 : TEXCOORD4; // 3x3 matrix
   float3 Basis2 : TEXCOORD5; // 3x3 matrix
   float3 Basis3 : TEXCOORD6; // 3x3 matrix
};

VS_OUTPUT MAINFUNCTION (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float4 inTangent : TANGENT, float4 inBiNorm : BINORMAL, float2 inTex : TEXCOORD0, float2 inSpec : TEXCOORD1, float2 inBump : TEXCOORD2)
{
  VS_OUTPUT Out;
  
  // Transform vertex position into view space:
  Out.Pos = mul (viewProjMatrix, inPos);

//Out.Basis1 = float3(1.0, 0.0, 0.0);
//Out.Basis2 = float3(1.0, 0.0, 0.0);
//Out.Basis3 = float3(1.0, 0.0, 0.0);

//Out.Light = float3(1.0, 0.0, 0.0);
//Out.EyeVec = float3(1.0, 0.0, 0.0);


  float3 worldPos = mul (worldMatrix, inPos);

  float3 worldS = mul (worldMatrix, inTangent.xyz);
  float3 worldT = mul (worldMatrix, inBiNorm.xyz);
  float3 worldSxT = mul (worldMatrix, inNorm.xyz);

  Out.Basis1.x = worldS.x;
  Out.Basis1.y = worldT.x;
  Out.Basis1.z = worldSxT.x;
  Out.Basis2.x = worldS.y;
  Out.Basis2.y = worldT.y;
  Out.Basis2.z = worldSxT.y;
  Out.Basis3.x = worldS.z;
  Out.Basis3.y = worldT.z;
  Out.Basis3.z = worldSxT.z;
  
  Out.Light = dirLight;
  Out.EyeVec = worldPos - invViewMatrix[3];

  float3 worldNorm = mul (worldMatrix, inNorm.xyz);
  float3 eyeVec = (worldEye + worldPos.xyz);

  Out.Fog = (fogData[1] - (length (eyeVec.xyz) * fogData[0]));

  // Propagate color and texture coordinates:
  Out.Diff = inColor;
  Out.Tex.xy = inTex;
  Out.Tex.zw = inSpec;
  Out.Bump.xy = inBump;

  return (Out);
}

