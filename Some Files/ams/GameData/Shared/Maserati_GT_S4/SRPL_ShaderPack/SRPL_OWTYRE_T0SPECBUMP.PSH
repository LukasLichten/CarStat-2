float4 ambientColor  : register (c0);
float4 diffuseColor  : register (c1);
float4 worldLight    : register (c2);
float4 specularColor : register (c3); //power in c3.a
float4 blendPct      : register (c4); //base/cube blend pct in c4.a

struct PS_INPUT
{
   float4 Color  : COLOR0;
#ifdef OMNISCALE
   float4 Omni   : COLOR1;
#endif
   float  Fog    : FOG;

   float4 Base   : TEXCOORD0; // base=xy, spec=zw
   float2 Bump   : TEXCOORD1; // bump map
 
   float3 Light  : TEXCOORD2;
   float3 EyeVec : TEXCOORD3;
   
   float3 Basis1 : TEXCOORD4; // tangent space eye vec
   float3 Basis2 : TEXCOORD5; // tangent space eye vec
   float3 Basis3 : TEXCOORD6; // tangent space eye vec
};

struct PS_OUTPUT
{
   float4 Color : COLOR0;
};

sampler2D sBase : register (s0);
sampler2D sSpec : register (s1);
sampler2D sBump : register (s2);

PS_OUTPUT MAINFUNCTION(PS_INPUT Input)
{
  PS_OUTPUT Out;

  // sample the textures
  float4 tex = tex2D (sBase, Input.Base.xy) * DIFFUSESCALE;              // sample base tex

  float4 spec = tex2D (sSpec, Input.Base.xy);             // sample spec tex (gloss)
  float3 bump = (tex2D (sBump, Input.Base.xy) - 0.5) * 2.0;  // sample and unbias normal

  // put the normal into eye space using transpose texture basis
  float3 viewBump;
  viewBump.x = dot (Input.Basis1, bump);
  viewBump.y = dot (Input.Basis2, bump);
  viewBump.z = dot (Input.Basis3, bump);

  //float4 graySpec = mul (spec, (0.30, 0.59, 0.11, 0.0));
  float3 normLight = normalize (Input.Light);
  float3 normEyeVec = normalize (Input.EyeVec);
  
  // Use the specular alpha for reflection amount and add fresnel to that
  float fresnel = FRESNELBASE + FRESNELSCALE * pow(1.0 - abs(dot(normEyeVec, viewBump)), FRESNELPOWER);
  fresnel = min(fresnel, FRESNELMAX);

  // compute specular color 
  float3 halfway = normalize (normLight - normEyeVec); 
  
  half4 specular;
  specular.rgb = float3(1.0, 0.95, 0.9) * SPECULARSCALE * saturate (pow (dot( halfway, viewBump ), SPECULARPOWER)) * spec.rgb;
  specular.a = 0;

  // compute diffuse color
  half NdotL = dot (normLight, viewBump);
  half4 diffuse = saturate(NdotL) * diffuseColor + ambientColor * AMBIENTSCALE;
#ifdef OMNISCALE
  diffuse = diffuse + Input.Omni * OMNISCALE;
#endif
  diffuse = saturate (diffuse);

  specular.rgb *= pow(saturate(NdotL + 0.25), 0.5);

  // Compute cube map vector [E - 2(E dot N)N], then sample the cube map
  float3 cubeCoord;
  cubeCoord = reflect(normEyeVec, viewBump);
  cubeCoord.y = max(cubeCoord.y * 0.65 + 0.35, 0.0);
  float4 cube;
  cube.rgb = pow(cubeCoord.yyy, float3(0.9, 1.0, 1.4)) * float3(0.5, 0.6, 0.7) + float3(0.2, 0.2, 0.2);
  cube.rgb = cube.rgb * SKYREFLECTSCALE;
  cube.a = 1.0;

  //less reflection during night
  float day = (diffuseColor.r + diffuseColor.g + diffuseColor.b) / 3.0;
  fresnel *= day;

  Out.Color = (tex * diffuse) + ((cube + specular) * fresnel);
  Out.Color.a = 1.0;

  return (Out);
}
