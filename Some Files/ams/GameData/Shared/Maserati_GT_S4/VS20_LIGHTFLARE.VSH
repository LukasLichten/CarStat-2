// Vertex shader for light flares 
// ------------------------------
// This shader makes the quad face the camera and change its size according to the camera vs quad normal.
// This shader should only be applied to quads (square consisting of 2 triangles) that face forward. 
//
// Created by Siim Annuk
// Date: June 28, 2009

#define USEFOG

float4x4 viewProjMatrix : register (c0);
float4x4 worldMatrix : register (c4);
float4x4 viewMatrix : register (c16);
float4 worldEye: register (c8);
float4 fogData : register (c9);
float4 texMod : register (c11); // tex0.u=x, tex0.v=y, tex1.u=z, tex1.v=w
float4 specularColor : register (c15); // power in c15.a

struct VS_OUTPUT
{
   float4 Pos  : POSITION;
   float4 Diff : COLOR0;
#ifdef USEFOG
   float  Fog  : FOG;
#endif   
   float2 Tex  : TEXCOORD0;
   float  Depth: TEXCOORD1;
};

#if HIGHFLASH == 1
VS_OUTPUT vs20_blinnLightFlareHeadlights (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex : TEXCOORD0)  
#else
VS_OUTPUT vs20_blinnLightFlare (float4 inPos : POSITION, float4 inNorm : NORMAL, float4 inColor : COLOR0, float2 inTex : TEXCOORD0)  
#endif
{
  VS_OUTPUT Out;

  // Assume the quad is a square, side length is 0.5 units
  float quadSize = 0.5f;

  // Build the world view matrix
  float4x4 worldViewMatrix = mul(viewMatrix, worldMatrix);

  // normalize inNorm
  float3 unitInNorm = normalize(inNorm.xyz);

  // Construct a forward vector
  float3 forwardVec = float3(0.0f, 0.0f, 1.0f);

  // Calculate angles between the in normal and up, right and forward vectors
  float dotFwNorm = dot(forwardVec, unitInNorm);

  // Corner identificators of the vertex
  float tx = inTex.x - 0.5f;
  float ty = inTex.y + 0.5f;

  // Calculate the particle position (center of quad)
  float halfQuad = quadSize / 2.0f;
  float3 n1 = normalize(float3(inNorm.z, 0, -inNorm.x));
  float3 n2 = cross(-unitInNorm, n1);
  float3 particlePosition = inPos.xyz + (sign(tx) * n1 * halfQuad) + (-sign(ty) * n2 * halfQuad);

  // Normal of the vertex
  float3 worldNormal = normalize(mul(worldMatrix, inNorm.xyz));

  // Eve vector
  float3 eyeVector = normalize(mul(worldMatrix, float4(particlePosition, 1.0f)) + worldEye);
  float dotProduct = max(0, -(dot(eyeVector, worldNormal) / (length(eyeVector) * length(worldNormal))));
  
  // The size of the particle
#if HIGHFLASH == 1
  float particleSize = max(pow(dotProduct * FLASHSIZE, FLASHMULTIPLIER), pow(dotProduct * GLOWSIZE, GLOWMULTIPLIER));
#else
  float particleSize = pow(dotProduct * GLOWSIZE, GLOWMULTIPLIER);
#endif

  // Use specular power to set the size of the particle
  particleSize *= specularColor.a;

  // Billboard the quad
  float3 pos = float3(0.0, 0.0, 0.0);
  pos += (particleSize * tx) * worldViewMatrix[0] + (particleSize * -ty) * worldViewMatrix[1];
  pos += particlePosition;

  // The absolute position of the vertex
  Out.Pos = mul(viewProjMatrix, float4(pos, 1.0f));

  // Fix the depth
  pos.z += sign(dotFwNorm) * pow(abs(dotProduct), 3);
  float4 depthPos = mul(viewProjMatrix, float4(pos, 1.0f));
  Out.Depth = depthPos.z / depthPos.w;

#ifdef USEFOG
  // Compute fog
  float3 eyeVec = mul (worldMatrix, inPos) + worldEye;
  Out.Fog.x = (fogData[1] - (length(eyeVec.xyz) * fogData[0]));
#endif
  
  // Propagate color and texture coordinates
  Out.Diff = inColor;
  Out.Tex.xy = inTex;

  return (Out);
}








