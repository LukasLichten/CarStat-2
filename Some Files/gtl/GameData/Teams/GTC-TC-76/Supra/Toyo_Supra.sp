//made by Petrol for TCL 1987 Toyota Supra Mk3 Turbo - (physics for rF, V1.0)
//ask permission before you edit or copy
//////////////////////////////////////////////////////////////////////////
//
// Conventions:
//
// +x = left
// +z = rear
// +y = up
// +pitch = nose up
// +yaw = nose right
// +roll = right
//
// [BODY]  - a rigid mass with mass and inertial properties
// [JOINT] - a ball joint constraining an offset of one body to an
//           offset of another body (eliminates 3 DOF)
// [HINGE] - a constraint restricting the relative rotations of two
//           bodies to be around a single axis (eliminates 2 DOF).
// [BAR]   - a constraint holding an offset of one body from an offset of
//           another body at a fixed distance (eliminates 1 DOF).
// [JOINT&HINGE] - both the joint and hinge constraints, forming the
//           conventional definition of a hinge (eliminates 5 DOF).
//
//////////////////////////////////////////////////////////////////////////

// Body including all rigidly attached parts (wings, barge boards, etc.)
// NOTE: the mass and inertia for the main vehicle "body" is not used
// because it is derived from the HDV file by subtracting out all the
// wheels, etc.  For all other bodies (wheels, spindles), they are important!
[BODY]
name=body mass=(1.0) inertia=(1.0,1.0,1.0)
pos=(0.0,0.0,0.0) ori=(0.0,0.0,0.0)

// Front spindles
[BODY]
name=fl_spindle mass=(12.0) inertia=(0.0330,0.0312,0.0295)
pos=(0.745,0.0,-1.30) ori=(0.0,0.0,0.0)

[BODY]
name=fr_spindle mass=(12.0) inertia=(0.0330,0.0312,0.0295)
pos=(-0.745,0.0,-1.30) ori=(0.0,0.0,0.0)

// Front wheels
[BODY]
name=fl_wheel mass=(25.0) inertia=(1.196,0.786,0.786)
pos=(0.75,0.0,-1.30) ori=(0.0,0.0,0.0)

[BODY]
name=fr_wheel mass=(25.0) inertia=(1.196,0.786,0.786)
pos=(-0.75,0.0,-1.30) ori=(0.0,0.0,0.0)

// Rear spindles
[BODY]
name=rl_spindle mass=(12.5) inertia=(0.0330,0.0312,0.0295)
pos=(0.76,0.0,1.29) ori=(0.0,0.0,0.0)

[BODY]
name=rr_spindle mass=(12.5) inertia=(0.0330,0.0312,0.0295)
pos=(-0.76,0.0,1.29) ori=(0.0,0.0,0.0)

// Rear wheels (includes half of rear-axle)
[BODY]
name=rl_wheel mass=(26.0) inertia=(1.205,0.795,0.795)
pos=(0.76,0.0,1.29) ori=(0.0,0.0,0.0)

[BODY]
name=rr_wheel mass=(26.0) inertia=(1.205,0.795,0.795)
pos=(-0.76,0.0,1.29) ori=(0.0,0.0,0.0)

// Fuel in tank is not rigidly attached - it is attached with springs and
// dampers to simulate movement.  Properties are defined in the HDV file.
[BODY]
name=fuel_tank mass=(1.0) inertia=(1.0,1.0,1.0)
pos=(0.0,0.0,0.0) ori=(0.0,0.0,0.0)

// Driver's head is not rigidly attached, and it does NOT affect the vehicle
// physics.  Position is from the eyepoint defined in the VEH file, while
// other properties are defined in the head physics file.
[BODY]
name=driver_head mass=(5.0) inertia=(0.02,0.02,0.02)
pos=(0.0,0.0,0.0) ori=(0.0,0.0,0.0)

//////////////////////////////////////////////////////////////////////////
//
// Constraints
//
//////////////////////////////////////////////////////////////////////////

// Front wheel and spindle connections
[JOINT&HINGE]
posbody=fl_wheel negbody=fl_spindle pos=fl_wheel axis=(-0.748,0.0,0.0)

[JOINT&HINGE]
posbody=fr_wheel negbody=fr_spindle pos=fr_wheel axis=(0.748,0.0,0.0)


// Front left suspension (2 A-arms + 1 steering arm = 5 links)
[BAR] // forward upper arm
name=fl_fore_upper posbody=body negbody=fl_spindle pos=(0.450,0.170,-1.334) neg=(0.650,0.210,-1.302)

[BAR] // rearward upper arm
posbody=body negbody=fl_spindle pos=(0.450,0.170,-1.245) neg=(0.650,0.210,-1.298)

[BAR] // forward lower arm
posbody=body negbody=fl_spindle pos=(0.380,-0.115,-1.320) neg=(0.744,-0.120,-1.301)

[BAR] // rearward lower arm
name=fl_fore_lower posbody=body negbody=fl_spindle pos=(0.380,-0.115,-1.180) neg=(0.744,-0.120,-1.290)

[BAR] // steering arm (must be named for identification)
name=fl_steering posbody=body negbody=fl_spindle pos=(0.370,-0.076,-1.430) neg=(0.725,-0.070,-1.440)


// Front right suspension (2 A-arms + 1 steering arm = 5 links)
[BAR] // forward upper arm (used in steering lock calculation)
name=fr_fore_upper posbody=body negbody=fr_spindle pos=(-0.450,0.170,-1.334) neg=(-0.650,0.210,-1.302)

[BAR] // rearward upper arm
posbody=body negbody=fr_spindle pos=(-0.450,0.170,-1.245) neg=(-0.650,0.210,-1.298)

[BAR] // forward lower arm
name=fr_fore_lower posbody=body negbody=fr_spindle pos=(-0.380,-0.115,-1.320) neg=(-0.744,-0.120,-1.301)

[BAR] // rearward lower arm
posbody=body negbody=fr_spindle pos=(-0.380,-0.115,-1.180) neg=(-0.744,-0.120,-1.290)

[BAR] // steering arm (must be named for identification)
name=fr_steering posbody=body negbody=fr_spindle pos=(-0.370,-0.076,-1.430) neg=(-0.725,-0.070,-1.440)


// Rear left suspension (2 A-arms + 1 straight link = 5 links)
[BAR] // forward upper arm
posbody=body negbody=rl_spindle pos=(0.420,0.160,1.255) neg=(0.68,0.180,1.285)

[BAR] // rearward upper arm
posbody=body negbody=rl_spindle pos=(0.420,0.160,1.325) neg=(0.68,0.180,1.295)

[BAR] // forward lower arm
posbody=body negbody=rl_spindle pos=(0.310,-0.095,0.85) neg=(0.70,-0.100,1.28)

[BAR] // rearward lower arm
posbody=body negbody=rl_spindle pos=(0.300,-0.092,1.36) neg=(0.70,-0.096,1.30)

[BAR] // straight link
posbody=body negbody=rl_spindle pos=(0.330,-0.011,1.47) neg=(0.70,-0.008,1.47)


// Rear right suspension (2 A-arms + 1 straight link = 5 links)
[BAR] // forward upper arm
posbody=body negbody=rr_spindle pos=(-0.420,0.160,1.255) neg=(-0.68,0.180,1.285)

[BAR] // rearward upper arm
posbody=body negbody=rr_spindle pos=(-0.420,0.160,1.325) neg=(-0.68,0.180,1.295)

[BAR] // forward lower arm
posbody=body negbody=rr_spindle pos=(-0.310,-0.095,0.85) neg=(-0.70,-0.100,1.28)

[BAR] // rearward lower arm
posbody=body negbody=rr_spindle pos=(-0.300,-0.092,1.36) neg=(-0.70,-0.096,1.30)

[BAR] // straight link
posbody=body negbody=rr_spindle pos=(-0.330,-0.011,1.47) neg=(-0.70,-0.008,1.47)

// Rear spindle and wheel connections
[JOINT&HINGE]
posbody=rl_wheel negbody=rl_spindle pos=rl_wheel axis=(-0.76,0.0,0.0)

[JOINT&HINGE]
posbody=rr_wheel negbody=rr_spindle pos=rr_wheel axis=(0.76,0.0,0.0)


