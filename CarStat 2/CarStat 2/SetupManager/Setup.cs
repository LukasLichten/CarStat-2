﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarStat_2.Setup_Manager
{
    public class Setup
    {
        public string FileLocation { get; set; }

        public string VehicleClass { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public string Track { get; set; }
        public Dictionary<string, Dictionary<string, string[]>> SetupValues { get; set; }
    }
}
