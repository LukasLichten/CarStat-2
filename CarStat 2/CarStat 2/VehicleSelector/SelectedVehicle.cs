﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarStat_2.VehicleSelector
{
    public class SelectedVehicle
    {
        string fullpath;
        string shortname;
        string MASlocation;
        public SelectedVehicle(string fullpath, string shortname, string MASlocation)
        {
            this.fullpath = fullpath;
            this.shortname = shortname;
            this.MASlocation = MASlocation;
        }
        public string getFullpath()
        {
            return fullpath;
        }

        public string getMASlocation()
        {
            return MASlocation;
        }

        public override string ToString() 
        {
            return shortname;
        }

    }
    
}
