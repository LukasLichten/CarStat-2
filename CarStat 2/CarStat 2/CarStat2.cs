﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;

using Microsoft.Win32;
using ZedGraph;
//using SelectorCarStat2;
using CarStat_2.VehicleSelector;
using System.Diagnostics;
using Misc;
using CarStat_2.DataObjects;
using CarStat_2.Windows;
using CarStat_2.Setup_Manager;

namespace CarStat_2
{
    public partial class CarStat2 : Form
    {
        VehSelector selecta = null;


        bool init = false;
        List<CarStat2> instances = new List<CarStat2>();
        FormSizeData FormSize = new FormSizeData();
        internal CarStat2 master { get; private set; }
        Thread fileExtractionThread = null;

        //Report rep = new Report();
        internal GameEnvironmentFunc gameEnv { get; set; }

        //This needs to be replaced in the future
        CarData data { get { return gameEnv.Data; } }

        public VersionNumber versionNumber;

        //Inital Constructor
        public CarStat2(VersionNumber versionNumber)
        {
            master = null;
            this.versionNumber = versionNumber;

            //File.Delete("log.txt");
            InitializeComponent();
            Init();

            switch (GameEnvironmentFunc.LoadAutoStartSetting())
            {
                case "RF1":
                    gameEnv = new CarStat_2.GameEnv.RF1.RF1Enviroment(this);
                    break;

                case "RF2":
                    gameEnv = new CarStat_2.GameEnv.RF2.RF2Enviroment(this);
                    break;

                case "GTR":
                    gameEnv = new CarStat_2.GameEnv.GTR.GTREnviroment(this);
                    break;

                case "AMS":
                    gameEnv = new CarStat_2.GameEnv.AMS.AMSEnviroment(this);
                    break;

                default:
                    gameEnv = null;
                    break;
            }

            this.Text = "CarStat " + versionNumber.Major + "." + versionNumber.Minor;

            if (versionNumber.Beta != 0)
                this.Text = "CarStat " + versionNumber.Major + "." + versionNumber.Minor + "-Beta";

            //Check if quickbms.exe is present
            if (!File.Exists("quickbms.exe"))
            {
                DialogResult result = MessageBox.Show("CarStat needs the quickbms.exe file. This should be included in the zipfile. quickbms.exe must be placed into the same folder as CarStat.\n\nDo you want to download quickbms? CarStat will not work without it...", "quickbms not found", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);

                if (result == DialogResult.OK)
                {
                    System.Diagnostics.Process.Start("http://aluigi.altervista.org/papers/quickbms.zip");
                }

                Environment.Exit(0);
            }

            fileExtractionThread = new Thread(new ThreadStart(extractFiles));
            fileExtractionThread.Name = "Resource-Extraction";
            fileExtractionThread.Start();
            DataReader.readFileCleanup(); //run to cleanup after a crash



            dropChangeTires.DropDownStyle = ComboBoxStyle.DropDownList;
            makevisible(false, false);
            grpPitResult.Visible = false;
            btnMakeMainWindow.Visible = false;

            //if (Environment.GetCommandLineArgs().Length == 2)
            //{
            //    if (File.Exists(Environment.GetCommandLineArgs()[1]))
            //    {
            //        data.VEHlocation = Environment.GetCommandLineArgs()[1];
            //        loadFiles(true);
            //        this.Show();
            //    }
            //}
        }



        private static void extractFiles()
        {
            if (!File.Exists("rfactor2.bms"))
                File.WriteAllBytes("rfactor2.bms", CarStat_2.Properties.Resources.rfactor2);
            MiscFunctions.preparePictureConversion();
        }

        private static void deleteExtractedFiles()
        {
            if (File.Exists("rfactor2.bms"))
                File.Delete("rfactor2.bms");
            MiscFunctions.deletePicutreConversionKit();
        }

        private void Init()
        {
            this.Resize += Form_Resize;
            this.FormBorderStyle = FormBorderStyle.Sizable;
            this.FormClosed += new FormClosedEventHandler(CarStat_FormClosed);


            //Used for resizing the form:
            FormSize.UpgradeStartPos = grpUpgrades.Location;
            FormSize.ExpandSize = 180;
            FormSize.WindowWidth = this.Width; //Save the default window size
            FormSize.WindowHeight = this.Height; //Save the default window size
            FormSize.WindowHeightDefault = this.Height; //Save the default window size
            FormSize.WindowWidthDefault = this.Width;
        }

        private static bool IsUpgradeType(TreeNode node)
        {
            if (node.Tag.ToString().Contains("UpgradeType"))
                return true;
            else
                return false;
        }

        //Close the whole application after closing the form.
        void CarStat_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (caricon.Image != null)
                caricon.Image.Dispose();

            if (gameEnv != null)
                gameEnv.Data.Reset();


            if (this.master == null)
            {
                while (instances.Count > 0)
                {
                    instances[0].Close();
                }


                //Deleting unneccessary files
                DataReader.readFileCleanup();
                deleteExtractedFiles();
                Environment.Exit(0);
            }
            else
            {
                this.master.removeMeFromInstanceList(this);
                this.Dispose();
            }

        }

        //Called by a child instance in multiview when the child is closed
        private void removeMeFromInstanceList(CarStat2 instance)
        {
            this.instances.Remove(instance);
        }


        //When resizing we should resize the appropriate controls with it: the graphs and the TabControl
        //Also, the grpUpgrades should be moved accordingly
        private void Form_Resize(Object sender, EventArgs e)
        {
            //We first determine the amount of window resizement(?):
            int resizeWidth = this.Width - FormSize.WindowWidth;
            int resizeHeight = 0;

            //If we checked the Upgrades-checkbox we have to subtract the Expandsize of our first resize-action
            if ((checkShowUpgrades.Checked == true) && ((this.Height - FormSize.WindowHeight) >= FormSize.ExpandSize))
                resizeHeight = this.Height - FormSize.ExpandSize - FormSize.WindowHeight;
            else
                resizeHeight = this.Height - FormSize.WindowHeight;

            //Always modify the control's size:
            tabs.Width += resizeWidth;
            tabTires.Width += resizeWidth;

            zedEngine.Width += resizeWidth;
            zedWear.Width += resizeWidth;
            zedTire.Width += resizeWidth;
            zedTireTemps.Width += resizeWidth;
            zedTire2.Width += resizeWidth;
            zedThermal1.Width += resizeWidth;
            zedThermal2.Width += resizeWidth;
            FormSize.WindowWidth = this.Width;
            tabs.Height += resizeHeight;
            tabTires.Height += resizeHeight;
            zedEngine.Height += resizeHeight;
            zedTire.Height += resizeHeight;
            zedTire2.Height += resizeHeight;
            zedTireTemps.Height += resizeHeight;
            zedThermal1.Height += resizeHeight;
            zedThermal2.Height += resizeHeight;
            zedWear.Height += resizeHeight;
            FormSize.WindowHeight = this.Height;

            //Move the grpUpgrades
            grpUpgrades.Location = new Point(FormSize.UpgradeStartPos.X, FormSize.UpgradeStartPos.Y + resizeHeight);
            FormSize.UpgradeStartPos = grpUpgrades.Location;
        }

        //Create the a child carstat 2
        public CarStat2(CarData data, string Game, Point windowlocation, CarStat2 master)
        {
            this.master = master;

            this.Location = windowlocation;
            this.versionNumber = master.versionNumber;

            InitializeComponent();

            Init();

            dropChangeTires.DropDownStyle = ComboBoxStyle.DropDownList;

            switch (Game)
            {
                case "RF1":
                    gameEnv = new CarStat_2.GameEnv.RF1.RF1Enviroment(this);
                    break;

                case "RF2":
                    gameEnv = new CarStat_2.GameEnv.RF2.RF2Enviroment(this);
                    break;

                case "GTR":
                    gameEnv = new CarStat_2.GameEnv.GTR.GTREnviroment(this);
                    break;

                case "AMS":
                    gameEnv = new CarStat_2.GameEnv.AMS.AMSEnviroment(this);
                    break;

                default:
                    gameEnv = null;
                    break;
            }

            this.gameEnv.Data = data;

            makevisible(true, false);

            chkEngineMixture.Checked = false;
            chkEngineMixture.Enabled = false;
            cbFuelMixture.Enabled = false;

            //Loads the upgrades
            treeUpgrades.Nodes.Clear();
            if (this.data.UpgradeFile != null)
            {
                readUpgrades();

                foreach (TreeNode node in treeUpgrades.Nodes)
                    readCheckedUpgrades(node);
            }

            if (treeUpgrades.Nodes.Count == 0)
            {
                checkShowUpgrades.Checked = false;
                checkShowUpgrades.Visible = false;
            }

            fillView(true);
            this.Show();
            this.Location = windowlocation;

            this.CheckCompounds1.Enabled = false;
            this.CheckCompounds2.Enabled = false;
            this.CheckCompounds3.Enabled = false;
            this.CheckCompounds4.Enabled = false;
            this.CheckCompounds5.Enabled = false;
            this.CheckCompounds6.Enabled = false;
            this.CheckEngineFriction.Enabled = false;


            groupBox5.Visible = false;
            btnSimulatePitstop.Visible = false;
            chkPitStopManualOverwrite.Visible = false;
            grpPitResult.Visible = false;

            //this.menuStrip2.Enabled = false;

            this.fileToolStripMenuItem.Enabled = false;
            this.openOptionsMenu.Enabled = false;
            this.openSetupsManager.Enabled = false;
            this.helpToolStripMenuItem.Enabled = false;


            foreach (Control cont in this.Controls)
            {
                if (!cont.Equals(this.checkShowUpgrades) && !cont.Equals(this.btnMakeMainWindow) && !cont.Equals(this.menuStrip2))
                {
                    cont.Click += new EventHandler(CarStat_Click);
                    if (cont.GetType() == typeof(CheckBox))
                        cont.Enabled = false;
                }
            }
            tabEngineData.Click += new EventHandler(this.CarStat_Click);
            tabTires.Click += new EventHandler(this.CarStat_Click);
            tabControlPitstop.Click += new EventHandler(this.CarStat_Click);
            tabControlGeneral.Click += new EventHandler(this.CarStat_Click);
            tabSetupControl.Click += new EventHandler(this.CarStat_Click);

            checkShowUpgrades.Enabled = true;
            btnRescale_Click(null, null);
        }

        //Only used to created a CarData instance for multiview
        public CarStat2(string vehlocation, string MASlocation, CarStat2 master, string game)
        {
            this.Hide();
            this.master = master;
            this.versionNumber = master.versionNumber;

            switch (game)
            {
                case "RF1":
                    gameEnv = new CarStat_2.GameEnv.RF1.RF1Enviroment(this);
                    break;

                case "RF2":
                    gameEnv = new CarStat_2.GameEnv.RF2.RF2Enviroment(this);
                    break;

                case "GTR":
                    gameEnv = new CarStat_2.GameEnv.GTR.GTREnviroment(this);
                    break;

                case "AMS":
                    gameEnv = new CarStat_2.GameEnv.AMS.AMSEnviroment(this);
                    break;

                default:
                    gameEnv = null;
                    break;
            }

            data.VEHlocation = vehlocation;
            data.MASlocation = MASlocation;
        }

        private void childInitCarDataCreation()
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            InitializeComponent();

            Init();

            dropChangeTires.DropDownStyle = ComboBoxStyle.DropDownList;

            makevisible(false, false);

            loadFiles(true);

            if (caricon.Image != null)
                caricon.Image.Dispose();
        }

        #region Load files and do work

        //Loads all the files that are found in the VEHicle and HDV file
        private void loadFiles(bool newfile)
        {
            //Checking if the extraction is done
            if (fileExtractionThread != null)
            {
                fileExtractionThread.Join();
            }

            init = true;
            try
            {
                Application.DoEvents();
                DataReader reader = gameEnv.Reader;

                reader.loadFiles(newfile);

                //Read Upgrade file:
                treeUpgrades.Nodes.Clear();

                if (this.data.UpgradeFile != null)
                {
                    readUpgrades();

                    foreach (TreeNode node in treeUpgrades.Nodes)
                        readCheckedUpgrades(node);
                }

                Thread thread = new Thread(reader.readTireFile);
                thread.Name = "TireDataReader";
                thread.Start();

                reader.readEngineFile();
                reader.readGearRatios();
                
                thread.Join();

                if (data.Tyrelocation == null)
                    throw new FileNotFoundException(); //This means we hit encrypted file only during the tire extraction (Howston G4 1967 Endu Pack)

                applyEngineUpgrades();

                if (master == null)
                    DataReader.readFileCleanup();

                makevisible(true, false);
                if (treeUpgrades.Nodes.Count == 0)
                {
                    checkShowUpgrades.Checked = false;
                    checkShowUpgrades.Visible = false;
                }
                fillView(newfile);
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show("Sorry, this car has encrypted data, can't read it.\n\nUncheck the 'Only open non-encrypted files'-option in the Options-menu...", "Encrypted file", MessageBoxButtons.OK, MessageBoxIcon.Information);
                makevisible(false, false);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while loading files:\n" + ex.Message + "\n" + ex.StackTrace, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
                makevisible(false, false);
            }
            init = false;
        }


        //Fills the view; draws graphs and sets labels
        private void fillView(bool newfile)
        {
            //Prevents crash from switching units without a car loaded
            if (data.Tyrelocation == null)
            {
                return;
            }

            if (newfile)
            {
                this.Text = "CarStat " + versionNumber.Major + "." + versionNumber.Minor + "-" + gameEnv.GAME + ": " + Path.GetFileName(data.VEHlocation);

                if (versionNumber.Beta != 0)
                    this.Text = "CarStat " + versionNumber.Major + "." + versionNumber.Minor + "-Beta" + "-" + gameEnv.GAME + ": " + Path.GetFileName(data.VEHlocation);

                compounds();
                this.grpPitResult.Visible = false;
                this.chkPitStopManualOverwrite.Checked = false;
            }

            try
            {
                gameEnv.Graphs.EngineGraph(init);
                gameEnv.Graphs.TireWearGraphs();
                gameEnv.Graphs.TireTempGraph();
                gameEnv.Graphs.TireThermalDegGraph1();
                gameEnv.Graphs.TireThermalDegGraph2();
                gameEnv.Graphs.WearheatGraph();
                gameEnv.Graphs.BrakeTempsGraph(true);
                gameEnv.Graphs.BrakeTempsGraph(false);

                gameEnv.LabelsSetter.setTeamLabels();
                gameEnv.LabelsSetter.setEngineComboBoxes();
                gameEnv.LabelsSetter.setEngineLabels();
                gameEnv.LabelsSetter.setCarLabels();
                gameEnv.LabelsSetter.setBrakesLabels();
                gameEnv.LabelsSetter.setPitstopLabels();
                gameEnv.LabelsSetter.setTireLabels();

                pitstopSimOptions(chkPitStopManualOverwrite.Checked);

                tabSetupControl.Visible = true;
                gameEnv.SetupCreator.build();

                caricon.Image = data.GetIcon();

                lblTurbo.Visible = radbAspTurbo.Checked;
            }
            catch (Exception ex)
            {
                if (data.corrected)
                {
                    //Data is unusable and error should be shown
                    MessageBox.Show("Error while rendering data.\n" + ex.Message + "\n\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    DialogResult result = MessageBox.Show(gameEnv.GAME + " will accept mods to have certain propertys missing, and will just load default values." + "\n" +
                        "This is not a good practice by modders." + "\n" +
                        "CarStat can load a set of filler data that will fill in the missing propertys, but the values could be wrong (as default values of "+gameEnv.GAME+"  are unknown)" + "\n\n" +
                        "Do you want to continue and load the filler data?\nAlso this may not solve the error, at which point this is a missinterpretation of data by CarStat!", "Data missing from the mod. Load Filler Data?", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);


                    if (result == DialogResult.OK)
                    {
                        //Fill default values
                        gameEnv.FillWithDefaultValues();

                        data.corrected = true;

                        //Restart fill:
                        fillView(false);
                        return;
                    }
                    else
                    {
                        if (caricon.Image != null)
                        {
                            caricon.Image.Dispose();
                            caricon.Image = null;
                        }
                        data.Reset();
                        this.Text = "CarStat " + versionNumber.Major + "." + versionNumber.Minor;

                        if (versionNumber.Beta != 0)
                            this.Text = "CarStat " + versionNumber.Major + "." + versionNumber.Minor + "-Beta";

                        makevisible(false, false);
                        return;
                    }
                }

            }



        }

        //Used to redraw from outside forces
        internal void redrawView()
        {
            if (gameEnv == null)
                return;

            //Prevents crash from switching units without a car loaded
            if (data.Tyrelocation == null)
            {
                return;
            }

            fillView(false);

            foreach (CarStat2 instance in this.instances)
            {
                instance.redrawView();
            }
        }

        #endregion

        #region MiscFunctions functions

        //Read the upgradefile and fill the treeUpgrades with these upgrades
        private void readUpgrades()
        {
            for (int i = 0; i < data.UpgradeFile.Count; i++)
            {
                string line = data.UpgradeFile[i];
                if (line.Contains("UpgradeType"))
                {
                    Misc.HiddenCheckBoxTreeNode typeNode = new Misc.HiddenCheckBoxTreeNode(line.Substring(line.LastIndexOf("=") + 1).Replace("\"", "")); //Give the new node the UpgradeType-name

                    typeNode.Tag = line.Trim();
                    i++;
                    while ((i < data.UpgradeFile.Count) && (data.UpgradeFile[i].Contains("UpgradeType") == false))
                    {

                        line = data.UpgradeFile[i];
                        if (line.Contains("UpgradeLevel=") || line.Contains("Upgradelevel=") || line.Contains("upgradelevel="))//UpgradeLevel found:
                        {

                            TreeNode levelNode = new TreeNode(line.Substring(line.LastIndexOf("=") + 1).Replace("\"", "")); //Give the new node the UpgradeLevel-name
                            levelNode.Tag = line.Trim();

                            typeNode.Nodes.Add(levelNode);
                        }
                        i++;
                    }

                    i--;
                    if (typeNode.Nodes.Count != 0)
                    {
                        typeNode.Nodes[0].Checked = true;
                        treeUpgrades.Nodes.Add(typeNode);
                    }
                }
            }
            treeUpgrades.ExpandAll();
            if (treeUpgrades.Nodes.Count > 0)
                treeUpgrades.Nodes[0].EnsureVisible();
        }

        //Makes everything visible or invisible
        internal void makevisible(bool visible, bool onlyTires)
        {
            makevisible(visible, onlyTires, false);
        }

        internal void makevisible(bool visible, bool onlyTires, bool onlyEngine)
        {
            checkShowUpgrades_CheckedChanged(null, null);

            //Both can not be true at the same time
            if (onlyTires && onlyEngine)
            {
                onlyTires = false;
                onlyEngine = false;
            }
            else if (onlyTires || onlyEngine)
            {
                makevisible(false, false, false); //switching off everything else
            }

            //Tires
            if (visible && (onlyTires || !onlyEngine) || !visible)
            {
                tabTires.Visible = visible;

                grpCompound1.Visible = visible;
                grpCompound2.Visible = visible;
                grpCompound3.Visible = visible;
                grpCompound4.Visible = visible;
                grpThermal1.Visible = visible;
                grpThermal2.Visible = visible;

                zedTire.Visible = visible;
                zedTire2.Visible = visible;
                zedTireTemps.Visible = visible;
                zedThermal1.Visible = visible;
                zedThermal2.Visible = visible;
                zedWear.Visible = visible;


                pictureBox1.Visible = visible;
                pictureBox2.Visible = visible;
                pictureBox3.Visible = visible;
                pictureBox4.Visible = visible;
            }

            //Engine
            if (visible && (onlyEngine || !onlyTires) || !visible)
            {
                chkEngineMixture.Visible = visible;
                cbFuelMixture.Visible = visible;
                chkBoostMapping.Visible = visible;
                cbBoostMapping.Visible = visible;
                radbAspNaturally.Visible = visible;
                radbAspTurbo.Visible = visible;
                tabEngineData.Visible = visible;
                zedEngine.Visible = visible;
                btnRescale.Visible = visible;

                lblTurbo.Visible = false;

                if (onlyEngine)
                {
                    CheckEngineFriction.Checked = false;
                    CheckEngineFriction.Enabled = false;
                }
                else
                {
                    CheckEngineFriction.Enabled = true;
                    CheckEngineFriction.Checked = true;
                }
            }

            //General
            if (visible && (!onlyEngine && !onlyTires) || !visible)
            {
                tabSetupControl.Visible = visible;

                checkShowUpgrades.Visible = visible;
                caricon.Visible = visible;
                caricon.Image = null;
                grpTeam.Visible = visible;
                tabControlGeneral.Visible = visible;
                grpFrontBrakes.Visible = visible;
                grpRearBrakes.Visible = visible;

                tabControlPitstop.Visible = visible;
                grpRefueling.Visible = visible;
                grpTireChange.Visible = visible;
                grpDamage.Visible = visible;
                grpPitcrew.Visible = visible;
                grpAdjustments.Visible = visible;
                zedBrakeFront.Visible = visible;
                zedBrakeRear.Visible = visible;
                lblConcurrent.Visible = visible;
                lblConcurrent2.Visible = visible;
                label66.Visible = visible;
                lblBrakeBias.Visible = visible;
            }
        
        }

        //Disables certain functions in the pitstop simulation menu if not supported
        private void pitstopSimOptions(bool overwrite)
        {
            Dictionary<string, string[]> pitmenu = null;
            data.HDVehicleData.Values.TryGetValue("pitmenu", out pitmenu);

            this.txtAmountFuel.ReadOnly = !(overwrite || Int32.Parse(MiscFunctions.getProperty(pitmenu, "Fuel", 0).Trim()) == 1);

            ComboBox.ObjectCollection dropChangeTiresItems = this.dropChangeTires.Items;
            dropChangeTiresItems.Clear();
            if (MiscFunctions.getProperty(pitmenu, "AllTires", 0) == "1")
                dropChangeTiresItems.AddRange(new object[] { "0", "4" });
            else if (MiscFunctions.getProperty(pitmenu, "FrontRearTires", 0) == "1"
                || MiscFunctions.getProperty(pitmenu, "LeftRightTires", 0) == "1")
                dropChangeTiresItems.AddRange(new object[] { "0", "2", "4" });
            else if (MiscFunctions.getProperty(pitmenu, "IndividualTires", 0) == "1")
                dropChangeTiresItems.AddRange(new object[] { "0", "1", "2", "3", "4" });
            else if (overwrite)
                dropChangeTiresItems.AddRange(new object[] { "0", "2", "4" });
            else
                dropChangeTiresItems.AddRange(new object[] { "0" });

            this.chkAeroDamage.Enabled = overwrite || Int32.Parse(MiscFunctions.getProperty(pitmenu, "Damage", 0).Trim()) >= 1;
            this.chkSuspDamage.Enabled = overwrite || Int32.Parse(MiscFunctions.getProperty(pitmenu, "Damage", 0).Trim()) >= 2;

            this.chkFrontWing.Enabled = overwrite || MiscFunctions.getProperty(pitmenu, "FrontWing", 0).Trim() == "1";
            this.chkRearWing.Enabled = overwrite || MiscFunctions.getProperty(pitmenu, "RearWing", 0) == "1";
            this.chkRadiatorTap.Enabled = overwrite || MiscFunctions.getProperty(pitmenu, "Radiator", 0) == "1";
            this.chkWedge.Enabled = overwrite || MiscFunctions.getProperty(pitmenu, "Wedge", 0) == "1";
            this.chkTrackbar.Enabled = overwrite || MiscFunctions.getProperty(pitmenu, "TrackBar", 0) == "1";
            this.chkDriverswap.Enabled = overwrite || MiscFunctions.getProperty(pitmenu, "Driver", 0) == "1";

            this.chkTirePressure.Enabled = overwrite || MiscFunctions.getProperty(pitmenu, "PressureOnTheFly", 0) == "1";
            this.chkSpringRubber.Enabled = overwrite || MiscFunctions.getProperty(pitmenu, "SpringRubber", 3) == "1";
            //TODO: Fender Flare
        }

        //Finds and adds the compounds to the listbox's. Also loads the TGM-files while we're at it.
        private void compounds()
        {
            CheckCompounds1.Items.Clear();
            CheckCompounds2.Items.Clear();
            CheckCompounds3.Items.Clear();
            CheckCompounds4.Items.Clear();
            CheckCompounds5.Items.Clear();
            CheckCompounds6.Items.Clear();

            Dictionary<string, DataObjects.TireCompoundData>.Enumerator enumerator = data.TireCompounds.GetEnumerator();
            while (enumerator.MoveNext())
            {
                string compound = enumerator.Current.Key;
                CheckCompounds1.Items.Add(compound);
                CheckCompounds1.SetItemChecked(CheckCompounds1.Items.Count - 1, true);
                CheckCompounds2.Items.Add(compound);
                CheckCompounds3.Items.Add(compound);
                CheckCompounds4.Items.Add(compound);
                CheckCompounds5.Items.Add(compound);
                CheckCompounds6.Items.Add(compound);
            }

            CheckCompounds1.SelectedIndexChanged -= CheckCompounds_SelectedIndexChanged;
            CheckCompounds2.SelectedIndexChanged -= CheckCompounds_SelectedIndexChanged;
            CheckCompounds3.SelectedIndexChanged -= CheckCompounds_SelectedIndexChanged;
            CheckCompounds4.SelectedIndexChanged -= CheckCompounds_SelectedIndexChanged;
            CheckCompounds5.SelectedIndexChanged -= CheckCompounds_SelectedIndexChanged;
            CheckCompounds6.SelectedIndexChanged -= CheckCompounds_SelectedIndexChanged;

            CheckCompounds1.SelectedIndex = 0;
            CheckCompounds2.SelectedIndex = 0;
            CheckCompounds2.SetItemChecked(0, true);
            CheckCompounds3.SelectedIndex = 0;
            CheckCompounds3.SetItemChecked(0, true);
            CheckCompounds4.SelectedIndex = 0;
            CheckCompounds4.SetItemChecked(0, true);
            CheckCompounds5.SelectedIndex = 0;
            CheckCompounds5.SetItemChecked(0, true);
            CheckCompounds6.SelectedIndex = 0;

            CheckCompounds1.SelectedIndexChanged += CheckCompounds_SelectedIndexChanged;
            CheckCompounds2.SelectedIndexChanged += CheckCompounds_SelectedIndexChanged;
            CheckCompounds3.SelectedIndexChanged += CheckCompounds_SelectedIndexChanged;
            CheckCompounds4.SelectedIndexChanged += CheckCompounds_SelectedIndexChanged;
            CheckCompounds5.SelectedIndexChanged += CheckCompounds_SelectedIndexChanged;
            CheckCompounds6.SelectedIndexChanged += CheckCompounds_SelectedIndexChanged;
        }



        #endregion

        #region Controls handling

        //Shows a dialog to select a TBC tire file
        private void openTBCFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //First close other CarStat instances if the exist
            while (0 < instances.Count)
            {
                instances[0].Close();
            }
            instances.Clear();

            if (caricon.Image != null)
                caricon.Image.Dispose();

            if (gameEnv != null)
                data.Reset();

            string TyreFile = null;

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "TBC files (*.tbc, *.tyr)|*.tbc; *.tyr";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                TyreFile = dialog.FileName;
            }
            else
            {
                MessageBox.Show("No file selected.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (TyreFile == null)
                return;

            //Game specific dealing
            if (Path.GetExtension(TyreFile).ToLower() == ".tyr")
            {
                if (gameEnv == null || gameEnv.GAME != "GTR")
                {
                    gameEnv = new CarStat_2.GameEnv.GTR.GTREnviroment(this);
                }

                new PostGameSelector(this, TyreFile, null).GameSelector_Tyre_FormClosed(null, null);
            }
            else
            {
                if (gameEnv != null && gameEnv.GAME != "GTR")
                {
                    DialogResult result = MessageBox.Show("Is this a Tyre File (TBC) from " + gameEnv.GAME + "?" +
                        "\n" + "If yes, then press yes and the tire will load as a " + gameEnv.GAME + " tire." +
                        "\n" + "If not, then select no and then select the correct game." +
                        "\n" + "\n" + "Loading a tire with the wrong game selected will result in issues", "Is this a Tire from " + gameEnv.GAME + "?", MessageBoxButtons.YesNo);

                    if (result == DialogResult.Yes)
                    {
                        new PostGameSelector(this, TyreFile, null).GameSelector_Tyre_FormClosed(null, null);
                    }
                    else
                    {
                        //Non selected yet

                        Selector selector = new Selector(this);
                        MiscFunctions.makeParentChildWindow(this, selector);

                        PostGameSelector values = new PostGameSelector(this, TyreFile, selector, true);
                    }
                }
                else
                {
                    //We must make the user select a Game
                    Selector selector = new Selector(this);
                    MiscFunctions.makeParentChildWindow(this, selector);

                    PostGameSelector values = new PostGameSelector(this, TyreFile, selector, true);
                }
            }
        }

        //Open a vehicle file
        private void openVEHicleFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //First close other CarStat instances if the exist
            while (0 < instances.Count)
            {
                instances[0].Close();
            }
            instances.Clear();

            if (caricon.Image != null)
                caricon.Image.Dispose();

            if (gameEnv != null)
                data.Reset();

            string VEHlocation = null;

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Vehicle files (*.veh, *.car)|*.veh; *.car"; //"Vehicle files (*.veh)|*.veh|Car files (*.car)|*.car"
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                VEHlocation = dialog.FileName;
            }
            else
            {
                MessageBox.Show("No file selected.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (VEHlocation == null)
                return;

            if (Path.GetExtension(VEHlocation).ToLower() == ".car")
            {
                if (gameEnv == null || gameEnv.GAME != "GTR")
                {
                    gameEnv = new CarStat_2.GameEnv.GTR.GTREnviroment(this);
                }

                new PostGameSelector(this, VEHlocation, null).GameSelector_Vehicle_FormClosed(null, null);
            }
            else
            {
                if (gameEnv != null && gameEnv.GAME != "GTR")
                {
                    DialogResult result = MessageBox.Show("Is this a vehicle from " + gameEnv.GAME + "?" +
                        "\n" + "If yes, then press yes and the vehicle will load as a " + gameEnv.GAME + " Vehicle." +
                        "\n" + "If not, then select no and then select the correct game." +
                        "\n" + "\n" + "Loading a vehicle with the wrong game selected will result in issues", "Is this a Vehicle from " + gameEnv.GAME + "?", MessageBoxButtons.YesNo);
                    

                    if (result == DialogResult.Yes)
                    {
                        new PostGameSelector(this, VEHlocation, null).GameSelector_Vehicle_FormClosed(null, null);
                    }
                    else
                    {
                        //No selected

                        Selector selector = new Selector(this);
                        MiscFunctions.makeParentChildWindow(this, selector);

                        PostGameSelector values = new PostGameSelector(this, VEHlocation, selector);
                    }
                }
                else
                {
                    //We must make the user select a Game
                    Selector selector = new Selector(this);
                    MiscFunctions.makeParentChildWindow(this, selector);

                    PostGameSelector values = new PostGameSelector(this, VEHlocation, selector);
                }
            }
        }

        private void openEngineFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //First close other CarStat instances if the exist
            while (0 < instances.Count)
            {
                instances[0].Close();
            }
            instances.Clear();

            if (caricon.Image != null)
                caricon.Image.Dispose();

            if (gameEnv != null)
                data.Reset();

            string EngineLocation = null;

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Engine files (*.ini, *.eng)|*.ini; *.eng"; //"Engine files (*.ini)"
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                EngineLocation = dialog.FileName;
            }
            else
            {
                MessageBox.Show("No file selected.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (EngineLocation == null)
                return;

            //Check of the engine file
            try
            {
                StreamReader reader = new StreamReader(EngineLocation);
                string line;

                bool isEngine = false;

                while ((line = reader.ReadLine()) != null)
                {
                    line = line.ToLower();

                    if (line.Contains("rpmbase") || line.Contains("rpmtorque"))
                    {
                        isEngine = true;
                        break;
                    }
                }

                reader.Close();


                if (!isEngine)
                {
                    MessageBox.Show("The File you selected appears not to be an Engine File. Please select a Engine File", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while reading the file.\n" + ex.Message + "\n\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            //Opening it with the correct game
            if (Path.GetExtension(EngineLocation).ToLower() == ".eng")
            {
                if (gameEnv == null || gameEnv.GAME != "GTR")
                {
                    gameEnv = new CarStat_2.GameEnv.GTR.GTREnviroment(this);
                }

                new PostGameSelector(this, EngineLocation, null).GameSelector_Engine_FormClosed(null, null);
            }
            else
            {
                if (gameEnv != null && gameEnv.GAME != "GTR")
                {
                    DialogResult result = MessageBox.Show("Is this a Engine File from " + gameEnv.GAME + "?" +
                        "\n" + "If yes, then press yes and the Engine will load as a " + gameEnv.GAME + " Engine." +
                        "\n" + "If not, then select no and then select the correct game." +
                        "\n" + "\n" + "Loading a vehicle with the wrong game selected will result in issues", "Is this a Engine from " + gameEnv.GAME + "?", MessageBoxButtons.YesNo);

                    if (result == DialogResult.Yes)
                    {
                        new PostGameSelector(this, EngineLocation, null).GameSelector_Engine_FormClosed(null, null);
                    }
                    else
                    {
                        //No selected

                        Selector selector = new Selector(this);
                        MiscFunctions.makeParentChildWindow(this, selector);

                        PostGameSelector values = new PostGameSelector(this, EngineLocation, selector, false);
                    }
                }
                else
                {
                    //We must make the user select a Game
                    Selector selector = new Selector(this);
                    MiscFunctions.makeParentChildWindow(this, selector);

                    PostGameSelector values = new PostGameSelector(this, EngineLocation, selector, false);
                }
            }
        }

        private class PostGameSelector
        {
            private CarStat2 master;
            private string FileLoc;

            private Form form;

            internal PostGameSelector(CarStat2 master, string fileloc, Form form)
            {
                this.master = master;
                this.FileLoc = fileloc;
                this.form = form;

                if (form != null)
                    form.FormClosed += this.GameSelector_Vehicle_FormClosed;
            }

            internal PostGameSelector(CarStat2 master, string fileloc, Form form, bool tire)
            {
                this.master = master;
                this.FileLoc = fileloc;
                this.form = form;

                if (form != null)
                {
                    if (tire)
                        form.FormClosed += this.GameSelector_Tyre_FormClosed;
                    else
                        form.FormClosed += this.GameSelector_Engine_FormClosed;
                }


            }

            public void GameSelector_Vehicle_FormClosed(object sender, FormClosedEventArgs e)
            {
                if (form != null)
                    form.Visible = false;

                Loading load = new Loading();

                if (FileLoc != null && master.gameEnv != null)
                {
                    master.gameEnv.Data.VEHlocation = FileLoc;
                    master.gameEnv.Data.MASlocation = "";
                    master.loadFiles(true);

                    master.tabs.SelectedIndex = 0;
                }

                load.Close();
            }

            public void GameSelector_Tyre_FormClosed(object sender, FormClosedEventArgs e)
            {
                if (form != null)
                    form.Visible = false;

                if (master.gameEnv == null)
                    return;

                Loading load = new Loading();

                master.gameEnv.Data.Tyrelocation = FileLoc;
                master.gameEnv.Data.VehicleRoot = Path.GetDirectoryName(FileLoc);

                master.gameEnv.Reader.readTireFile(true);
                master.compounds();
                master.gameEnv.Graphs.TireWearGraphs();
                master.gameEnv.Graphs.TireThermalDegGraph1();
                master.gameEnv.Graphs.TireThermalDegGraph2();
                master.gameEnv.Graphs.TireTempGraph();
                master.gameEnv.Graphs.WearheatGraph();
                master.gameEnv.LabelsSetter.setTireLabels();
                master.tabs.SelectedIndex = 3;
                master.makevisible(true, true);

                load.Close();
            }

            public void GameSelector_Engine_FormClosed(object sender, FormClosedEventArgs e)
            {
                if (form != null)
                    form.Visible = false;

                if (master.gameEnv == null)
                    return;

                Loading load = new Loading();

                master.gameEnv.Data.EngineFileLocation = FileLoc;
                master.gameEnv.Data.VehicleRoot = Path.GetDirectoryName(FileLoc);

                master.gameEnv.Reader.readEngineFile(FileLoc);

                master.gameEnv.Graphs.EngineGraph(true);
                master.gameEnv.LabelsSetter.setEngineLabels();
                master.gameEnv.LabelsSetter.setEngineComboBoxes();

                master.tabs.SelectedIndex = 1;
                master.makevisible(true, false, true);
                master.btnRescale_Click(null, null);

                load.Close();
            }
        }

        //Exit program
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Open Vehicle Selector
        private void openVehicleSelectorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //First close other CarStat instances if the exist
            while (0 < instances.Count)
            {
                instances[0].Close();
            }
            instances.Clear();

            selecta = new VehSelector(this);

            MiscFunctions.makeParentChildWindow(this, selecta);

            selecta.FormClosing += new FormClosingEventHandler(selecta_FormClosing);
        }

        //On Vehicle Selector screen close
        private void selecta_FormClosing(object sender, FormClosingEventArgs e)
        {
            List<Thread> threads = new List<Thread>();
            List<CarStat2> cardataCreators = new List<CarStat2>();

            bool inMultiCarMode = false;
            if (selecta.getCheckedVehicles().Count != 0)
            {
                Cursor = Cursors.WaitCursor;
                if (caricon.Image != null)
                    caricon.Image.Dispose();
                data.Reset();
                data.VEHlocation = selecta.getCheckedVehicles()[0].Locatie;
                data.MASlocation = selecta.getCheckedVehicles()[0].MasFile;
                this.Height = FormSize.WindowHeightDefault;
                this.Width = FormSize.WindowWidthDefault;
                checkShowUpgrades.Checked = false;

                //Now starting a new CarStat instance for every vehicle in the vehicle list:
                List<Vehicle> listSelected = selecta.getCheckedVehicles();

                for (int i = 1; i < listSelected.Count; i++)
                {

                    CarStat2 child = new CarStat2(listSelected[i].Locatie, listSelected[i].MasFile, this, gameEnv.GAME);
                    cardataCreators.Add(child);
                    Thread th = new Thread(child.childInitCarDataCreation);
                    th.Name = "Init-" + i;
                    th.Start();
                    threads.Add(th);
                }
                if (selecta.getCheckedVehicles().Count > 1)
                {
                    this.master = this;
                    this.Location = new Point(0, 0);
                    this.Show();

                    //this.TopMost = true;
                    inMultiCarMode = true;
                }
                try
                {
                    loadFiles(true);
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }

                Cursor = Cursors.Default;
            }
            selecta.Dispose();
            if (inMultiCarMode)
            {
                this.master = null;
                foreach (Thread th in threads)
                    th.Join();

                int i = 1;
                foreach (CarStat2 carStat2 in cardataCreators)
                {
                    Point size = MiscFunctions.getLocation(i, cardataCreators.Count);
                    instances.Add(new CarStat2(carStat2.getCarData, gameEnv.GAME, size, this));
                    i++;
                }

                MessageBox.Show("You are now in multi-car mode. You can control all the CarStat-screens with the top left one.\nIf you want to close all instances at once, close the top left one.", "In multi-car mode", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DataReader.readFileCleanup();
            }
        }



        private void btnDefaultSetup_Click(object sender, EventArgs e)
        {
            gameEnv.SetupCreator.setToDefault();
        }

        private void btnLoadSetup_Click(object sender, EventArgs e)
        {
            new SetupManager(this);
        }

        private void btnSaveSetup_Click(object sender, EventArgs e)
        {
            string placeholder = Path.GetTempPath() + Guid.NewGuid().ToString() + ".svm";
            while (File.Exists(placeholder))
                placeholder = Path.GetTempPath() + Guid.NewGuid().ToString() + ".svm";

            try
            {
                gameEnv.SetupCreator.saveSetup(placeholder);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while saving setup.\n" + ex.Message + "\n\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            new MoveSetupDialog(this, gameEnv.SetupCreator.getSetup(), Path.Combine(gameEnv.GetGameLocation(), @"UserData\player\Settings"));
        }

        private void chkRealValues_CheckedChanged(object sender, EventArgs e)
        {
            gameEnv.SetupCreator.setRealValues(chkRealValues.Checked);
        }

        //Open About screen
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new About(this, versionNumber);
        }

        //Open Help screen
        private void helpToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new Windows.Help(this);
        }

        //Opens the Options Menu
        private void openOptionsMenu_Click(object sender, EventArgs e)
        {
            new OptionsMenu(this);
        }

        private void openSetupsManager_Click(object sender, EventArgs e)
        {
            new SetupManager(this);
        }

        private void btnMakeMainWindow_Click(object sender, EventArgs e)
        {
            if (master == null)
                return;

            CarStat2 oldMaster = this.master;
            this.master = null;

            this.instances = oldMaster.instances;
            this.instances.Add(oldMaster);
            this.instances.Remove(this);

            oldMaster.instances = new List<CarStat2>();

            //Making this instance a master instance

            //this.Location = windowlocation;

            this.CheckCompounds1.Enabled = true;
            this.CheckCompounds2.Enabled = true;
            this.CheckCompounds3.Enabled = true;
            this.CheckCompounds4.Enabled = true;
            this.CheckCompounds5.Enabled = true;
            this.CheckCompounds6.Enabled = true;
            this.CheckEngineFriction.Enabled = true;


            groupBox5.Visible = true;
            btnSimulatePitstop.Visible = true;
            chkPitStopManualOverwrite.Visible = true;
            grpPitResult.Visible = false;

            //this.menuStrip2.Enabled = false;

            this.fileToolStripMenuItem.Enabled = true;
            this.openOptionsMenu.Enabled = true;
            this.openSetupsManager.Enabled = true;
            this.helpToolStripMenuItem.Enabled = true;

            this.btnMakeMainWindow.Visible = false;

            foreach (Control cont in this.Controls)
            {
                cont.Click -= new EventHandler(CarStat_Click);
                if (cont.GetType() == typeof(CheckBox))
                    cont.Enabled = true;
            }
            tabEngineData.Click -= new EventHandler(this.CarStat_Click);
            tabTires.Click -= new EventHandler(this.CarStat_Click);
            tabControlPitstop.Click -= new EventHandler(this.CarStat_Click);
            tabControlGeneral.Click -= new EventHandler(this.CarStat_Click);
            tabSetupControl.Click -= new EventHandler(this.CarStat_Click);

            checkShowUpgrades.Enabled = true;


            //making the old master a slave

            //oldMaster.Location = windowlocation;

            oldMaster.CheckCompounds1.Enabled = false;
            oldMaster.CheckCompounds2.Enabled = false;
            oldMaster.CheckCompounds3.Enabled = false;
            oldMaster.CheckCompounds4.Enabled = false;
            oldMaster.CheckCompounds5.Enabled = false;
            oldMaster.CheckCompounds6.Enabled = false;
            oldMaster.CheckEngineFriction.Enabled = false;


            oldMaster.groupBox5.Visible = false;
            oldMaster.btnSimulatePitstop.Visible = false;
            oldMaster.chkPitStopManualOverwrite.Visible = false;
            oldMaster.grpPitResult.Visible = false;

            //oldMaster.menuStrip2.Enabled = false;

            oldMaster.fileToolStripMenuItem.Enabled = false;
            oldMaster.openOptionsMenu.Enabled = false;
            oldMaster.openSetupsManager.Enabled = false;
            oldMaster.helpToolStripMenuItem.Enabled = false;

            oldMaster.btnMakeMainWindow.Visible = true;


            foreach (Control cont in oldMaster.Controls)
            {
                if (!cont.Equals(oldMaster.checkShowUpgrades) && !cont.Equals(oldMaster.btnMakeMainWindow) && !cont.Equals(this.menuStrip2))
                {
                    cont.Click += new EventHandler(oldMaster.CarStat_Click);
                    if (cont.GetType() == typeof(CheckBox))
                        cont.Enabled = false;
                }
            }

            oldMaster.tabEngineData.Click += new EventHandler(oldMaster.CarStat_Click);
            oldMaster.tabTires.Click += new EventHandler(oldMaster.CarStat_Click);
            oldMaster.tabControlPitstop.Click += new EventHandler(oldMaster.CarStat_Click);
            oldMaster.tabControlGeneral.Click += new EventHandler(oldMaster.CarStat_Click);
            oldMaster.tabSetupControl.Click += new EventHandler(oldMaster.CarStat_Click);

            oldMaster.checkShowUpgrades.Enabled = true;


            //Updating the Rest of the instances:
            foreach (CarStat2 inst in instances)
            {
                inst.master = this;
                inst.fillView(false);
            }

            fillView(false);
        }

        private void CheckCompounds_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!init)
            {
                gameEnv.Graphs.TireWearGraphs();
                gameEnv.Graphs.TireThermalDegGraph1();
                gameEnv.Graphs.TireThermalDegGraph2();
                gameEnv.Graphs.TireTempGraph();
                gameEnv.Graphs.WearheatGraph();
                gameEnv.LabelsSetter.setTireLabels();



                //Updating each instance
                foreach (CarStat2 instance in instances)
                {
                    try
                    {

                        //1 Tire wear/heating/grip compounds checkbox changed
                        for (int i = 0; i < this.CheckCompounds1.Items.Count; i++)
                        {
                            instance.CheckCompounds1.SetItemChecked(i, this.CheckCompounds1.GetItemChecked(i));
                        }
                        instance.CheckCompounds1.SetSelected(this.CheckCompounds1.SelectedIndex, true);
                        instance.CheckCompounds1.Refresh();

                        //2 Tire temps compounds checkbox changed
                        for (int i = 0; i < this.CheckCompounds2.Items.Count; i++)
                        {
                            instance.CheckCompounds2.SetItemChecked(i, this.CheckCompounds2.GetItemChecked(i));
                        }
                        instance.CheckCompounds2.SetSelected(this.CheckCompounds2.SelectedIndex, true);
                        instance.CheckCompounds2.Refresh();

                        //3 Tirewear1 compounds checkbox changed
                        for (int i = 0; i < this.CheckCompounds3.Items.Count; i++)
                        {
                            instance.CheckCompounds3.SetItemChecked(i, this.CheckCompounds3.GetItemChecked(i));
                        }
                        instance.CheckCompounds3.SetSelected(this.CheckCompounds3.SelectedIndex, true);
                        instance.CheckCompounds3.Refresh();

                        //4 Tirewear2 compounds checkbox changed
                        for (int i = 0; i < this.CheckCompounds4.Items.Count; i++)
                        {
                            instance.CheckCompounds4.SetItemChecked(i, this.CheckCompounds4.GetItemChecked(i));
                        }
                        instance.CheckCompounds4.SetSelected(this.CheckCompounds4.SelectedIndex, true);
                        instance.CheckCompounds4.Refresh();

                        //5 Thermal degradation compounds checkbox changed
                        for (int i = 0; i < this.CheckCompounds5.Items.Count; i++)
                        {
                            instance.CheckCompounds5.SetItemChecked(i, this.CheckCompounds5.GetItemChecked(i));
                        }
                        instance.CheckCompounds5.SetSelected(this.CheckCompounds5.SelectedIndex, true);
                        instance.CheckCompounds5.Refresh();

                        //6 Thermal degradation 2 compounds checkbox changed
                        instance.CheckCompounds6.SetSelected(this.CheckCompounds6.SelectedIndex, true);
                        instance.CheckCompounds6.Refresh();
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        MessageBox.Show("Error while trying to read multiple vehicles.\n\nOnly open the vehicle files from the same mod!");
                    }

                }

            }
        }

        //Exclude or include extra engine friction
        private void checkEngineFriction_CheckedChanged(object sender, EventArgs e)
        {
            if (!init)
            {
                if (gameEnv != null && gameEnv.Data.EngineFileLocation == null)
                    return;


                gameEnv.Graphs.EngineGraph(false);
                gameEnv.LabelsSetter.setEngineLabels();
                try
                {
                    foreach (CarStat2 instance in instances)
                    {
                        instance.CheckEngineFriction.Checked = this.CheckEngineFriction.Checked;
                    }
                }
                catch (ArgumentOutOfRangeException)
                {
                    MessageBox.Show("Error while trying to read multiple vehicles.\n\nOnly open the vehicle files from the same mod!");
                }
            }

        }

        //Rescale button clicked
        internal void btnRescale_Click(object sender, EventArgs e)
        {
            for (int i = zedEngine.GraphPane.CurveList.Count - 1; i > 1; i--)
            {
                zedEngine.GraphPane.CurveList.RemoveAt(i);
            }
            zedEngine.AxisChange();
            zedEngine.GraphPane.CurveList.AddRange(gameEnv.Graphs.getEngineBars());
            zedEngine.Refresh();
            zedEngine.ZoomOutAll(zedEngine.GraphPane);
            foreach (CarStat2 instance in instances)
            {
                instance.btnRescale_Click(null, null);
            }
        }

        //Other tab selected-event. Select the right tab in the other instances.
        private void tabs_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            this.TopMost = true;
            foreach (CarStat2 instance in instances)
            {
                instance.tabs.SelectTab(this.tabs.SelectedIndex);
            }
            this.TopMost = false;
        }

        //Other tab selected-event. Select the right tab in the other instances.
        private void tabTires_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            this.TopMost = true;
            foreach (CarStat2 instance in instances)
            {
                instance.tabTires.SelectTab(this.tabTires.SelectedIndex);
            }
            this.TopMost = false;
        }

        private void tabControlGeneral_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            this.TopMost = true;
            foreach (CarStat2 instance in instances)
            {
                instance.tabControlGeneral.SelectTab(this.tabControlGeneral.SelectedIndex);
            }
            this.TopMost = false;
        }

        private void tabControlPitstop_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            this.TopMost = true;
            foreach (CarStat2 instance in instances)
            {
                instance.tabControlPitstop.SelectTab(this.tabControlPitstop.SelectedIndex);
            }
            this.TopMost = false;
        }

        private void tabEngineData_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.TopMost = true;
            foreach (CarStat2 instance in instances)
            {
                instance.tabEngineData.SelectTab(this.tabEngineData.SelectedIndex);
            }
            this.TopMost = false;
        }

        private void tabSetupControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.TopMost = true;
            foreach (CarStat2 instance in instances)
            {
                instance.tabSetupControl.SelectTab(this.tabSetupControl.SelectedIndex);
            }
            this.TopMost = false;
        }

        void CarStat_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Please navigate through the different CarStat instances by using the CarStat in the top left corner.\nIf you do anything in that window, all other windows will do the same.\n\nIf you want to close CarStat and all its windows, close the 'master' CarStat window.");
        }


        //After selecting an upgrade, get the description of this update out of the UpgradeFile
        void treeUpgrades_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeUpgrades.SelectedNode.Tag.ToString().Contains("UpgradeType"))
            {
                txtUpgradeDescription.Text = "No description";
                return;
            }
            int i = 0;
            while (!data.UpgradeFile[i].Contains((string)treeUpgrades.SelectedNode.Tag)) //While the line doesn't equal the Upgradename, we skip to it
                i++;
            //Found it
            while (!data.UpgradeFile[i].Contains("Description")) //Skip to description
            {
                if (data.UpgradeFile[i].Contains("}")) //This means we're at another upgrade and so didn't get the description
                {
                    txtUpgradeDescription.Text = "No description";
                    return;
                }
                else
                    i++;
            }
            //We're at the Description now:
            string line = data.UpgradeFile[i];
            txtUpgradeDescription.Text = line.Substring(line.LastIndexOf("=") + 1).Replace("\"", "");
        }

        //After (un)checking an upgrade
        private void treeUpgrades_AfterCheck(object sender, System.Windows.Forms.TreeViewEventArgs e)
        {
            //After (un)checking an upgrade, we first have to check/uncheck the other upgrades of that type.    
            this.treeUpgrades.AfterCheck -= new System.Windows.Forms.TreeViewEventHandler(this.treeUpgrades_AfterCheck); //Disable event or we go in an infinite loop
            TreeNode parentNode = e.Node.Parent;
            if (e.Node.Checked == false) //If we unckeck an upgrade, we have to automatically check the first upgrade
                parentNode.Nodes[0].Checked = true;
            else
            {
                for (int i = 0; i < parentNode.Nodes.Count; i++) //If we check an upgrade, the other upgrades under that upgradetype should be unchecked
                {
                    if (parentNode.Nodes[i] != e.Node)
                        parentNode.Nodes[i].Checked = !e.Node.Checked;
                }
            }
            this.treeUpgrades.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeUpgrades_AfterCheck); //Re-enable event 


            //We read the files again, but with the upgrades implemented
            Cursor = Cursors.WaitCursor;
            data.HDVehicleData.Values = DataReader.ListToMultiLevelDict(data.HDVehicleData.HDVfile); //Restores the stock hdv
            data.corrected = false; //because rf2 default values have not been writen in

            data.TheEngineUpgrade.dict = new Dictionary<string, string[]>();

            //data.HDVehicleData.Values = DataReader.addDefaultValues(data.HDVehicleData.Values, defaultHDV); //Adds missing values from the default
            foreach (TreeNode node in treeUpgrades.Nodes)
            {
                readCheckedUpgrades(node);
            }
            //We have to reread the tire and engine file:
            gameEnv.Reader.rereadForUpgrading();


            //Applying the engine upgrades
            applyEngineUpgrades();

            fillView(true); //Fill the views again
            Cursor = Cursors.Default;
        }

        private void applyEngineUpgrades()
        {
            foreach (KeyValuePair<string, string[]> pair in data.TheEngineUpgrade.dict )
            {
                string[] values = pair.Value;

                for (int i = 0; i < values.Length; i++)
                {
                    MiscFunctions.changeParameter(data.EngineData.Values, pair.Key, values[i], i);
                }
            }
        }

        //Read all the nodes in the treeUpgrades. If checked, we implement the upgrade in the HDV-file. Is recursive.
        private void readCheckedUpgrades(TreeNode node)
        {
            if (node.Checked && node.Tag.ToString().Contains("UpgradeLevel=")) //The node should checked AND be of the type "UpgradeLevel"
            {
                int i = 0;
                while (data.UpgradeFile[i].Contains(node.Tag.ToString()) == false) //We skip to the checked Node
                    i++;

                Dictionary<string, int> repeat = new Dictionary<string, int>();

                string currentSection = "";
                //At the to-add upgrade:
                while (data.UpgradeFile[i].Contains("}") == false) //Go over every upgrade-parameter untill we're doen ('}')
                {
                    string line = data.UpgradeFile[i];
                    if ((line.Contains("HDV=[") || line.Contains("HDV={")) && line.Contains("]"))  //Get the current section of the upgrade. The "HDV={"-check is for fixing the upgrade error in the Howston G4
                    {
                        data.UpgradeFile[i] = data.UpgradeFile[i].Replace("HDV={", "HDV=[");
                        currentSection = data.UpgradeFile[i].Substring(data.UpgradeFile[i].IndexOf("=") + 1);
                        repeat.Clear();
                    }
                    else if (line.Contains("GeneralTorqueMult"))
                    {
                        data.TheEngineUpgrade.GeneralTorqueMult = double.Parse(line.Substring(line.LastIndexOf("=") + 1));
                    }
                    else if (line.Contains("GeneralPowerMult"))
                    {
                        data.TheEngineUpgrade.GeneralPowerMult = double.Parse(line.Substring(line.LastIndexOf("=") + 1));
                    }
                    else if (line.Contains("HDV=") && line.Split('=').Length >= 3)
                    {
                        string key = line.Split('=')[1].Trim();
                        string value = line.Substring((line.Split('=')[0] + '=' + line.Split('=')[1]).Length + 1);
                        value = value.Replace("(", "").Replace(")", "")/*.Replace("/", "")*/.Replace("\"", "").Replace(";", ",").Trim();

                        currentSection = currentSection.Replace("[", "").Replace("]", "");

                        int segment = 0;
                        if (repeat.ContainsKey(key.ToLower()))
                        {
                            repeat[key.ToLower()]++;
                            segment = repeat[key.ToLower()];
                        }
                        else
                        {
                            repeat.Add(key.ToLower(), 0);
                        }

                        if (currentSection.ToLower() == "engine")
                        {
                            if (data.TheEngineUpgrade.dict.ContainsKey(key.ToLower()))
                            {
                                string[] oldArray = data.TheEngineUpgrade.dict[key.ToLower()];
                                string[] arr = new string[oldArray.Length + 1];

                                for (int j = 0; j < oldArray.Length; j++)
                                {
                                    arr[j] = oldArray[j];
                                }

                                arr[arr.Length - 1] = value;
                                data.TheEngineUpgrade.dict[key.ToLower()] = arr;
                            }
                            else
                            {
                                data.TheEngineUpgrade.dict.Add(key.ToLower(), new string[] { value });
                            }
                        }

                        MiscFunctions.changeParameter(data.HDVehicleData.Values, currentSection, key, value, segment);  //Change the parameter directly in the HDV-file
                    }
                    i++;
                }
            }

            //Loop recursively over the next treeNodes/upgrades:
            foreach (TreeNode subnode in node.Nodes)
            {
                readCheckedUpgrades(subnode);
            }
        }


        //When checking the "ShowUpgrades"-checkbox, we have to expand the form to show the upgrades.
        private void checkShowUpgrades_CheckedChanged(object sender, EventArgs e)
        {
            this.Resize -= Form_Resize;
            grpUpgrades.Visible = checkShowUpgrades.Checked;

            if (checkShowUpgrades.Checked)
            {
                this.Height += FormSize.ExpandSize;
                this.MinimumSize = new System.Drawing.Size(this.FormSize.WindowWidthDefault, FormSize.WindowHeightDefault + FormSize.ExpandSize);
            }
            else
            {
                this.MinimumSize = new System.Drawing.Size(this.FormSize.WindowWidthDefault, FormSize.WindowHeightDefault);
                this.Height = FormSize.WindowHeightDefault;

            }
            this.Resize += Form_Resize;
        }

        //Enables access to all checkboxes in the pitstop simualtion menu
        private void chkPitStopManualOverwrite_CheckedChanged(object sender, EventArgs e)
        {
            this.pitstopSimOptions(this.chkPitStopManualOverwrite.Checked);
            if (this.master != null)
            {
                this.master.chkPitStopManualOverwrite.Checked = this.chkPitStopManualOverwrite.Checked;
            }
            foreach (CarStat2 instance in this.instances)
            {
                instance.chkPitStopManualOverwrite.Checked = this.chkPitStopManualOverwrite.Checked;
            }
        }

        //To disable tire pressure adjustment
        private void dropChangeTires_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.dropChangeTires.Text == "4")
            {
                this.chkTirePressure.Checked = false;
                this.chkTirePressure.Enabled = false;
            }
            else if (this.chkTirePressure.Enabled == false)
            {
                if (this.chkPitStopManualOverwrite.Checked || Int32.Parse(MiscFunctions.getProperty(data.HDVehicleData.Values, "Pitmenu", "PressureOnTheFly", 0)) == 1)
                {
                    this.chkTirePressure.Enabled = true;
                }
            }
        }

        //Engine Mixture selector
        private void chkEngineMixture_CheckedChanged(object sender, EventArgs e)
        {
            bool enabled = chkEngineMixture.Checked;
            cbFuelMixture.Enabled = enabled;
            gameEnv.Graphs.EngineGraph(false);
            gameEnv.LabelsSetter.setEngineLabels();
        }

        //Fuel mixture changed
        private void cbFuelMixture_SelectedIndexChanged(object sender, EventArgs e)
        {
            gameEnv.Graphs.EngineGraph(false);
            gameEnv.LabelsSetter.setEngineLabels();
        }

        //Boost Mapping selector
        private void chkBoostMapping_CheckedChanged(object sender, EventArgs e)
        {
            bool enabled = chkBoostMapping.Checked;
            cbBoostMapping.Enabled = enabled;
            gameEnv.Graphs.EngineGraph(false);
            gameEnv.LabelsSetter.setEngineLabels();
        }

        //Boost Mapping changed
        private void cbBoostMapping_SelectedIndexChanged(object sender, EventArgs e)
        {
            gameEnv.Graphs.EngineGraph(false);
            gameEnv.LabelsSetter.setEngineLabels();
        }

        //Aspiration
        private void radbAspNaturally_CheckedChanged(object sender, EventArgs e)
        {
            //Stops the code from excuting when it is not this elemet that is checked
            if (!radbAspNaturally.Checked)
                return;

            this.lblTurbo.Visible = false;
            gameEnv.Graphs.EngineGraph(false);
            gameEnv.LabelsSetter.setEngineLabels();
        }

        private void radbAspTurbo_CheckedChanged(object sender, EventArgs e)
        {
            //Stops the code from excuting when it is not this elemet that is checked
            if (!radbAspTurbo.Checked)
                return;

            this.lblTurbo.Visible = true;
            gameEnv.Graphs.EngineGraph(false);
            gameEnv.LabelsSetter.setEngineLabels();
        }

        //Calculates/simulates a pitstop 
        private void btnSimulatePitstop_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, string[]> pitmenu = data.HDVehicleData.Values["pitmenu"];

                //Calculate the predicted refuelingtime:
                double fuelMin = 0;
                double fuelMax = 0;
                double fuelToTank = 0;

                double tank = Double.Parse(this.lblFuel.Text.Substring(0, this.lblFuel.Text.IndexOf("l")).Trim());

                if (Double.TryParse(txtAmountFuel.Text.Replace(',', '.'), out fuelToTank) == false || fuelToTank < 0 || (fuelToTank > tank && !chkPitStopManualOverwrite.Checked)) //false = parsing failed so not a valid value
                {
                    MessageBox.Show("Not a valid amount of fuel to tank.\n\nPlease enter a valid number (positiv and smaller then " + tank + ")\nor 0 if you don't want to simulate refueling.\nIn multicarview the smallest fuel tank is the limit. To disable the limit check the Manual Overwrite Box", "Not a valid value", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    grpPitResult.Visible = false;
                    return;
                }
                else if (fuelToTank != 0)
                {
                    double speed = Double.Parse(MiscFunctions.getProperty(pitmenu, "FuelTime", 0));
                    fuelMin = fuelToTank / speed;
                    fuelMin += Double.Parse(MiscFunctions.getProperty(pitmenu, "FuelTime", 2)) + Double.Parse(MiscFunctions.getProperty(pitmenu, "FuelTime", 4));
                    fuelMax = fuelMin + Double.Parse(MiscFunctions.getProperty(pitmenu, "FuelTime", 1));
                    lblPitFuel.Text = Math.Round(fuelMin, 1) + " - " + Math.Round(fuelMax, 0) + " seconds";
                }
                else
                {
                    lblPitFuel.Text = 0 + " seconds";
                }

                //Calculate the predicted tirechange-time:
                double tireMin = 0;
                double tireMax = 0;
                if ((dropChangeTires.Text == "0") || (dropChangeTires.Text == ""))
                    lblPitTires.Text = "0 seconds";
                else if (dropChangeTires.Text == "2" || dropChangeTires.Text == "1")
                {
                    tireMin = Double.Parse(MiscFunctions.getProperty(pitmenu, "TireTime", 0));
                    tireMax = tireMin + Double.Parse(MiscFunctions.getProperty(pitmenu, "TireTime", 2)) * 2;
                    lblPitTires.Text = Math.Round(tireMin, 1) + " - " + Math.Round(tireMax, 1) + " seconds";
                }
                else if (dropChangeTires.Text == "4" || dropChangeTires.Text == "3")
                {
                    tireMin += Double.Parse(MiscFunctions.getProperty(pitmenu, "TireTime", 1));
                    tireMax = tireMin + Double.Parse(MiscFunctions.getProperty(pitmenu, "TireTime", 2)) * 4;
                    lblPitTires.Text = Math.Round(tireMin, 1) + " - " + Math.Round(tireMax, 1) + " seconds";
                }


                //Calculate the predicted repairtime:
                double repairMin = 0;
                double repairMax = 0;
                if (chkAeroDamage.Checked || chkSuspDamage.Checked)
                {
                    if (chkAeroDamage.Checked)
                        repairMin += Double.Parse(MiscFunctions.getProperty(pitmenu, "DamageTime", 0));
                    if (chkSuspDamage.Checked)
                        repairMin += Double.Parse(MiscFunctions.getProperty(pitmenu, "DamageTime", 2));
                    repairMax = repairMin + Double.Parse(MiscFunctions.getProperty(pitmenu, "DamageTime", 1));
                    lblPitDamage.Text = Math.Round(repairMin, 1) + " - " + Math.Round(repairMax, 1) + " seconds";
                }
                else
                    lblPitDamage.Text = "0 seconds";

                //Misc calculation
                double miscMin = 0;
                double miscMax = 0;

                string[] miscKeywords = { "FrontWingTime", "RearWingTime", "RadiatorTime", "PressureTime", "WedgeTime", "SpringRubberTime", "TrackBarTime" };
                CheckBox[] miscCheckBoxs = { this.chkFrontWing, this.chkRearWing, this.chkRadiatorTap, this.chkTirePressure, this.chkWedge, this.chkSpringRubber, this.chkTrackbar };

                for (int i = 0; i < miscKeywords.Length; i++)
                {
                    if (miscCheckBoxs[i].Checked)
                    {
                        double valueMin = 0;
                        double valueMax = 0;
                        Double.TryParse(MiscFunctions.getProperty(pitmenu, miscKeywords[i], 0), out valueMin);
                        Double.TryParse(MiscFunctions.getProperty(pitmenu, miscKeywords[i], 1), out valueMax);

                        if (valueMax == 0)
                            valueMax = valueMin;

                        miscMin += valueMin;
                        miscMax += valueMax;
                    }
                }

                //Calculate the predicted total time, depends on concurrent refueling/repairing/tire changes:
                double totalMin = 0;
                double totalMax = 0;
                if (lblConcurrent.Text == "Yes")    //if concurrent refueling/tire changes
                {
                    double[] valuesMin = { miscMin, tireMin, fuelMin };
                    double[] valuesMax = { miscMax, tireMax, fuelMax };
                    double[][] values = { valuesMin, valuesMax };
                    double[] total = { totalMin, totalMax };

                    for (int i = 0; i < values.Length; i++)
                    {
                        for (int j = 0; j < values[i].Length; j++)
                        {
                            if (values[i][j] > total[i])
                                total[i] = values[i][j];
                        }
                    }

                    totalMin = total[0];
                    totalMax = total[1];
                }
                else
                {
                    totalMin = tireMin + fuelMin + miscMin;
                    totalMax = tireMax + fuelMax + miscMax;
                }
                if (lblConcurRepair.Text == "Yes")  //if concurrent repairing/refueling
                {
                    if (repairMin > totalMin)
                        totalMin = repairMin;
                    if (repairMax > totalMax)
                        totalMax = repairMax;
                }
                else
                {
                    totalMin += repairMin;
                    totalMax += repairMax;
                }

                //Driver change
                if (this.chkDriverswap.Checked)
                {
                    double driverMin = 0;
                    double driverMax = 0;

                    Double.TryParse(MiscFunctions.getProperty(pitmenu, "DriverTime", 0), out driverMin);
                    Double.TryParse(MiscFunctions.getProperty(pitmenu, "DriverTime", 1).Trim(), out driverMax);
                    driverMax += driverMin;

                    if (this.lblConcurrentDriver.Text == "Yes")
                    {
                        if (totalMin < driverMin)
                            totalMin = driverMin;
                        if (totalMax < driverMax)
                            totalMax = driverMax;
                        if (miscMin < driverMin)
                            miscMin = driverMin;
                        if (miscMax < driverMax)
                            miscMax = driverMax;
                    }
                    else
                    {
                        miscMin += driverMin;
                        miscMax += driverMax;
                        totalMin += driverMin;
                        totalMax += driverMax;
                    }
                }

                if (miscMin != 0)
                    this.lblPitMisc.Text = Math.Round(miscMin, 1) + " - " + Math.Round(miscMax, 1) + " seconds";
                else
                    this.lblPitMisc.Text = "0 seconds";

                lblPitTotal.Text = Math.Round(totalMin, 1) + " - " + Math.Round(totalMax, 1) + " seconds";

                grpPitResult.Visible = true;

                //Multicar view
                foreach (CarStat2 instance in instances)
                {
                    instance.txtAmountFuel.Text = this.txtAmountFuel.Text;
                    instance.dropChangeTires.Text = this.dropChangeTires.Text;
                    instance.chkAeroDamage.Checked = this.chkAeroDamage.Checked;
                    instance.chkSuspDamage.Checked = this.chkSuspDamage.Checked;

                    instance.chkFrontWing.Checked = this.chkFrontWing.Checked;
                    instance.chkRearWing.Checked = this.chkRearWing.Checked;
                    instance.chkRadiatorTap.Checked = this.chkRadiatorTap.Checked;
                    instance.chkTirePressure.Checked = this.chkTirePressure.Checked;
                    instance.chkWedge.Checked = this.chkWedge.Checked;
                    instance.chkSpringRubber.Checked = this.chkSpringRubber.Checked;
                    instance.chkTrackbar.Checked = this.chkTrackbar.Checked;
                    instance.chkDriverswap.Checked = this.chkDriverswap.Checked;

                    instance.btnSimulatePitstop_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                grpPitResult.Visible = false;
                MessageBox.Show("There was an exception thrown while calculating the Pitstop time (properbly doing something the car does not support) \n\n" + ex.StackTrace, "Pitstop Calculation went horribly wrong", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        

        #endregion

        #region Getters and Setters - graphs

        //get ZedControl for the front brakes graph
        public ZedGraphControl getZedFrontBrakes
        {
            get { return zedBrakeFront; }
            set { zedBrakeFront = value; }
        }

        //get ZedControl for the rear brakes graph
        public ZedGraphControl getZedRearBrakes
        {
            get { return zedBrakeRear; }
            set { zedBrakeRear = value; }
        }

        //get ZedControl for Tire graph 1
        public ZedGraphControl getZedTire
        {
            get { return zedTire; }
            set { zedTire = value; }
        }

        //get ZedControl for Tire graph 2
        public ZedGraphControl getZedTire2
        {
            get { return zedTire2; }
            set { zedTire2 = value; }
        }

        //get ZedControl for Tire temp graph
        public ZedGraphControl getZedTireTemps
        {
            get { return zedTireTemps; }
            set { zedTireTemps = value; }
        }

        //get ZedControl for Tire thermal degradation
        public ZedGraphControl getZedTireThermalDeg1
        {
            get { return zedThermal1; }
            set { zedThermal1 = value; }
        }

        //get ZedControl for Tire thermal degradation
        public ZedGraphControl getZedTireThermalDeg2
        {
            get { return zedThermal2; }
            set { zedThermal2 = value; }
        }

        //get ZedControl for Tire wear graph
        public ZedGraphControl getZedWear
        {
            get { return zedWear; }
            set { zedWear = value; }
        }

        //get ZedControl for engine graph
        public ZedGraphControl getZedEngine
        {
            get { return zedEngine; }
            set { zedEngine = value; }
        }

        #endregion

        #region Getters and Setters - Menu settings

        //If checked, it means it's Fahrenheit
        public bool getMenuTempChecked
        {
            get { return GameEnvironmentFunc.LoadMenuSetting("Temperature") == MiscFunctions.Units.F; }
        }

        //If checked, it means it's NM
        public bool getMenuTorqueChecked
        {
            get { return GameEnvironmentFunc.LoadMenuSetting("Torque") == MiscFunctions.Units.Nm; }
        }

        //If checked, it means it's HP
        public bool getMenuPowerChecked
        {
            get { return GameEnvironmentFunc.LoadMenuSetting("Power") == MiscFunctions.Units.HP; }
        }

        public bool getOnlyNonEncrypted
        {
            get { return !gameEnv.LoadReadEncrytedFiles(); }
        }
        #endregion

        #region Getters and Setters - Misc

        //Returns the selected Boostmapping
        public int getBoostMapping()
        {
            int mapping = 0;
            if (chkBoostMapping.Checked && !init)
            {
                string selected = cbBoostMapping.Text.Trim();
                if (selected != "")
                    mapping = Int32.Parse(selected) - 1;
            }
            return mapping;
        }

        public int getBoostPressure()
        {
            int boostPressure = 0;

            Dictionary<string, string[]> dict = data.EngineData.Values;


            //standard engine based boost
            string line = null;
            int i = 0;

            while ((line = MiscFunctions.getProperty(dict, "BoostTurboPressure", i, 0)) != null)
            {
                int value = (int)Double.Parse(line);

                if (getBoostMapping() > 0)
                    value = value + (int)Double.Parse(MiscFunctions.getProperty(dict, "BoostTurboPressure", i, 1)) * getBoostMapping();

                //Wastegate, this calcularion will be removed in the future
                int wastegate = 0;
                Int32.TryParse(MiscFunctions.getProperty(data.EngineData.Values, "Wastegate", i, 2), out wastegate);

                if (wastegate > 15000)
                    value = value + wastegate;

                if (value > boostPressure)
                {
                    boostPressure = value;
                }
                i++;
            }

            return boostPressure;
        }

        //Returns the relativ amount of fuel
        public double getFuelMixture()
        {
            double fuelMixture = 1;

            if (chkEngineMixture.Checked && !init)
            {
                fuelMixture = -1; //so I can check if the enginemixtable has a value

                string[] engineMix = getCarData.EngineData.Values["enginemixturerange"][0].Split(',');
                string[] engineMixTable = new string[0];

                if (getCarData.EngineData.Values.ContainsKey("enginemixturespecial"))
                {
                    engineMixTable = getCarData.EngineData.Values["enginemixturespecial"];
                }

                string selected = cbFuelMixture.Text.Trim();

                if (selected != null && selected != "")
                {
                    if (selected.Contains("-"))
                        selected = selected.Split('-')[0];

                    //Searches the table for the values
                    for (int i = 0; i < engineMixTable.Length; i++)
                    {
                        if (Double.Parse(engineMixTable[i].Split(',')[0]) == (Double.Parse(selected) - 1))
                        {

                            string[] valArr = engineMixTable[i].Split(',');
                            string value = null;

                            if (valArr.Length == 3)
                                value = valArr[2];
                            else if (valArr.Length == 4)
                                value = valArr[3];

                            if (value != "" && value != null)
                                fuelMixture = Double.Parse(value);
                            else
                                fuelMixture = Double.Parse(engineMix[0]) + ((Double.Parse(selected) - 1) * Double.Parse(engineMix[1]));

                            break;
                        }
                    }

                    //The value is not in the table
                    if (fuelMixture == -1)
                        fuelMixture = Double.Parse(engineMix[0]) + ((Double.Parse(selected) - 1) * Double.Parse(engineMix[1]));
                }

                //If now value was found it returns to default
                if (fuelMixture == -1)
                    fuelMixture = 1;
            }

            return fuelMixture;
        }

        public CarData getCarData
        {
            get { return data; }
        }

        //To access the CheckCompounds1-control
        public CheckedListBox getCheckCompounds1
        {
            get { return CheckCompounds1; }
            set { CheckCompounds1 = value; }
        }

        //To access the CheckCompounds2-control
        public CheckedListBox getCheckCompounds2
        {
            get { return CheckCompounds2; }
            set { CheckCompounds2 = value; }
        }

        //To access the CheckCompounds3-control
        public CheckedListBox getCheckCompounds3
        {
            get { return CheckCompounds3; }
            set { CheckCompounds3 = value; }
        }

        //To access the CheckCompounds4-control
        public CheckedListBox getCheckCompounds4
        {
            get { return CheckCompounds4; }
            set { CheckCompounds4 = value; }
        }

        //To access the CheckCompounds5-control
        public CheckedListBox getCheckCompounds5
        {
            get { return CheckCompounds5; }
            set { CheckCompounds5 = value; }
        }

        //To access the CheckCompounds6-control
        public ListBox getCheckCompounds6
        {
            get { return CheckCompounds6; }
            set { CheckCompounds6 = value; }
        }

        //To access the CheckEngineFriction-control
        public CheckBox getCheckEngineFriction
        {
            get { return CheckEngineFriction; }
            set { CheckEngineFriction = value; }
        }

        //To access the Turbo warning label
        public System.Windows.Forms.Label getTurboLabel
        {
            get { return lblTurbo; }
            set { lblTurbo = value; }
        }

        //To set the text of a label
        public void setLabelText(string lblName, string value)
        {
            if (value == null)
                value = "";

            ((System.Windows.Forms.Label)(this.Controls.Find(lblName, true))[0]).Text = value;
        }

        //To get the text of a label
        public string getLabelText(string lblName)
        {
            return ((System.Windows.Forms.Label)(this.Controls.Find(lblName, true))[0]).Text;
        }

        //To get the text of a textbox
        public string getTextboxText(string txtName)
        {
            return ((System.Windows.Forms.TextBox)(this.Controls.Find(txtName, true))[0]).Text;
        }


        #endregion

        private void txtCustomTemp_TextChanged(object sender, EventArgs e)
        {
            gameEnv.Graphs.TireThermalDegGraph2();
        }

        void txtCustomTemp_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
                && !char.IsDigit(e.KeyChar)
                && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void btnGetUpgradeFile_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "upgrade files (*.ini)|*.ini|All files (*.*)|*.*";
            saveDialog.FileName = Path.GetFileName(data.Upgradelocation);

            if (saveDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter writer = new StreamWriter(saveDialog.OpenFile());
                foreach (string line in data.UpgradeFile)
                {
                    writer.WriteLine(line);
                }
                writer.Close();
            }
        }

        
    }
}
