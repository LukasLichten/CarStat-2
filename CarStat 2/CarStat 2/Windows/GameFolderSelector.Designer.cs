﻿namespace CarStat_2.Windows
{
    partial class GameFolderSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GameFolderSelector));
            this.btnSetLoc = new System.Windows.Forms.Button();
            this.chkRemeber = new System.Windows.Forms.CheckBox();
            this.btnFindGame = new System.Windows.Forms.Button();
            this.cbProfile = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // btnSetLoc
            // 
            this.btnSetLoc.Location = new System.Drawing.Point(9, 102);
            this.btnSetLoc.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSetLoc.Name = "btnSetLoc";
            this.btnSetLoc.Size = new System.Drawing.Size(176, 27);
            this.btnSetLoc.TabIndex = 10;
            this.btnSetLoc.Text = "Select Game Folder";
            this.btnSetLoc.UseVisualStyleBackColor = true;
            this.btnSetLoc.Click += new System.EventHandler(this.btnSetGameLoc_Click);
            // 
            // chkRemeber
            // 
            this.chkRemeber.Checked = true;
            this.chkRemeber.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRemeber.Location = new System.Drawing.Point(16, 71);
            this.chkRemeber.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkRemeber.Name = "chkRemeber";
            this.chkRemeber.Size = new System.Drawing.Size(158, 26);
            this.chkRemeber.TabIndex = 11;
            this.chkRemeber.Text = "Remeber Game Location";
            this.chkRemeber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkRemeber.UseVisualStyleBackColor = true;
            this.chkRemeber.CheckedChanged += new System.EventHandler(this.chkRemeber_CheckedChanged);
            // 
            // btnFindGame
            // 
            this.btnFindGame.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnFindGame.Image = global::CarStat_2.Properties.Resources.Folder_icon_small1;
            this.btnFindGame.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnFindGame.Location = new System.Drawing.Point(9, 10);
            this.btnFindGame.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnFindGame.Name = "btnFindGame";
            this.btnFindGame.Size = new System.Drawing.Size(176, 56);
            this.btnFindGame.TabIndex = 12;
            this.btnFindGame.Text = "Find Game Folder";
            this.btnFindGame.UseVisualStyleBackColor = true;
            this.btnFindGame.Click += new System.EventHandler(this.btnFindGame_Click);
            // 
            // cbProfile
            // 
            this.cbProfile.FormattingEnabled = true;
            this.cbProfile.Location = new System.Drawing.Point(9, 134);
            this.cbProfile.Name = "cbProfile";
            this.cbProfile.Size = new System.Drawing.Size(176, 21);
            this.cbProfile.TabIndex = 14;
            this.cbProfile.SelectedIndexChanged += new System.EventHandler(this.cbProfile_SelectedIndexChanged);
            // 
            // GameFolderSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(194, 166);
            this.Controls.Add(this.cbProfile);
            this.Controls.Add(this.btnSetLoc);
            this.Controls.Add(this.chkRemeber);
            this.Controls.Add(this.btnFindGame);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GameFolderSelector";
            this.ShowIcon = false;
            this.Text = "Select Game Folder";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSetLoc;
        private System.Windows.Forms.CheckBox chkRemeber;
        private System.Windows.Forms.Button btnFindGame;
        private System.Windows.Forms.ComboBox cbProfile;
    }
}