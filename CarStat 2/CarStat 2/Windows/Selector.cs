﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Misc;
using Microsoft.Win32;


namespace CarStat_2.Windows
{
    public partial class Selector : Form
    {
        CarStat2 master;

        public Selector(CarStat2 master)
        {
            InitializeComponent();

            this.master = master;
            this.Owner = master;

            this.Show();
        }

        //Open a CarStat 1-instance
        private void btnCarStat1_Click(object sender, EventArgs e)
        {
            master.gameEnv = new GameEnv.RF1.RF1Enviroment(master);
            storeAutoStart();
        }

        //Open a CarStat 2-instance
        private void btnCarStat2_Click(object sender, EventArgs e)
        {
            master.gameEnv = new GameEnv.RF2.RF2Enviroment(master);
            storeAutoStart();
        }

        //Open a CarStat 1 instance, but look for GTR2/Legends files
        private void btnCarStatGTR_Click(object sender, EventArgs e)
        {
            master.gameEnv = new GameEnv.GTR.GTREnviroment(master);
            storeAutoStart();
        }

        private void storeAutoStart()
        {
            if (checkRemember.Checked)
                GameEnvironmentFunc.StoreAutoStartSetting(master.gameEnv.GAME);

            this.Close();
        }

        private void btnAMS_Click(object sender, EventArgs e)
        {
            master.gameEnv = new GameEnv.AMS.AMSEnviroment(master);
            storeAutoStart();
        }
    }
}
