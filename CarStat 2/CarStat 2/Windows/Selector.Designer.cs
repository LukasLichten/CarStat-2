﻿namespace CarStat_2.Windows
{
    partial class Selector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Selector));
            this.checkRemember = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAMS = new System.Windows.Forms.Button();
            this.btnCarStatGTR = new System.Windows.Forms.Button();
            this.btnCarStat2 = new System.Windows.Forms.Button();
            this.btnCarStat1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // checkRemember
            // 
            this.checkRemember.AutoSize = true;
            this.checkRemember.Location = new System.Drawing.Point(211, 159);
            this.checkRemember.Name = "checkRemember";
            this.checkRemember.Size = new System.Drawing.Size(127, 17);
            this.checkRemember.TabIndex = 2;
            this.checkRemember.Text = "Remember my choice";
            this.checkRemember.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.checkRemember, "Remember this choice for the future");
            this.checkRemember.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(177, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 14);
            this.label1.TabIndex = 3;
            this.label1.Text = "rFactor 1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(304, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 14);
            this.label2.TabIndex = 4;
            this.label2.Text = "rFactor 2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 1000;
            this.toolTip1.ReshowDelay = 1000;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(44, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 28);
            this.label3.TabIndex = 6;
            this.label3.Text = "GTR 2\r\nGT Legends";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(420, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 14);
            this.label4.TabIndex = 8;
            this.label4.Text = "Automobilista";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAMS
            // 
            this.btnAMS.BackgroundImage = global::CarStat_2.Properties.Resources.AMStoBlackLogo;
            this.btnAMS.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAMS.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAMS.Location = new System.Drawing.Point(423, 24);
            this.btnAMS.Name = "btnAMS";
            this.btnAMS.Size = new System.Drawing.Size(87, 87);
            this.btnAMS.TabIndex = 7;
            this.btnAMS.UseVisualStyleBackColor = true;
            this.btnAMS.Click += new System.EventHandler(this.btnAMS_Click);
            // 
            // btnCarStatGTR
            // 
            this.btnCarStatGTR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCarStatGTR.Image = global::CarStat_2.Properties.Resources.GTRLegends;
            this.btnCarStatGTR.Location = new System.Drawing.Point(39, 24);
            this.btnCarStatGTR.Margin = new System.Windows.Forms.Padding(0);
            this.btnCarStatGTR.Name = "btnCarStatGTR";
            this.btnCarStatGTR.Size = new System.Drawing.Size(87, 87);
            this.btnCarStatGTR.TabIndex = 5;
            this.btnCarStatGTR.UseVisualStyleBackColor = true;
            this.btnCarStatGTR.Click += new System.EventHandler(this.btnCarStatGTR_Click);
            // 
            // btnCarStat2
            // 
            this.btnCarStat2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCarStat2.Image = global::CarStat_2.Properties.Resources.rf2_logo_klein_modern;
            this.btnCarStat2.Location = new System.Drawing.Point(294, 24);
            this.btnCarStat2.Name = "btnCarStat2";
            this.btnCarStat2.Size = new System.Drawing.Size(87, 87);
            this.btnCarStat2.TabIndex = 0;
            this.btnCarStat2.UseVisualStyleBackColor = true;
            this.btnCarStat2.Click += new System.EventHandler(this.btnCarStat2_Click);
            // 
            // btnCarStat1
            // 
            this.btnCarStat1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCarStat1.Image = global::CarStat_2.Properties.Resources.rf1_logo_klein;
            this.btnCarStat1.Location = new System.Drawing.Point(166, 24);
            this.btnCarStat1.Margin = new System.Windows.Forms.Padding(0);
            this.btnCarStat1.Name = "btnCarStat1";
            this.btnCarStat1.Size = new System.Drawing.Size(87, 87);
            this.btnCarStat1.TabIndex = 1;
            this.btnCarStat1.UseVisualStyleBackColor = true;
            this.btnCarStat1.Click += new System.EventHandler(this.btnCarStat1_Click);
            // 
            // Selector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 188);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnAMS);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCarStatGTR);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkRemember);
            this.Controls.Add(this.btnCarStat2);
            this.Controls.Add(this.btnCarStat1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Selector";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select the Game";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCarStat2;
        private System.Windows.Forms.Button btnCarStat1;
        private System.Windows.Forms.CheckBox checkRemember;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCarStatGTR;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAMS;
    }
}

