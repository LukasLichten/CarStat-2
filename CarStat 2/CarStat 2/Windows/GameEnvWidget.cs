﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Misc;

namespace CarStat_2.Windows
{
    public partial class GameEnvWidget : UserControl
    {
        private CarStat2 carStat;
        private Form master;

        private GameFolderSelector folderSelector;

        public EventHandler Change { get; set; }

        public GameEnvWidget()
        {
            InitializeComponent();
        }

        public void Prep(CarStat2 carStat, Form master)
        {
            this.carStat = carStat;
            this.master = master;

            RefreshGameIcon();
        }

        public void RefreshGameIcon()
        {
            GameEnvironmentFunc gameEnv = carStat.gameEnv;

            if (gameEnv != null)
            {
                btnGame.Text = "";
                btnGame.BackgroundImage = null;
                btnGame.BackColor = Color.White;

                switch (gameEnv.GAME)
                {
                    case "RF1":
                        btnGame.Image = Properties.Resources.rf1_logo_klein;
                        break;

                    case "RF2":
                        btnGame.Image = Properties.Resources.rf2_logo_klein_modern;
                        break;

                    case "GTR":
                        btnGame.Image = Properties.Resources.GTRLegends;
                        break;

                    case "AMS":
                        btnGame.Image = null;
                        btnGame.BackgroundImage = Properties.Resources.AMStoBlackLogo;
                        btnGame.BackgroundImageLayout = ImageLayout.Stretch;
                        break;

                    default:
                        btnGame.BackgroundImage = null;
                        btnGame.Image = null;
                        btnGame.BackColor = SystemColors.Control;
                        btnGame.Text = "Unknown Game";
                        break;
                }

                btnSelectFolder.Enabled = true;
            }
            else
            {
                btnGame.Image = null;
                btnGame.BackColor = SystemColors.Control;
                btnGame.Text = "Select Game";

                btnSelectFolder.Enabled = false;
            }
        }

        private void btnGame_Click(object sender, EventArgs e)
        {
            Selector selector = new Selector(carStat);
            MiscFunctions.makeParentChildWindow(master, selector);

            selector.FormClosing += Selector_FormClosing;
        }

        private void Selector_FormClosing(object sender, FormClosingEventArgs e)
        {
            RefreshGameIcon();

            Change.Invoke(sender, e);
        }

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            if (this.folderSelector != null)
            {
                MessageBox.Show("Use the open Game Folder Selector!");
                return;
            }

            GameFolderSelector folderSelector = new GameFolderSelector(this.carStat);
            MiscFunctions.makeParentChildWindow(master, folderSelector);

            folderSelector.FormClosing += FolderSelector_FormClosing;
            this.folderSelector = folderSelector;
        }

        private void FolderSelector_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.folderSelector = null;

            Change.Invoke(sender, e);
        }
    }
}
