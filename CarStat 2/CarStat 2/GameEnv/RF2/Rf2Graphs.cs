﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarStat_2;
using ZedGraph;
using Misc;
using System.Drawing;

namespace CarStat_2.GameEnv.RF2
{
    public class RF2Graphs : Graphs
    {
        double brakeOffsetFront = 0;
        double brakeOffsetRear = 0;

        public RF2Graphs(CarStat2 Form) : base(Form)
        {
        }

        public override void BrakeTempsGraph(bool rearBrake)
        {
            GraphPane myPane;
            if (rearBrake == true) //So read the rear brake
            {
                myPane = Form.getZedRearBrakes.GraphPane;
                Form.getZedRearBrakes.GraphPane.CurveList.Clear();
            }
            else
            {
                myPane = Form.getZedFrontBrakes.GraphPane;
                Form.getZedFrontBrakes.GraphPane.CurveList.Clear();
            }

            string brakeLocation = "FRONTLEFT";
            if (rearBrake == true) //So read the rear brake
            {
                brakeLocation = "REARLEFT";
            }
            double F_lowhalved = 0;
            double F_lowoptimal = 0;
            double F_highoptimal = 0;
            double F_highhalved = 0;

            if (MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeOptimumTemp", 0) != null
                && MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeOptimumTemp", 0) != "n/a")
            {
                double BrakeOptimumTemp = Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeOptimumTemp", 0));
                double BrakeFadeRange = Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeFadeRange", 0));
                F_lowhalved = BrakeOptimumTemp - BrakeFadeRange;
                F_lowoptimal = BrakeOptimumTemp;
                F_highoptimal = BrakeOptimumTemp;
                F_highhalved = BrakeOptimumTemp + BrakeFadeRange;
            }
            else
            {
                F_lowhalved = Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeResponseCurve", 0));
                F_lowoptimal = Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeResponseCurve", 1));
                F_highoptimal = Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeResponseCurve", 2));
                F_highhalved = Double.Parse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, brakeLocation, "BrakeResponseCurve", 3));
            }

            if (Form.getMenuTempChecked == true) //In °F
            {
                F_lowhalved = MiscFunctions.CelsiusToFahrenheit(F_lowhalved);
                F_lowoptimal = MiscFunctions.CelsiusToFahrenheit(F_lowoptimal);
                F_highoptimal = MiscFunctions.CelsiusToFahrenheit(F_highoptimal);
                F_highhalved = MiscFunctions.CelsiusToFahrenheit(F_highhalved);
            }

            myPane.BarSettings.Type = BarType.Stack;


            // Generate a blue bar with "Curve 2" in the legend
            BarItem myCurve = myPane.AddBar("There", new double[] { F_lowoptimal - F_lowhalved }, null, Color.Blue);
            // Fill the bar with a Blue-white-Blue color gradient for a 3d look
            myCurve.Bar.Fill = new Fill(Color.Red, Color.Green);

            // Generate a green bar with "Curve 3" in the legend
            myCurve = myPane.AddBar("Elsewhere", new double[] { F_highoptimal - F_lowoptimal }, null, Color.Green);
            // Fill the bar with a Green-white-Green color gradient for a 3d look
            myCurve.Bar.Fill = new Fill(Color.Green, Color.Green);
            // Generate a green bar with "Curve 3" in the legend
            myCurve = myPane.AddBar("Elsewhere", new double[] { F_highhalved - F_highoptimal }, null, Color.Green);
            // Fill the bar with a Green-white-Green color gradient for a 3d look
            myCurve.Bar.Fill = new Fill(Color.Green, Color.Red);


            myPane.BarSettings.Base = BarBase.Y;
            myPane.BarSettings.ClusterScaleWidth = 4;

            // Fill the chart background with a color gradient
            myPane.Fill.Color = SystemColors.ButtonFace;
            myPane.Margin.All = 0;
            myPane.Border.IsVisible = false;
            myPane.IsFontsScaled = false;
            myPane.Title.IsVisible = false;
            myPane.Legend.IsVisible = false;
            myPane.YAxis.Scale.Min = 0;
            myPane.YAxis.Scale.Max = 2;
            myPane.YAxis.IsVisible = false;
            myPane.XAxis.Title.IsVisible = false;
            myPane.XAxis.MajorTic.IsOpposite = false;
            myPane.XAxis.MinorTic.IsOpposite = false;
            myPane.YAxis.Type = AxisType.Linear;

            myPane.XAxis.Scale.FontSpec.Size = 12;
            // Calculate the Axis Scale Ranges

            if (rearBrake == true) //So read the rear brake
            {
                brakeOffsetRear = F_lowhalved;
                myPane.XAxis.Scale.Max = F_highhalved - brakeOffsetRear;
                Form.getZedRearBrakes.MasterPane.Border.IsVisible = false;
                Form.getZedRearBrakes.GraphPane.XAxis.ScaleFormatEvent += new Axis.ScaleFormatHandler(XScaleFormatEventRear);
                Form.getZedRearBrakes.AxisChange();
                Form.getZedRearBrakes.Refresh();
            }
            else
            {
                brakeOffsetFront = F_lowhalved;
                myPane.XAxis.Scale.Max = F_highhalved - brakeOffsetFront;
                Form.getZedFrontBrakes.MasterPane.Border.IsVisible = false;
                Form.getZedFrontBrakes.GraphPane.XAxis.ScaleFormatEvent += new Axis.ScaleFormatHandler(XScaleFormatEventFront);
                Form.getZedFrontBrakes.AxisChange();
                Form.getZedFrontBrakes.Refresh();
            }

        }

        public string XScaleFormatEventFront(GraphPane pane, Axis axis, double val, int index)
        {
            if (Form.getMenuTempChecked == false) //In °C
                return (val + brakeOffsetFront).ToString() + "°C";
            else
                return (val + brakeOffsetFront).ToString() + "°F";
        }

        public string XScaleFormatEventRear(GraphPane pane, Axis axis, double val, int index)
        {
            if (Form.getMenuTempChecked == false) //In °C
                return (val + brakeOffsetRear).ToString() + "°C";
            else
                return (val + brakeOffsetRear).ToString() + "°F";
        }

        //Creates the Thermal degradation 1 graph
        public override void TireThermalDegGraph1()
        {
            try
            {
                Form.getZedTireThermalDeg1.GraphPane.CurveList.Clear();
                GraphPane myPane = Form.getZedTireThermalDeg1.GraphPane;
                myPane.Title.Text = "% grip due to thermal degradation";
                myPane.XAxis.Title.Text = "Tire history units";
                myPane.YAxis.Title.Text = "% grip";
                List<int> selectedCompounds = new List<int>();
                for (int i = 0; i < Form.getCheckCompounds5.Items.Count; i++)
                {
                    if (Form.getCheckCompounds5.GetItemChecked(i) == true)
                        selectedCompounds.Add(i);
                }

                foreach (int i in selectedCompounds)
                {
                    PointPairList grip = new PointPairList();
                    string selected = Form.getCheckCompounds5.Items[i].ToString();

                    string[] valuesString = Form.getCarData.TireCompounds[selected].TGMValues1["realtime"]["degradationperunithistory"][0].Split(',');
                    double[] values = new double[32];

                    for (int j = 0; j < valuesString.Length; j++)
                    {
                        values[j] = double.Parse(valuesString[j]);
                    }

                    for (int j = 0; j < values.Length; j++)
                    {
                        grip.Add(j, values[j] * 100);

                    }

                    LineItem myCurve;
                    if (i < colors.Length)
                        myCurve = myPane.AddCurve(selected, grip, colors[i]);
                    else
                        myCurve = myPane.AddCurve(selected, grip, Color.Black);
                    myCurve.Line.IsSmooth = true;
                    myCurve.Line.IsOptimizedDraw = true;
                    myCurve.Line.IsAntiAlias = true;
                    myCurve.Symbol.IsVisible = true;
                }
                myPane.XAxis.Scale.Min = 0;
                myPane.XAxis.Scale.Max = 31;
                myPane.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 166), 90F);
                myPane.Fill.Color = SystemColors.ButtonFace;
                myPane.Border.IsVisible = false;
                Form.getZedTireThermalDeg1.MasterPane.Border.IsVisible = false;
                Form.getZedTireThermalDeg1.AxisChange();
                Form.getZedTireThermalDeg1.Refresh();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ex.ToString();
                //MessageBox.Show("Error while creating the tire grip/wear graph:\n" + ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error while creating the tire grip/wear graph:\n" + ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
        }

        //Creates the Thermal degradation 2 graph
        public override void TireThermalDegGraph2()
        {
            try
            {
                Form.getZedTireThermalDeg2.GraphPane.CurveList.Clear();
                GraphPane myPane = Form.getZedTireThermalDeg2.GraphPane;
                myPane.Title.Text = "% grip of some sustained temps";
                myPane.XAxis.Title.Text = "Time in minutes";
                myPane.YAxis.Title.Text = "% grip";
                List<int> selectedCompounds = new List<int>();
                List<double> tempOverActivation = new List<double> { 10, 20, 30 };


                string selected = Form.getCheckCompounds6.SelectedItem.ToString();
                double ActivationTemp = MiscFunctions.KelvinToCelsius(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selected].TGMValues1, "RealTime", "DegradationCurveParameters", 0)));
                double HistoryStep = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selected].TGMValues1, "RealTime", "DegradationCurveParameters", 1));
                Form.setLabelText("lblInvalidTemp", "");
                if (Form.getTextboxText("txtCustomTemp") != "")
                {
                    if (double.Parse(Form.getTextboxText("txtCustomTemp")) > ActivationTemp)
                        tempOverActivation.Add(double.Parse(Form.getTextboxText("txtCustomTemp")) - ActivationTemp);
                    else
                        Form.setLabelText("lblInvalidTemp", "Should be > " + ActivationTemp);
                }
                double maxSeconds = 0;

                string[] valuesString = Form.getCarData.TireCompounds[selected].TGMValues1["realtime"]["degradationperunithistory"][0].Split(',');
                double[] values = new double[32];

                for (int j = 0; j < valuesString.Length; j++)
                {
                    values[j] = double.Parse(valuesString[j]);
                }

                for (int k = 0; k < tempOverActivation.Count; k++)
                {
                    PointPairList grip = new PointPairList();
                    for (int j = 0; j < values.Length; j++)
                    {
                        double history = j * HistoryStep;
                        double tijd = history / tempOverActivation[k];
                        if (tijd > maxSeconds)
                            maxSeconds = tijd;
                        grip.Add(Math.Round(tijd / 60, 2), values[j] * 100);

                    }

                    LineItem myCurve;
                    myCurve = myPane.AddCurve((ActivationTemp + tempOverActivation[k]) + " °C", grip, colors[k]);
                    myCurve.Line.IsSmooth = true;
                    myCurve.Line.IsOptimizedDraw = true;
                    myCurve.Line.IsAntiAlias = true;
                    myCurve.Symbol.IsVisible = true;
                }

                myPane.XAxis.Scale.Min = 0;
                myPane.XAxis.Scale.Max = maxSeconds / 60;
                myPane.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 166), 90F);
                myPane.Fill.Color = SystemColors.ButtonFace;
                myPane.Border.IsVisible = false;

                Form.getZedTireThermalDeg2.MasterPane.Border.IsVisible = false;
                Form.getZedTireThermalDeg2.AxisChange();
                Form.getZedTireThermalDeg2.Refresh();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ex.ToString();
                //MessageBox.Show("Error while creating the tire grip/wear graph:\n" + ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error while creating the tire grip/wear graph:\n" + ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
        }

        //Creates the Tire temp graph
        public override void TireTempGraph()
        {
            double minTemp = 0;
            double maxTemp = 0;
            try
            {
                Form.getZedTireTemps.GraphPane.CurveList.Clear();
                GraphPane myPane = Form.getZedTireTemps.GraphPane;
                myPane.Title.Text = "% grip at some temperatures";
                myPane.XAxis.Title.Text = "Temperature in °C";
                if (Form.getMenuTempChecked == true) //In Fahrenheit
                    myPane.XAxis.Title.Text = "Temperature in °F";
                myPane.YAxis.Title.Text = "% grip";
                List<int> selectedCompounds = new List<int>();
                for (int i = 0; i < Form.getCheckCompounds2.Items.Count; i++)
                {
                    if (Form.getCheckCompounds2.GetItemChecked(i) == true)
                        selectedCompounds.Add(i);
                }

                foreach (int i in selectedCompounds)
                {


                    PointPairList grip = new PointPairList();
                    string selected = Form.getCheckCompounds2.Items[i].ToString();

                    int j = 0;

                    double staticGrip = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selected].TGMValues1, "Realtime", "StaticBaseCoefficient", 0));
                    string[] valuesString = Form.getCarData.TireCompounds[selected].TGMValues1["realtime"]["staticcurve"][0].Split(',');
                    double[] values = new double[6];

                    for (j = 0; j < valuesString.Length; j++)
                    {
                        values[j] = double.Parse(valuesString[j]);
                    }
                    if (minTemp > MiscFunctions.KelvinToCelsius(values[0]) - 20)
                        minTemp = MiscFunctions.KelvinToCelsius(values[0]) - 20;
                    if (maxTemp < MiscFunctions.KelvinToCelsius(values[4]) + 20)
                        maxTemp = MiscFunctions.KelvinToCelsius(values[4]) + 20;
                    for (j = 0; j < values.Length; j = j + 2)
                    {
                        if (Form.getMenuTempChecked == true) //In Fahrenheit
                            grip.Add(MiscFunctions.CelsiusToFahrenheit(MiscFunctions.KelvinToCelsius(values[j])), values[j + 1] * 100);
                        else
                            grip.Add(MiscFunctions.KelvinToCelsius(values[j]), values[j + 1] * 100);

                    }

                    LineItem myCurve;
                    if (i < colors.Length)
                        myCurve = myPane.AddCurve(selected, grip, colors[i]);
                    else
                        myCurve = myPane.AddCurve(selected, grip, Color.Black);
                    myCurve.Line.IsSmooth = true;
                    myCurve.Line.IsOptimizedDraw = true;
                    myCurve.Line.IsAntiAlias = true;
                    myCurve.Symbol.IsVisible = true;
                }
                //myPane1.XAxis.Type = AxisType.Text;
                //myPane1.XAxis.Scale.TextLabels = labels;
                if (Form.getMenuTempChecked == true) //In Fahrenheit
                {
                    myPane.XAxis.Scale.Min = MiscFunctions.CelsiusToFahrenheit(minTemp);
                    myPane.XAxis.Scale.Max = MiscFunctions.CelsiusToFahrenheit(maxTemp);
                }
                else
                {
                    myPane.XAxis.Scale.Min = minTemp;
                    myPane.XAxis.Scale.Max = maxTemp;
                }
                myPane.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 166), 90F);
                myPane.Fill.Color = SystemColors.ButtonFace;
                myPane.Border.IsVisible = false;
                Form.getZedTireTemps.MasterPane.Border.IsVisible = false;
                Form.getZedTireTemps.AxisChange();
                Form.getZedTireTemps.Refresh();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error while creating the tire grip/wear graph:\n" + ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
        }

        //Creates the Tire wear Graphs
        public override void TireWearGraphs()
        {
            try
            {
                Form.getZedTire.GraphPane.CurveList.Clear();
                Form.getZedTire2.GraphPane.CurveList.Clear();
                GraphPane myPane1 = Form.getZedTire.GraphPane;
                GraphPane myPane2 = Form.getZedTire2.GraphPane;
                myPane1.Title.Text = "% grip at a % of wear";
                myPane1.XAxis.Title.Text = "% wear";
                myPane1.YAxis.Title.Text = "% grip";
                myPane2.Title.Text = "Grip at a certain % of wear:";
                myPane2.XAxis.Title.Text = "% wear";
                myPane2.YAxis.Title.Text = "Grip";
                string[] labels = null;

                for (int i = 0; i < Form.getCheckCompounds3.Items.Count; i++)
                {
                    PointPairList grip = new PointPairList();
                    PointPairList wear = new PointPairList();
                    string selected = Form.getCheckCompounds3.Items[i].ToString();

                    string[] wearFractionString = Form.getCarData.TireCompounds[selected].TGMValues1["realtime"]["degradationperwearfraction"][0].Replace(",,", ",").Split(',');
                    double[] wearFractions = new double[32];
                    for (int j = 0; j < wearFractionString.Length; j++)
                        wearFractions[j] = double.Parse(wearFractionString[j]);

                    double[] percentages = new double[32];
                    labels = new string[32];
                    for (int k = 0; k < 32; k++)
                    {
                        percentages[k] = k * 3.225806451612903;
                        labels[k] = percentages[k].ToString("N1");
                    }

                    double staticGrip = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selected].TGMValues1, "REALTIME", "StaticBaseCoefficient", 0));

                    //grip.Add(0, 100);
                    for (int j = 0; j < labels.Length; j++)
                    {
                        grip.Add(percentages[j], wearFractions[j] * 100);
                        wear.Add(percentages[j], wearFractions[j] * staticGrip);

                    }
                    if (Form.getCheckCompounds3.GetItemChecked(i))
                    {
                        LineItem myCurve;
                        if (i < colors.Length)
                            myCurve = myPane1.AddCurve(selected, grip, colors[i]);
                        else
                            myCurve = myPane1.AddCurve(selected, grip, Color.Black);
                        myCurve.Line.IsOptimizedDraw = true;
                    }
                    if (Form.getCheckCompounds4.GetItemChecked(i))
                    {
                        LineItem myCurve;
                        if (i < colors.Length)
                            myCurve = myPane2.AddCurve(selected, wear, colors[i]);
                        else
                            myCurve = myPane2.AddCurve(selected, wear, Color.Black);
                        myCurve.Line.IsOptimizedDraw = true;
                    }
                }
                myPane1.XAxis.Type = AxisType.Text;
                myPane1.XAxis.Scale.TextLabels = labels;
                myPane1.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 166), 90F);
                myPane2.XAxis.Type = AxisType.Text;
                myPane2.XAxis.Scale.TextLabels = labels;
                myPane2.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 166), 90F);
                myPane1.Fill.Color = SystemColors.ButtonFace;
                myPane2.Fill.Color = SystemColors.ButtonFace;
                myPane1.Border.IsVisible = false;
                myPane2.Border.IsVisible = false;
                Form.getZedTire.MasterPane.Border.IsVisible = false;
                Form.getZedTire2.MasterPane.Border.IsVisible = false;


                Form.getZedTire.AxisChange();
                Form.getZedTire.Refresh();
                Form.getZedTire2.AxisChange();
                Form.getZedTire2.Refresh();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ex.ToString();
                //MessageBox.Show("Error while creating the tire grip/wear graph:\n" + ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error while creating the tire grip/wear graph:\n" + ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
        }

        //Creates the Wear/Heating/Grip graph (General Tire)
        public override void WearheatGraph()
        {
            try
            {
                Form.getZedWear.GraphPane.CurveList.Clear();
                GraphPane myPane = Form.getZedWear.GraphPane;

                // Set the Titles

                myPane.Title.Text = "WearRate, Heating and Grip of different compounds";
                myPane.XAxis.Title.Text = "";
                myPane.YAxis.Title.Text = "";

                string[] labels = { "WearRate", "Heating", "Grip", "Wet grip (*)" };

                double maxheat = 0;
                double maxwear = 0;
                double maxgrip = 0;
                double maxwet = 0;

                List<TireData> tiredata = new List<TireData>();
                for (int i = 0; i < Form.getCheckCompounds1.CheckedItems.Count; i++)
                {
                    string selected = Form.getCheckCompounds1.CheckedItems[i].ToString();
                    Dictionary<string, Dictionary<string, string[]>> tgm = Form.getCarData.TireCompounds[selected].TGMValues1;
                    TireData data = new TireData();

                    data.Wear = getMinAbrasion(selected, true); // Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selected].TGMFile1, "StaticBaseCoefficient", 1, Form.getCheckCompounds1.CheckedItems[i].ToString()));
                    double temp = 0;
                    string groundContact = MiscFunctions.getProperty(tgm, "Realtime", "GroundContactConductance", 0);
                    if (double.TryParse(groundContact, out temp) == false)
                        data.Heat = Double.Parse(MiscFunctions.getProperty(tgm, "Realtime", "GroundConductance", 0));
                    else
                        data.Heat = Double.Parse(groundContact);
                    data.Grip = Double.Parse(MiscFunctions.getProperty(tgm, "Realtime", "StaticBaseCoefficient", 0));
                    data.Wetgrip = data.Grip + (Double.Parse(MiscFunctions.getProperty(tgm, "Realtime", "DampnessEffects", 0)));

                    if (data.Heat > maxheat)
                        maxheat = data.Heat;
                    if (data.Wear > maxwear)
                        maxwear = data.Wear;
                    if (data.Grip > maxgrip)
                        maxgrip = data.Grip;
                    if (data.Wetgrip > maxwet)
                        maxwet = data.Wetgrip;

                    tiredata.Add(data);
                }

                if (Form.master == null || Form.master == Form)
                {
                    heatscale = 100 / maxheat;
                    wearscale = 100 / maxwear;
                    gripscale = 100 / maxgrip;
                }
                else
                {
                    Graphs master = Form.master.gameEnv.Graphs;
                    heatscale = master.heatscale;
                    wearscale = master.wearscale;
                    gripscale = master.gripscale;
                }


                for (int i = 0; i < Form.getCheckCompounds1.CheckedItems.Count; i++)
                {
                    PointPairList list = new PointPairList();
                    list.Add(i, wearscale * tiredata[i].Wear);
                    list.Add(i, heatscale * tiredata[i].Heat);
                    list.Add(i, gripscale * tiredata[i].Grip);
                    list.Add(i, gripscale * tiredata[i].Wetgrip);
                    BarItem myBar = myPane.AddBar(Form.getCheckCompounds1.CheckedItems[i].ToString(), list, colors[i]);
                    myBar.Bar.Fill = new Fill(colors[i], Color.White, colors[i]);
                }
                myPane.BarSettings.MinClusterGap = 2;
                myPane.XAxis.MajorTic.IsBetweenLabels = true;
                myPane.XAxis.Scale.TextLabels = labels;
                myPane.XAxis.Type = AxisType.Text;

                // Fill the Axis and Pane backgrounds
                myPane.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 166), 90F);
                myPane.Fill.Color = SystemColors.ButtonFace;
                myPane.Border.IsVisible = false;
                Form.getZedWear.MasterPane.Border.IsVisible = false;
                Form.getZedWear.AxisChange();
                Form.getZedWear.Refresh();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                ex.ToString();
                //MessageBox.Show("Error while creating the tire grip/wear graph:\n" + ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error while creating grip/wear/heatig graph:\n" + ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
        }

        private double getMinAbrasion(string compound, bool first)
        {
            string AbrasionVolume = Form.getCarData.TireCompounds[compound].TGMValues1["realtime"]["abrasionvolumeperunitenergy"][0];
            string[] AbrasionVolumeStrings = AbrasionVolume.Split(',');
            double minAbrasion = 1;
            foreach (string abrasion in AbrasionVolumeStrings)
            {
                try
                {
                    double abr = Double.Parse(abrasion);
                    if (abr < minAbrasion)
                        minAbrasion = abr;
                }
                catch
                {
                    //Nothing. Also thx FSR!
                }
            }
            return minAbrasion;
        }

        //Creates the Engine Graphs
        public override void EngineGraph(bool init)
        {
            int standardAirPressure = 101325;
            bool newEngineModel = true;

            Form.getZedEngine.GraphPane.CurveList.Clear();
            GraphPane myPane = Form.getZedEngine.GraphPane;
            myPane.Title.Text = "Torque/Power";
            myPane.XAxis.Title.Text = "RPM";
            if (Form.getMenuTorqueChecked == true)
                myPane.YAxis.Title.Text = "Torque (Nm)";
            else
                myPane.YAxis.Title.Text = "Torque (ft-lb)";
            if (Form.getMenuPowerChecked == true)
                myPane.Y2Axis.Title.Text = "Power (Hp)";
            else
                myPane.Y2Axis.Title.Text = "Power (kW)";
            PointPairList torque = new PointPairList();
            PointPairList power = new PointPairList();

            //In- or exclude extra engine friction
            double powerMultiplier = 1;
            double torqueMultiplier = 1;
            if (Form.getCheckEngineFriction.Checked == true)
            {
                powerMultiplier = Form.getCarData.EngineData.GeneralPowerMult;
                torqueMultiplier = Form.getCarData.EngineData.GeneralTorqueMult;
            }

            //Refernce Airpressure (and "new" Engine Model)
            int referncePressure = standardAirPressure;
            if (Form.getCarData.EngineData.Values.ContainsKey("referenceconditions"))
            {
                referncePressure = (int) Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "referenceconditions", 0));
            }
            else
            {
                newEngineModel = false;
            }

            //Boost Mapping
            int mapping = Form.getBoostMapping();


            //Turbo
            int boostPressure = -1;
            if (MiscFunctions.getTurboAmount(Form.getCarData.EngineData.EngineFile) > 0 && Form.radbAspTurbo.Checked)
            {
                boostPressure = Form.getBoostPressure();
            }


            //Boost Mapping multi
            double boostMappingTorqueMulti = 1;
            double boostMappingPowerMulti = 1;
            double temp = 0;

            if (Double.TryParse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "BoostTorque", 0), out temp))
                boostMappingTorqueMulti = 1 + (temp * mapping);

            if (Double.TryParse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "BoostPower", 0), out temp))
                boostMappingPowerMulti = 1 + (temp * mapping);

            //Engine Mixture
            double fuelMixture = Form.getFuelMixture();

            double throttlePos = 1; //TODO

            Form.getCarData.EngineData.resetMax();
            try
            {
                string[] rpmBase = null;
                string[] mixturePerThrottle = null; //TODO
                string[] volumeFract = null;

                if (newEngineModel)
                {
                    rpmBase = Form.getCarData.EngineData.Values["rpmbase"];
                    mixturePerThrottle = Form.getCarData.EngineData.Values["mixturefract"]; //TODO
                    volumeFract = Form.getCarData.EngineData.Values["volumefract"];
                }
                else
                {
                    rpmBase = Form.getCarData.EngineData.Values["rpmtorque"];
                }

                double torqueCurveShift = 1;
                if (!Double.TryParse(MiscFunctions.getProperty(Form.getCarData.HDVehicleData.Values, "ENGINE", "TorqueCurveShift", 0), out torqueCurveShift))
                    torqueCurveShift = 1;

                for (int i = 0; i < rpmBase.Length; i++)
                {
                    string[] baseSplit = rpmBase[i].Split(',');

                    double rpm = Double.Parse(baseSplit[0]);
                    if (baseSplit[1].IndexOf('.') != baseSplit[1].LastIndexOf('.'))
                        baseSplit[1] = baseSplit[1].Substring(0, baseSplit[1].LastIndexOf('.'));

                    //Torque curve shift
                    if (Form.CheckEngineFriction.Checked)
                        rpm = rpm * torqueCurveShift;

                    double mintrek = Double.Parse(baseSplit[1].Replace("—", "-"));
                    double maxtrek = Double.Parse(baseSplit[2]);

                    double volume = 1;
                    double fuelmixMulti = 1;
                    double pressureMulti = 1;

                    if (newEngineModel)
                    {
                        volume = MiscFunctions.calcValueInTable(MiscFunctions.getNumbersFromString(volumeFract[i], ','), throttlePos);
                        fuelmixMulti = Form.getCarData.EngineData.fuelAirMixtureTorqueMulti(fuelMixture);

                        if (boostPressure != -1 && boostPressure > standardAirPressure)
                            pressureMulti = ((double)boostPressure) / referncePressure;
                        else
                            pressureMulti = ((double)standardAirPressure) / referncePressure;

                        double powerIncreaseOverPressure = 1;
                        if (!Double.TryParse(baseSplit[4], out powerIncreaseOverPressure))
                            powerIncreaseOverPressure = 1;

                        pressureMulti = (Math.Pow(pressureMulti, powerIncreaseOverPressure));
                    }

                    double trek = MiscFunctions.calcValueBetweenTwoValues(mintrek, maxtrek, volume) * fuelmixMulti * pressureMulti;

                    if (trek < 0)
                        trek = 0;

                    double pow = MiscFunctions.torqueToHP(trek, rpm) * Form.getCarData.TheEngineUpgrade.GeneralPowerMult * powerMultiplier * boostMappingPowerMulti;
                    trek = Math.Round(trek * Form.getCarData.TheEngineUpgrade.GeneralTorqueMult * torqueMultiplier * boostMappingTorqueMulti, 0);

                    if (Form.getMenuTorqueChecked)
                        torque.Add(rpm, trek);
                    else
                        torque.Add(rpm, MiscFunctions.nmToLb(trek));

                    if (Form.getMenuPowerChecked)
                        power.Add(rpm, MiscFunctions.kwToHP(pow));
                    else
                        power.Add(rpm, pow);

                    if (trek > Form.getCarData.EngineData.Maxtorque)
                    {
                        Form.getCarData.EngineData.Maxtorque = trek;
                        Form.getCarData.EngineData.Torquerpm = rpm;
                    }
                    if (pow > Form.getCarData.EngineData.Maxpower)
                    {
                        Form.getCarData.EngineData.Maxpower = pow;
                        Form.getCarData.EngineData.Powerrpm = rpm;
                    }

                }

                LineItem torqueCurve = new LineItem("Torque", torque, Color.Red, SymbolType.Default);
                LineItem powerCurve = new LineItem("Power", power, Color.Blue, SymbolType.Default);




                myPane.CurveList.Add(torqueCurve);
                myPane.CurveList.Add(powerCurve);

                torqueCurve.Line.IsOptimizedDraw = true;
                powerCurve.Line.IsOptimizedDraw = true;
                powerCurve.IsY2Axis = true;
                powerCurve.YAxisIndex = 2;

                myPane.YAxis.Scale.FontSpec.FontColor = Color.Red;
                myPane.YAxis.Title.FontSpec.FontColor = Color.Red;
                myPane.Y2Axis.IsVisible = true;
                myPane.Y2Axis.Scale.FontSpec.FontColor = Color.Blue;
                myPane.Y2Axis.Title.FontSpec.FontColor = Color.Blue;
                myPane.Y2Axis.MajorTic.IsOpposite = false;
                myPane.Y2Axis.MinorTic.IsOpposite = false;
                myPane.Y2Axis.Scale.Align = AlignP.Center;
                myPane.Chart.Fill = new Fill(Color.White, Color.FromArgb(255, 255, 166), 90F);

                if (init)
                    Form.getZedEngine.AxisChange();

                myPane.CurveList.AddRange(this.getEngineBars());

                myPane.Fill.Color = SystemColors.ButtonFace;
                myPane.Border.IsVisible = false;
                Form.getZedEngine.MasterPane.Border.IsVisible = false;
                Form.getZedEngine.Refresh();
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Error while creating the engine graph:\n" + ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw ex;
            }
        }

        internal override List<LineItem> getEngineBars()
        {
            List<LineItem> bars = new List<LineItem>();

            PointPairList limiter = new PointPairList();
            //limiter.Add(Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "RevLimitRange", 0)), 10000);
            double revlimit = Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "RevLimitRange", 1))
                * (Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "RevLimitRange", 2)) - 1)
                + Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "RevLimitRange", 0));
            limiter.Add(revlimit, -100000);
            limiter.Add(revlimit, 100000);

            bars.Add(new LineItem("Rev limit", limiter, Color.Black, SymbolType.None));

            //PointPairList saferpm = new PointPairList();
            //saferpm.Add(Double.Parse(MiscFunctions.getProperty(Form.getCarData.EngineData.Values, "LifetimeEngineRPM", 0)), 10000);
            //bars.Add(new BarItem("Safe RPM", saferpm, Color.Red));

            return bars;
        }
    }
}
