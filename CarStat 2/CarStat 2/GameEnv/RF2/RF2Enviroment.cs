﻿using Microsoft.Win32;
using Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarStat_2.DataObjects;

namespace CarStat_2.GameEnv.RF2
{
    public class RF2Enviroment : GameEnvironmentFunc
    {

        public RF2Enviroment(CarStat2 carStat2) : base("RF2")
        {
            base.Data = new CarData();
            base.Reader = new DataReader(this);

            base.LabelsSetter = new RF2LabelsSetter(carStat2);
            base.Graphs = new RF2Graphs(carStat2);
            base.SetupCreator = new SetupCreator(carStat2);
        }

        public override void FillWithDefaultValues()
        {
            Dictionary<string, Dictionary<string, string[]>> defaultHDV = DataReader.ListToMultiLevelDict(MiscFunctions.FileToList(new MemoryStream(Properties.Resources.skipbarber)));
            Dictionary<string, Dictionary<string, string[]>> defaultTGM = DataReader.ListToMultiLevelDict(MiscFunctions.FileToListTGM(new MemoryStream(Properties.Resources.BFGoodrich_g_ForceSport_195_55_R15x7)));

            //Fill default values
            DataReader.addDefaultValues(Data.HDVehicleData.Values, defaultHDV);

            foreach (KeyValuePair<string, TireCompoundData> tire in Data.TireCompounds)
            {
                DataReader.addDefaultValues(tire.Value.TGMValues1, defaultTGM);
                DataReader.addDefaultValues(tire.Value.TGMValues2, defaultTGM);
            }
        }

        public override string FindGameLocation()
        {
            try
            {
                RegistryKey MyReg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\rFactor2", false);
                string rfactorloc = "" + MyReg.GetValue("DataPath");
                if (!Directory.Exists(Path.Combine(rfactorloc, @"Installed\Vehicles")))
                {
                    throw new NullReferenceException();
                }

                return rfactorloc;
            }
            catch (NullReferenceException)
            {
                try
                {
                    using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                    using (var key = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Steam App 365960", false))
                    {
                        string rfactorloc = "" + key.GetValue("InstallLocation");

                        if (!Directory.Exists(Path.Combine(rfactorloc, @"Installed\Vehicles")))
                        {
                            throw new NullReferenceException();
                        }

                        return rfactorloc;
                    }
                }
                catch (NullReferenceException)
                {
                }
            }

            return "";
        }

        public override string GetSetupFolder()
        {
            throw new NotImplementedException();
        }

        public override string GetVehFolder()
        {
            return @"Installed\Vehicles\";
        }
    }
}
