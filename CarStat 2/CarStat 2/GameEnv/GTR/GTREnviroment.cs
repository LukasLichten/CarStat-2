﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarStat_2.GameEnv.RF1;

namespace CarStat_2.GameEnv.GTR
{
    public class GTREnviroment : RF1Enviroment
    {
        public GTREnviroment(CarStat2 carStat2) : base(carStat2, "GTR")
        {
            base.Reader = new GTRReader(this);
        }

        public override string FindGameLocation()
        {
            return null;
        }

        public override string GetVehFolder()
        {
            return @"GameData\Teams\";
        }
    }
}
