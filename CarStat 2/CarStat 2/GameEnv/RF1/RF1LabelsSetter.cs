﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarStat_2;
using Misc;

namespace CarStat_2.GameEnv.RF1
{
    public class RF1LabelsSetter : RF2.RF2LabelsSetter
    {
        public RF1LabelsSetter(CarStat2 Form) : base(Form)
        {
        }

        public override void setBrakesLabels()
        {
            base.setBrakesLabels();
        }

        public override void setCarLabels()
        {
            base.setCarLabels();
        }

        public override void setEngineLabels()
        {
            base.setEngineLabels();
        }

        public override void setPitstopLabels()
        {
            base.setPitstopLabels();
        }

        public override void setTeamLabels()
        {
            base.setTeamLabels();
        }

        public override void setTireLabels()
        {
            string selectedCompound1 = Form.getCheckCompounds1.SelectedItem.ToString();
            string selectedCompound2 = Form.getCheckCompounds2.SelectedItem.ToString();

            //Form.setLabelText("lblFrontTireMass", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selectedCompound1].TGMFile1, "TotalMass", 1)), 3) + " kg");
            //Form.setLabelText("lblRearTireMass", Math.Round(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selectedCompound1].TGMFile2, "TotalMass", 1)), 3) + " kg");

            if (!Form.getCarData.TireCompounds[selectedCompound1].FrontRear || true)
            {
                Form.labelTire1Mass.Text = Form.labelTire1Mass.Text.Replace("Front", "Right");
                Form.labelTire2Mass.Text = Form.labelTire2Mass.Text.Replace("Rear", "Left");
                Form.labelRim1.Text = Form.labelRim1.Text.Replace("Front", "Right");
                Form.labelRim2.Text = Form.labelRim2.Text.Replace("Rear", "Left");
            }
            else
            {
                Form.labelTire1Mass.Text = Form.labelTire1Mass.Text.Replace("Right", "Front");
                Form.labelTire2Mass.Text = Form.labelTire2Mass.Text.Replace("Left", "Rear");
                Form.labelRim1.Text = Form.labelRim1.Text.Replace("Right", "Front");
                Form.labelRim2.Text = Form.labelRim2.Text.Replace("Left", "Rear");
            }

            //Set tire radii
            double frontRadius = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "Radius", 1, false, selectedCompound1));
            double rearRadius = Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "Radius", 1, true, selectedCompound1));

            Form.setLabelText("lblFrontTireRim", 2 * frontRadius + " m - " + Math.Round(2 * MiscFunctions.MetersToInches(frontRadius), 1) + "\"");
            Form.setLabelText("lblRearTireRim", 2 * rearRadius + " m - " + Math.Round(2 * MiscFunctions.MetersToInches(rearRadius), 1) + "\"");

            //Set tire starttemps:
            if (Form.getMenuTempChecked == false) //In °C
                Form.setLabelText("lblTireStartTemp", Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "Temperatures", 2, selectedCompound1)) + " °C");
            else
                Form.setLabelText("lblTireStartTemp", MiscFunctions.CelsiusToFahrenheit(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "Temperatures", 2, selectedCompound1))) + " °F");

            if (Form.getMenuTempChecked == false)
                Form.setLabelText("lblTireTemp", MiscFunctions.getProperty(Form.getCarData.TyreFile, "Temperatures", 1, Form.getCheckCompounds2.SelectedItem.ToString()) + " °C");
            else
                Form.setLabelText("lblTireTemp", MiscFunctions.CelsiusToFahrenheit(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TyreFile, "Temperatures", 1, Form.getCheckCompounds2.SelectedItem.ToString()))) + " °F");

            //Set tire optimal temps:
            //if (Form.getMenuTempChecked == false) //In °C
            //Form.setLabelText("lblTireTemp", MiscFunctions.KelvinToCelsius(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selectedCompound2].TGMFile1, "StaticCurve", 3))) + " °C");
            //else
            //Form.setLabelText("lblTireTemp", MiscFunctions.CelsiusToFahrenheit(MiscFunctions.KelvinToCelsius(Double.Parse(MiscFunctions.getProperty(Form.getCarData.TireCompounds[selectedCompound2].TGMFile1, "StaticCurve", 3)))) + " °F");


        }
    }
}
