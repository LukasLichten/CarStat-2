﻿namespace CarStat_2
{
    partial class CarStat2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CarStat2));
            this.txtUpgradeDescription = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openVEHicleFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openVehicleSelectorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openTBCFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openEngineFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openOptionsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.openSetupsManager = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.CheckCompounds4 = new System.Windows.Forms.CheckedListBox();
            this.CheckCompounds3 = new System.Windows.Forms.CheckedListBox();
            this.CheckCompounds2 = new System.Windows.Forms.CheckedListBox();
            this.CheckCompounds1 = new System.Windows.Forms.CheckedListBox();
            this.zedBrakeFront = new ZedGraph.ZedGraphControl();
            this.zedBrakeRear = new ZedGraph.ZedGraphControl();
            this.btnRescale = new System.Windows.Forms.Button();
            this.lblFrontTireRim = new System.Windows.Forms.Label();
            this.lblRearTireRim = new System.Windows.Forms.Label();
            this.CheckCompounds5 = new System.Windows.Forms.CheckedListBox();
            this.lblDegActivation = new System.Windows.Forms.Label();
            this.lblDegHistoryStep = new System.Windows.Forms.Label();
            this.lblDegActivation2 = new System.Windows.Forms.Label();
            this.lblDegHistoryStep2 = new System.Windows.Forms.Label();
            this.lblDownshift = new System.Windows.Forms.Label();
            this.lblNumberGears = new System.Windows.Forms.Label();
            this.lblWheelRotation = new System.Windows.Forms.Label();
            this.lblWeightDistribution = new System.Windows.Forms.Label();
            this.lblDrag = new System.Windows.Forms.Label();
            this.lblWheels = new System.Windows.Forms.Label();
            this.lblUpshift = new System.Windows.Forms.Label();
            this.lblStarter = new System.Windows.Forms.Label();
            this.lblLimiter = new System.Windows.Forms.Label();
            this.lblFuel = new System.Windows.Forms.Label();
            this.lblCG = new System.Windows.Forms.Label();
            this.lblWeight = new System.Windows.Forms.Label();
            this.lblConcurrentDriver = new System.Windows.Forms.Label();
            this.lblDriverswap = new System.Windows.Forms.Label();
            this.lblSpringRubber = new System.Windows.Forms.Label();
            this.lblPressureAdjustment = new System.Windows.Forms.Label();
            this.lblTrackbar = new System.Windows.Forms.Label();
            this.lblRadiator = new System.Windows.Forms.Label();
            this.lblWedge = new System.Windows.Forms.Label();
            this.lblRearWing = new System.Windows.Forms.Label();
            this.lblFrontWing = new System.Windows.Forms.Label();
            this.lblConcurRepair = new System.Windows.Forms.Label();
            this.lblDamageDelay = new System.Windows.Forms.Label();
            this.lblDamageSusp = new System.Windows.Forms.Label();
            this.lblDamageAero = new System.Windows.Forms.Label();
            this.lblConcurrent = new System.Windows.Forms.Label();
            this.lblDelayTirechange = new System.Windows.Forms.Label();
            this.lblChange4tires = new System.Windows.Forms.Label();
            this.lblChange2tires = new System.Windows.Forms.Label();
            this.lblNozzleRemoval = new System.Windows.Forms.Label();
            this.lblNozzleInsertion = new System.Windows.Forms.Label();
            this.lblMaxFuelDelay = new System.Windows.Forms.Label();
            this.lblFuelFillRate = new System.Windows.Forms.Label();
            this.lblMaxDelay = new System.Windows.Forms.Label();
            this.lblDelayMulti = new System.Windows.Forms.Label();
            this.lblPreparation = new System.Windows.Forms.Label();
            this.lblGiveup = new System.Windows.Forms.Label();
            this.lblTireStartTemp = new System.Windows.Forms.Label();
            this.lblTireTemp = new System.Windows.Forms.Label();
            this.lblRearTireMass = new System.Windows.Forms.Label();
            this.lblFrontTireMass = new System.Windows.Forms.Label();
            this.txtCustomTemp = new System.Windows.Forms.TextBox();
            this.lblBrakeBias = new System.Windows.Forms.Label();
            this.lblRearBrakeFail = new System.Windows.Forms.Label();
            this.lblRearBrakeThick = new System.Windows.Forms.Label();
            this.lblRearBrakeTorque = new System.Windows.Forms.Label();
            this.lblRearBrakes = new System.Windows.Forms.Label();
            this.lblFrontBrakeFail = new System.Windows.Forms.Label();
            this.lblFrontBrakeThick = new System.Windows.Forms.Label();
            this.lblFrontBrakeTorque = new System.Windows.Forms.Label();
            this.lblFrontBrakes = new System.Windows.Forms.Label();
            this.lblAspiration = new System.Windows.Forms.Label();
            this.lblLifetimeVar = new System.Windows.Forms.Label();
            this.lblPowerWeight = new System.Windows.Forms.Label();
            this.lblPower = new System.Windows.Forms.Label();
            this.lblTorque = new System.Windows.Forms.Label();
            this.lblRevlimit = new System.Windows.Forms.Label();
            this.lblMaxrpm = new System.Windows.Forms.Label();
            this.lblLifetime = new System.Windows.Forms.Label();
            this.lblSafeOil = new System.Windows.Forms.Label();
            this.lblOptimal = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.btnGetUpgradeFile = new System.Windows.Forms.Button();
            this.CheckEngineFriction = new System.Windows.Forms.CheckBox();
            this.lblPressureChangePossible = new System.Windows.Forms.Label();
            this.lblAntiRoll = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.lblTrackbarFly = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.chkEngineMixture = new System.Windows.Forms.CheckBox();
            this.chkBoostMapping = new System.Windows.Forms.CheckBox();
            this.radbAspNaturally = new System.Windows.Forms.RadioButton();
            this.radbAspTurbo = new System.Windows.Forms.RadioButton();
            this.lblFuelConsumtion = new System.Windows.Forms.Label();
            this.lblEngineInertia = new System.Windows.Forms.Label();
            this.lblTurboPressure = new System.Windows.Forms.Label();
            this.lblSemiAuto = new System.Windows.Forms.Label();
            this.lblAntiStall = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.chkPitStopManualOverwrite = new System.Windows.Forms.CheckBox();
            this.lblPitTotal = new System.Windows.Forms.Label();
            this.lblPitMisc = new System.Windows.Forms.Label();
            this.lblPitDamage = new System.Windows.Forms.Label();
            this.lblPitTires = new System.Windows.Forms.Label();
            this.lblPitFuel = new System.Windows.Forms.Label();
            this.btnSimulatePitstop = new System.Windows.Forms.Button();
            this.chkDriverswap = new System.Windows.Forms.CheckBox();
            this.chkTrackbar = new System.Windows.Forms.CheckBox();
            this.chkSpringRubber = new System.Windows.Forms.CheckBox();
            this.chkWedge = new System.Windows.Forms.CheckBox();
            this.chkTirePressure = new System.Windows.Forms.CheckBox();
            this.chkRadiatorTap = new System.Windows.Forms.CheckBox();
            this.chkRearWing = new System.Windows.Forms.CheckBox();
            this.chkFrontWing = new System.Windows.Forms.CheckBox();
            this.chkSuspDamage = new System.Windows.Forms.CheckBox();
            this.chkAeroDamage = new System.Windows.Forms.CheckBox();
            this.dropChangeTires = new System.Windows.Forms.ComboBox();
            this.txtAmountFuel = new System.Windows.Forms.TextBox();
            this.lblTCLow = new System.Windows.Forms.Label();
            this.lblTCHigh = new System.Windows.Forms.Label();
            this.lblABSHigh = new System.Windows.Forms.Label();
            this.lblABSLow = new System.Windows.Forms.Label();
            this.lblSCLow = new System.Windows.Forms.Label();
            this.lblSCHigh = new System.Windows.Forms.Label();
            this.lblAutoShiftFull = new System.Windows.Forms.Label();
            this.lblAutoShiftOne = new System.Windows.Forms.Label();
            this.lblAutoPit = new System.Windows.Forms.Label();
            this.lblAutoBlip = new System.Windows.Forms.Label();
            this.lblAutoLift = new System.Windows.Forms.Label();
            this.lblFuelMaxPower = new System.Windows.Forms.Label();
            this.lblFuelMaxTorque = new System.Windows.Forms.Label();
            this.lblFuelMaxRPM = new System.Windows.Forms.Label();
            this.caricon = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.chkRealValues = new System.Windows.Forms.CheckBox();
            this.srDiffPreload = new Misc.SetupRow();
            this.srDiffCoast = new Misc.SetupRow();
            this.srDiffPower = new Misc.SetupRow();
            this.srDiffPump = new Misc.SetupRow();
            this.srEngineBreaking = new Misc.SetupRow();
            this.lblWearRPM = new System.Windows.Forms.Label();
            this.lblWearTemp = new System.Windows.Forms.Label();
            this.treeUpgrades = new Misc.MixedCheckBoxesTreeView();
            this.srFinalDrive = new CarStat_2.DataObjects.GearSetupRow();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.lblConcurrent2 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.labelRim2 = new System.Windows.Forms.Label();
            this.labelTire1Mass = new System.Windows.Forms.Label();
            this.labelTire2Mass = new System.Windows.Forms.Label();
            this.labelRim1 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label12347 = new System.Windows.Forms.Label();
            this.grpUpgrades = new System.Windows.Forms.GroupBox();
            this.label75 = new System.Windows.Forms.Label();
            this.tabPitstops = new System.Windows.Forms.TabPage();
            this.tabControlPitstop = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.grpPitcrew = new System.Windows.Forms.GroupBox();
            this.grpDamage = new System.Windows.Forms.GroupBox();
            this.grpTireChange = new System.Windows.Forms.GroupBox();
            this.label93 = new System.Windows.Forms.Label();
            this.grpAdjustments = new System.Windows.Forms.GroupBox();
            this.grpRefueling = new System.Windows.Forms.GroupBox();
            this.tabPitstopSim = new System.Windows.Forms.TabPage();
            this.grpPitResult = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label92 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.lblPitstopTime = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.grpCompound4 = new System.Windows.Forms.GroupBox();
            this.zedTire2 = new ZedGraph.ZedGraphControl();
            this.grpCompound3 = new System.Windows.Forms.GroupBox();
            this.zedTire = new ZedGraph.ZedGraphControl();
            this.zedTireTemps = new ZedGraph.ZedGraphControl();
            this.grpCompound2 = new System.Windows.Forms.GroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this.tabTires2 = new System.Windows.Forms.TabPage();
            this.tabTires = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label59 = new System.Windows.Forms.Label();
            this.zedWear = new ZedGraph.ZedGraphControl();
            this.grpCompound1 = new System.Windows.Forms.GroupBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.grpThermal1 = new System.Windows.Forms.GroupBox();
            this.zedThermal1 = new ZedGraph.ZedGraphControl();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.grpThermal2 = new System.Windows.Forms.GroupBox();
            this.lblInvalidTemp = new System.Windows.Forms.Label();
            this.CheckCompounds6 = new System.Windows.Forms.ListBox();
            this.zedThermal2 = new ZedGraph.ZedGraphControl();
            this.checkShowUpgrades = new System.Windows.Forms.CheckBox();
            this.tabBrakes = new System.Windows.Forms.TabPage();
            this.grpRearBrakes = new System.Windows.Forms.GroupBox();
            this.label74 = new System.Windows.Forms.Label();
            this.grpFrontBrakes = new System.Windows.Forms.GroupBox();
            this.label72 = new System.Windows.Forms.Label();
            this.tabEngine = new System.Windows.Forms.TabPage();
            this.tabEngineData = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label179 = new System.Windows.Forms.Label();
            this.label178 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.label106 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.cbBoostMapping = new System.Windows.Forms.ComboBox();
            this.cbFuelMixture = new System.Windows.Forms.ComboBox();
            this.lblTurbo = new System.Windows.Forms.Label();
            this.zedEngine = new ZedGraph.ZedGraphControl();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.tabControlGeneral = new System.Windows.Forms.TabControl();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.label94 = new System.Windows.Forms.Label();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.label98 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.lblLeftWheelBase = new System.Windows.Forms.Label();
            this.lblRearTrackWidth = new System.Windows.Forms.Label();
            this.lblRightWheelBase = new System.Windows.Forms.Label();
            this.lblFrontTrackWidth = new System.Windows.Forms.Label();
            this.tabPage24 = new System.Windows.Forms.TabPage();
            this.lblChassisINI = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.lblUpgradesINI = new System.Windows.Forms.Label();
            this.label185 = new System.Windows.Forms.Label();
            this.lblEngineINI = new System.Windows.Forms.Label();
            this.lblTBC = new System.Windows.Forms.Label();
            this.lblVEH = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.lblHDV = new System.Windows.Forms.Label();
            this.label192 = new System.Windows.Forms.Label();
            this.label193 = new System.Windows.Forms.Label();
            this.label194 = new System.Windows.Forms.Label();
            this.label195 = new System.Windows.Forms.Label();
            this.grpTeam = new System.Windows.Forms.GroupBox();
            this.lblClass = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.lblCategory = new System.Windows.Forms.Label();
            this.lblWins = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.lblPoles = new System.Windows.Forms.Label();
            this.lblStarts = new System.Windows.Forms.Label();
            this.lblHQ = new System.Windows.Forms.Label();
            this.lblFullTeam = new System.Windows.Forms.Label();
            this.lblFounded = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblManufacturer = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblEngine = new System.Windows.Forms.Label();
            this.lblDriver = new System.Windows.Forms.Label();
            this.lblTeam = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblNumber = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabs = new System.Windows.Forms.TabControl();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.tabSetupControl = new System.Windows.Forms.TabControl();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.lblPitStop3 = new System.Windows.Forms.Label();
            this.lblPitStop2 = new System.Windows.Forms.Label();
            this.lblPitStop1 = new System.Windows.Forms.Label();
            this.srPitStop3 = new Misc.SetupRow();
            this.srPitStop2 = new Misc.SetupRow();
            this.srPitStop1 = new Misc.SetupRow();
            this.label109 = new System.Windows.Forms.Label();
            this.srPitStops = new Misc.SetupRow();
            this.label107 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbSetupNotes = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnLoadSetup = new System.Windows.Forms.Button();
            this.btnSaveSetup = new System.Windows.Forms.Button();
            this.btnDefaultSetup = new System.Windows.Forms.Button();
            this.srFuel = new Misc.SetupRow();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.tabControlDriveline = new System.Windows.Forms.TabControl();
            this.tabPage21 = new System.Windows.Forms.TabPage();
            this.btnRescaleSpeedGraph = new System.Windows.Forms.Button();
            this.zedGearboxGraph = new ZedGraph.ZedGraphControl();
            this.tabPage22 = new System.Windows.Forms.TabPage();
            this.lblTopSpeed = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.lblGearR = new System.Windows.Forms.Label();
            this.lblGear9 = new System.Windows.Forms.Label();
            this.lblGear2 = new System.Windows.Forms.Label();
            this.lblGear3 = new System.Windows.Forms.Label();
            this.lblGear4 = new System.Windows.Forms.Label();
            this.lblGear5 = new System.Windows.Forms.Label();
            this.lblGear6 = new System.Windows.Forms.Label();
            this.lblGear7 = new System.Windows.Forms.Label();
            this.lblGear8 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.lblGear1 = new System.Windows.Forms.Label();
            this.srGearR = new CarStat_2.DataObjects.GearSetupRow();
            this.srGear9 = new CarStat_2.DataObjects.GearSetupRow();
            this.srGear2 = new CarStat_2.DataObjects.GearSetupRow();
            this.srGear3 = new CarStat_2.DataObjects.GearSetupRow();
            this.srGear4 = new CarStat_2.DataObjects.GearSetupRow();
            this.srGear5 = new CarStat_2.DataObjects.GearSetupRow();
            this.srGear6 = new CarStat_2.DataObjects.GearSetupRow();
            this.srGear7 = new CarStat_2.DataObjects.GearSetupRow();
            this.srGear8 = new CarStat_2.DataObjects.GearSetupRow();
            this.srGear1 = new CarStat_2.DataObjects.GearSetupRow();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.chkSymmetric = new System.Windows.Forms.CheckBox();
            this.label127 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.lblFToeOffset = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.lblFThirdSpring = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.srFRCaster = new Misc.SetupRow();
            this.srFRCamber = new Misc.SetupRow();
            this.srFLCaster = new Misc.SetupRow();
            this.srFLCamber = new Misc.SetupRow();
            this.srFToeOffset = new Misc.SetupRow();
            this.srFToeIn = new Misc.SetupRow();
            this.srFARB = new Misc.SetupRow();
            this.srFRRideHeight = new Misc.SetupRow();
            this.srFRPacker = new Misc.SetupRow();
            this.srFRFastRebound = new Misc.SetupRow();
            this.srFRFastBump = new Misc.SetupRow();
            this.srFRSlowRebound = new Misc.SetupRow();
            this.srFRSlowBump = new Misc.SetupRow();
            this.srFRSpring = new Misc.SetupRow();
            this.srFPacker = new Misc.SetupRow();
            this.srFFastRebound = new Misc.SetupRow();
            this.srFFastBump = new Misc.SetupRow();
            this.srFSlowRebound = new Misc.SetupRow();
            this.srFSlowBump = new Misc.SetupRow();
            this.srFSpring = new Misc.SetupRow();
            this.srFLRideHeight = new Misc.SetupRow();
            this.srFLPacker = new Misc.SetupRow();
            this.srFLFastRebound = new Misc.SetupRow();
            this.srFLFastBump = new Misc.SetupRow();
            this.srFLSlowRebound = new Misc.SetupRow();
            this.srFLSlowBump = new Misc.SetupRow();
            this.srFLSpring = new Misc.SetupRow();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.chkSymmetric01 = new System.Windows.Forms.CheckBox();
            this.lblSetupTrackbar = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.lblRToeOffset = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.lblRThirdSpring = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.srRRRideHeight = new Misc.SetupRow();
            this.srRRPacker = new Misc.SetupRow();
            this.srRRFastRebound = new Misc.SetupRow();
            this.srRRFastBump = new Misc.SetupRow();
            this.srRRSlowRebound = new Misc.SetupRow();
            this.srRRSlowBump = new Misc.SetupRow();
            this.srRRCamber = new Misc.SetupRow();
            this.srRRTrackbar = new Misc.SetupRow();
            this.srRRSpring = new Misc.SetupRow();
            this.srRPacker = new Misc.SetupRow();
            this.srRFastRebound = new Misc.SetupRow();
            this.srRFastBump = new Misc.SetupRow();
            this.srRSlowRebound = new Misc.SetupRow();
            this.srRSlowBump = new Misc.SetupRow();
            this.srRSpring = new Misc.SetupRow();
            this.srRLRideHeight = new Misc.SetupRow();
            this.srRLPacker = new Misc.SetupRow();
            this.srRLFastRebound = new Misc.SetupRow();
            this.srRLFastBump = new Misc.SetupRow();
            this.srRLSlowRebound = new Misc.SetupRow();
            this.srRLSlowBump = new Misc.SetupRow();
            this.srRLSpring = new Misc.SetupRow();
            this.srRLTrackbar = new Misc.SetupRow();
            this.srRLCamber = new Misc.SetupRow();
            this.srRToeOffset = new Misc.SetupRow();
            this.srRToeIn = new Misc.SetupRow();
            this.srRARB = new Misc.SetupRow();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this.gbSpringRubber = new System.Windows.Forms.GroupBox();
            this.label145 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.srSpringRubberRL = new Misc.SetupRow();
            this.srSpringRubberRR = new Misc.SetupRow();
            this.srSpringRubberFL = new Misc.SetupRow();
            this.label143 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.srSpringRubberFR = new Misc.SetupRow();
            this.gbChassisAdj = new System.Windows.Forms.GroupBox();
            this.srChassisAdj00 = new Misc.SetupRow();
            this.lblChassisAdj00 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.srWeightLong = new CarStat_2.DataObjects.BalanceSetupRow();
            this.srWeightLateral = new CarStat_2.DataObjects.BalanceSetupRow();
            this.srWedge = new Misc.SetupRow();
            this.label141 = new System.Windows.Forms.Label();
            this.srWeightHeight = new Misc.SetupRow();
            this.label131 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.srTorqueSplit = new CarStat_2.DataObjects.BalanceSetupRow();
            this.srBrakeBais = new CarStat_2.DataObjects.BalanceSetupRow();
            this.lblTorqueSplit = new System.Windows.Forms.Label();
            this.srHandBrakePressure = new Misc.SetupRow();
            this.srSteeringLock = new Misc.SetupRow();
            this.lblHandBrakePressure = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.srBrakePressure = new Misc.SetupRow();
            this.srRRDisc = new Misc.SetupRow();
            this.srRRPressure = new Misc.SetupRow();
            this.srRLDisc = new Misc.SetupRow();
            this.srRCompound = new Misc.SetupRow();
            this.srRLPressure = new Misc.SetupRow();
            this.srFRDisc = new Misc.SetupRow();
            this.srFRPressure = new Misc.SetupRow();
            this.srFLDisc = new Misc.SetupRow();
            this.srFCompound = new Misc.SetupRow();
            this.srFLPressure = new Misc.SetupRow();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label183 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.tbAeroSpeed = new System.Windows.Forms.TextBox();
            this.label181 = new System.Windows.Forms.Label();
            this.lblAeroDragFactor = new System.Windows.Forms.Label();
            this.label182 = new System.Windows.Forms.Label();
            this.lblRearLift = new System.Windows.Forms.Label();
            this.lblFrontDownforce = new System.Windows.Forms.Label();
            this.lblFrontLift = new System.Windows.Forms.Label();
            this.lblRearDownforce = new System.Windows.Forms.Label();
            this.lblAeroDragForce = new System.Windows.Forms.Label();
            this.gbFender = new System.Windows.Forms.GroupBox();
            this.label168 = new System.Windows.Forms.Label();
            this.srLFender = new Misc.SetupRow();
            this.srRFender = new Misc.SetupRow();
            this.label169 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label166 = new System.Windows.Forms.Label();
            this.srFWing = new Misc.SetupRow();
            this.srRWing = new Misc.SetupRow();
            this.label167 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.srRadiator = new Misc.SetupRow();
            this.srBrakeDucts = new Misc.SetupRow();
            this.label170 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.tabPage23 = new System.Windows.Forms.TabPage();
            this.gbAssists = new System.Windows.Forms.GroupBox();
            this.srTC = new Misc.SetupRow();
            this.label176 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.srABS = new Misc.SetupRow();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label172 = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.srRevlimit = new Misc.SetupRow();
            this.label174 = new System.Windows.Forms.Label();
            this.srBoost = new Misc.SetupRow();
            this.label173 = new System.Windows.Forms.Label();
            this.srEngineMixture = new Misc.SetupRow();
            this.btnMakeMainWindow = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.caricon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.grpUpgrades.SuspendLayout();
            this.tabPitstops.SuspendLayout();
            this.tabControlPitstop.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.grpPitcrew.SuspendLayout();
            this.grpDamage.SuspendLayout();
            this.grpTireChange.SuspendLayout();
            this.grpAdjustments.SuspendLayout();
            this.grpRefueling.SuspendLayout();
            this.tabPitstopSim.SuspendLayout();
            this.grpPitResult.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.grpCompound4.SuspendLayout();
            this.grpCompound3.SuspendLayout();
            this.grpCompound2.SuspendLayout();
            this.tabTires2.SuspendLayout();
            this.tabTires.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.grpCompound1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.grpThermal1.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.grpThermal2.SuspendLayout();
            this.tabBrakes.SuspendLayout();
            this.grpRearBrakes.SuspendLayout();
            this.grpFrontBrakes.SuspendLayout();
            this.tabEngine.SuspendLayout();
            this.tabEngineData.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            this.tabControlGeneral.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage13.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.tabPage24.SuspendLayout();
            this.grpTeam.SuspendLayout();
            this.tabs.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.tabSetupControl.SuspendLayout();
            this.tabPage14.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage15.SuspendLayout();
            this.tabControlDriveline.SuspendLayout();
            this.tabPage21.SuspendLayout();
            this.tabPage22.SuspendLayout();
            this.tabPage16.SuspendLayout();
            this.tabPage17.SuspendLayout();
            this.tabPage18.SuspendLayout();
            this.gbSpringRubber.SuspendLayout();
            this.gbChassisAdj.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabPage19.SuspendLayout();
            this.tabPage20.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.gbFender.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabPage23.SuspendLayout();
            this.gbAssists.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtUpgradeDescription
            // 
            this.txtUpgradeDescription.Enabled = false;
            this.txtUpgradeDescription.Location = new System.Drawing.Point(374, 38);
            this.txtUpgradeDescription.Multiline = true;
            this.txtUpgradeDescription.Name = "txtUpgradeDescription";
            this.txtUpgradeDescription.Size = new System.Drawing.Size(280, 88);
            this.txtUpgradeDescription.TabIndex = 43;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Location = new System.Drawing.Point(0, 24);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(698, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuStrip2
            // 
            this.menuStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.openOptionsMenu,
            this.openSetupsManager,
            this.helpToolStripMenuItem,
            this.btnMakeMainWindow});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(698, 24);
            this.menuStrip2.TabIndex = 2;
            this.menuStrip2.TabStop = true;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openVEHicleFileToolStripMenuItem,
            this.openVehicleSelectorToolStripMenuItem,
            this.openTBCFileToolStripMenuItem,
            this.openEngineFileToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.ShortcutKeyDisplayString = "File";
            this.fileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F)));
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openVEHicleFileToolStripMenuItem
            // 
            this.openVEHicleFileToolStripMenuItem.Name = "openVEHicleFileToolStripMenuItem";
            this.openVEHicleFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.V)));
            this.openVEHicleFileToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.openVEHicleFileToolStripMenuItem.Text = "Open &VEHicle file";
            this.openVEHicleFileToolStripMenuItem.Click += new System.EventHandler(this.openVEHicleFileToolStripMenuItem_Click);
            // 
            // openVehicleSelectorToolStripMenuItem
            // 
            this.openVehicleSelectorToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.openVehicleSelectorToolStripMenuItem.Name = "openVehicleSelectorToolStripMenuItem";
            this.openVehicleSelectorToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.openVehicleSelectorToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.openVehicleSelectorToolStripMenuItem.Text = "Open Vehicle &Selector";
            this.openVehicleSelectorToolStripMenuItem.Click += new System.EventHandler(this.openVehicleSelectorToolStripMenuItem_Click);
            // 
            // openTBCFileToolStripMenuItem
            // 
            this.openTBCFileToolStripMenuItem.Name = "openTBCFileToolStripMenuItem";
            this.openTBCFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.T)));
            this.openTBCFileToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.openTBCFileToolStripMenuItem.Text = "Open &TBC file";
            this.openTBCFileToolStripMenuItem.Click += new System.EventHandler(this.openTBCFileToolStripMenuItem_Click);
            // 
            // openEngineFileToolStripMenuItem
            // 
            this.openEngineFileToolStripMenuItem.Name = "openEngineFileToolStripMenuItem";
            this.openEngineFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.E)));
            this.openEngineFileToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.openEngineFileToolStripMenuItem.Text = "Open Engine File";
            this.openEngineFileToolStripMenuItem.Click += new System.EventHandler(this.openEngineFileToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // openOptionsMenu
            // 
            this.openOptionsMenu.Name = "openOptionsMenu";
            this.openOptionsMenu.Size = new System.Drawing.Size(61, 20);
            this.openOptionsMenu.Text = "Options";
            this.openOptionsMenu.Click += new System.EventHandler(this.openOptionsMenu_Click);
            // 
            // openSetupsManager
            // 
            this.openSetupsManager.Name = "openSetupsManager";
            this.openSetupsManager.Size = new System.Drawing.Size(99, 20);
            this.openSetupsManager.Text = "Setup Manager";
            this.openSetupsManager.Click += new System.EventHandler(this.openSetupsManager_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.helpToolStripMenuItem1});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.helpToolStripMenuItem1.Text = "Help!";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem1_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 20000;
            this.toolTip1.InitialDelay = 0;
            this.toolTip1.ReshowDelay = 100;
            // 
            // CheckCompounds4
            // 
            this.CheckCompounds4.CheckOnClick = true;
            this.CheckCompounds4.FormattingEnabled = true;
            this.CheckCompounds4.Location = new System.Drawing.Point(6, 19);
            this.CheckCompounds4.Name = "CheckCompounds4";
            this.CheckCompounds4.Size = new System.Drawing.Size(183, 52);
            this.CheckCompounds4.TabIndex = 36;
            this.toolTip1.SetToolTip(this.CheckCompounds4, "Select the compounds you want to see in the graph.");
            this.CheckCompounds4.SelectedIndexChanged += new System.EventHandler(this.CheckCompounds_SelectedIndexChanged);
            // 
            // CheckCompounds3
            // 
            this.CheckCompounds3.CheckOnClick = true;
            this.CheckCompounds3.FormattingEnabled = true;
            this.CheckCompounds3.Location = new System.Drawing.Point(6, 19);
            this.CheckCompounds3.Name = "CheckCompounds3";
            this.CheckCompounds3.Size = new System.Drawing.Size(183, 52);
            this.CheckCompounds3.TabIndex = 34;
            this.toolTip1.SetToolTip(this.CheckCompounds3, "Check the compounds you want to see in the graphs.\r\nThe values you see beneath th" +
        "is are the values of the currently selected compound.");
            this.CheckCompounds3.SelectedIndexChanged += new System.EventHandler(this.CheckCompounds_SelectedIndexChanged);
            // 
            // CheckCompounds2
            // 
            this.CheckCompounds2.FormattingEnabled = true;
            this.CheckCompounds2.Location = new System.Drawing.Point(6, 19);
            this.CheckCompounds2.Name = "CheckCompounds2";
            this.CheckCompounds2.Size = new System.Drawing.Size(183, 52);
            this.CheckCompounds2.TabIndex = 34;
            this.toolTip1.SetToolTip(this.CheckCompounds2, "Check the compounds you want to see in the graphs.\r\nThe values you see beneath th" +
        "is are the values of the currently selected compound.");
            this.CheckCompounds2.SelectedIndexChanged += new System.EventHandler(this.CheckCompounds_SelectedIndexChanged);
            // 
            // CheckCompounds1
            // 
            this.CheckCompounds1.FormattingEnabled = true;
            this.CheckCompounds1.Location = new System.Drawing.Point(6, 19);
            this.CheckCompounds1.Name = "CheckCompounds1";
            this.CheckCompounds1.Size = new System.Drawing.Size(183, 52);
            this.CheckCompounds1.TabIndex = 36;
            this.toolTip1.SetToolTip(this.CheckCompounds1, "Select the compounds you want to see in the graph.");
            this.CheckCompounds1.SelectedIndexChanged += new System.EventHandler(this.CheckCompounds_SelectedIndexChanged);
            // 
            // zedBrakeFront
            // 
            this.zedBrakeFront.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.zedBrakeFront.IsAntiAlias = true;
            this.zedBrakeFront.IsEnableVPan = false;
            this.zedBrakeFront.IsEnableVZoom = false;
            this.zedBrakeFront.Location = new System.Drawing.Point(252, 41);
            this.zedBrakeFront.Margin = new System.Windows.Forms.Padding(4);
            this.zedBrakeFront.Name = "zedBrakeFront";
            this.zedBrakeFront.ScrollGrace = 0D;
            this.zedBrakeFront.ScrollMaxX = 0D;
            this.zedBrakeFront.ScrollMaxY = 0D;
            this.zedBrakeFront.ScrollMaxY2 = 0D;
            this.zedBrakeFront.ScrollMinX = 0D;
            this.zedBrakeFront.ScrollMinY = 0D;
            this.zedBrakeFront.ScrollMinY2 = 0D;
            this.zedBrakeFront.Size = new System.Drawing.Size(374, 47);
            this.zedBrakeFront.TabIndex = 41;
            this.toolTip1.SetToolTip(this.zedBrakeFront, "This graph shows the optimum brake temperatures.\r\nThe graph starts at the cold te" +
        "mperature where the braking torque\r\nis halved and ends where it is halved due to" +
        " overheating.");
            // 
            // zedBrakeRear
            // 
            this.zedBrakeRear.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.zedBrakeRear.IsAntiAlias = true;
            this.zedBrakeRear.IsEnableVPan = false;
            this.zedBrakeRear.IsEnableVZoom = false;
            this.zedBrakeRear.Location = new System.Drawing.Point(253, 40);
            this.zedBrakeRear.Margin = new System.Windows.Forms.Padding(4);
            this.zedBrakeRear.Name = "zedBrakeRear";
            this.zedBrakeRear.ScrollGrace = 0D;
            this.zedBrakeRear.ScrollMaxX = 0D;
            this.zedBrakeRear.ScrollMaxY = 0D;
            this.zedBrakeRear.ScrollMaxY2 = 0D;
            this.zedBrakeRear.ScrollMinX = 0D;
            this.zedBrakeRear.ScrollMinY = 0D;
            this.zedBrakeRear.ScrollMinY2 = 0D;
            this.zedBrakeRear.Size = new System.Drawing.Size(374, 47);
            this.zedBrakeRear.TabIndex = 43;
            this.toolTip1.SetToolTip(this.zedBrakeRear, "This graph shows the optimum brake temperatures.\r\nThe graph starts at the cold te" +
        "mperature where the braking torque\r\nis halved and ends where it is halved due to" +
        " overheating.");
            // 
            // btnRescale
            // 
            this.btnRescale.Font = new System.Drawing.Font("Tahoma", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRescale.Location = new System.Drawing.Point(217, 282);
            this.btnRescale.Name = "btnRescale";
            this.btnRescale.Size = new System.Drawing.Size(47, 23);
            this.btnRescale.TabIndex = 2;
            this.btnRescale.Text = "Rescale";
            this.toolTip1.SetToolTip(this.btnRescale, "Rescale the Engine graph to the current engine update.");
            this.btnRescale.UseVisualStyleBackColor = true;
            this.btnRescale.Click += new System.EventHandler(this.btnRescale_Click);
            // 
            // lblFrontTireRim
            // 
            this.lblFrontTireRim.AutoSize = true;
            this.lblFrontTireRim.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrontTireRim.Location = new System.Drawing.Point(111, 137);
            this.lblFrontTireRim.Name = "lblFrontTireRim";
            this.lblFrontTireRim.Size = new System.Drawing.Size(51, 13);
            this.lblFrontTireRim.TabIndex = 54;
            this.lblFrontTireRim.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblFrontTireRim, "The diameter of this sides rims (not the entire wheel!).");
            // 
            // lblRearTireRim
            // 
            this.lblRearTireRim.AutoSize = true;
            this.lblRearTireRim.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRearTireRim.Location = new System.Drawing.Point(111, 150);
            this.lblRearTireRim.Name = "lblRearTireRim";
            this.lblRearTireRim.Size = new System.Drawing.Size(51, 13);
            this.lblRearTireRim.TabIndex = 48;
            this.lblRearTireRim.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblRearTireRim, "The diameter of this side rims (not the entire wheel!).");
            // 
            // CheckCompounds5
            // 
            this.CheckCompounds5.FormattingEnabled = true;
            this.CheckCompounds5.Location = new System.Drawing.Point(6, 19);
            this.CheckCompounds5.Name = "CheckCompounds5";
            this.CheckCompounds5.Size = new System.Drawing.Size(183, 52);
            this.CheckCompounds5.TabIndex = 37;
            this.toolTip1.SetToolTip(this.CheckCompounds5, "Select the compounds you want to see in the graph.");
            this.CheckCompounds5.SelectedIndexChanged += new System.EventHandler(this.CheckCompounds_SelectedIndexChanged);
            // 
            // lblDegActivation
            // 
            this.lblDegActivation.AutoSize = true;
            this.lblDegActivation.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDegActivation.Location = new System.Drawing.Point(111, 137);
            this.lblDegActivation.Name = "lblDegActivation";
            this.lblDegActivation.Size = new System.Drawing.Size(51, 13);
            this.lblDegActivation.TabIndex = 54;
            this.lblDegActivation.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblDegActivation, "The temperature above which the tires suffer from thermal degradation.");
            // 
            // lblDegHistoryStep
            // 
            this.lblDegHistoryStep.AutoSize = true;
            this.lblDegHistoryStep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDegHistoryStep.Location = new System.Drawing.Point(111, 150);
            this.lblDegHistoryStep.Name = "lblDegHistoryStep";
            this.lblDegHistoryStep.Size = new System.Drawing.Size(51, 13);
            this.lblDegHistoryStep.TabIndex = 48;
            this.lblDegHistoryStep.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblDegHistoryStep, resources.GetString("lblDegHistoryStep.ToolTip"));
            // 
            // lblDegActivation2
            // 
            this.lblDegActivation2.AutoSize = true;
            this.lblDegActivation2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDegActivation2.Location = new System.Drawing.Point(111, 137);
            this.lblDegActivation2.Name = "lblDegActivation2";
            this.lblDegActivation2.Size = new System.Drawing.Size(51, 13);
            this.lblDegActivation2.TabIndex = 54;
            this.lblDegActivation2.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblDegActivation2, "The temperature above which the tires suffer from thermal degradation.");
            // 
            // lblDegHistoryStep2
            // 
            this.lblDegHistoryStep2.AutoSize = true;
            this.lblDegHistoryStep2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDegHistoryStep2.Location = new System.Drawing.Point(111, 150);
            this.lblDegHistoryStep2.Name = "lblDegHistoryStep2";
            this.lblDegHistoryStep2.Size = new System.Drawing.Size(51, 13);
            this.lblDegHistoryStep2.TabIndex = 48;
            this.lblDegHistoryStep2.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblDegHistoryStep2, resources.GetString("lblDegHistoryStep2.ToolTip"));
            // 
            // lblDownshift
            // 
            this.lblDownshift.AutoSize = true;
            this.lblDownshift.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDownshift.Location = new System.Drawing.Point(119, 106);
            this.lblDownshift.Name = "lblDownshift";
            this.lblDownshift.Size = new System.Drawing.Size(51, 13);
            this.lblDownshift.TabIndex = 33;
            this.lblDownshift.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblDownshift, "The delay when downshifting with AutoClutch enabled. \r\nThis is the full delay bet" +
        "ween engaging the clutch, shifting and disengaging the clutch again. \r\nIn millis" +
        "econds.");
            // 
            // lblNumberGears
            // 
            this.lblNumberGears.AutoSize = true;
            this.lblNumberGears.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumberGears.Location = new System.Drawing.Point(119, 79);
            this.lblNumberGears.Name = "lblNumberGears";
            this.lblNumberGears.Size = new System.Drawing.Size(51, 13);
            this.lblNumberGears.TabIndex = 31;
            this.lblNumberGears.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblNumberGears, "The number of forward gears the car\'s transmission has.");
            // 
            // lblWheelRotation
            // 
            this.lblWheelRotation.AutoSize = true;
            this.lblWheelRotation.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWheelRotation.Location = new System.Drawing.Point(119, 212);
            this.lblWheelRotation.Name = "lblWheelRotation";
            this.lblWheelRotation.Size = new System.Drawing.Size(51, 13);
            this.lblWheelRotation.TabIndex = 29;
            this.lblWheelRotation.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblWheelRotation, "The lock-to-lock steering degrees of the wheel.");
            // 
            // lblWeightDistribution
            // 
            this.lblWeightDistribution.AutoSize = true;
            this.lblWeightDistribution.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeightDistribution.Location = new System.Drawing.Point(119, 39);
            this.lblWeightDistribution.Name = "lblWeightDistribution";
            this.lblWeightDistribution.Size = new System.Drawing.Size(51, 13);
            this.lblWeightDistribution.TabIndex = 27;
            this.lblWeightDistribution.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblWeightDistribution, "The weight distribution of the car: how many percent of the total weight is on th" +
        "e front and on the rear tires.");
            // 
            // lblDrag
            // 
            this.lblDrag.AutoSize = true;
            this.lblDrag.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDrag.Location = new System.Drawing.Point(119, 199);
            this.lblDrag.Name = "lblDrag";
            this.lblDrag.Size = new System.Drawing.Size(51, 13);
            this.lblDrag.TabIndex = 25;
            this.lblDrag.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblDrag, resources.GetString("lblDrag.ToolTip"));
            // 
            // lblWheels
            // 
            this.lblWheels.AutoSize = true;
            this.lblWheels.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWheels.Location = new System.Drawing.Point(119, 119);
            this.lblWheels.Name = "lblWheels";
            this.lblWheels.Size = new System.Drawing.Size(51, 13);
            this.lblWheels.TabIndex = 21;
            this.lblWheels.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblWheels, "The driven wheels; FWD, RWD or AWD.");
            // 
            // lblUpshift
            // 
            this.lblUpshift.AutoSize = true;
            this.lblUpshift.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpshift.Location = new System.Drawing.Point(119, 93);
            this.lblUpshift.Name = "lblUpshift";
            this.lblUpshift.Size = new System.Drawing.Size(51, 13);
            this.lblUpshift.TabIndex = 19;
            this.lblUpshift.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblUpshift, "The delay when upshifting with AutoClutch enabled. \r\nThis is the full delay betwe" +
        "en engaging the clutch, shifting and disengaging the clutch again. \r\nIn millisec" +
        "onds.");
            // 
            // lblStarter
            // 
            this.lblStarter.AutoSize = true;
            this.lblStarter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStarter.Location = new System.Drawing.Point(119, 146);
            this.lblStarter.Name = "lblStarter";
            this.lblStarter.Size = new System.Drawing.Size(51, 13);
            this.lblStarter.TabIndex = 17;
            this.lblStarter.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblStarter, "Whether or not this car has an onboard starter. \r\nIf it doesn\'t have one and you " +
        "stall the car on track, it\'s game over.");
            // 
            // lblLimiter
            // 
            this.lblLimiter.AutoSize = true;
            this.lblLimiter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLimiter.Location = new System.Drawing.Point(119, 133);
            this.lblLimiter.Name = "lblLimiter";
            this.lblLimiter.Size = new System.Drawing.Size(51, 13);
            this.lblLimiter.TabIndex = 16;
            this.lblLimiter.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblLimiter, "Whether or not this car has a speed limiter.");
            // 
            // lblFuel
            // 
            this.lblFuel.AutoSize = true;
            this.lblFuel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuel.Location = new System.Drawing.Point(119, 52);
            this.lblFuel.Name = "lblFuel";
            this.lblFuel.Size = new System.Drawing.Size(51, 13);
            this.lblFuel.TabIndex = 8;
            this.lblFuel.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblFuel, "Fuel tank capacity.");
            // 
            // lblCG
            // 
            this.lblCG.AutoSize = true;
            this.lblCG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCG.Location = new System.Drawing.Point(119, 26);
            this.lblCG.Name = "lblCG";
            this.lblCG.Size = new System.Drawing.Size(51, 13);
            this.lblCG.TabIndex = 6;
            this.lblCG.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblCG, "Vertical positon of the Centre of Grativity relative to the base of the car. \r\nTh" +
        "e lower, the better.");
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWeight.Location = new System.Drawing.Point(119, 13);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(51, 13);
            this.lblWeight.TabIndex = 5;
            this.lblWeight.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblWeight, "The weight of the car including the driver, but without any fuel.");
            // 
            // lblConcurrentDriver
            // 
            this.lblConcurrentDriver.AutoSize = true;
            this.lblConcurrentDriver.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConcurrentDriver.Location = new System.Drawing.Point(150, 125);
            this.lblConcurrentDriver.Name = "lblConcurrentDriver";
            this.lblConcurrentDriver.Size = new System.Drawing.Size(51, 13);
            this.lblConcurrentDriver.TabIndex = 31;
            this.lblConcurrentDriver.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblConcurrentDriver, "Whether or not driver swaps can happen while \r\nrefueling or changing tires.");
            // 
            // lblDriverswap
            // 
            this.lblDriverswap.AutoSize = true;
            this.lblDriverswap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDriverswap.Location = new System.Drawing.Point(150, 112);
            this.lblDriverswap.Name = "lblDriverswap";
            this.lblDriverswap.Size = new System.Drawing.Size(51, 13);
            this.lblDriverswap.TabIndex = 27;
            this.lblDriverswap.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblDriverswap, "The time it takes to do a driver swap (if it is allowed).");
            // 
            // lblSpringRubber
            // 
            this.lblSpringRubber.AutoSize = true;
            this.lblSpringRubber.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSpringRubber.Location = new System.Drawing.Point(150, 99);
            this.lblSpringRubber.Name = "lblSpringRubber";
            this.lblSpringRubber.Size = new System.Drawing.Size(51, 13);
            this.lblSpringRubber.TabIndex = 25;
            this.lblSpringRubber.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblSpringRubber, "The time it takes to make a spring rubber adjustment (if it is allowed).");
            // 
            // lblPressureAdjustment
            // 
            this.lblPressureAdjustment.AutoSize = true;
            this.lblPressureAdjustment.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPressureAdjustment.Location = new System.Drawing.Point(150, 85);
            this.lblPressureAdjustment.Name = "lblPressureAdjustment";
            this.lblPressureAdjustment.Size = new System.Drawing.Size(51, 13);
            this.lblPressureAdjustment.TabIndex = 23;
            this.lblPressureAdjustment.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblPressureAdjustment, "The time it takes to make a tire pressure adjustment without changing tires (if i" +
        "t is allowed).");
            // 
            // lblTrackbar
            // 
            this.lblTrackbar.AutoSize = true;
            this.lblTrackbar.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrackbar.Location = new System.Drawing.Point(150, 73);
            this.lblTrackbar.Name = "lblTrackbar";
            this.lblTrackbar.Size = new System.Drawing.Size(51, 13);
            this.lblTrackbar.TabIndex = 21;
            this.lblTrackbar.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblTrackbar, "The time it takes to make a track bar adjustment (if it is allowed).");
            // 
            // lblRadiator
            // 
            this.lblRadiator.AutoSize = true;
            this.lblRadiator.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRadiator.Location = new System.Drawing.Point(150, 60);
            this.lblRadiator.Name = "lblRadiator";
            this.lblRadiator.Size = new System.Drawing.Size(51, 13);
            this.lblRadiator.TabIndex = 19;
            this.lblRadiator.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblRadiator, "The time it takes to make a radiator adjustment (if it is allowed).");
            // 
            // lblWedge
            // 
            this.lblWedge.AutoSize = true;
            this.lblWedge.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWedge.Location = new System.Drawing.Point(150, 47);
            this.lblWedge.Name = "lblWedge";
            this.lblWedge.Size = new System.Drawing.Size(51, 13);
            this.lblWedge.TabIndex = 17;
            this.lblWedge.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblWedge, "The time it takes to make a wedge adjustment (if it is allowed).");
            // 
            // lblRearWing
            // 
            this.lblRearWing.AutoSize = true;
            this.lblRearWing.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRearWing.Location = new System.Drawing.Point(150, 34);
            this.lblRearWing.Name = "lblRearWing";
            this.lblRearWing.Size = new System.Drawing.Size(51, 13);
            this.lblRearWing.TabIndex = 15;
            this.lblRearWing.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblRearWing, "The time it takes to make a rear wing adjustment (if it is allowed).");
            // 
            // lblFrontWing
            // 
            this.lblFrontWing.AutoSize = true;
            this.lblFrontWing.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrontWing.Location = new System.Drawing.Point(150, 21);
            this.lblFrontWing.Name = "lblFrontWing";
            this.lblFrontWing.Size = new System.Drawing.Size(51, 13);
            this.lblFrontWing.TabIndex = 13;
            this.lblFrontWing.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblFrontWing, "The time it takes to make a front wing adjustment (if it is allowed).");
            // 
            // lblConcurRepair
            // 
            this.lblConcurRepair.AutoSize = true;
            this.lblConcurRepair.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConcurRepair.Location = new System.Drawing.Point(150, 55);
            this.lblConcurRepair.Name = "lblConcurRepair";
            this.lblConcurRepair.Size = new System.Drawing.Size(51, 13);
            this.lblConcurRepair.TabIndex = 16;
            this.lblConcurRepair.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblConcurRepair, "Whether or not the car can be repaired simultaneously with\r\nrefueling and/or tire" +
        " changes.");
            // 
            // lblDamageDelay
            // 
            this.lblDamageDelay.AutoSize = true;
            this.lblDamageDelay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDamageDelay.Location = new System.Drawing.Point(150, 42);
            this.lblDamageDelay.Name = "lblDamageDelay";
            this.lblDamageDelay.Size = new System.Drawing.Size(51, 13);
            this.lblDamageDelay.TabIndex = 14;
            this.lblDamageDelay.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblDamageDelay, "The maximum random delay when repairing your car.\r\nIf you\'re very lucky this is 0" +
        ", when you\'re unlucky this is the maximum.");
            // 
            // lblDamageSusp
            // 
            this.lblDamageSusp.AutoSize = true;
            this.lblDamageSusp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDamageSusp.Location = new System.Drawing.Point(150, 29);
            this.lblDamageSusp.Name = "lblDamageSusp";
            this.lblDamageSusp.Size = new System.Drawing.Size(51, 13);
            this.lblDamageSusp.TabIndex = 12;
            this.lblDamageSusp.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblDamageSusp, "The time it takes to repair suspension damage. This includes\r\nbroken off wheels.");
            // 
            // lblDamageAero
            // 
            this.lblDamageAero.AutoSize = true;
            this.lblDamageAero.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDamageAero.Location = new System.Drawing.Point(150, 16);
            this.lblDamageAero.Name = "lblDamageAero";
            this.lblDamageAero.Size = new System.Drawing.Size(51, 13);
            this.lblDamageAero.TabIndex = 11;
            this.lblDamageAero.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblDamageAero, "The time it takes to repair aero damage (like wings).");
            // 
            // lblConcurrent
            // 
            this.lblConcurrent.AutoSize = true;
            this.lblConcurrent.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConcurrent.Location = new System.Drawing.Point(561, 258);
            this.lblConcurrent.Name = "lblConcurrent";
            this.lblConcurrent.Size = new System.Drawing.Size(51, 13);
            this.lblConcurrent.TabIndex = 18;
            this.lblConcurrent.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblConcurrent, "Whether or not the refueling and tire changes can happen simultaneously.");
            // 
            // lblDelayTirechange
            // 
            this.lblDelayTirechange.AutoSize = true;
            this.lblDelayTirechange.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDelayTirechange.Location = new System.Drawing.Point(152, 42);
            this.lblDelayTirechange.Name = "lblDelayTirechange";
            this.lblDelayTirechange.Size = new System.Drawing.Size(51, 13);
            this.lblDelayTirechange.TabIndex = 14;
            this.lblDelayTirechange.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblDelayTirechange, "The maximum random delay PER TIRE when changing tires on your car.");
            // 
            // lblChange4tires
            // 
            this.lblChange4tires.AutoSize = true;
            this.lblChange4tires.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChange4tires.Location = new System.Drawing.Point(152, 29);
            this.lblChange4tires.Name = "lblChange4tires";
            this.lblChange4tires.Size = new System.Drawing.Size(51, 13);
            this.lblChange4tires.TabIndex = 12;
            this.lblChange4tires.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblChange4tires, "The time it takes to change 4 wheels.");
            // 
            // lblChange2tires
            // 
            this.lblChange2tires.AutoSize = true;
            this.lblChange2tires.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChange2tires.Location = new System.Drawing.Point(152, 16);
            this.lblChange2tires.Name = "lblChange2tires";
            this.lblChange2tires.Size = new System.Drawing.Size(51, 13);
            this.lblChange2tires.TabIndex = 11;
            this.lblChange2tires.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblChange2tires, "The time it takes to change 2 wheels.");
            // 
            // lblNozzleRemoval
            // 
            this.lblNozzleRemoval.AutoSize = true;
            this.lblNozzleRemoval.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNozzleRemoval.Location = new System.Drawing.Point(152, 55);
            this.lblNozzleRemoval.Name = "lblNozzleRemoval";
            this.lblNozzleRemoval.Size = new System.Drawing.Size(51, 13);
            this.lblNozzleRemoval.TabIndex = 16;
            this.lblNozzleRemoval.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblNozzleRemoval, "The time it takes to disconnect the refueling nozzle from the car.\r\nThis is a con" +
        "stant time, not random.");
            // 
            // lblNozzleInsertion
            // 
            this.lblNozzleInsertion.AutoSize = true;
            this.lblNozzleInsertion.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNozzleInsertion.Location = new System.Drawing.Point(152, 42);
            this.lblNozzleInsertion.Name = "lblNozzleInsertion";
            this.lblNozzleInsertion.Size = new System.Drawing.Size(51, 13);
            this.lblNozzleInsertion.TabIndex = 14;
            this.lblNozzleInsertion.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblNozzleInsertion, "The time it takes to connect the refueling nozzle to the car.\r\nThis is a constant" +
        " time, not random.");
            // 
            // lblMaxFuelDelay
            // 
            this.lblMaxFuelDelay.AutoSize = true;
            this.lblMaxFuelDelay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxFuelDelay.Location = new System.Drawing.Point(152, 29);
            this.lblMaxFuelDelay.Name = "lblMaxFuelDelay";
            this.lblMaxFuelDelay.Size = new System.Drawing.Size(51, 13);
            this.lblMaxFuelDelay.TabIndex = 12;
            this.lblMaxFuelDelay.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblMaxFuelDelay, "The maximum random delay when refueling your car.\r\nIf you\'re very lucky this is 0" +
        ", when you\'re unlucky this is the maximum.");
            // 
            // lblFuelFillRate
            // 
            this.lblFuelFillRate.AutoSize = true;
            this.lblFuelFillRate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuelFillRate.Location = new System.Drawing.Point(152, 16);
            this.lblFuelFillRate.Name = "lblFuelFillRate";
            this.lblFuelFillRate.Size = new System.Drawing.Size(51, 13);
            this.lblFuelFillRate.TabIndex = 11;
            this.lblFuelFillRate.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblFuelFillRate, "How much fuel per second goes into the car.");
            // 
            // lblMaxDelay
            // 
            this.lblMaxDelay.AutoSize = true;
            this.lblMaxDelay.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxDelay.Location = new System.Drawing.Point(150, 59);
            this.lblMaxDelay.Name = "lblMaxDelay";
            this.lblMaxDelay.Size = new System.Drawing.Size(51, 13);
            this.lblMaxDelay.TabIndex = 16;
            this.lblMaxDelay.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblMaxDelay, "The maximum delay you\'ll get when the pit crew wasn\'t prepared.");
            // 
            // lblDelayMulti
            // 
            this.lblDelayMulti.AutoSize = true;
            this.lblDelayMulti.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDelayMulti.Location = new System.Drawing.Point(150, 46);
            this.lblDelayMulti.Name = "lblDelayMulti";
            this.lblDelayMulti.Size = new System.Drawing.Size(51, 13);
            this.lblDelayMulti.TabIndex = 14;
            this.lblDelayMulti.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblDelayMulti, resources.GetString("lblDelayMulti.ToolTip"));
            // 
            // lblPreparation
            // 
            this.lblPreparation.AutoSize = true;
            this.lblPreparation.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreparation.Location = new System.Drawing.Point(150, 33);
            this.lblPreparation.Name = "lblPreparation";
            this.lblPreparation.Size = new System.Drawing.Size(51, 13);
            this.lblPreparation.TabIndex = 12;
            this.lblPreparation.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblPreparation, "The time the pit crew needs to prepare your pitstop.\r\nIf you request a pitstop wi" +
        "th less time than this, the pit crew\r\nwon\'t be ready and your pitstop will take " +
        "longer.");
            // 
            // lblGiveup
            // 
            this.lblGiveup.AutoSize = true;
            this.lblGiveup.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiveup.Location = new System.Drawing.Point(150, 20);
            this.lblGiveup.Name = "lblGiveup";
            this.lblGiveup.Size = new System.Drawing.Size(51, 13);
            this.lblGiveup.TabIndex = 11;
            this.lblGiveup.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblGiveup, "The time the pit crew gives up after you made a pit request.\r\nIf you pit after th" +
        "is time, they won\'t be prepared.");
            // 
            // lblTireStartTemp
            // 
            this.lblTireStartTemp.AutoSize = true;
            this.lblTireStartTemp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTireStartTemp.Location = new System.Drawing.Point(120, 160);
            this.lblTireStartTemp.Name = "lblTireStartTemp";
            this.lblTireStartTemp.Size = new System.Drawing.Size(51, 13);
            this.lblTireStartTemp.TabIndex = 49;
            this.lblTireStartTemp.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblTireStartTemp, "The temperature the tires have when leaving the pits.");
            // 
            // lblTireTemp
            // 
            this.lblTireTemp.AutoSize = true;
            this.lblTireTemp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTireTemp.Location = new System.Drawing.Point(120, 147);
            this.lblTireTemp.Name = "lblTireTemp";
            this.lblTireTemp.Size = new System.Drawing.Size(51, 13);
            this.lblTireTemp.TabIndex = 46;
            this.lblTireTemp.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblTireTemp, "The temperature where the grip of the tire is at its maximum.");
            // 
            // lblRearTireMass
            // 
            this.lblRearTireMass.AutoSize = true;
            this.lblRearTireMass.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRearTireMass.Location = new System.Drawing.Point(111, 176);
            this.lblRearTireMass.Name = "lblRearTireMass";
            this.lblRearTireMass.Size = new System.Drawing.Size(51, 13);
            this.lblRearTireMass.TabIndex = 52;
            this.lblRearTireMass.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblRearTireMass, "The tire mass of one of this side\'s wheels. ");
            // 
            // lblFrontTireMass
            // 
            this.lblFrontTireMass.AutoSize = true;
            this.lblFrontTireMass.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrontTireMass.Location = new System.Drawing.Point(111, 163);
            this.lblFrontTireMass.Name = "lblFrontTireMass";
            this.lblFrontTireMass.Size = new System.Drawing.Size(51, 13);
            this.lblFrontTireMass.TabIndex = 50;
            this.lblFrontTireMass.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblFrontTireMass, "The tire mass of one of this side\'s wheels. ");
            // 
            // txtCustomTemp
            // 
            this.txtCustomTemp.Location = new System.Drawing.Point(117, 183);
            this.txtCustomTemp.Name = "txtCustomTemp";
            this.txtCustomTemp.Size = new System.Drawing.Size(38, 21);
            this.txtCustomTemp.TabIndex = 57;
            this.toolTip1.SetToolTip(this.txtCustomTemp, "Insert a temperature (above the activation temp) that you want to display in the " +
        "graph.");
            this.txtCustomTemp.TextChanged += new System.EventHandler(this.txtCustomTemp_TextChanged);
            // 
            // lblBrakeBias
            // 
            this.lblBrakeBias.AutoSize = true;
            this.lblBrakeBias.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBrakeBias.Location = new System.Drawing.Point(139, 146);
            this.lblBrakeBias.Name = "lblBrakeBias";
            this.lblBrakeBias.Size = new System.Drawing.Size(51, 13);
            this.lblBrakeBias.TabIndex = 31;
            this.lblBrakeBias.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblBrakeBias, "Whether or not you can adjust the brake bias onboard.");
            // 
            // lblRearBrakeFail
            // 
            this.lblRearBrakeFail.AutoSize = true;
            this.lblRearBrakeFail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRearBrakeFail.Location = new System.Drawing.Point(120, 68);
            this.lblRearBrakeFail.Name = "lblRearBrakeFail";
            this.lblRearBrakeFail.Size = new System.Drawing.Size(51, 13);
            this.lblRearBrakeFail.TabIndex = 51;
            this.lblRearBrakeFail.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblRearBrakeFail, "The brake disc thickness at wich a brake failure can occur.");
            // 
            // lblRearBrakeThick
            // 
            this.lblRearBrakeThick.AutoSize = true;
            this.lblRearBrakeThick.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRearBrakeThick.Location = new System.Drawing.Point(120, 54);
            this.lblRearBrakeThick.Name = "lblRearBrakeThick";
            this.lblRearBrakeThick.Size = new System.Drawing.Size(51, 13);
            this.lblRearBrakeThick.TabIndex = 49;
            this.lblRearBrakeThick.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblRearBrakeThick, "The brake disc thickness if you leave the pits.");
            // 
            // lblRearBrakeTorque
            // 
            this.lblRearBrakeTorque.AutoSize = true;
            this.lblRearBrakeTorque.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRearBrakeTorque.Location = new System.Drawing.Point(120, 40);
            this.lblRearBrakeTorque.Name = "lblRearBrakeTorque";
            this.lblRearBrakeTorque.Size = new System.Drawing.Size(51, 13);
            this.lblRearBrakeTorque.TabIndex = 47;
            this.lblRearBrakeTorque.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblRearBrakeTorque, "The maximum brake torque at zero wear and optimum temp.");
            // 
            // lblRearBrakes
            // 
            this.lblRearBrakes.AutoSize = true;
            this.lblRearBrakes.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRearBrakes.Location = new System.Drawing.Point(120, 26);
            this.lblRearBrakes.Name = "lblRearBrakes";
            this.lblRearBrakes.Size = new System.Drawing.Size(51, 13);
            this.lblRearBrakes.TabIndex = 45;
            this.lblRearBrakes.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblRearBrakes, "The brake temperatures where the braking power of the rear brakes is the highest." +
        "");
            // 
            // lblFrontBrakeFail
            // 
            this.lblFrontBrakeFail.AutoSize = true;
            this.lblFrontBrakeFail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrontBrakeFail.Location = new System.Drawing.Point(120, 63);
            this.lblFrontBrakeFail.Name = "lblFrontBrakeFail";
            this.lblFrontBrakeFail.Size = new System.Drawing.Size(51, 13);
            this.lblFrontBrakeFail.TabIndex = 47;
            this.lblFrontBrakeFail.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblFrontBrakeFail, "The brake disc thickness at wich a brake failure can occur.");
            // 
            // lblFrontBrakeThick
            // 
            this.lblFrontBrakeThick.AutoSize = true;
            this.lblFrontBrakeThick.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrontBrakeThick.Location = new System.Drawing.Point(120, 49);
            this.lblFrontBrakeThick.Name = "lblFrontBrakeThick";
            this.lblFrontBrakeThick.Size = new System.Drawing.Size(51, 13);
            this.lblFrontBrakeThick.TabIndex = 45;
            this.lblFrontBrakeThick.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblFrontBrakeThick, "The brake disc thickness if you leave the pits.");
            // 
            // lblFrontBrakeTorque
            // 
            this.lblFrontBrakeTorque.AutoSize = true;
            this.lblFrontBrakeTorque.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrontBrakeTorque.Location = new System.Drawing.Point(120, 35);
            this.lblFrontBrakeTorque.Name = "lblFrontBrakeTorque";
            this.lblFrontBrakeTorque.Size = new System.Drawing.Size(51, 13);
            this.lblFrontBrakeTorque.TabIndex = 43;
            this.lblFrontBrakeTorque.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblFrontBrakeTorque, "The maximum brake torque at zero wear and optimum temp.");
            // 
            // lblFrontBrakes
            // 
            this.lblFrontBrakes.AutoSize = true;
            this.lblFrontBrakes.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrontBrakes.Location = new System.Drawing.Point(120, 21);
            this.lblFrontBrakes.Name = "lblFrontBrakes";
            this.lblFrontBrakes.Size = new System.Drawing.Size(51, 13);
            this.lblFrontBrakes.TabIndex = 23;
            this.lblFrontBrakes.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblFrontBrakes, "The brake temperatures where the braking power of the front brakes is the highest" +
        ".\r\n");
            // 
            // lblAspiration
            // 
            this.lblAspiration.AutoSize = true;
            this.lblAspiration.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAspiration.Location = new System.Drawing.Point(108, 2);
            this.lblAspiration.Name = "lblAspiration";
            this.lblAspiration.Size = new System.Drawing.Size(51, 13);
            this.lblAspiration.TabIndex = 44;
            this.lblAspiration.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblAspiration, "The type of aspiration this car uses. \r\nThis can be naturally aspirated or (twin-" +
        ") turbocharged.");
            // 
            // lblLifetimeVar
            // 
            this.lblLifetimeVar.AutoSize = true;
            this.lblLifetimeVar.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLifetimeVar.Location = new System.Drawing.Point(108, 106);
            this.lblLifetimeVar.Name = "lblLifetimeVar";
            this.lblLifetimeVar.Size = new System.Drawing.Size(51, 13);
            this.lblLifetimeVar.TabIndex = 39;
            this.lblLifetimeVar.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblLifetimeVar, resources.GetString("lblLifetimeVar.ToolTip"));
            // 
            // lblPowerWeight
            // 
            this.lblPowerWeight.AutoSize = true;
            this.lblPowerWeight.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPowerWeight.Location = new System.Drawing.Point(108, 42);
            this.lblPowerWeight.Name = "lblPowerWeight";
            this.lblPowerWeight.Size = new System.Drawing.Size(51, 13);
            this.lblPowerWeight.TabIndex = 20;
            this.lblPowerWeight.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblPowerWeight, "The power-to-weight ratio of the car. \r\nThis is the max power divided by the tota" +
        "l mass.");
            // 
            // lblPower
            // 
            this.lblPower.AutoSize = true;
            this.lblPower.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPower.Location = new System.Drawing.Point(108, 15);
            this.lblPower.Name = "lblPower";
            this.lblPower.Size = new System.Drawing.Size(51, 13);
            this.lblPower.TabIndex = 18;
            this.lblPower.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblPower, "The maximum power this car delivers at a certain rpm.");
            // 
            // lblTorque
            // 
            this.lblTorque.AutoSize = true;
            this.lblTorque.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTorque.Location = new System.Drawing.Point(108, 29);
            this.lblTorque.Name = "lblTorque";
            this.lblTorque.Size = new System.Drawing.Size(51, 13);
            this.lblTorque.TabIndex = 15;
            this.lblTorque.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblTorque, "The maximum torque this car delivers at a certain rpm.");
            // 
            // lblRevlimit
            // 
            this.lblRevlimit.AutoSize = true;
            this.lblRevlimit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRevlimit.Location = new System.Drawing.Point(108, 54);
            this.lblRevlimit.Name = "lblRevlimit";
            this.lblRevlimit.Size = new System.Drawing.Size(51, 13);
            this.lblRevlimit.TabIndex = 14;
            this.lblRevlimit.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblRevlimit, "The rev limit range of the engine. May or may not be changeable in the garage.");
            // 
            // lblMaxrpm
            // 
            this.lblMaxrpm.AutoSize = true;
            this.lblMaxrpm.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxrpm.Location = new System.Drawing.Point(108, 119);
            this.lblMaxrpm.Name = "lblMaxrpm";
            this.lblMaxrpm.Size = new System.Drawing.Size(51, 13);
            this.lblMaxrpm.TabIndex = 11;
            this.lblMaxrpm.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblMaxrpm, "The highest \"safe\" rpm where there will be no damage to the engine. \r\nGo higher t" +
        "han this, and your lifetime will be affected.");
            // 
            // lblLifetime
            // 
            this.lblLifetime.AutoSize = true;
            this.lblLifetime.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLifetime.Location = new System.Drawing.Point(108, 94);
            this.lblLifetime.Name = "lblLifetime";
            this.lblLifetime.Size = new System.Drawing.Size(51, 13);
            this.lblLifetime.TabIndex = 10;
            this.lblLifetime.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblLifetime, "This is the average lifetime of the engine if the oil temperature\r\n and rpm\'s sta" +
        "y below their \"safe\" limits.");
            // 
            // lblSafeOil
            // 
            this.lblSafeOil.AutoSize = true;
            this.lblSafeOil.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSafeOil.Location = new System.Drawing.Point(108, 80);
            this.lblSafeOil.Name = "lblSafeOil";
            this.lblSafeOil.Size = new System.Drawing.Size(51, 13);
            this.lblSafeOil.TabIndex = 9;
            this.lblSafeOil.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblSafeOil, "The maximum safe oil temperature. If your engine gets hotter than this, the lifet" +
        "ime will be affected.");
            // 
            // lblOptimal
            // 
            this.lblOptimal.AutoSize = true;
            this.lblOptimal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOptimal.Location = new System.Drawing.Point(108, 67);
            this.lblOptimal.Name = "lblOptimal";
            this.lblOptimal.Size = new System.Drawing.Size(51, 13);
            this.lblOptimal.TabIndex = 8;
            this.lblOptimal.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblOptimal, "The oil temperature where the engine works at its best. Aim for this temperatur");
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(9, 186);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(110, 13);
            this.label88.TabIndex = 56;
            this.label88.Text = "Custom temperature:";
            this.toolTip1.SetToolTip(this.label88, "Insert a temperature (above the activation temp) that you want to display in the " +
        "graph.");
            // 
            // btnGetUpgradeFile
            // 
            this.btnGetUpgradeFile.Location = new System.Drawing.Point(460, 136);
            this.btnGetUpgradeFile.Name = "btnGetUpgradeFile";
            this.btnGetUpgradeFile.Size = new System.Drawing.Size(96, 23);
            this.btnGetUpgradeFile.TabIndex = 45;
            this.btnGetUpgradeFile.Text = "Get upgrade file";
            this.toolTip1.SetToolTip(this.btnGetUpgradeFile, "This allows you to save the upgradefile.");
            this.btnGetUpgradeFile.UseVisualStyleBackColor = true;
            this.btnGetUpgradeFile.Click += new System.EventHandler(this.btnGetUpgradeFile_Click);
            // 
            // CheckEngineFriction
            // 
            this.CheckEngineFriction.AutoSize = true;
            this.CheckEngineFriction.Checked = true;
            this.CheckEngineFriction.CheckState = System.Windows.Forms.CheckState.Checked;
            this.CheckEngineFriction.Location = new System.Drawing.Point(6, 6);
            this.CheckEngineFriction.Name = "CheckEngineFriction";
            this.CheckEngineFriction.Size = new System.Drawing.Size(161, 17);
            this.CheckEngineFriction.TabIndex = 45;
            this.CheckEngineFriction.Text = "Include extra engine friction";
            this.toolTip1.SetToolTip(this.CheckEngineFriction, "With a lot of mods there is a power/torque/rpm multiplier in the hdv file to simu" +
        "late \r\npower loss through extra engine friction, wear, other losses,...");
            this.CheckEngineFriction.UseVisualStyleBackColor = true;
            this.CheckEngineFriction.CheckedChanged += new System.EventHandler(this.checkEngineFriction_CheckedChanged);
            // 
            // lblPressureChangePossible
            // 
            this.lblPressureChangePossible.AutoSize = true;
            this.lblPressureChangePossible.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPressureChangePossible.Location = new System.Drawing.Point(152, 55);
            this.lblPressureChangePossible.Name = "lblPressureChangePossible";
            this.lblPressureChangePossible.Size = new System.Drawing.Size(51, 13);
            this.lblPressureChangePossible.TabIndex = 16;
            this.lblPressureChangePossible.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblPressureChangePossible, "If it is allowed to change the pressure of tires that are being changed.");
            // 
            // lblAntiRoll
            // 
            this.lblAntiRoll.AutoSize = true;
            this.lblAntiRoll.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAntiRoll.Location = new System.Drawing.Point(119, 159);
            this.lblAntiRoll.Name = "lblAntiRoll";
            this.lblAntiRoll.Size = new System.Drawing.Size(51, 13);
            this.lblAntiRoll.TabIndex = 35;
            this.lblAntiRoll.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblAntiRoll, "If and where you can adjust the Anti-Rollbar(s) on the fly");
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(6, 159);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(89, 13);
            this.label80.TabIndex = 34;
            this.label80.Text = "Anti-Rollbar adj.:";
            this.toolTip1.SetToolTip(this.label80, "Anti-Rollbar adjustment on the fly");
            // 
            // lblTrackbarFly
            // 
            this.lblTrackbarFly.AutoSize = true;
            this.lblTrackbarFly.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrackbarFly.Location = new System.Drawing.Point(119, 173);
            this.lblTrackbarFly.Name = "lblTrackbarFly";
            this.lblTrackbarFly.Size = new System.Drawing.Size(51, 13);
            this.lblTrackbarFly.TabIndex = 37;
            this.lblTrackbarFly.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblTrackbarFly, "If you can adjust the Trackbar on the fly");
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(6, 173);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(75, 13);
            this.label82.TabIndex = 36;
            this.label82.Text = "Trackbar adj.:";
            this.toolTip1.SetToolTip(this.label82, "Trackbar adjustment on the fly");
            // 
            // chkEngineMixture
            // 
            this.chkEngineMixture.AutoSize = true;
            this.chkEngineMixture.Location = new System.Drawing.Point(14, 194);
            this.chkEngineMixture.Margin = new System.Windows.Forms.Padding(2);
            this.chkEngineMixture.Name = "chkEngineMixture";
            this.chkEngineMixture.Size = new System.Drawing.Size(101, 17);
            this.chkEngineMixture.TabIndex = 6;
            this.chkEngineMixture.Text = "Engine Mixture:";
            this.toolTip1.SetToolTip(this.chkEngineMixture, "Checking this will include Fuel Mixture into the power calculation");
            this.chkEngineMixture.UseVisualStyleBackColor = true;
            this.chkEngineMixture.CheckedChanged += new System.EventHandler(this.chkEngineMixture_CheckedChanged);
            // 
            // chkBoostMapping
            // 
            this.chkBoostMapping.AutoSize = true;
            this.chkBoostMapping.Location = new System.Drawing.Point(14, 216);
            this.chkBoostMapping.Margin = new System.Windows.Forms.Padding(2);
            this.chkBoostMapping.Name = "chkBoostMapping";
            this.chkBoostMapping.Size = new System.Drawing.Size(100, 17);
            this.chkBoostMapping.TabIndex = 8;
            this.chkBoostMapping.Text = "Boost Mapping:";
            this.toolTip1.SetToolTip(this.chkBoostMapping, "Enableing this will include the boost mappings into the graph (This does NOT enab" +
        "le turbo modeling!!)");
            this.chkBoostMapping.UseVisualStyleBackColor = true;
            this.chkBoostMapping.CheckedChanged += new System.EventHandler(this.chkBoostMapping_CheckedChanged);
            // 
            // radbAspNaturally
            // 
            this.radbAspNaturally.AutoSize = true;
            this.radbAspNaturally.Location = new System.Drawing.Point(14, 238);
            this.radbAspNaturally.Margin = new System.Windows.Forms.Padding(2);
            this.radbAspNaturally.Name = "radbAspNaturally";
            this.radbAspNaturally.Size = new System.Drawing.Size(68, 17);
            this.radbAspNaturally.TabIndex = 9;
            this.radbAspNaturally.TabStop = true;
            this.radbAspNaturally.Text = "Naturally";
            this.toolTip1.SetToolTip(this.radbAspNaturally, "Displays the engine power without turbocharging at sealevel air pressure (101.325" +
        " kpa)");
            this.radbAspNaturally.UseVisualStyleBackColor = true;
            this.radbAspNaturally.CheckedChanged += new System.EventHandler(this.radbAspNaturally_CheckedChanged);
            // 
            // radbAspTurbo
            // 
            this.radbAspTurbo.AutoSize = true;
            this.radbAspTurbo.Location = new System.Drawing.Point(119, 238);
            this.radbAspTurbo.Margin = new System.Windows.Forms.Padding(2);
            this.radbAspTurbo.Name = "radbAspTurbo";
            this.radbAspTurbo.Size = new System.Drawing.Size(96, 17);
            this.radbAspTurbo.TabIndex = 10;
            this.radbAspTurbo.TabStop = true;
            this.radbAspTurbo.Text = "Turbo-charged";
            this.toolTip1.SetToolTip(this.radbAspTurbo, "Displays the engine power with turbocharging");
            this.radbAspTurbo.UseVisualStyleBackColor = true;
            this.radbAspTurbo.CheckedChanged += new System.EventHandler(this.radbAspTurbo_CheckedChanged);
            // 
            // lblFuelConsumtion
            // 
            this.lblFuelConsumtion.AutoSize = true;
            this.lblFuelConsumtion.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuelConsumtion.Location = new System.Drawing.Point(108, 53);
            this.lblFuelConsumtion.Name = "lblFuelConsumtion";
            this.lblFuelConsumtion.Size = new System.Drawing.Size(51, 13);
            this.lblFuelConsumtion.TabIndex = 51;
            this.lblFuelConsumtion.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblFuelConsumtion, "The Fuel consumtion value (does not include RPM)");
            // 
            // lblEngineInertia
            // 
            this.lblEngineInertia.AutoSize = true;
            this.lblEngineInertia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEngineInertia.Location = new System.Drawing.Point(108, 26);
            this.lblEngineInertia.Name = "lblEngineInertia";
            this.lblEngineInertia.Size = new System.Drawing.Size(51, 13);
            this.lblEngineInertia.TabIndex = 49;
            this.lblEngineInertia.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblEngineInertia, "Inertia of the Engine. Higher values mean the engine needs longer to rev and coas" +
        "ts down slower");
            // 
            // lblTurboPressure
            // 
            this.lblTurboPressure.AutoSize = true;
            this.lblTurboPressure.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTurboPressure.Location = new System.Drawing.Point(108, 39);
            this.lblTurboPressure.Name = "lblTurboPressure";
            this.lblTurboPressure.Size = new System.Drawing.Size(51, 13);
            this.lblTurboPressure.TabIndex = 47;
            this.lblTurboPressure.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblTurboPressure, "The Turbo Pressure");
            // 
            // lblSemiAuto
            // 
            this.lblSemiAuto.AutoSize = true;
            this.lblSemiAuto.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSemiAuto.Location = new System.Drawing.Point(119, 66);
            this.lblSemiAuto.Name = "lblSemiAuto";
            this.lblSemiAuto.Size = new System.Drawing.Size(51, 13);
            this.lblSemiAuto.TabIndex = 39;
            this.lblSemiAuto.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblSemiAuto, "If the car controls the throttle and clutch on shifts (without Auto-Clutch)");
            // 
            // lblAntiStall
            // 
            this.lblAntiStall.AutoSize = true;
            this.lblAntiStall.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAntiStall.Location = new System.Drawing.Point(119, 186);
            this.lblAntiStall.Name = "lblAntiStall";
            this.lblAntiStall.Size = new System.Drawing.Size(51, 13);
            this.lblAntiStall.TabIndex = 41;
            this.lblAntiStall.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblAntiStall, "If the car has an intigrated Anti-Stall system (puts you into 1st and asks you to" +
        " press in the clutch)");
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(6, 186);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(54, 13);
            this.label96.TabIndex = 40;
            this.label96.Text = "Anti-Stall:";
            this.toolTip1.SetToolTip(this.label96, "Anti-Rollbar adjustment on the fly");
            // 
            // chkPitStopManualOverwrite
            // 
            this.chkPitStopManualOverwrite.AutoSize = true;
            this.chkPitStopManualOverwrite.Location = new System.Drawing.Point(95, 192);
            this.chkPitStopManualOverwrite.Margin = new System.Windows.Forms.Padding(2);
            this.chkPitStopManualOverwrite.Name = "chkPitStopManualOverwrite";
            this.chkPitStopManualOverwrite.Size = new System.Drawing.Size(111, 17);
            this.chkPitStopManualOverwrite.TabIndex = 22;
            this.chkPitStopManualOverwrite.Text = "Manual Overwrite";
            this.toolTip1.SetToolTip(this.chkPitStopManualOverwrite, "Will enable everything (Including disableing the Fueltanklimit), even though thes" +
        "e options are not avaible ingame. !Warning! this can lead and will most likely l" +
        "ead to errors!");
            this.chkPitStopManualOverwrite.UseVisualStyleBackColor = true;
            this.chkPitStopManualOverwrite.CheckedChanged += new System.EventHandler(this.chkPitStopManualOverwrite_CheckedChanged);
            // 
            // lblPitTotal
            // 
            this.lblPitTotal.AutoSize = true;
            this.lblPitTotal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPitTotal.Location = new System.Drawing.Point(176, 139);
            this.lblPitTotal.Name = "lblPitTotal";
            this.lblPitTotal.Size = new System.Drawing.Size(51, 13);
            this.lblPitTotal.TabIndex = 11;
            this.lblPitTotal.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblPitTotal, "The minimum and maximum time the simulated pitstop would take.\r\n\r\nThere\'s a minim" +
        "um and maximum time because there\'s always some \r\nrandom delay when refueling, c" +
        "hanging tires or fixing damage.");
            // 
            // lblPitMisc
            // 
            this.lblPitMisc.AutoSize = true;
            this.lblPitMisc.Location = new System.Drawing.Point(127, 60);
            this.lblPitMisc.Name = "lblPitMisc";
            this.lblPitMisc.Size = new System.Drawing.Size(46, 13);
            this.lblPitMisc.TabIndex = 7;
            this.lblPitMisc.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblPitMisc, "The time it would take just to complete the miscellaneous task on the car");
            // 
            // lblPitDamage
            // 
            this.lblPitDamage.AutoSize = true;
            this.lblPitDamage.Location = new System.Drawing.Point(127, 46);
            this.lblPitDamage.Name = "lblPitDamage";
            this.lblPitDamage.Size = new System.Drawing.Size(46, 13);
            this.lblPitDamage.TabIndex = 5;
            this.lblPitDamage.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblPitDamage, "The individual time it would take to repair the car with the \r\ngiven damage.\r\n\r\nD" +
        "oesn\'t take the refueling and tire change in consideration.");
            // 
            // lblPitTires
            // 
            this.lblPitTires.AutoSize = true;
            this.lblPitTires.Location = new System.Drawing.Point(127, 33);
            this.lblPitTires.Name = "lblPitTires";
            this.lblPitTires.Size = new System.Drawing.Size(46, 13);
            this.lblPitTires.TabIndex = 3;
            this.lblPitTires.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblPitTires, "The individual time it would take to change the selected tires on the car.\r\n\r\nDoe" +
        "sn\'t take the refueling and damage repair in consideration.");
            // 
            // lblPitFuel
            // 
            this.lblPitFuel.AutoSize = true;
            this.lblPitFuel.Location = new System.Drawing.Point(127, 20);
            this.lblPitFuel.Name = "lblPitFuel";
            this.lblPitFuel.Size = new System.Drawing.Size(46, 13);
            this.lblPitFuel.TabIndex = 1;
            this.lblPitFuel.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblPitFuel, "The individual time it would take to refuel the car with the given \r\namount of fu" +
        "el. \r\nDoesn\'t take the tire change and damage repair in consideration.");
            // 
            // btnSimulatePitstop
            // 
            this.btnSimulatePitstop.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSimulatePitstop.Location = new System.Drawing.Point(103, 214);
            this.btnSimulatePitstop.Name = "btnSimulatePitstop";
            this.btnSimulatePitstop.Size = new System.Drawing.Size(88, 36);
            this.btnSimulatePitstop.TabIndex = 9;
            this.btnSimulatePitstop.Text = "Calculate";
            this.toolTip1.SetToolTip(this.btnSimulatePitstop, "Calculate the projected pit times with the entered values.");
            this.btnSimulatePitstop.UseVisualStyleBackColor = true;
            this.btnSimulatePitstop.Click += new System.EventHandler(this.btnSimulatePitstop_Click);
            // 
            // chkDriverswap
            // 
            this.chkDriverswap.AutoSize = true;
            this.chkDriverswap.Location = new System.Drawing.Point(9, 154);
            this.chkDriverswap.Name = "chkDriverswap";
            this.chkDriverswap.Size = new System.Drawing.Size(80, 17);
            this.chkDriverswap.TabIndex = 15;
            this.chkDriverswap.Text = "Driverswap";
            this.toolTip1.SetToolTip(this.chkDriverswap, "Select this if you\'d like to simulate a Driverswap.");
            this.chkDriverswap.UseVisualStyleBackColor = true;
            // 
            // chkTrackbar
            // 
            this.chkTrackbar.AutoSize = true;
            this.chkTrackbar.Location = new System.Drawing.Point(9, 139);
            this.chkTrackbar.Name = "chkTrackbar";
            this.chkTrackbar.Size = new System.Drawing.Size(68, 17);
            this.chkTrackbar.TabIndex = 14;
            this.chkTrackbar.Text = "Trackbar";
            this.toolTip1.SetToolTip(this.chkTrackbar, "Select this if you\'d like to simulate Trackbar adjustments.");
            this.chkTrackbar.UseVisualStyleBackColor = true;
            // 
            // chkSpringRubber
            // 
            this.chkSpringRubber.AutoSize = true;
            this.chkSpringRubber.Location = new System.Drawing.Point(137, 123);
            this.chkSpringRubber.Name = "chkSpringRubber";
            this.chkSpringRubber.Size = new System.Drawing.Size(94, 17);
            this.chkSpringRubber.TabIndex = 13;
            this.chkSpringRubber.Text = "Spring Rubber";
            this.toolTip1.SetToolTip(this.chkSpringRubber, "Select this if you\'d like to simulate Spring Rubber adjustments.");
            this.chkSpringRubber.UseVisualStyleBackColor = true;
            // 
            // chkWedge
            // 
            this.chkWedge.AutoSize = true;
            this.chkWedge.Location = new System.Drawing.Point(9, 123);
            this.chkWedge.Name = "chkWedge";
            this.chkWedge.Size = new System.Drawing.Size(60, 17);
            this.chkWedge.TabIndex = 12;
            this.chkWedge.Text = "Wedge";
            this.toolTip1.SetToolTip(this.chkWedge, "Select this if you\'d like to simulate Wedge adjustments.");
            this.chkWedge.UseVisualStyleBackColor = true;
            // 
            // chkTirePressure
            // 
            this.chkTirePressure.AutoSize = true;
            this.chkTirePressure.Location = new System.Drawing.Point(137, 107);
            this.chkTirePressure.Name = "chkTirePressure";
            this.chkTirePressure.Size = new System.Drawing.Size(89, 17);
            this.chkTirePressure.TabIndex = 11;
            this.chkTirePressure.Text = "Tire Pressure";
            this.toolTip1.SetToolTip(this.chkTirePressure, "Select this if you\'d like to simulate tire pressure adjustments WITHOUT changing " +
        "tires. When changing tires, there is no penalty");
            this.chkTirePressure.UseVisualStyleBackColor = true;
            // 
            // chkRadiatorTap
            // 
            this.chkRadiatorTap.AutoSize = true;
            this.chkRadiatorTap.Location = new System.Drawing.Point(9, 107);
            this.chkRadiatorTap.Name = "chkRadiatorTap";
            this.chkRadiatorTap.Size = new System.Drawing.Size(88, 17);
            this.chkRadiatorTap.TabIndex = 10;
            this.chkRadiatorTap.Text = "Radiator Tap";
            this.toolTip1.SetToolTip(this.chkRadiatorTap, "Select this if you\'d like to simulate Radiator Tap adjustments.");
            this.chkRadiatorTap.UseVisualStyleBackColor = true;
            // 
            // chkRearWing
            // 
            this.chkRearWing.AutoSize = true;
            this.chkRearWing.Location = new System.Drawing.Point(137, 91);
            this.chkRearWing.Name = "chkRearWing";
            this.chkRearWing.Size = new System.Drawing.Size(76, 17);
            this.chkRearWing.TabIndex = 9;
            this.chkRearWing.Text = "Rear Wing";
            this.toolTip1.SetToolTip(this.chkRearWing, "Select this if you\'d like to simulate rear wing adjustments.");
            this.chkRearWing.UseVisualStyleBackColor = true;
            // 
            // chkFrontWing
            // 
            this.chkFrontWing.AutoSize = true;
            this.chkFrontWing.Location = new System.Drawing.Point(9, 91);
            this.chkFrontWing.Name = "chkFrontWing";
            this.chkFrontWing.Size = new System.Drawing.Size(79, 17);
            this.chkFrontWing.TabIndex = 8;
            this.chkFrontWing.Text = "Front Wing";
            this.toolTip1.SetToolTip(this.chkFrontWing, "Select this if you\'d like to simulate front wing adjustments.");
            this.chkFrontWing.UseVisualStyleBackColor = true;
            // 
            // chkSuspDamage
            // 
            this.chkSuspDamage.AutoSize = true;
            this.chkSuspDamage.Location = new System.Drawing.Point(137, 60);
            this.chkSuspDamage.Name = "chkSuspDamage";
            this.chkSuspDamage.Size = new System.Drawing.Size(154, 17);
            this.chkSuspDamage.TabIndex = 7;
            this.chkSuspDamage.Text = "Repair suspension damage";
            this.toolTip1.SetToolTip(this.chkSuspDamage, "Select this if you\'d like to simulate the repair of some suspension damage.");
            this.chkSuspDamage.UseVisualStyleBackColor = true;
            // 
            // chkAeroDamage
            // 
            this.chkAeroDamage.AutoSize = true;
            this.chkAeroDamage.Location = new System.Drawing.Point(9, 60);
            this.chkAeroDamage.Name = "chkAeroDamage";
            this.chkAeroDamage.Size = new System.Drawing.Size(123, 17);
            this.chkAeroDamage.TabIndex = 6;
            this.chkAeroDamage.Text = "Repair aero damage";
            this.toolTip1.SetToolTip(this.chkAeroDamage, "Select this if you\'d like to simulate the repair of some aero damage.");
            this.chkAeroDamage.UseVisualStyleBackColor = true;
            // 
            // dropChangeTires
            // 
            this.dropChangeTires.FormattingEnabled = true;
            this.dropChangeTires.Items.AddRange(new object[] {
            "0",
            "2",
            "4"});
            this.dropChangeTires.Location = new System.Drawing.Point(143, 32);
            this.dropChangeTires.Name = "dropChangeTires";
            this.dropChangeTires.Size = new System.Drawing.Size(36, 21);
            this.dropChangeTires.TabIndex = 4;
            this.toolTip1.SetToolTip(this.dropChangeTires, "Select the number of tires you\'ll want to change in your pitstop.");
            this.dropChangeTires.SelectedIndexChanged += new System.EventHandler(this.dropChangeTires_SelectedIndexChanged);
            // 
            // txtAmountFuel
            // 
            this.txtAmountFuel.Location = new System.Drawing.Point(143, 13);
            this.txtAmountFuel.Name = "txtAmountFuel";
            this.txtAmountFuel.Size = new System.Drawing.Size(36, 21);
            this.txtAmountFuel.TabIndex = 2;
            this.txtAmountFuel.Text = "0";
            this.txtAmountFuel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.txtAmountFuel, "Enter the amount of fuel here that you\'ll want to add when refueling (Min 0, Max " +
        "Fuel Tank Capacity).");
            // 
            // lblTCLow
            // 
            this.lblTCLow.AutoSize = true;
            this.lblTCLow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTCLow.Location = new System.Drawing.Point(119, 13);
            this.lblTCLow.Name = "lblTCLow";
            this.lblTCLow.Size = new System.Drawing.Size(51, 13);
            this.lblTCLow.TabIndex = 30;
            this.lblTCLow.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblTCLow, "Weight added when Traction Control is on Low");
            // 
            // lblTCHigh
            // 
            this.lblTCHigh.AutoSize = true;
            this.lblTCHigh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTCHigh.Location = new System.Drawing.Point(119, 26);
            this.lblTCHigh.Name = "lblTCHigh";
            this.lblTCHigh.Size = new System.Drawing.Size(51, 13);
            this.lblTCHigh.TabIndex = 31;
            this.lblTCHigh.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblTCHigh, "Weight added when Traction Control is on High");
            // 
            // lblABSHigh
            // 
            this.lblABSHigh.AutoSize = true;
            this.lblABSHigh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblABSHigh.Location = new System.Drawing.Point(119, 52);
            this.lblABSHigh.Name = "lblABSHigh";
            this.lblABSHigh.Size = new System.Drawing.Size(51, 13);
            this.lblABSHigh.TabIndex = 33;
            this.lblABSHigh.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblABSHigh, "Weight added when Anti-Lock Braking System is on High");
            // 
            // lblABSLow
            // 
            this.lblABSLow.AutoSize = true;
            this.lblABSLow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblABSLow.Location = new System.Drawing.Point(119, 39);
            this.lblABSLow.Name = "lblABSLow";
            this.lblABSLow.Size = new System.Drawing.Size(51, 13);
            this.lblABSLow.TabIndex = 35;
            this.lblABSLow.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblABSLow, "Weight added when Anti-Lock Braking System is on Low");
            // 
            // lblSCLow
            // 
            this.lblSCLow.AutoSize = true;
            this.lblSCLow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSCLow.Location = new System.Drawing.Point(119, 66);
            this.lblSCLow.Name = "lblSCLow";
            this.lblSCLow.Size = new System.Drawing.Size(51, 13);
            this.lblSCLow.TabIndex = 38;
            this.lblSCLow.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblSCLow, "Weight added when Stability Control is on Low");
            // 
            // lblSCHigh
            // 
            this.lblSCHigh.AutoSize = true;
            this.lblSCHigh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSCHigh.Location = new System.Drawing.Point(119, 78);
            this.lblSCHigh.Name = "lblSCHigh";
            this.lblSCHigh.Size = new System.Drawing.Size(51, 13);
            this.lblSCHigh.TabIndex = 39;
            this.lblSCHigh.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblSCHigh, "Weight added when Stability Control is on High");
            // 
            // lblAutoShiftFull
            // 
            this.lblAutoShiftFull.AutoSize = true;
            this.lblAutoShiftFull.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutoShiftFull.Location = new System.Drawing.Point(119, 105);
            this.lblAutoShiftFull.Name = "lblAutoShiftFull";
            this.lblAutoShiftFull.Size = new System.Drawing.Size(51, 13);
            this.lblAutoShiftFull.TabIndex = 41;
            this.lblAutoShiftFull.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblAutoShiftFull, "Weight added when Auto-Shift is fully enabled");
            // 
            // lblAutoShiftOne
            // 
            this.lblAutoShiftOne.AutoSize = true;
            this.lblAutoShiftOne.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutoShiftOne.Location = new System.Drawing.Point(119, 92);
            this.lblAutoShiftOne.Name = "lblAutoShiftOne";
            this.lblAutoShiftOne.Size = new System.Drawing.Size(51, 13);
            this.lblAutoShiftOne.TabIndex = 43;
            this.lblAutoShiftOne.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblAutoShiftOne, "Weight added when Auto-Shift is enabled for either upshifts only or downshifts on" +
        "ly");
            // 
            // lblAutoPit
            // 
            this.lblAutoPit.AutoSize = true;
            this.lblAutoPit.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutoPit.Location = new System.Drawing.Point(119, 118);
            this.lblAutoPit.Name = "lblAutoPit";
            this.lblAutoPit.Size = new System.Drawing.Size(51, 13);
            this.lblAutoPit.TabIndex = 45;
            this.lblAutoPit.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblAutoPit, "Weight added when Auto Pitlane is turned on");
            // 
            // lblAutoBlip
            // 
            this.lblAutoBlip.AutoSize = true;
            this.lblAutoBlip.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutoBlip.Location = new System.Drawing.Point(119, 145);
            this.lblAutoBlip.Name = "lblAutoBlip";
            this.lblAutoBlip.Size = new System.Drawing.Size(51, 13);
            this.lblAutoBlip.TabIndex = 47;
            this.lblAutoBlip.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblAutoBlip, "Weight added when Autoclutch is enabled and Auto Blip is enabled (can be turned o" +
        "n in the player.json)");
            // 
            // lblAutoLift
            // 
            this.lblAutoLift.AutoSize = true;
            this.lblAutoLift.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutoLift.Location = new System.Drawing.Point(119, 132);
            this.lblAutoLift.Name = "lblAutoLift";
            this.lblAutoLift.Size = new System.Drawing.Size(51, 13);
            this.lblAutoLift.TabIndex = 49;
            this.lblAutoLift.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblAutoLift, "Weight added when Autoclutch is enabled and Auto Lift is enabled (can be turned o" +
        "ff in the player.json)");
            // 
            // lblFuelMaxPower
            // 
            this.lblFuelMaxPower.AutoSize = true;
            this.lblFuelMaxPower.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuelMaxPower.Location = new System.Drawing.Point(108, 66);
            this.lblFuelMaxPower.Name = "lblFuelMaxPower";
            this.lblFuelMaxPower.Size = new System.Drawing.Size(51, 13);
            this.lblFuelMaxPower.TabIndex = 53;
            this.lblFuelMaxPower.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblFuelMaxPower, "Fuel consumned at the max power rpm in one minute");
            // 
            // lblFuelMaxTorque
            // 
            this.lblFuelMaxTorque.AutoSize = true;
            this.lblFuelMaxTorque.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuelMaxTorque.Location = new System.Drawing.Point(108, 80);
            this.lblFuelMaxTorque.Name = "lblFuelMaxTorque";
            this.lblFuelMaxTorque.Size = new System.Drawing.Size(51, 13);
            this.lblFuelMaxTorque.TabIndex = 55;
            this.lblFuelMaxTorque.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblFuelMaxTorque, "Fuel consumned at the max torque rpm in one minute");
            // 
            // lblFuelMaxRPM
            // 
            this.lblFuelMaxRPM.AutoSize = true;
            this.lblFuelMaxRPM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuelMaxRPM.Location = new System.Drawing.Point(108, 94);
            this.lblFuelMaxRPM.Name = "lblFuelMaxRPM";
            this.lblFuelMaxRPM.Size = new System.Drawing.Size(51, 13);
            this.lblFuelMaxRPM.TabIndex = 57;
            this.lblFuelMaxRPM.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblFuelMaxRPM, "Fuel consumned at the max revlimiter rpm in one minute");
            // 
            // caricon
            // 
            this.caricon.Location = new System.Drawing.Point(277, 19);
            this.caricon.Margin = new System.Windows.Forms.Padding(2);
            this.caricon.Name = "caricon";
            this.caricon.Size = new System.Drawing.Size(102, 102);
            this.caricon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.caricon.TabIndex = 2;
            this.caricon.TabStop = false;
            this.toolTip1.SetToolTip(this.caricon, "The icon of the car");
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(208, 271);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.TabIndex = 42;
            this.pictureBox2.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox2, resources.GetString("pictureBox2.ToolTip"));
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(208, 271);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.TabIndex = 41;
            this.pictureBox1.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox1, "This graph shows the influence on the grip of your tires when being\r\nover or unde" +
        "r the optimal tire temperature.");
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(208, 271);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.TabIndex = 42;
            this.pictureBox3.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox3, resources.GetString("pictureBox3.ToolTip"));
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(208, 271);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(16, 16);
            this.pictureBox4.TabIndex = 43;
            this.pictureBox4.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox4, resources.GetString("pictureBox4.ToolTip"));
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(208, 271);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(16, 16);
            this.pictureBox5.TabIndex = 44;
            this.pictureBox5.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox5, "In this graph you see the thermal degradation curve of a tire.\r\nThis curve is def" +
        "ined in 32 steps, to calculate the heat history,\r\nhover over the \"Heat history s" +
        "tep\"-value.\r\n");
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(208, 271);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(16, 16);
            this.pictureBox6.TabIndex = 44;
            this.pictureBox6.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox6, resources.GetString("pictureBox6.ToolTip"));
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImage = global::CarStat_2.Properties.Resources.DistanceBetweenWheels;
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox7.Location = new System.Drawing.Point(53, 26);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(109, 177);
            this.pictureBox7.TabIndex = 0;
            this.pictureBox7.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox7, "Distance between the wheels, does not take into account wheel size");
            // 
            // chkRealValues
            // 
            this.chkRealValues.AutoSize = true;
            this.chkRealValues.Location = new System.Drawing.Point(18, 90);
            this.chkRealValues.Margin = new System.Windows.Forms.Padding(2);
            this.chkRealValues.Name = "chkRealValues";
            this.chkRealValues.Size = new System.Drawing.Size(81, 17);
            this.chkRealValues.TabIndex = 5;
            this.chkRealValues.Text = "Real Values";
            this.toolTip1.SetToolTip(this.chkRealValues, "Makes the Setup Rows Display the Real Values instead of the special text");
            this.chkRealValues.UseVisualStyleBackColor = true;
            this.chkRealValues.CheckedChanged += new System.EventHandler(this.chkRealValues_CheckedChanged);
            // 
            // srDiffPreload
            // 
            this.srDiffPreload.BackColor = System.Drawing.Color.Transparent;
            this.srDiffPreload.ChangeEnabled = true;
            this.srDiffPreload.Detachable = false;
            this.srDiffPreload.Location = new System.Drawing.Point(235, 74);
            this.srDiffPreload.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srDiffPreload.Name = "srDiffPreload";
            this.srDiffPreload.Size = new System.Drawing.Size(134, 18);
            this.srDiffPreload.TabIndex = 39;
            this.toolTip1.SetToolTip(this.srDiffPreload, "Preload torque that must be overcome to have wheelspeed difference");
            this.srDiffPreload.ValueChangeListener = null;
            // 
            // srDiffCoast
            // 
            this.srDiffCoast.BackColor = System.Drawing.Color.Transparent;
            this.srDiffCoast.ChangeEnabled = true;
            this.srDiffCoast.Detachable = false;
            this.srDiffCoast.Location = new System.Drawing.Point(235, 51);
            this.srDiffCoast.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srDiffCoast.Name = "srDiffCoast";
            this.srDiffCoast.Size = new System.Drawing.Size(134, 18);
            this.srDiffCoast.TabIndex = 38;
            this.toolTip1.SetToolTip(this.srDiffCoast, "Fraction of coast-side input torque transferred through diff");
            this.srDiffCoast.ValueChangeListener = null;
            // 
            // srDiffPower
            // 
            this.srDiffPower.BackColor = System.Drawing.Color.Transparent;
            this.srDiffPower.ChangeEnabled = true;
            this.srDiffPower.Detachable = false;
            this.srDiffPower.Location = new System.Drawing.Point(235, 28);
            this.srDiffPower.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srDiffPower.Name = "srDiffPower";
            this.srDiffPower.Size = new System.Drawing.Size(134, 18);
            this.srDiffPower.TabIndex = 37;
            this.toolTip1.SetToolTip(this.srDiffPower, "Fraction of power-side input torque transferred through diff");
            this.srDiffPower.ValueChangeListener = null;
            // 
            // srDiffPump
            // 
            this.srDiffPump.BackColor = System.Drawing.Color.Transparent;
            this.srDiffPump.ChangeEnabled = true;
            this.srDiffPump.Detachable = false;
            this.srDiffPump.Location = new System.Drawing.Point(235, 5);
            this.srDiffPump.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srDiffPump.Name = "srDiffPump";
            this.srDiffPump.Size = new System.Drawing.Size(134, 18);
            this.srDiffPump.TabIndex = 36;
            this.toolTip1.SetToolTip(this.srDiffPump, "At 100% pump diff setting, the torque redirected per wheelspeed difference in rad" +
        "ians/sec (roughly 1.2kph)");
            this.srDiffPump.ValueChangeListener = null;
            // 
            // srEngineBreaking
            // 
            this.srEngineBreaking.BackColor = System.Drawing.Color.Transparent;
            this.srEngineBreaking.ChangeEnabled = true;
            this.srEngineBreaking.Detachable = false;
            this.srEngineBreaking.Location = new System.Drawing.Point(91, 89);
            this.srEngineBreaking.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srEngineBreaking.Name = "srEngineBreaking";
            this.srEngineBreaking.Size = new System.Drawing.Size(134, 19);
            this.srEngineBreaking.TabIndex = 96;
            this.toolTip1.SetToolTip(this.srEngineBreaking, "Reduces engine breaking by inputing artifical throttle, reducing the negativ torq" +
        "ue off throttle for stability");
            this.srEngineBreaking.ValueChangeListener = null;
            // 
            // lblWearRPM
            // 
            this.lblWearRPM.AutoSize = true;
            this.lblWearRPM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWearRPM.Location = new System.Drawing.Point(108, 132);
            this.lblWearRPM.Name = "lblWearRPM";
            this.lblWearRPM.Size = new System.Drawing.Size(51, 13);
            this.lblWearRPM.TabIndex = 47;
            this.lblWearRPM.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblWearRPM, "Amount of rpm above the \"safe\" rpm at which point the engine wear doubles. Being " +
        "double this amount above the safe means 4x wear and so on");
            // 
            // lblWearTemp
            // 
            this.lblWearTemp.AutoSize = true;
            this.lblWearTemp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWearTemp.Location = new System.Drawing.Point(108, 146);
            this.lblWearTemp.Name = "lblWearTemp";
            this.lblWearTemp.Size = new System.Drawing.Size(51, 13);
            this.lblWearTemp.TabIndex = 48;
            this.lblWearTemp.Text = "No Data";
            this.toolTip1.SetToolTip(this.lblWearTemp, "Oil Temperatur above the \"safe\" oil temp at which point the engine wear doubles. " +
        "Being double this amount above the safe means 4x wear and so on");
            // 
            // treeUpgrades
            // 
            this.treeUpgrades.CheckBoxes = true;
            this.treeUpgrades.Location = new System.Drawing.Point(20, 19);
            this.treeUpgrades.Name = "treeUpgrades";
            this.treeUpgrades.Size = new System.Drawing.Size(258, 140);
            this.treeUpgrades.TabIndex = 0;
            this.toolTip1.SetToolTip(this.treeUpgrades, "Check the upgrades that you want to see");
            this.treeUpgrades.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeUpgrades_AfterCheck);
            this.treeUpgrades.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeUpgrades_AfterSelect);
            // 
            // srFinalDrive
            // 
            this.srFinalDrive.BackColor = System.Drawing.Color.Transparent;
            this.srFinalDrive.ChangeEnabled = true;
            this.srFinalDrive.Detachable = false;
            this.srFinalDrive.Location = new System.Drawing.Point(40, 238);
            this.srFinalDrive.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFinalDrive.Name = "srFinalDrive";
            this.srFinalDrive.Size = new System.Drawing.Size(134, 18);
            this.srFinalDrive.TabIndex = 7;
            this.toolTip1.SetToolTip(this.srFinalDrive, "Final Drive Ratio. This multiplies all Gear Ratios");
            this.srFinalDrive.ValueChangeListener = null;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 20);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(79, 13);
            this.label40.TabIndex = 9;
            this.label40.Text = "\"Give up\"-time:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 33);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(90, 13);
            this.label39.TabIndex = 10;
            this.label39.Text = "Preperation time:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(6, 46);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(83, 13);
            this.label36.TabIndex = 13;
            this.label36.Text = "Delay multiplier:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 59);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(84, 13);
            this.label37.TabIndex = 15;
            this.label37.Text = "Maximum delay:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(6, 16);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(46, 13);
            this.label46.TabIndex = 9;
            this.label46.Text = "Fill rate:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(6, 29);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(84, 13);
            this.label45.TabIndex = 10;
            this.label45.Text = "Maximum delay:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 42);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(113, 13);
            this.label42.TabIndex = 13;
            this.label42.Text = "Nozzle insertion time::";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(6, 55);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(106, 13);
            this.label38.TabIndex = 15;
            this.label38.Text = "Nozzle removal time:";
            // 
            // lblConcurrent2
            // 
            this.lblConcurrent2.AutoSize = true;
            this.lblConcurrent2.Location = new System.Drawing.Point(358, 258);
            this.lblConcurrent2.Name = "lblConcurrent2";
            this.lblConcurrent2.Size = new System.Drawing.Size(204, 13);
            this.lblConcurrent2.TabIndex = 17;
            this.lblConcurrent2.Text = "Simultaneous refuelling and tire changes:";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(6, 16);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(102, 13);
            this.label53.TabIndex = 9;
            this.label53.Text = "2 tires change time:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(6, 29);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(102, 13);
            this.label52.TabIndex = 10;
            this.label52.Text = "4 tires change time:";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(6, 42);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(144, 13);
            this.label49.TabIndex = 13;
            this.label49.Text = "Maximum delay on each tire:";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(6, 16);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(88, 13);
            this.label50.TabIndex = 9;
            this.label50.Text = "Aero repair time:";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(6, 29);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(119, 13);
            this.label48.TabIndex = 10;
            this.label48.Text = "Suspension repair time:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 42);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(84, 13);
            this.label43.TabIndex = 13;
            this.label43.Text = "Maximum delay:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(6, 55);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(101, 13);
            this.label44.TabIndex = 15;
            this.label44.Text = "Concurrent repairs:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 21);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(119, 13);
            this.label29.TabIndex = 12;
            this.label29.Text = "Front wing adjustment:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(6, 34);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(116, 13);
            this.label31.TabIndex = 14;
            this.label31.Text = "Rear wing adjustment:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(6, 47);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(102, 13);
            this.label32.TabIndex = 16;
            this.label32.Text = "Wedge adjustment:";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(6, 60);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(109, 13);
            this.label58.TabIndex = 18;
            this.label58.Text = "Radiator adjustment:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 73);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(113, 13);
            this.label56.TabIndex = 20;
            this.label56.Text = "Track bar adjustment:";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(6, 86);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(131, 13);
            this.label61.TabIndex = 22;
            this.label61.Text = "Tire pressure adjustment:";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 99);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(133, 13);
            this.label62.TabIndex = 24;
            this.label62.Text = "Spring rubber adjustment:";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(6, 112);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(97, 13);
            this.label63.TabIndex = 26;
            this.label63.Text = "Driver swap delay:";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(3, 147);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(93, 13);
            this.label60.TabIndex = 44;
            this.label60.Text = "Optimal tire temp:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 21);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(104, 13);
            this.label26.TabIndex = 22;
            this.label26.Text = "Optimal brake temp:";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(6, 35);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(96, 13);
            this.label67.TabIndex = 42;
            this.label67.Text = "Max brake torque:";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(6, 49);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(106, 13);
            this.label68.TabIndex = 44;
            this.label68.Text = "Brake disc thickness:";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(6, 64);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(110, 13);
            this.label70.TabIndex = 46;
            this.label70.Text = "Disc failure thickness:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 26);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(109, 13);
            this.label27.TabIndex = 44;
            this.label27.Text = "Optimal brake temps:";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(6, 40);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(96, 13);
            this.label69.TabIndex = 46;
            this.label69.Text = "Max brake torque:";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(6, 54);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(106, 13);
            this.label71.TabIndex = 48;
            this.label71.Text = "Brake disc thickness:";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(6, 68);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(110, 13);
            this.label73.TabIndex = 50;
            this.label73.Text = "Disc failure thickness:";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(25, 146);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(105, 13);
            this.label66.TabIndex = 30;
            this.label66.Text = "Onboard brake bias:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(12, 29);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Max. Torque:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 54);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Rev limit range:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 67);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Optimal Oil Temp:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(12, 80);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(97, 13);
            this.label16.TabIndex = 3;
            this.label16.Text = "Max safe Oil temp:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 94);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(89, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Average lifetime:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(12, 119);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(57, 13);
            this.label18.TabIndex = 5;
            this.label18.Text = "Safe RPM:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(12, 15);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(68, 13);
            this.label30.TabIndex = 17;
            this.label30.Text = "Max. Power:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(12, 42);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(91, 13);
            this.label28.TabIndex = 19;
            this.label28.Text = "Power-to-weight:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(12, 106);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(92, 13);
            this.label33.TabIndex = 38;
            this.label33.Text = "Lifetime variance:";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(12, 2);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(59, 13);
            this.label77.TabIndex = 43;
            this.label77.Text = "Aspiration:";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(6, 125);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(129, 13);
            this.label81.TabIndex = 30;
            this.label81.Text = "Concurrent driver swaps:";
            // 
            // labelRim2
            // 
            this.labelRim2.AutoSize = true;
            this.labelRim2.Location = new System.Drawing.Point(6, 150);
            this.labelRim2.Name = "labelRim2";
            this.labelRim2.Size = new System.Drawing.Size(96, 13);
            this.labelRim2.TabIndex = 47;
            this.labelRim2.Text = "Rear rim diameter:";
            // 
            // labelTire1Mass
            // 
            this.labelTire1Mass.AutoSize = true;
            this.labelTire1Mass.Location = new System.Drawing.Point(6, 163);
            this.labelTire1Mass.Name = "labelTire1Mass";
            this.labelTire1Mass.Size = new System.Drawing.Size(83, 13);
            this.labelTire1Mass.TabIndex = 49;
            this.labelTire1Mass.Text = "Front tire mass:";
            // 
            // labelTire2Mass
            // 
            this.labelTire2Mass.AutoSize = true;
            this.labelTire2Mass.Location = new System.Drawing.Point(6, 176);
            this.labelTire2Mass.Name = "labelTire2Mass";
            this.labelTire2Mass.Size = new System.Drawing.Size(80, 13);
            this.labelTire2Mass.TabIndex = 51;
            this.labelTire2Mass.Text = "Rear tire mass:";
            // 
            // labelRim1
            // 
            this.labelRim1.AutoSize = true;
            this.labelRim1.Location = new System.Drawing.Point(6, 137);
            this.labelRim1.Name = "labelRim1";
            this.labelRim1.Size = new System.Drawing.Size(99, 13);
            this.labelRim1.TabIndex = 53;
            this.labelRim1.Text = "Front rim diameter:";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(3, 160);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(112, 13);
            this.label84.TabIndex = 48;
            this.label84.Text = "Starting temperature:";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(6, 137);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(86, 13);
            this.label79.TabIndex = 53;
            this.label79.Text = "Activation temp:";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(6, 150);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(94, 13);
            this.label90.TabIndex = 47;
            this.label90.Text = "Heat history step:";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(6, 137);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(86, 13);
            this.label85.TabIndex = 53;
            this.label85.Text = "Activation temp:";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(6, 150);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(94, 13);
            this.label87.TabIndex = 47;
            this.label87.Text = "Heat history step:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 13);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(63, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Dry weight:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 26);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(58, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "CG height:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 52);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(55, 13);
            this.label23.TabIndex = 7;
            this.label23.Text = "Fuel tank:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 133);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(72, 13);
            this.label19.TabIndex = 14;
            this.label19.Text = "Speed limiter:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 146);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(89, 13);
            this.label20.TabIndex = 15;
            this.label20.Text = "Onboard starter:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 93);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(74, 13);
            this.label24.TabIndex = 18;
            this.label24.Text = "Upshift delay:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 119);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(78, 13);
            this.label25.TabIndex = 20;
            this.label25.Text = "Driven wheels:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(6, 199);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(59, 13);
            this.label34.TabIndex = 24;
            this.label34.Text = "Base drag:";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(6, 39);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(101, 13);
            this.label64.TabIndex = 26;
            this.label64.Text = "Weight distribution:";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(6, 212);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(108, 13);
            this.label65.TabIndex = 28;
            this.label65.Text = "Steering lock-to-lock:";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(6, 79);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(91, 13);
            this.label76.TabIndex = 30;
            this.label76.Text = "Number of gears:";
            // 
            // label12347
            // 
            this.label12347.AutoSize = true;
            this.label12347.Location = new System.Drawing.Point(6, 106);
            this.label12347.Name = "label12347";
            this.label12347.Size = new System.Drawing.Size(88, 13);
            this.label12347.TabIndex = 32;
            this.label12347.Text = "Downshift delay:";
            // 
            // grpUpgrades
            // 
            this.grpUpgrades.Controls.Add(this.btnGetUpgradeFile);
            this.grpUpgrades.Controls.Add(this.label75);
            this.grpUpgrades.Controls.Add(this.treeUpgrades);
            this.grpUpgrades.Controls.Add(this.txtUpgradeDescription);
            this.grpUpgrades.Location = new System.Drawing.Point(12, 376);
            this.grpUpgrades.Name = "grpUpgrades";
            this.grpUpgrades.Size = new System.Drawing.Size(682, 172);
            this.grpUpgrades.TabIndex = 44;
            this.grpUpgrades.TabStop = false;
            this.grpUpgrades.Text = "Upgrades:";
            this.grpUpgrades.Visible = false;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(374, 19);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(150, 13);
            this.label75.TabIndex = 44;
            this.label75.Text = "Selected upgrade description:";
            // 
            // tabPitstops
            // 
            this.tabPitstops.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPitstops.Controls.Add(this.tabControlPitstop);
            this.tabPitstops.Location = new System.Drawing.Point(4, 26);
            this.tabPitstops.Name = "tabPitstops";
            this.tabPitstops.Padding = new System.Windows.Forms.Padding(3);
            this.tabPitstops.Size = new System.Drawing.Size(678, 315);
            this.tabPitstops.TabIndex = 5;
            this.tabPitstops.Text = "Pitstop";
            // 
            // tabControlPitstop
            // 
            this.tabControlPitstop.Controls.Add(this.tabPage9);
            this.tabControlPitstop.Controls.Add(this.tabPitstopSim);
            this.tabControlPitstop.Location = new System.Drawing.Point(-3, 0);
            this.tabControlPitstop.Margin = new System.Windows.Forms.Padding(2);
            this.tabControlPitstop.Name = "tabControlPitstop";
            this.tabControlPitstop.SelectedIndex = 0;
            this.tabControlPitstop.Size = new System.Drawing.Size(686, 321);
            this.tabControlPitstop.TabIndex = 20;
            this.tabControlPitstop.SelectedIndexChanged += new System.EventHandler(this.tabControlPitstop_SelectedIndexChanged);
            // 
            // tabPage9
            // 
            this.tabPage9.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage9.Controls.Add(this.grpPitcrew);
            this.tabPage9.Controls.Add(this.lblConcurrent);
            this.tabPage9.Controls.Add(this.grpDamage);
            this.tabPage9.Controls.Add(this.lblConcurrent2);
            this.tabPage9.Controls.Add(this.grpTireChange);
            this.tabPage9.Controls.Add(this.grpAdjustments);
            this.tabPage9.Controls.Add(this.grpRefueling);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage9.Size = new System.Drawing.Size(678, 295);
            this.tabPage9.TabIndex = 0;
            this.tabPage9.Text = "Pitstop Times";
            // 
            // grpPitcrew
            // 
            this.grpPitcrew.Controls.Add(this.lblMaxDelay);
            this.grpPitcrew.Controls.Add(this.label37);
            this.grpPitcrew.Controls.Add(this.lblDelayMulti);
            this.grpPitcrew.Controls.Add(this.label36);
            this.grpPitcrew.Controls.Add(this.lblPreparation);
            this.grpPitcrew.Controls.Add(this.lblGiveup);
            this.grpPitcrew.Controls.Add(this.label39);
            this.grpPitcrew.Controls.Add(this.label40);
            this.grpPitcrew.Location = new System.Drawing.Point(6, 8);
            this.grpPitcrew.Name = "grpPitcrew";
            this.grpPitcrew.Size = new System.Drawing.Size(270, 88);
            this.grpPitcrew.TabIndex = 0;
            this.grpPitcrew.TabStop = false;
            this.grpPitcrew.Text = "Pitcrew:";
            // 
            // grpDamage
            // 
            this.grpDamage.Controls.Add(this.lblConcurRepair);
            this.grpDamage.Controls.Add(this.label44);
            this.grpDamage.Controls.Add(this.lblDamageDelay);
            this.grpDamage.Controls.Add(this.label43);
            this.grpDamage.Controls.Add(this.lblDamageSusp);
            this.grpDamage.Controls.Add(this.lblDamageAero);
            this.grpDamage.Controls.Add(this.label48);
            this.grpDamage.Controls.Add(this.label50);
            this.grpDamage.Location = new System.Drawing.Point(360, 10);
            this.grpDamage.Name = "grpDamage";
            this.grpDamage.Size = new System.Drawing.Size(297, 79);
            this.grpDamage.TabIndex = 15;
            this.grpDamage.TabStop = false;
            this.grpDamage.Text = "Damage repair:";
            // 
            // grpTireChange
            // 
            this.grpTireChange.Controls.Add(this.lblPressureChangePossible);
            this.grpTireChange.Controls.Add(this.label93);
            this.grpTireChange.Controls.Add(this.lblDelayTirechange);
            this.grpTireChange.Controls.Add(this.label49);
            this.grpTireChange.Controls.Add(this.lblChange4tires);
            this.grpTireChange.Controls.Add(this.lblChange2tires);
            this.grpTireChange.Controls.Add(this.label52);
            this.grpTireChange.Controls.Add(this.label53);
            this.grpTireChange.Location = new System.Drawing.Point(360, 176);
            this.grpTireChange.Name = "grpTireChange";
            this.grpTireChange.Size = new System.Drawing.Size(297, 78);
            this.grpTireChange.TabIndex = 2;
            this.grpTireChange.TabStop = false;
            this.grpTireChange.Text = "Tire change:";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(6, 55);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(130, 13);
            this.label93.TabIndex = 15;
            this.label93.Text = "Pressure change allowed:";
            // 
            // grpAdjustments
            // 
            this.grpAdjustments.Controls.Add(this.lblConcurrentDriver);
            this.grpAdjustments.Controls.Add(this.label81);
            this.grpAdjustments.Controls.Add(this.lblDriverswap);
            this.grpAdjustments.Controls.Add(this.label63);
            this.grpAdjustments.Controls.Add(this.lblSpringRubber);
            this.grpAdjustments.Controls.Add(this.label62);
            this.grpAdjustments.Controls.Add(this.lblPressureAdjustment);
            this.grpAdjustments.Controls.Add(this.label61);
            this.grpAdjustments.Controls.Add(this.lblTrackbar);
            this.grpAdjustments.Controls.Add(this.label56);
            this.grpAdjustments.Controls.Add(this.lblRadiator);
            this.grpAdjustments.Controls.Add(this.label58);
            this.grpAdjustments.Controls.Add(this.lblWedge);
            this.grpAdjustments.Controls.Add(this.label32);
            this.grpAdjustments.Controls.Add(this.lblRearWing);
            this.grpAdjustments.Controls.Add(this.label31);
            this.grpAdjustments.Controls.Add(this.lblFrontWing);
            this.grpAdjustments.Controls.Add(this.label29);
            this.grpAdjustments.Location = new System.Drawing.Point(6, 98);
            this.grpAdjustments.Name = "grpAdjustments";
            this.grpAdjustments.Size = new System.Drawing.Size(270, 154);
            this.grpAdjustments.TabIndex = 19;
            this.grpAdjustments.TabStop = false;
            this.grpAdjustments.Text = "Miscellaneous:";
            // 
            // grpRefueling
            // 
            this.grpRefueling.Controls.Add(this.lblNozzleRemoval);
            this.grpRefueling.Controls.Add(this.label38);
            this.grpRefueling.Controls.Add(this.lblNozzleInsertion);
            this.grpRefueling.Controls.Add(this.label42);
            this.grpRefueling.Controls.Add(this.lblMaxFuelDelay);
            this.grpRefueling.Controls.Add(this.lblFuelFillRate);
            this.grpRefueling.Controls.Add(this.label45);
            this.grpRefueling.Controls.Add(this.label46);
            this.grpRefueling.Location = new System.Drawing.Point(360, 91);
            this.grpRefueling.Name = "grpRefueling";
            this.grpRefueling.Size = new System.Drawing.Size(297, 81);
            this.grpRefueling.TabIndex = 1;
            this.grpRefueling.TabStop = false;
            this.grpRefueling.Text = "Refueling:";
            // 
            // tabPitstopSim
            // 
            this.tabPitstopSim.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPitstopSim.Controls.Add(this.grpPitResult);
            this.tabPitstopSim.Controls.Add(this.chkPitStopManualOverwrite);
            this.tabPitstopSim.Controls.Add(this.groupBox5);
            this.tabPitstopSim.Controls.Add(this.btnSimulatePitstop);
            this.tabPitstopSim.Location = new System.Drawing.Point(4, 22);
            this.tabPitstopSim.Margin = new System.Windows.Forms.Padding(2);
            this.tabPitstopSim.Name = "tabPitstopSim";
            this.tabPitstopSim.Padding = new System.Windows.Forms.Padding(2);
            this.tabPitstopSim.Size = new System.Drawing.Size(678, 295);
            this.tabPitstopSim.TabIndex = 1;
            this.tabPitstopSim.Text = "Pitstop Simulation";
            // 
            // grpPitResult
            // 
            this.grpPitResult.Controls.Add(this.lblPitTotal);
            this.grpPitResult.Controls.Add(this.groupBox7);
            this.grpPitResult.Controls.Add(this.lblPitstopTime);
            this.grpPitResult.Location = new System.Drawing.Point(329, 6);
            this.grpPitResult.Name = "grpPitResult";
            this.grpPitResult.Size = new System.Drawing.Size(345, 181);
            this.grpPitResult.TabIndex = 21;
            this.grpPitResult.TabStop = false;
            this.grpPitResult.Text = "Result:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lblPitMisc);
            this.groupBox7.Controls.Add(this.label92);
            this.groupBox7.Controls.Add(this.lblPitDamage);
            this.groupBox7.Controls.Add(this.label57);
            this.groupBox7.Controls.Add(this.lblPitTires);
            this.groupBox7.Controls.Add(this.label55);
            this.groupBox7.Controls.Add(this.lblPitFuel);
            this.groupBox7.Controls.Add(this.label54);
            this.groupBox7.Location = new System.Drawing.Point(6, 19);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(272, 98);
            this.groupBox7.TabIndex = 10;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Individual pit times:";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(15, 60);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(107, 13);
            this.label92.TabIndex = 6;
            this.label92.Text = "Other Changes time:";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(15, 46);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(104, 13);
            this.label57.TabIndex = 4;
            this.label57.Text = "Damage repair time:";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(15, 33);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(90, 13);
            this.label55.TabIndex = 2;
            this.label55.Text = "Tire change time:";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(15, 20);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(79, 13);
            this.label54.TabIndex = 0;
            this.label54.Text = "Refueling time:";
            // 
            // lblPitstopTime
            // 
            this.lblPitstopTime.AutoSize = true;
            this.lblPitstopTime.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPitstopTime.Location = new System.Drawing.Point(17, 139);
            this.lblPitstopTime.Name = "lblPitstopTime";
            this.lblPitstopTime.Size = new System.Drawing.Size(142, 13);
            this.lblPitstopTime.TabIndex = 8;
            this.lblPitstopTime.Text = "Projected total pit time:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.chkDriverswap);
            this.groupBox5.Controls.Add(this.chkTrackbar);
            this.groupBox5.Controls.Add(this.chkSpringRubber);
            this.groupBox5.Controls.Add(this.chkWedge);
            this.groupBox5.Controls.Add(this.chkTirePressure);
            this.groupBox5.Controls.Add(this.chkRadiatorTap);
            this.groupBox5.Controls.Add(this.chkRearWing);
            this.groupBox5.Controls.Add(this.chkFrontWing);
            this.groupBox5.Controls.Add(this.chkSuspDamage);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.label47);
            this.groupBox5.Controls.Add(this.chkAeroDamage);
            this.groupBox5.Controls.Add(this.dropChangeTires);
            this.groupBox5.Controls.Add(this.txtAmountFuel);
            this.groupBox5.Controls.Add(this.label51);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(303, 181);
            this.groupBox5.TabIndex = 20;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Input:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(6, 16);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(119, 13);
            this.label35.TabIndex = 0;
            this.label35.Text = "Amount of fuel to tank:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(6, 35);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(136, 13);
            this.label47.TabIndex = 1;
            this.label47.Text = "Amount of tires to change:";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(185, 15);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(30, 13);
            this.label51.TabIndex = 3;
            this.label51.Text = "liters";
            // 
            // grpCompound4
            // 
            this.grpCompound4.Controls.Add(this.CheckCompounds4);
            this.grpCompound4.Location = new System.Drawing.Point(13, 19);
            this.grpCompound4.Name = "grpCompound4";
            this.grpCompound4.Size = new System.Drawing.Size(211, 152);
            this.grpCompound4.TabIndex = 38;
            this.grpCompound4.TabStop = false;
            this.grpCompound4.Text = "Compound:";
            // 
            // zedTire2
            // 
            this.zedTire2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.zedTire2.IsShowPointValues = true;
            this.zedTire2.Location = new System.Drawing.Point(230, 6);
            this.zedTire2.Margin = new System.Windows.Forms.Padding(4);
            this.zedTire2.Name = "zedTire2";
            this.zedTire2.ScrollGrace = 0D;
            this.zedTire2.ScrollMaxX = 0D;
            this.zedTire2.ScrollMaxY = 0D;
            this.zedTire2.ScrollMaxY2 = 0D;
            this.zedTire2.ScrollMinX = 0D;
            this.zedTire2.ScrollMinY = 0D;
            this.zedTire2.ScrollMinY2 = 0D;
            this.zedTire2.Size = new System.Drawing.Size(440, 281);
            this.zedTire2.TabIndex = 35;
            // 
            // grpCompound3
            // 
            this.grpCompound3.Controls.Add(this.CheckCompounds3);
            this.grpCompound3.Location = new System.Drawing.Point(13, 19);
            this.grpCompound3.Name = "grpCompound3";
            this.grpCompound3.Size = new System.Drawing.Size(211, 152);
            this.grpCompound3.TabIndex = 38;
            this.grpCompound3.TabStop = false;
            this.grpCompound3.Text = "Compound:";
            // 
            // zedTire
            // 
            this.zedTire.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.zedTire.IsShowPointValues = true;
            this.zedTire.Location = new System.Drawing.Point(230, 6);
            this.zedTire.Margin = new System.Windows.Forms.Padding(4);
            this.zedTire.Name = "zedTire";
            this.zedTire.ScrollGrace = 0D;
            this.zedTire.ScrollMaxX = 0D;
            this.zedTire.ScrollMaxY = 0D;
            this.zedTire.ScrollMaxY2 = 0D;
            this.zedTire.ScrollMinX = 0D;
            this.zedTire.ScrollMinY = 0D;
            this.zedTire.ScrollMinY2 = 0D;
            this.zedTire.Size = new System.Drawing.Size(440, 281);
            this.zedTire.TabIndex = 34;
            // 
            // zedTireTemps
            // 
            this.zedTireTemps.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.zedTireTemps.IsShowPointValues = true;
            this.zedTireTemps.Location = new System.Drawing.Point(230, 6);
            this.zedTireTemps.Margin = new System.Windows.Forms.Padding(4);
            this.zedTireTemps.Name = "zedTireTemps";
            this.zedTireTemps.ScrollGrace = 0D;
            this.zedTireTemps.ScrollMaxX = 0D;
            this.zedTireTemps.ScrollMaxY = 0D;
            this.zedTireTemps.ScrollMaxY2 = 0D;
            this.zedTireTemps.ScrollMinX = 0D;
            this.zedTireTemps.ScrollMinY = 0D;
            this.zedTireTemps.ScrollMinY2 = 0D;
            this.zedTireTemps.Size = new System.Drawing.Size(440, 281);
            this.zedTireTemps.TabIndex = 40;
            // 
            // grpCompound2
            // 
            this.grpCompound2.Controls.Add(this.lblTireStartTemp);
            this.grpCompound2.Controls.Add(this.label41);
            this.grpCompound2.Controls.Add(this.lblTireTemp);
            this.grpCompound2.Controls.Add(this.label60);
            this.grpCompound2.Controls.Add(this.label84);
            this.grpCompound2.Controls.Add(this.CheckCompounds2);
            this.grpCompound2.Location = new System.Drawing.Point(13, 19);
            this.grpCompound2.Name = "grpCompound2";
            this.grpCompound2.Size = new System.Drawing.Size(211, 201);
            this.grpCompound2.TabIndex = 39;
            this.grpCompound2.TabStop = false;
            this.grpCompound2.Text = "Compound:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(3, 134);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(82, 13);
            this.label41.TabIndex = 47;
            this.label41.Text = "Selected tire:";
            // 
            // tabTires2
            // 
            this.tabTires2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabTires2.Controls.Add(this.tabTires);
            this.tabTires2.Location = new System.Drawing.Point(4, 26);
            this.tabTires2.Name = "tabTires2";
            this.tabTires2.Size = new System.Drawing.Size(678, 315);
            this.tabTires2.TabIndex = 3;
            this.tabTires2.Text = "Tires";
            // 
            // tabTires
            // 
            this.tabTires.Controls.Add(this.tabPage2);
            this.tabTires.Controls.Add(this.tabPage3);
            this.tabTires.Controls.Add(this.tabPage4);
            this.tabTires.Controls.Add(this.tabPage5);
            this.tabTires.Controls.Add(this.tabPage6);
            this.tabTires.Controls.Add(this.tabPage7);
            this.tabTires.Location = new System.Drawing.Point(-3, 0);
            this.tabTires.Name = "tabTires";
            this.tabTires.Padding = new System.Drawing.Point(18, 3);
            this.tabTires.SelectedIndex = 0;
            this.tabTires.Size = new System.Drawing.Size(690, 319);
            this.tabTires.TabIndex = 45;
            this.tabTires.SelectedIndexChanged += new System.EventHandler(this.tabTires_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage2.Controls.Add(this.label59);
            this.tabPage2.Controls.Add(this.pictureBox2);
            this.tabPage2.Controls.Add(this.zedWear);
            this.tabPage2.Controls.Add(this.grpCompound1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(682, 293);
            this.tabPage2.TabIndex = 0;
            this.tabPage2.Text = "General info";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Tahoma", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(347, 277);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(214, 12);
            this.label59.TabIndex = 43;
            this.label59.Text = "* only the influence on the static grip is shown";
            // 
            // zedWear
            // 
            this.zedWear.IsShowPointValues = true;
            this.zedWear.Location = new System.Drawing.Point(230, 6);
            this.zedWear.Margin = new System.Windows.Forms.Padding(4);
            this.zedWear.Name = "zedWear";
            this.zedWear.ScrollGrace = 0D;
            this.zedWear.ScrollMaxX = 0D;
            this.zedWear.ScrollMaxY = 0D;
            this.zedWear.ScrollMaxY2 = 0D;
            this.zedWear.ScrollMinX = 0D;
            this.zedWear.ScrollMinY = 0D;
            this.zedWear.ScrollMinY2 = 0D;
            this.zedWear.Size = new System.Drawing.Size(440, 266);
            this.zedWear.TabIndex = 0;
            // 
            // grpCompound1
            // 
            this.grpCompound1.Controls.Add(this.lblFrontTireRim);
            this.grpCompound1.Controls.Add(this.labelRim1);
            this.grpCompound1.Controls.Add(this.lblRearTireMass);
            this.grpCompound1.Controls.Add(this.labelTire2Mass);
            this.grpCompound1.Controls.Add(this.lblFrontTireMass);
            this.grpCompound1.Controls.Add(this.labelTire1Mass);
            this.grpCompound1.Controls.Add(this.lblRearTireRim);
            this.grpCompound1.Controls.Add(this.labelRim2);
            this.grpCompound1.Controls.Add(this.CheckCompounds1);
            this.grpCompound1.Location = new System.Drawing.Point(13, 19);
            this.grpCompound1.Name = "grpCompound1";
            this.grpCompound1.Size = new System.Drawing.Size(211, 242);
            this.grpCompound1.TabIndex = 37;
            this.grpCompound1.TabStop = false;
            this.grpCompound1.Text = "Compound:";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage3.Controls.Add(this.pictureBox1);
            this.tabPage3.Controls.Add(this.zedTireTemps);
            this.tabPage3.Controls.Add(this.grpCompound2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(682, 293);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "Temperatures";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage4.Controls.Add(this.pictureBox3);
            this.tabPage4.Controls.Add(this.zedTire);
            this.tabPage4.Controls.Add(this.grpCompound3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(682, 293);
            this.tabPage4.TabIndex = 2;
            this.tabPage4.Text = "Wear-grip 1";
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage5.Controls.Add(this.pictureBox4);
            this.tabPage5.Controls.Add(this.zedTire2);
            this.tabPage5.Controls.Add(this.grpCompound4);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(682, 293);
            this.tabPage5.TabIndex = 3;
            this.tabPage5.Text = "Wear-grip 2";
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage6.Controls.Add(this.pictureBox5);
            this.tabPage6.Controls.Add(this.grpThermal1);
            this.tabPage6.Controls.Add(this.zedThermal1);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(682, 293);
            this.tabPage6.TabIndex = 4;
            this.tabPage6.Text = "Thermal degradation 1";
            // 
            // grpThermal1
            // 
            this.grpThermal1.Controls.Add(this.CheckCompounds5);
            this.grpThermal1.Controls.Add(this.lblDegActivation);
            this.grpThermal1.Controls.Add(this.label79);
            this.grpThermal1.Controls.Add(this.lblDegHistoryStep);
            this.grpThermal1.Controls.Add(this.label90);
            this.grpThermal1.Location = new System.Drawing.Point(13, 19);
            this.grpThermal1.Name = "grpThermal1";
            this.grpThermal1.Size = new System.Drawing.Size(211, 204);
            this.grpThermal1.TabIndex = 38;
            this.grpThermal1.TabStop = false;
            this.grpThermal1.Text = "Compound:";
            // 
            // zedThermal1
            // 
            this.zedThermal1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.zedThermal1.IsShowPointValues = true;
            this.zedThermal1.Location = new System.Drawing.Point(230, 6);
            this.zedThermal1.Margin = new System.Windows.Forms.Padding(4);
            this.zedThermal1.Name = "zedThermal1";
            this.zedThermal1.ScrollGrace = 0D;
            this.zedThermal1.ScrollMaxX = 0D;
            this.zedThermal1.ScrollMaxY = 0D;
            this.zedThermal1.ScrollMaxY2 = 0D;
            this.zedThermal1.ScrollMinX = 0D;
            this.zedThermal1.ScrollMinY = 0D;
            this.zedThermal1.ScrollMinY2 = 0D;
            this.zedThermal1.Size = new System.Drawing.Size(440, 281);
            this.zedThermal1.TabIndex = 36;
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage7.Controls.Add(this.pictureBox6);
            this.tabPage7.Controls.Add(this.grpThermal2);
            this.tabPage7.Controls.Add(this.zedThermal2);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(682, 293);
            this.tabPage7.TabIndex = 5;
            this.tabPage7.Text = "Thermal degradation 2";
            // 
            // grpThermal2
            // 
            this.grpThermal2.Controls.Add(this.lblInvalidTemp);
            this.grpThermal2.Controls.Add(this.txtCustomTemp);
            this.grpThermal2.Controls.Add(this.label88);
            this.grpThermal2.Controls.Add(this.CheckCompounds6);
            this.grpThermal2.Controls.Add(this.lblDegActivation2);
            this.grpThermal2.Controls.Add(this.label85);
            this.grpThermal2.Controls.Add(this.lblDegHistoryStep2);
            this.grpThermal2.Controls.Add(this.label87);
            this.grpThermal2.Location = new System.Drawing.Point(13, 19);
            this.grpThermal2.Name = "grpThermal2";
            this.grpThermal2.Size = new System.Drawing.Size(211, 238);
            this.grpThermal2.TabIndex = 40;
            this.grpThermal2.TabStop = false;
            this.grpThermal2.Text = "Compound:";
            // 
            // lblInvalidTemp
            // 
            this.lblInvalidTemp.AutoSize = true;
            this.lblInvalidTemp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInvalidTemp.Location = new System.Drawing.Point(116, 206);
            this.lblInvalidTemp.Name = "lblInvalidTemp";
            this.lblInvalidTemp.Size = new System.Drawing.Size(36, 13);
            this.lblInvalidTemp.TabIndex = 58;
            this.lblInvalidTemp.Text = "error";
            // 
            // CheckCompounds6
            // 
            this.CheckCompounds6.FormattingEnabled = true;
            this.CheckCompounds6.Location = new System.Drawing.Point(6, 19);
            this.CheckCompounds6.Name = "CheckCompounds6";
            this.CheckCompounds6.Size = new System.Drawing.Size(183, 69);
            this.CheckCompounds6.TabIndex = 55;
            this.CheckCompounds6.SelectedIndexChanged += new System.EventHandler(this.CheckCompounds_SelectedIndexChanged);
            // 
            // zedThermal2
            // 
            this.zedThermal2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.zedThermal2.IsShowPointValues = true;
            this.zedThermal2.Location = new System.Drawing.Point(230, 6);
            this.zedThermal2.Margin = new System.Windows.Forms.Padding(4);
            this.zedThermal2.Name = "zedThermal2";
            this.zedThermal2.ScrollGrace = 0D;
            this.zedThermal2.ScrollMaxX = 0D;
            this.zedThermal2.ScrollMaxY = 0D;
            this.zedThermal2.ScrollMaxY2 = 0D;
            this.zedThermal2.ScrollMinX = 0D;
            this.zedThermal2.ScrollMinY = 0D;
            this.zedThermal2.ScrollMinY2 = 0D;
            this.zedThermal2.Size = new System.Drawing.Size(440, 281);
            this.zedThermal2.TabIndex = 39;
            // 
            // checkShowUpgrades
            // 
            this.checkShowUpgrades.AutoSize = true;
            this.checkShowUpgrades.Location = new System.Drawing.Point(40, 338);
            this.checkShowUpgrades.Name = "checkShowUpgrades";
            this.checkShowUpgrades.Size = new System.Drawing.Size(100, 17);
            this.checkShowUpgrades.TabIndex = 2;
            this.checkShowUpgrades.Text = "Show upgrades";
            this.checkShowUpgrades.UseVisualStyleBackColor = true;
            this.checkShowUpgrades.CheckedChanged += new System.EventHandler(this.checkShowUpgrades_CheckedChanged);
            // 
            // tabBrakes
            // 
            this.tabBrakes.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabBrakes.Controls.Add(this.lblBrakeBias);
            this.tabBrakes.Controls.Add(this.label66);
            this.tabBrakes.Controls.Add(this.grpRearBrakes);
            this.tabBrakes.Controls.Add(this.grpFrontBrakes);
            this.tabBrakes.Location = new System.Drawing.Point(4, 26);
            this.tabBrakes.Name = "tabBrakes";
            this.tabBrakes.Padding = new System.Windows.Forms.Padding(3);
            this.tabBrakes.Size = new System.Drawing.Size(678, 315);
            this.tabBrakes.TabIndex = 8;
            this.tabBrakes.Text = "Brakes";
            // 
            // grpRearBrakes
            // 
            this.grpRearBrakes.Controls.Add(this.label74);
            this.grpRearBrakes.Controls.Add(this.label73);
            this.grpRearBrakes.Controls.Add(this.lblRearBrakeFail);
            this.grpRearBrakes.Controls.Add(this.label71);
            this.grpRearBrakes.Controls.Add(this.lblRearBrakeThick);
            this.grpRearBrakes.Controls.Add(this.label69);
            this.grpRearBrakes.Controls.Add(this.lblRearBrakeTorque);
            this.grpRearBrakes.Controls.Add(this.label27);
            this.grpRearBrakes.Controls.Add(this.lblRearBrakes);
            this.grpRearBrakes.Controls.Add(this.zedBrakeRear);
            this.grpRearBrakes.Location = new System.Drawing.Point(19, 183);
            this.grpRearBrakes.Name = "grpRearBrakes";
            this.grpRearBrakes.Size = new System.Drawing.Size(631, 93);
            this.grpRearBrakes.TabIndex = 44;
            this.grpRearBrakes.TabStop = false;
            this.grpRearBrakes.Text = "Rear brakes:";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(367, 19);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(155, 13);
            this.label74.TabIndex = 49;
            this.label74.Text = "Brake temperature range:";
            // 
            // grpFrontBrakes
            // 
            this.grpFrontBrakes.Controls.Add(this.label72);
            this.grpFrontBrakes.Controls.Add(this.label70);
            this.grpFrontBrakes.Controls.Add(this.lblFrontBrakeFail);
            this.grpFrontBrakes.Controls.Add(this.label68);
            this.grpFrontBrakes.Controls.Add(this.lblFrontBrakeThick);
            this.grpFrontBrakes.Controls.Add(this.label67);
            this.grpFrontBrakes.Controls.Add(this.lblFrontBrakeTorque);
            this.grpFrontBrakes.Controls.Add(this.label26);
            this.grpFrontBrakes.Controls.Add(this.lblFrontBrakes);
            this.grpFrontBrakes.Controls.Add(this.zedBrakeFront);
            this.grpFrontBrakes.Location = new System.Drawing.Point(19, 24);
            this.grpFrontBrakes.Name = "grpFrontBrakes";
            this.grpFrontBrakes.Size = new System.Drawing.Size(631, 95);
            this.grpFrontBrakes.TabIndex = 42;
            this.grpFrontBrakes.TabStop = false;
            this.grpFrontBrakes.Text = "Front brakes:";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(367, 16);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(155, 13);
            this.label72.TabIndex = 52;
            this.label72.Text = "Brake temperature range:";
            // 
            // tabEngine
            // 
            this.tabEngine.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabEngine.Controls.Add(this.tabEngineData);
            this.tabEngine.Controls.Add(this.radbAspTurbo);
            this.tabEngine.Controls.Add(this.radbAspNaturally);
            this.tabEngine.Controls.Add(this.chkBoostMapping);
            this.tabEngine.Controls.Add(this.cbBoostMapping);
            this.tabEngine.Controls.Add(this.chkEngineMixture);
            this.tabEngine.Controls.Add(this.cbFuelMixture);
            this.tabEngine.Controls.Add(this.lblTurbo);
            this.tabEngine.Controls.Add(this.btnRescale);
            this.tabEngine.Controls.Add(this.zedEngine);
            this.tabEngine.Location = new System.Drawing.Point(4, 26);
            this.tabEngine.Name = "tabEngine";
            this.tabEngine.Padding = new System.Windows.Forms.Padding(3);
            this.tabEngine.Size = new System.Drawing.Size(678, 315);
            this.tabEngine.TabIndex = 1;
            this.tabEngine.Text = "Engine";
            // 
            // tabEngineData
            // 
            this.tabEngineData.Controls.Add(this.tabPage1);
            this.tabEngineData.Controls.Add(this.tabPage8);
            this.tabEngineData.Location = new System.Drawing.Point(6, 2);
            this.tabEngineData.Margin = new System.Windows.Forms.Padding(2);
            this.tabEngineData.Name = "tabEngineData";
            this.tabEngineData.SelectedIndex = 0;
            this.tabEngineData.Size = new System.Drawing.Size(258, 187);
            this.tabEngineData.TabIndex = 11;
            this.tabEngineData.SelectedIndexChanged += new System.EventHandler(this.tabEngineData_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage1.Controls.Add(this.lblWearTemp);
            this.tabPage1.Controls.Add(this.lblWearRPM);
            this.tabPage1.Controls.Add(this.label179);
            this.tabPage1.Controls.Add(this.label178);
            this.tabPage1.Controls.Add(this.label77);
            this.tabPage1.Controls.Add(this.lblAspiration);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.lblLifetimeVar);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.label33);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.lblPowerWeight);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.lblPower);
            this.tabPage1.Controls.Add(this.lblOptimal);
            this.tabPage1.Controls.Add(this.label30);
            this.tabPage1.Controls.Add(this.lblSafeOil);
            this.tabPage1.Controls.Add(this.lblTorque);
            this.tabPage1.Controls.Add(this.lblLifetime);
            this.tabPage1.Controls.Add(this.lblRevlimit);
            this.tabPage1.Controls.Add(this.lblMaxrpm);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage1.Size = new System.Drawing.Size(250, 161);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Location = new System.Drawing.Point(12, 146);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(81, 13);
            this.label179.TabIndex = 46;
            this.label179.Text = "Wear x2 Temp:";
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(12, 132);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(76, 13);
            this.label178.TabIndex = 45;
            this.label178.Text = "Wear x2 RPM:";
            // 
            // tabPage8
            // 
            this.tabPage8.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage8.Controls.Add(this.label106);
            this.tabPage8.Controls.Add(this.lblFuelMaxRPM);
            this.tabPage8.Controls.Add(this.label103);
            this.tabPage8.Controls.Add(this.lblFuelMaxTorque);
            this.tabPage8.Controls.Add(this.label99);
            this.tabPage8.Controls.Add(this.lblFuelMaxPower);
            this.tabPage8.Controls.Add(this.label78);
            this.tabPage8.Controls.Add(this.lblFuelConsumtion);
            this.tabPage8.Controls.Add(this.label86);
            this.tabPage8.Controls.Add(this.lblEngineInertia);
            this.tabPage8.Controls.Add(this.label95);
            this.tabPage8.Controls.Add(this.lblTurboPressure);
            this.tabPage8.Controls.Add(this.CheckEngineFriction);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage8.Size = new System.Drawing.Size(250, 161);
            this.tabPage8.TabIndex = 1;
            this.tabPage8.Text = "Other";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(12, 94);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(82, 13);
            this.label106.TabIndex = 56;
            this.label106.Text = "@max rpm/min:";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(12, 80);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(96, 13);
            this.label103.TabIndex = 54;
            this.label103.Text = "@max torque/min:";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(12, 66);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(94, 13);
            this.label99.TabIndex = 52;
            this.label99.Text = "@max power/min:";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(12, 53);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(96, 13);
            this.label78.TabIndex = 50;
            this.label78.Text = "Fuel Consumption:";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(12, 39);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(84, 13);
            this.label86.TabIndex = 46;
            this.label86.Text = "Turbo Pressure:";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(12, 26);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(78, 13);
            this.label95.TabIndex = 48;
            this.label95.Text = "Engine Inertia:";
            // 
            // cbBoostMapping
            // 
            this.cbBoostMapping.FormattingEnabled = true;
            this.cbBoostMapping.Location = new System.Drawing.Point(119, 214);
            this.cbBoostMapping.Margin = new System.Windows.Forms.Padding(2);
            this.cbBoostMapping.Name = "cbBoostMapping";
            this.cbBoostMapping.Size = new System.Drawing.Size(84, 21);
            this.cbBoostMapping.TabIndex = 7;
            this.cbBoostMapping.SelectedIndexChanged += new System.EventHandler(this.cbBoostMapping_SelectedIndexChanged);
            // 
            // cbFuelMixture
            // 
            this.cbFuelMixture.FormattingEnabled = true;
            this.cbFuelMixture.Location = new System.Drawing.Point(119, 193);
            this.cbFuelMixture.Margin = new System.Windows.Forms.Padding(2);
            this.cbFuelMixture.Name = "cbFuelMixture";
            this.cbFuelMixture.Size = new System.Drawing.Size(84, 21);
            this.cbFuelMixture.TabIndex = 5;
            this.cbFuelMixture.SelectedIndexChanged += new System.EventHandler(this.cbFuelMixture_SelectedIndexChanged);
            // 
            // lblTurbo
            // 
            this.lblTurbo.AutoSize = true;
            this.lblTurbo.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.lblTurbo.ForeColor = System.Drawing.Color.Red;
            this.lblTurbo.Location = new System.Drawing.Point(21, 258);
            this.lblTurbo.Name = "lblTurbo";
            this.lblTurbo.Size = new System.Drawing.Size(176, 26);
            this.lblTurbo.TabIndex = 3;
            this.lblTurbo.Text = "Turbo\'s not completly implemented.\r\nPower values may be wrong!";
            this.lblTurbo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblTurbo.Visible = false;
            // 
            // zedEngine
            // 
            this.zedEngine.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.zedEngine.IsShowPointValues = true;
            this.zedEngine.Location = new System.Drawing.Point(270, 5);
            this.zedEngine.Margin = new System.Windows.Forms.Padding(4);
            this.zedEngine.Name = "zedEngine";
            this.zedEngine.ScrollGrace = 0D;
            this.zedEngine.ScrollMaxX = 0D;
            this.zedEngine.ScrollMaxY = 0D;
            this.zedEngine.ScrollMaxY2 = 0D;
            this.zedEngine.ScrollMinX = 0D;
            this.zedEngine.ScrollMinY = 0D;
            this.zedEngine.ScrollMinY2 = 0D;
            this.zedEngine.Size = new System.Drawing.Size(402, 300);
            this.zedEngine.TabIndex = 0;
            // 
            // tabGeneral
            // 
            this.tabGeneral.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabGeneral.Controls.Add(this.tabControlGeneral);
            this.tabGeneral.Controls.Add(this.grpTeam);
            this.tabGeneral.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabGeneral.ImageIndex = 0;
            this.tabGeneral.Location = new System.Drawing.Point(4, 26);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tabGeneral.Size = new System.Drawing.Size(678, 315);
            this.tabGeneral.TabIndex = 0;
            this.tabGeneral.Text = "General";
            // 
            // tabControlGeneral
            // 
            this.tabControlGeneral.Controls.Add(this.tabPage10);
            this.tabControlGeneral.Controls.Add(this.tabPage13);
            this.tabControlGeneral.Controls.Add(this.tabPage11);
            this.tabControlGeneral.Controls.Add(this.tabPage24);
            this.tabControlGeneral.Location = new System.Drawing.Point(6, 6);
            this.tabControlGeneral.Margin = new System.Windows.Forms.Padding(2);
            this.tabControlGeneral.Name = "tabControlGeneral";
            this.tabControlGeneral.SelectedIndex = 0;
            this.tabControlGeneral.Size = new System.Drawing.Size(283, 268);
            this.tabControlGeneral.TabIndex = 26;
            this.tabControlGeneral.SelectedIndexChanged += new System.EventHandler(this.tabControlGeneral_SelectedIndexChanged);
            // 
            // tabPage10
            // 
            this.tabPage10.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage10.Controls.Add(this.lblAntiStall);
            this.tabPage10.Controls.Add(this.label19);
            this.tabPage10.Controls.Add(this.label20);
            this.tabPage10.Controls.Add(this.label96);
            this.tabPage10.Controls.Add(this.lblSemiAuto);
            this.tabPage10.Controls.Add(this.label21);
            this.tabPage10.Controls.Add(this.lblLimiter);
            this.tabPage10.Controls.Add(this.label94);
            this.tabPage10.Controls.Add(this.lblDownshift);
            this.tabPage10.Controls.Add(this.lblStarter);
            this.tabPage10.Controls.Add(this.label22);
            this.tabPage10.Controls.Add(this.lblAntiRoll);
            this.tabPage10.Controls.Add(this.label12347);
            this.tabPage10.Controls.Add(this.lblTrackbarFly);
            this.tabPage10.Controls.Add(this.lblWeight);
            this.tabPage10.Controls.Add(this.label82);
            this.tabPage10.Controls.Add(this.lblNumberGears);
            this.tabPage10.Controls.Add(this.label80);
            this.tabPage10.Controls.Add(this.lblCG);
            this.tabPage10.Controls.Add(this.label76);
            this.tabPage10.Controls.Add(this.label23);
            this.tabPage10.Controls.Add(this.lblWheelRotation);
            this.tabPage10.Controls.Add(this.lblFuel);
            this.tabPage10.Controls.Add(this.label65);
            this.tabPage10.Controls.Add(this.label64);
            this.tabPage10.Controls.Add(this.lblDrag);
            this.tabPage10.Controls.Add(this.lblWeightDistribution);
            this.tabPage10.Controls.Add(this.label34);
            this.tabPage10.Controls.Add(this.label24);
            this.tabPage10.Controls.Add(this.lblWheels);
            this.tabPage10.Controls.Add(this.lblUpshift);
            this.tabPage10.Controls.Add(this.label25);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage10.Size = new System.Drawing.Size(275, 242);
            this.tabPage10.TabIndex = 0;
            this.tabPage10.Text = "General";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(6, 66);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(104, 13);
            this.label94.TabIndex = 38;
            this.label94.Text = "Semi-Auto Gearbox:";
            // 
            // tabPage13
            // 
            this.tabPage13.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage13.Controls.Add(this.label98);
            this.tabPage13.Controls.Add(this.lblAutoPit);
            this.tabPage13.Controls.Add(this.label101);
            this.tabPage13.Controls.Add(this.lblAutoBlip);
            this.tabPage13.Controls.Add(this.label112);
            this.tabPage13.Controls.Add(this.lblAutoLift);
            this.tabPage13.Controls.Add(this.label104);
            this.tabPage13.Controls.Add(this.label105);
            this.tabPage13.Controls.Add(this.lblSCLow);
            this.tabPage13.Controls.Add(this.lblSCHigh);
            this.tabPage13.Controls.Add(this.label108);
            this.tabPage13.Controls.Add(this.lblAutoShiftFull);
            this.tabPage13.Controls.Add(this.label110);
            this.tabPage13.Controls.Add(this.lblAutoShiftOne);
            this.tabPage13.Controls.Add(this.label83);
            this.tabPage13.Controls.Add(this.label97);
            this.tabPage13.Controls.Add(this.lblTCLow);
            this.tabPage13.Controls.Add(this.lblTCHigh);
            this.tabPage13.Controls.Add(this.label100);
            this.tabPage13.Controls.Add(this.lblABSHigh);
            this.tabPage13.Controls.Add(this.label102);
            this.tabPage13.Controls.Add(this.lblABSLow);
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage13.Size = new System.Drawing.Size(275, 242);
            this.tabPage13.TabIndex = 3;
            this.tabPage13.Text = "AID Penalties";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(6, 118);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(50, 13);
            this.label98.TabIndex = 44;
            this.label98.Text = "Auto-Pit:";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(6, 145);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(81, 13);
            this.label101.TabIndex = 46;
            this.label101.Text = "Autoclutch Blip:";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(6, 132);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(80, 13);
            this.label112.TabIndex = 48;
            this.label112.Text = "Autoclutch Lift:";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(6, 66);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(46, 13);
            this.label104.TabIndex = 36;
            this.label104.Text = "SC Low:";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 78);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(48, 13);
            this.label105.TabIndex = 37;
            this.label105.Text = "SC High:";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(6, 105);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(74, 13);
            this.label108.TabIndex = 40;
            this.label108.Text = "Autoshift Full:";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(6, 92);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(99, 13);
            this.label110.TabIndex = 42;
            this.label110.Text = "Autoshift one way:";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(6, 13);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(46, 13);
            this.label83.TabIndex = 28;
            this.label83.Text = "TC Low:";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(6, 26);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(48, 13);
            this.label97.TabIndex = 29;
            this.label97.Text = "TC High:";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(6, 52);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(54, 13);
            this.label100.TabIndex = 32;
            this.label100.Text = "ABS High:";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(6, 39);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(52, 13);
            this.label102.TabIndex = 34;
            this.label102.Text = "ABS Low:";
            // 
            // tabPage11
            // 
            this.tabPage11.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage11.Controls.Add(this.lblLeftWheelBase);
            this.tabPage11.Controls.Add(this.lblRearTrackWidth);
            this.tabPage11.Controls.Add(this.lblRightWheelBase);
            this.tabPage11.Controls.Add(this.lblFrontTrackWidth);
            this.tabPage11.Controls.Add(this.pictureBox7);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage11.Size = new System.Drawing.Size(275, 242);
            this.tabPage11.TabIndex = 4;
            this.tabPage11.Text = "Size";
            // 
            // lblLeftWheelBase
            // 
            this.lblLeftWheelBase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLeftWheelBase.AutoSize = true;
            this.lblLeftWheelBase.BackColor = System.Drawing.Color.Transparent;
            this.lblLeftWheelBase.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLeftWheelBase.Location = new System.Drawing.Point(5, 102);
            this.lblLeftWheelBase.Name = "lblLeftWheelBase";
            this.lblLeftWheelBase.Size = new System.Drawing.Size(49, 13);
            this.lblLeftWheelBase.TabIndex = 29;
            this.lblLeftWheelBase.Text = "0.000m";
            this.lblLeftWheelBase.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRearTrackWidth
            // 
            this.lblRearTrackWidth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRearTrackWidth.AutoSize = true;
            this.lblRearTrackWidth.BackColor = System.Drawing.Color.Transparent;
            this.lblRearTrackWidth.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRearTrackWidth.Location = new System.Drawing.Point(80, 190);
            this.lblRearTrackWidth.Name = "lblRearTrackWidth";
            this.lblRearTrackWidth.Size = new System.Drawing.Size(49, 13);
            this.lblRearTrackWidth.TabIndex = 28;
            this.lblRearTrackWidth.Text = "0.000m";
            this.lblRearTrackWidth.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblRightWheelBase
            // 
            this.lblRightWheelBase.AutoSize = true;
            this.lblRightWheelBase.BackColor = System.Drawing.Color.Transparent;
            this.lblRightWheelBase.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRightWheelBase.Location = new System.Drawing.Point(167, 102);
            this.lblRightWheelBase.Name = "lblRightWheelBase";
            this.lblRightWheelBase.Size = new System.Drawing.Size(49, 13);
            this.lblRightWheelBase.TabIndex = 27;
            this.lblRightWheelBase.Text = "0.000m";
            // 
            // lblFrontTrackWidth
            // 
            this.lblFrontTrackWidth.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFrontTrackWidth.AutoSize = true;
            this.lblFrontTrackWidth.BackColor = System.Drawing.Color.Transparent;
            this.lblFrontTrackWidth.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrontTrackWidth.Location = new System.Drawing.Point(80, 26);
            this.lblFrontTrackWidth.Name = "lblFrontTrackWidth";
            this.lblFrontTrackWidth.Size = new System.Drawing.Size(49, 13);
            this.lblFrontTrackWidth.TabIndex = 26;
            this.lblFrontTrackWidth.Text = "0.000m";
            this.lblFrontTrackWidth.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tabPage24
            // 
            this.tabPage24.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage24.Controls.Add(this.lblChassisINI);
            this.tabPage24.Controls.Add(this.label186);
            this.tabPage24.Controls.Add(this.lblUpgradesINI);
            this.tabPage24.Controls.Add(this.label185);
            this.tabPage24.Controls.Add(this.lblEngineINI);
            this.tabPage24.Controls.Add(this.lblTBC);
            this.tabPage24.Controls.Add(this.lblVEH);
            this.tabPage24.Controls.Add(this.label189);
            this.tabPage24.Controls.Add(this.lblHDV);
            this.tabPage24.Controls.Add(this.label192);
            this.tabPage24.Controls.Add(this.label193);
            this.tabPage24.Controls.Add(this.label194);
            this.tabPage24.Controls.Add(this.label195);
            this.tabPage24.Location = new System.Drawing.Point(4, 22);
            this.tabPage24.Name = "tabPage24";
            this.tabPage24.Size = new System.Drawing.Size(275, 242);
            this.tabPage24.TabIndex = 5;
            this.tabPage24.Text = "Other";
            // 
            // lblChassisINI
            // 
            this.lblChassisINI.AutoSize = true;
            this.lblChassisINI.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChassisINI.Location = new System.Drawing.Point(72, 89);
            this.lblChassisINI.Name = "lblChassisINI";
            this.lblChassisINI.Size = new System.Drawing.Size(51, 13);
            this.lblChassisINI.TabIndex = 25;
            this.lblChassisINI.Text = "No Data";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(9, 89);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(47, 13);
            this.label186.TabIndex = 24;
            this.label186.Text = "Chassis:";
            // 
            // lblUpgradesINI
            // 
            this.lblUpgradesINI.AutoSize = true;
            this.lblUpgradesINI.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUpgradesINI.Location = new System.Drawing.Point(72, 76);
            this.lblUpgradesINI.Name = "lblUpgradesINI";
            this.lblUpgradesINI.Size = new System.Drawing.Size(51, 13);
            this.lblUpgradesINI.TabIndex = 23;
            this.lblUpgradesINI.Text = "No Data";
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Location = new System.Drawing.Point(9, 76);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(57, 13);
            this.label185.TabIndex = 22;
            this.label185.Text = "Upgrades:";
            // 
            // lblEngineINI
            // 
            this.lblEngineINI.AutoSize = true;
            this.lblEngineINI.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEngineINI.Location = new System.Drawing.Point(72, 63);
            this.lblEngineINI.Name = "lblEngineINI";
            this.lblEngineINI.Size = new System.Drawing.Size(51, 13);
            this.lblEngineINI.TabIndex = 21;
            this.lblEngineINI.Text = "No Data";
            // 
            // lblTBC
            // 
            this.lblTBC.AutoSize = true;
            this.lblTBC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTBC.Location = new System.Drawing.Point(72, 50);
            this.lblTBC.Name = "lblTBC";
            this.lblTBC.Size = new System.Drawing.Size(51, 13);
            this.lblTBC.TabIndex = 20;
            this.lblTBC.Text = "No Data";
            // 
            // lblVEH
            // 
            this.lblVEH.AutoSize = true;
            this.lblVEH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVEH.Location = new System.Drawing.Point(72, 24);
            this.lblVEH.Name = "lblVEH";
            this.lblVEH.Size = new System.Drawing.Size(51, 13);
            this.lblVEH.TabIndex = 19;
            this.lblVEH.Text = "No Data";
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.Location = new System.Drawing.Point(9, 50);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(30, 13);
            this.label189.TabIndex = 18;
            this.label189.Text = "TBC:";
            // 
            // lblHDV
            // 
            this.lblHDV.AutoSize = true;
            this.lblHDV.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHDV.Location = new System.Drawing.Point(72, 37);
            this.lblHDV.Name = "lblHDV";
            this.lblHDV.Size = new System.Drawing.Size(51, 13);
            this.lblHDV.TabIndex = 17;
            this.lblHDV.Text = "No Data";
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Location = new System.Drawing.Point(9, 63);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(43, 13);
            this.label192.TabIndex = 15;
            this.label192.Text = "Engine:";
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Location = new System.Drawing.Point(9, 24);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(30, 13);
            this.label193.TabIndex = 14;
            this.label193.Text = "VEH:";
            // 
            // label194
            // 
            this.label194.AutoSize = true;
            this.label194.Location = new System.Drawing.Point(9, 37);
            this.label194.Name = "label194";
            this.label194.Size = new System.Drawing.Size(31, 13);
            this.label194.TabIndex = 13;
            this.label194.Text = "HDV:";
            // 
            // label195
            // 
            this.label195.AutoSize = true;
            this.label195.Location = new System.Drawing.Point(9, 11);
            this.label195.Name = "label195";
            this.label195.Size = new System.Drawing.Size(58, 13);
            this.label195.TabIndex = 12;
            this.label195.Text = "Filenames:";
            // 
            // grpTeam
            // 
            this.grpTeam.AutoSize = true;
            this.grpTeam.Controls.Add(this.caricon);
            this.grpTeam.Controls.Add(this.lblClass);
            this.grpTeam.Controls.Add(this.label91);
            this.grpTeam.Controls.Add(this.lblCategory);
            this.grpTeam.Controls.Add(this.lblWins);
            this.grpTeam.Controls.Add(this.label89);
            this.grpTeam.Controls.Add(this.lblPoles);
            this.grpTeam.Controls.Add(this.lblStarts);
            this.grpTeam.Controls.Add(this.lblHQ);
            this.grpTeam.Controls.Add(this.lblFullTeam);
            this.grpTeam.Controls.Add(this.lblFounded);
            this.grpTeam.Controls.Add(this.label12);
            this.grpTeam.Controls.Add(this.label11);
            this.grpTeam.Controls.Add(this.label10);
            this.grpTeam.Controls.Add(this.label9);
            this.grpTeam.Controls.Add(this.label8);
            this.grpTeam.Controls.Add(this.label6);
            this.grpTeam.Controls.Add(this.lblManufacturer);
            this.grpTeam.Controls.Add(this.label5);
            this.grpTeam.Controls.Add(this.lblEngine);
            this.grpTeam.Controls.Add(this.lblDriver);
            this.grpTeam.Controls.Add(this.lblTeam);
            this.grpTeam.Controls.Add(this.label7);
            this.grpTeam.Controls.Add(this.lblDescription);
            this.grpTeam.Controls.Add(this.lblNumber);
            this.grpTeam.Controls.Add(this.label4);
            this.grpTeam.Controls.Add(this.label3);
            this.grpTeam.Controls.Add(this.label2);
            this.grpTeam.Controls.Add(this.label1);
            this.grpTeam.Location = new System.Drawing.Point(294, 6);
            this.grpTeam.Name = "grpTeam";
            this.grpTeam.Size = new System.Drawing.Size(384, 275);
            this.grpTeam.TabIndex = 0;
            this.grpTeam.TabStop = false;
            this.grpTeam.Text = "Team information:";
            // 
            // lblClass
            // 
            this.lblClass.AutoSize = true;
            this.lblClass.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClass.Location = new System.Drawing.Point(86, 133);
            this.lblClass.Name = "lblClass";
            this.lblClass.Size = new System.Drawing.Size(51, 13);
            this.lblClass.TabIndex = 25;
            this.lblClass.Text = "No Data";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(7, 133);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(55, 13);
            this.label91.TabIndex = 24;
            this.label91.Text = "Class(es):";
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategory.Location = new System.Drawing.Point(86, 146);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(51, 13);
            this.lblCategory.TabIndex = 13;
            this.lblCategory.Text = "No Data";
            // 
            // lblWins
            // 
            this.lblWins.AutoSize = true;
            this.lblWins.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWins.Location = new System.Drawing.Point(95, 238);
            this.lblWins.Name = "lblWins";
            this.lblWins.Size = new System.Drawing.Size(51, 13);
            this.lblWins.TabIndex = 23;
            this.lblWins.Text = "No Data";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(7, 146);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(56, 13);
            this.label89.TabIndex = 12;
            this.label89.Text = "Category:";
            // 
            // lblPoles
            // 
            this.lblPoles.AutoSize = true;
            this.lblPoles.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPoles.Location = new System.Drawing.Point(95, 225);
            this.lblPoles.Name = "lblPoles";
            this.lblPoles.Size = new System.Drawing.Size(51, 13);
            this.lblPoles.TabIndex = 22;
            this.lblPoles.Text = "No Data";
            // 
            // lblStarts
            // 
            this.lblStarts.AutoSize = true;
            this.lblStarts.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStarts.Location = new System.Drawing.Point(95, 212);
            this.lblStarts.Name = "lblStarts";
            this.lblStarts.Size = new System.Drawing.Size(51, 13);
            this.lblStarts.TabIndex = 21;
            this.lblStarts.Text = "No Data";
            // 
            // lblHQ
            // 
            this.lblHQ.AutoSize = true;
            this.lblHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHQ.Location = new System.Drawing.Point(95, 199);
            this.lblHQ.Name = "lblHQ";
            this.lblHQ.Size = new System.Drawing.Size(51, 13);
            this.lblHQ.TabIndex = 20;
            this.lblHQ.Text = "No Data";
            // 
            // lblFullTeam
            // 
            this.lblFullTeam.AutoSize = true;
            this.lblFullTeam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFullTeam.Location = new System.Drawing.Point(95, 173);
            this.lblFullTeam.Name = "lblFullTeam";
            this.lblFullTeam.Size = new System.Drawing.Size(51, 13);
            this.lblFullTeam.TabIndex = 19;
            this.lblFullTeam.Text = "No Data";
            // 
            // lblFounded
            // 
            this.lblFounded.AutoSize = true;
            this.lblFounded.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFounded.Location = new System.Drawing.Point(95, 186);
            this.lblFounded.Name = "lblFounded";
            this.lblFounded.Size = new System.Drawing.Size(51, 13);
            this.lblFounded.TabIndex = 18;
            this.lblFounded.Text = "No Data";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(7, 238);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "Team Wins:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 225);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Team Poles:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 212);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 15;
            this.label10.Text = "Team Starts:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 199);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Headquarters:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 186);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Team Founded:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 173);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Full Teamname:";
            // 
            // lblManufacturer
            // 
            this.lblManufacturer.AutoSize = true;
            this.lblManufacturer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManufacturer.Location = new System.Drawing.Point(86, 85);
            this.lblManufacturer.Name = "lblManufacturer";
            this.lblManufacturer.Size = new System.Drawing.Size(51, 13);
            this.lblManufacturer.TabIndex = 11;
            this.lblManufacturer.Text = "No Data";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Manufacturer:";
            // 
            // lblEngine
            // 
            this.lblEngine.AutoSize = true;
            this.lblEngine.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEngine.Location = new System.Drawing.Point(86, 72);
            this.lblEngine.Name = "lblEngine";
            this.lblEngine.Size = new System.Drawing.Size(51, 13);
            this.lblEngine.TabIndex = 9;
            this.lblEngine.Text = "No Data";
            // 
            // lblDriver
            // 
            this.lblDriver.AutoSize = true;
            this.lblDriver.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDriver.Location = new System.Drawing.Point(86, 59);
            this.lblDriver.Name = "lblDriver";
            this.lblDriver.Size = new System.Drawing.Size(51, 13);
            this.lblDriver.TabIndex = 8;
            this.lblDriver.Text = "No Data";
            // 
            // lblTeam
            // 
            this.lblTeam.AutoSize = true;
            this.lblTeam.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeam.Location = new System.Drawing.Point(86, 33);
            this.lblTeam.Name = "lblTeam";
            this.lblTeam.Size = new System.Drawing.Size(51, 13);
            this.lblTeam.TabIndex = 7;
            this.lblTeam.Text = "No Data";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Driver:";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(86, 46);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(51, 13);
            this.lblDescription.TabIndex = 5;
            this.lblDescription.Text = "No Data";
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumber.Location = new System.Drawing.Point(86, 20);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(51, 13);
            this.lblNumber.TabIndex = 4;
            this.lblNumber.Text = "No Data";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Engine:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Team:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Description:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Number:";
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.tabGeneral);
            this.tabs.Controls.Add(this.tabEngine);
            this.tabs.Controls.Add(this.tabBrakes);
            this.tabs.Controls.Add(this.tabTires2);
            this.tabs.Controls.Add(this.tabPitstops);
            this.tabs.Controls.Add(this.tabPage12);
            this.tabs.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabs.Location = new System.Drawing.Point(6, 26);
            this.tabs.Name = "tabs";
            this.tabs.Padding = new System.Drawing.Point(34, 5);
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(686, 345);
            this.tabs.TabIndex = 0;
            this.tabs.Tag = "";
            this.tabs.SelectedIndexChanged += new System.EventHandler(this.tabs_SelectedIndexChanged);
            // 
            // tabPage12
            // 
            this.tabPage12.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage12.Controls.Add(this.tabSetupControl);
            this.tabPage12.Location = new System.Drawing.Point(4, 26);
            this.tabPage12.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage12.Size = new System.Drawing.Size(678, 315);
            this.tabPage12.TabIndex = 9;
            this.tabPage12.Text = "Setup";
            // 
            // tabSetupControl
            // 
            this.tabSetupControl.Controls.Add(this.tabPage14);
            this.tabSetupControl.Controls.Add(this.tabPage15);
            this.tabSetupControl.Controls.Add(this.tabPage16);
            this.tabSetupControl.Controls.Add(this.tabPage17);
            this.tabSetupControl.Controls.Add(this.tabPage18);
            this.tabSetupControl.Controls.Add(this.tabPage19);
            this.tabSetupControl.Controls.Add(this.tabPage20);
            this.tabSetupControl.Controls.Add(this.tabPage23);
            this.tabSetupControl.Location = new System.Drawing.Point(-3, 0);
            this.tabSetupControl.Margin = new System.Windows.Forms.Padding(2);
            this.tabSetupControl.Name = "tabSetupControl";
            this.tabSetupControl.SelectedIndex = 0;
            this.tabSetupControl.Size = new System.Drawing.Size(686, 321);
            this.tabSetupControl.TabIndex = 0;
            this.tabSetupControl.SelectedIndexChanged += new System.EventHandler(this.tabSetupControl_SelectedIndexChanged);
            // 
            // tabPage14
            // 
            this.tabPage14.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage14.Controls.Add(this.lblPitStop3);
            this.tabPage14.Controls.Add(this.lblPitStop2);
            this.tabPage14.Controls.Add(this.lblPitStop1);
            this.tabPage14.Controls.Add(this.srPitStop3);
            this.tabPage14.Controls.Add(this.srPitStop2);
            this.tabPage14.Controls.Add(this.srPitStop1);
            this.tabPage14.Controls.Add(this.label109);
            this.tabPage14.Controls.Add(this.srPitStops);
            this.tabPage14.Controls.Add(this.label107);
            this.tabPage14.Controls.Add(this.groupBox2);
            this.tabPage14.Controls.Add(this.groupBox1);
            this.tabPage14.Controls.Add(this.srFuel);
            this.tabPage14.Location = new System.Drawing.Point(4, 22);
            this.tabPage14.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage14.Size = new System.Drawing.Size(678, 295);
            this.tabPage14.TabIndex = 0;
            this.tabPage14.Text = "General";
            // 
            // lblPitStop3
            // 
            this.lblPitStop3.AutoSize = true;
            this.lblPitStop3.Location = new System.Drawing.Point(450, 122);
            this.lblPitStop3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPitStop3.Name = "lblPitStop3";
            this.lblPitStop3.Size = new System.Drawing.Size(80, 13);
            this.lblPitStop3.TabIndex = 11;
            this.lblPitStop3.Text = "Pit Stop 3 Fuel:";
            // 
            // lblPitStop2
            // 
            this.lblPitStop2.AutoSize = true;
            this.lblPitStop2.Location = new System.Drawing.Point(450, 98);
            this.lblPitStop2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPitStop2.Name = "lblPitStop2";
            this.lblPitStop2.Size = new System.Drawing.Size(80, 13);
            this.lblPitStop2.TabIndex = 10;
            this.lblPitStop2.Text = "Pit Stop 2 Fuel:";
            // 
            // lblPitStop1
            // 
            this.lblPitStop1.AutoSize = true;
            this.lblPitStop1.Location = new System.Drawing.Point(450, 74);
            this.lblPitStop1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPitStop1.Name = "lblPitStop1";
            this.lblPitStop1.Size = new System.Drawing.Size(80, 13);
            this.lblPitStop1.TabIndex = 9;
            this.lblPitStop1.Text = "Pit Stop 1 Fuel:";
            // 
            // srPitStop3
            // 
            this.srPitStop3.BackColor = System.Drawing.Color.Transparent;
            this.srPitStop3.ChangeEnabled = true;
            this.srPitStop3.Detachable = false;
            this.srPitStop3.Location = new System.Drawing.Point(535, 119);
            this.srPitStop3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srPitStop3.Name = "srPitStop3";
            this.srPitStop3.Size = new System.Drawing.Size(134, 18);
            this.srPitStop3.TabIndex = 8;
            this.srPitStop3.ValueChangeListener = null;
            // 
            // srPitStop2
            // 
            this.srPitStop2.BackColor = System.Drawing.Color.Transparent;
            this.srPitStop2.ChangeEnabled = true;
            this.srPitStop2.Detachable = false;
            this.srPitStop2.Location = new System.Drawing.Point(535, 95);
            this.srPitStop2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srPitStop2.Name = "srPitStop2";
            this.srPitStop2.Size = new System.Drawing.Size(134, 18);
            this.srPitStop2.TabIndex = 7;
            this.srPitStop2.ValueChangeListener = null;
            // 
            // srPitStop1
            // 
            this.srPitStop1.BackColor = System.Drawing.Color.Transparent;
            this.srPitStop1.ChangeEnabled = true;
            this.srPitStop1.Detachable = false;
            this.srPitStop1.Location = new System.Drawing.Point(535, 71);
            this.srPitStop1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srPitStop1.Name = "srPitStop1";
            this.srPitStop1.Size = new System.Drawing.Size(134, 18);
            this.srPitStop1.TabIndex = 6;
            this.srPitStop1.ValueChangeListener = null;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(477, 50);
            this.label109.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(53, 13);
            this.label109.TabIndex = 5;
            this.label109.Text = "Pit Stops:";
            // 
            // srPitStops
            // 
            this.srPitStops.BackColor = System.Drawing.Color.Transparent;
            this.srPitStops.ChangeEnabled = true;
            this.srPitStops.Detachable = false;
            this.srPitStops.Location = new System.Drawing.Point(535, 47);
            this.srPitStops.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srPitStops.Name = "srPitStops";
            this.srPitStops.Size = new System.Drawing.Size(134, 18);
            this.srPitStops.TabIndex = 4;
            this.srPitStops.ValueChangeListener = null;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(501, 26);
            this.label107.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(31, 13);
            this.label107.TabIndex = 3;
            this.label107.Text = "Fuel:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbSetupNotes);
            this.groupBox2.Location = new System.Drawing.Point(126, 5);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(308, 145);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Notes";
            // 
            // tbSetupNotes
            // 
            this.tbSetupNotes.Location = new System.Drawing.Point(5, 18);
            this.tbSetupNotes.Margin = new System.Windows.Forms.Padding(2);
            this.tbSetupNotes.Name = "tbSetupNotes";
            this.tbSetupNotes.Size = new System.Drawing.Size(299, 122);
            this.tbSetupNotes.TabIndex = 3;
            this.tbSetupNotes.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkRealValues);
            this.groupBox1.Controls.Add(this.btnLoadSetup);
            this.groupBox1.Controls.Add(this.btnSaveSetup);
            this.groupBox1.Controls.Add(this.btnDefaultSetup);
            this.groupBox1.Location = new System.Drawing.Point(5, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(116, 145);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // btnLoadSetup
            // 
            this.btnLoadSetup.Location = new System.Drawing.Point(5, 42);
            this.btnLoadSetup.Margin = new System.Windows.Forms.Padding(2);
            this.btnLoadSetup.Name = "btnLoadSetup";
            this.btnLoadSetup.Size = new System.Drawing.Size(106, 19);
            this.btnLoadSetup.TabIndex = 4;
            this.btnLoadSetup.Text = "Load Setup";
            this.btnLoadSetup.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnLoadSetup.UseVisualStyleBackColor = true;
            this.btnLoadSetup.Click += new System.EventHandler(this.btnLoadSetup_Click);
            // 
            // btnSaveSetup
            // 
            this.btnSaveSetup.Location = new System.Drawing.Point(5, 66);
            this.btnSaveSetup.Margin = new System.Windows.Forms.Padding(2);
            this.btnSaveSetup.Name = "btnSaveSetup";
            this.btnSaveSetup.Size = new System.Drawing.Size(106, 19);
            this.btnSaveSetup.TabIndex = 3;
            this.btnSaveSetup.Text = "Save";
            this.btnSaveSetup.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnSaveSetup.UseVisualStyleBackColor = true;
            this.btnSaveSetup.Click += new System.EventHandler(this.btnSaveSetup_Click);
            // 
            // btnDefaultSetup
            // 
            this.btnDefaultSetup.Location = new System.Drawing.Point(5, 18);
            this.btnDefaultSetup.Margin = new System.Windows.Forms.Padding(2);
            this.btnDefaultSetup.Name = "btnDefaultSetup";
            this.btnDefaultSetup.Size = new System.Drawing.Size(106, 19);
            this.btnDefaultSetup.TabIndex = 2;
            this.btnDefaultSetup.Text = "Load Default";
            this.btnDefaultSetup.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDefaultSetup.UseVisualStyleBackColor = true;
            this.btnDefaultSetup.Click += new System.EventHandler(this.btnDefaultSetup_Click);
            // 
            // srFuel
            // 
            this.srFuel.BackColor = System.Drawing.Color.Transparent;
            this.srFuel.ChangeEnabled = true;
            this.srFuel.Detachable = false;
            this.srFuel.Location = new System.Drawing.Point(535, 23);
            this.srFuel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFuel.Name = "srFuel";
            this.srFuel.Size = new System.Drawing.Size(134, 18);
            this.srFuel.TabIndex = 0;
            this.srFuel.ValueChangeListener = null;
            // 
            // tabPage15
            // 
            this.tabPage15.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage15.Controls.Add(this.tabControlDriveline);
            this.tabPage15.Controls.Add(this.label149);
            this.tabPage15.Controls.Add(this.label148);
            this.tabPage15.Controls.Add(this.label147);
            this.tabPage15.Controls.Add(this.label146);
            this.tabPage15.Controls.Add(this.lblGearR);
            this.tabPage15.Controls.Add(this.lblGear9);
            this.tabPage15.Controls.Add(this.lblGear2);
            this.tabPage15.Controls.Add(this.lblGear3);
            this.tabPage15.Controls.Add(this.lblGear4);
            this.tabPage15.Controls.Add(this.lblGear5);
            this.tabPage15.Controls.Add(this.lblGear6);
            this.tabPage15.Controls.Add(this.lblGear7);
            this.tabPage15.Controls.Add(this.lblGear8);
            this.tabPage15.Controls.Add(this.label111);
            this.tabPage15.Controls.Add(this.lblGear1);
            this.tabPage15.Controls.Add(this.srDiffPreload);
            this.tabPage15.Controls.Add(this.srDiffCoast);
            this.tabPage15.Controls.Add(this.srDiffPower);
            this.tabPage15.Controls.Add(this.srDiffPump);
            this.tabPage15.Controls.Add(this.srGearR);
            this.tabPage15.Controls.Add(this.srGear9);
            this.tabPage15.Controls.Add(this.srGear2);
            this.tabPage15.Controls.Add(this.srGear3);
            this.tabPage15.Controls.Add(this.srGear4);
            this.tabPage15.Controls.Add(this.srGear5);
            this.tabPage15.Controls.Add(this.srGear6);
            this.tabPage15.Controls.Add(this.srGear7);
            this.tabPage15.Controls.Add(this.srGear8);
            this.tabPage15.Controls.Add(this.srFinalDrive);
            this.tabPage15.Controls.Add(this.srGear1);
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage15.Size = new System.Drawing.Size(678, 295);
            this.tabPage15.TabIndex = 1;
            this.tabPage15.Text = "Drivetrain";
            // 
            // tabControlDriveline
            // 
            this.tabControlDriveline.Controls.Add(this.tabPage21);
            this.tabControlDriveline.Controls.Add(this.tabPage22);
            this.tabControlDriveline.Location = new System.Drawing.Point(374, 0);
            this.tabControlDriveline.Margin = new System.Windows.Forms.Padding(2);
            this.tabControlDriveline.Name = "tabControlDriveline";
            this.tabControlDriveline.SelectedIndex = 0;
            this.tabControlDriveline.Size = new System.Drawing.Size(306, 300);
            this.tabControlDriveline.TabIndex = 40;
            // 
            // tabPage21
            // 
            this.tabPage21.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage21.Controls.Add(this.btnRescaleSpeedGraph);
            this.tabPage21.Controls.Add(this.zedGearboxGraph);
            this.tabPage21.Location = new System.Drawing.Point(4, 22);
            this.tabPage21.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage21.Name = "tabPage21";
            this.tabPage21.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage21.Size = new System.Drawing.Size(298, 274);
            this.tabPage21.TabIndex = 0;
            this.tabPage21.Text = "Speed-Graph";
            // 
            // btnRescaleSpeedGraph
            // 
            this.btnRescaleSpeedGraph.Location = new System.Drawing.Point(5, 246);
            this.btnRescaleSpeedGraph.Margin = new System.Windows.Forms.Padding(2);
            this.btnRescaleSpeedGraph.Name = "btnRescaleSpeedGraph";
            this.btnRescaleSpeedGraph.Size = new System.Drawing.Size(60, 23);
            this.btnRescaleSpeedGraph.TabIndex = 41;
            this.btnRescaleSpeedGraph.Text = "Rescale";
            this.btnRescaleSpeedGraph.UseVisualStyleBackColor = true;
            // 
            // zedGearboxGraph
            // 
            this.zedGearboxGraph.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.zedGearboxGraph.IsShowPointValues = true;
            this.zedGearboxGraph.Location = new System.Drawing.Point(0, 0);
            this.zedGearboxGraph.Margin = new System.Windows.Forms.Padding(4);
            this.zedGearboxGraph.Name = "zedGearboxGraph";
            this.zedGearboxGraph.ScrollGrace = 0D;
            this.zedGearboxGraph.ScrollMaxX = 0D;
            this.zedGearboxGraph.ScrollMaxY = 0D;
            this.zedGearboxGraph.ScrollMaxY2 = 0D;
            this.zedGearboxGraph.ScrollMinX = 0D;
            this.zedGearboxGraph.ScrollMinY = 0D;
            this.zedGearboxGraph.ScrollMinY2 = 0D;
            this.zedGearboxGraph.Size = new System.Drawing.Size(300, 242);
            this.zedGearboxGraph.TabIndex = 25;
            // 
            // tabPage22
            // 
            this.tabPage22.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage22.Controls.Add(this.lblTopSpeed);
            this.tabPage22.Controls.Add(this.label150);
            this.tabPage22.Location = new System.Drawing.Point(4, 22);
            this.tabPage22.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage22.Name = "tabPage22";
            this.tabPage22.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage22.Size = new System.Drawing.Size(298, 274);
            this.tabPage22.TabIndex = 1;
            this.tabPage22.Text = "Values";
            // 
            // lblTopSpeed
            // 
            this.lblTopSpeed.AutoSize = true;
            this.lblTopSpeed.Location = new System.Drawing.Point(73, 7);
            this.lblTopSpeed.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTopSpeed.Name = "lblTopSpeed";
            this.lblTopSpeed.Size = new System.Drawing.Size(53, 13);
            this.lblTopSpeed.TabIndex = 42;
            this.lblTopSpeed.Text = "000 kmph";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(5, 7);
            this.label150.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(62, 13);
            this.label150.TabIndex = 41;
            this.label150.Text = "Top Speed:";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(184, 79);
            this.label149.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(47, 13);
            this.label149.TabIndex = 32;
            this.label149.Text = "Preload:";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(192, 56);
            this.label148.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(39, 13);
            this.label148.TabIndex = 31;
            this.label148.Text = "Coast:";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Location = new System.Drawing.Point(191, 10);
            this.label147.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(37, 13);
            this.label147.TabIndex = 30;
            this.label147.Text = "Pump:";
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(190, 33);
            this.label146.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(41, 13);
            this.label146.TabIndex = 29;
            this.label146.Text = "Power:";
            // 
            // lblGearR
            // 
            this.lblGearR.AutoSize = true;
            this.lblGearR.Location = new System.Drawing.Point(18, 219);
            this.lblGearR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGearR.Name = "lblGearR";
            this.lblGearR.Size = new System.Drawing.Size(18, 13);
            this.lblGearR.TabIndex = 26;
            this.lblGearR.Text = "R:";
            // 
            // lblGear9
            // 
            this.lblGear9.AutoSize = true;
            this.lblGear9.Location = new System.Drawing.Point(18, 196);
            this.lblGear9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGear9.Name = "lblGear9";
            this.lblGear9.Size = new System.Drawing.Size(17, 13);
            this.lblGear9.TabIndex = 23;
            this.lblGear9.Text = "9:";
            // 
            // lblGear2
            // 
            this.lblGear2.AutoSize = true;
            this.lblGear2.Location = new System.Drawing.Point(18, 33);
            this.lblGear2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGear2.Name = "lblGear2";
            this.lblGear2.Size = new System.Drawing.Size(17, 13);
            this.lblGear2.TabIndex = 21;
            this.lblGear2.Text = "2:";
            // 
            // lblGear3
            // 
            this.lblGear3.AutoSize = true;
            this.lblGear3.Location = new System.Drawing.Point(18, 56);
            this.lblGear3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGear3.Name = "lblGear3";
            this.lblGear3.Size = new System.Drawing.Size(17, 13);
            this.lblGear3.TabIndex = 19;
            this.lblGear3.Text = "3:";
            // 
            // lblGear4
            // 
            this.lblGear4.AutoSize = true;
            this.lblGear4.Location = new System.Drawing.Point(18, 79);
            this.lblGear4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGear4.Name = "lblGear4";
            this.lblGear4.Size = new System.Drawing.Size(17, 13);
            this.lblGear4.TabIndex = 17;
            this.lblGear4.Text = "4:";
            // 
            // lblGear5
            // 
            this.lblGear5.AutoSize = true;
            this.lblGear5.Location = new System.Drawing.Point(18, 102);
            this.lblGear5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGear5.Name = "lblGear5";
            this.lblGear5.Size = new System.Drawing.Size(17, 13);
            this.lblGear5.TabIndex = 15;
            this.lblGear5.Text = "5:";
            // 
            // lblGear6
            // 
            this.lblGear6.AutoSize = true;
            this.lblGear6.Location = new System.Drawing.Point(18, 126);
            this.lblGear6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGear6.Name = "lblGear6";
            this.lblGear6.Size = new System.Drawing.Size(17, 13);
            this.lblGear6.TabIndex = 13;
            this.lblGear6.Text = "6:";
            // 
            // lblGear7
            // 
            this.lblGear7.AutoSize = true;
            this.lblGear7.Location = new System.Drawing.Point(18, 149);
            this.lblGear7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGear7.Name = "lblGear7";
            this.lblGear7.Size = new System.Drawing.Size(17, 13);
            this.lblGear7.TabIndex = 11;
            this.lblGear7.Text = "7:";
            // 
            // lblGear8
            // 
            this.lblGear8.AutoSize = true;
            this.lblGear8.Location = new System.Drawing.Point(18, 172);
            this.lblGear8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGear8.Name = "lblGear8";
            this.lblGear8.Size = new System.Drawing.Size(17, 13);
            this.lblGear8.TabIndex = 9;
            this.lblGear8.Text = "8:";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(4, 242);
            this.label111.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(33, 13);
            this.label111.TabIndex = 8;
            this.label111.Text = "Final:";
            // 
            // lblGear1
            // 
            this.lblGear1.AutoSize = true;
            this.lblGear1.Location = new System.Drawing.Point(18, 10);
            this.lblGear1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGear1.Name = "lblGear1";
            this.lblGear1.Size = new System.Drawing.Size(17, 13);
            this.lblGear1.TabIndex = 5;
            this.lblGear1.Text = "1:";
            // 
            // srGearR
            // 
            this.srGearR.BackColor = System.Drawing.Color.Transparent;
            this.srGearR.ChangeEnabled = true;
            this.srGearR.Detachable = false;
            this.srGearR.Location = new System.Drawing.Point(40, 214);
            this.srGearR.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srGearR.Name = "srGearR";
            this.srGearR.Size = new System.Drawing.Size(134, 18);
            this.srGearR.TabIndex = 27;
            this.srGearR.ValueChangeListener = null;
            // 
            // srGear9
            // 
            this.srGear9.BackColor = System.Drawing.Color.Transparent;
            this.srGear9.ChangeEnabled = true;
            this.srGear9.Detachable = false;
            this.srGear9.Location = new System.Drawing.Point(40, 190);
            this.srGear9.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srGear9.Name = "srGear9";
            this.srGear9.Size = new System.Drawing.Size(134, 19);
            this.srGear9.TabIndex = 24;
            this.srGear9.ValueChangeListener = null;
            // 
            // srGear2
            // 
            this.srGear2.BackColor = System.Drawing.Color.Transparent;
            this.srGear2.ChangeEnabled = true;
            this.srGear2.Detachable = false;
            this.srGear2.Location = new System.Drawing.Point(40, 28);
            this.srGear2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srGear2.Name = "srGear2";
            this.srGear2.Size = new System.Drawing.Size(134, 18);
            this.srGear2.TabIndex = 22;
            this.srGear2.ValueChangeListener = null;
            // 
            // srGear3
            // 
            this.srGear3.BackColor = System.Drawing.Color.Transparent;
            this.srGear3.ChangeEnabled = true;
            this.srGear3.Detachable = false;
            this.srGear3.Location = new System.Drawing.Point(40, 51);
            this.srGear3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srGear3.Name = "srGear3";
            this.srGear3.Size = new System.Drawing.Size(134, 18);
            this.srGear3.TabIndex = 20;
            this.srGear3.ValueChangeListener = null;
            // 
            // srGear4
            // 
            this.srGear4.BackColor = System.Drawing.Color.Transparent;
            this.srGear4.ChangeEnabled = true;
            this.srGear4.Detachable = false;
            this.srGear4.Location = new System.Drawing.Point(40, 74);
            this.srGear4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srGear4.Name = "srGear4";
            this.srGear4.Size = new System.Drawing.Size(134, 18);
            this.srGear4.TabIndex = 18;
            this.srGear4.ValueChangeListener = null;
            // 
            // srGear5
            // 
            this.srGear5.BackColor = System.Drawing.Color.Transparent;
            this.srGear5.ChangeEnabled = true;
            this.srGear5.Detachable = false;
            this.srGear5.Location = new System.Drawing.Point(40, 98);
            this.srGear5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srGear5.Name = "srGear5";
            this.srGear5.Size = new System.Drawing.Size(134, 18);
            this.srGear5.TabIndex = 16;
            this.srGear5.ValueChangeListener = null;
            // 
            // srGear6
            // 
            this.srGear6.BackColor = System.Drawing.Color.Transparent;
            this.srGear6.ChangeEnabled = true;
            this.srGear6.Detachable = false;
            this.srGear6.Location = new System.Drawing.Point(40, 121);
            this.srGear6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srGear6.Name = "srGear6";
            this.srGear6.Size = new System.Drawing.Size(134, 18);
            this.srGear6.TabIndex = 14;
            this.srGear6.ValueChangeListener = null;
            // 
            // srGear7
            // 
            this.srGear7.BackColor = System.Drawing.Color.Transparent;
            this.srGear7.ChangeEnabled = true;
            this.srGear7.Detachable = false;
            this.srGear7.Location = new System.Drawing.Point(40, 144);
            this.srGear7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srGear7.Name = "srGear7";
            this.srGear7.Size = new System.Drawing.Size(134, 18);
            this.srGear7.TabIndex = 12;
            this.srGear7.ValueChangeListener = null;
            // 
            // srGear8
            // 
            this.srGear8.BackColor = System.Drawing.Color.Transparent;
            this.srGear8.ChangeEnabled = true;
            this.srGear8.Detachable = false;
            this.srGear8.Location = new System.Drawing.Point(40, 167);
            this.srGear8.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srGear8.Name = "srGear8";
            this.srGear8.Size = new System.Drawing.Size(134, 18);
            this.srGear8.TabIndex = 10;
            this.srGear8.ValueChangeListener = null;
            // 
            // srGear1
            // 
            this.srGear1.BackColor = System.Drawing.Color.Transparent;
            this.srGear1.ChangeEnabled = true;
            this.srGear1.Detachable = false;
            this.srGear1.Location = new System.Drawing.Point(40, 5);
            this.srGear1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srGear1.Name = "srGear1";
            this.srGear1.Size = new System.Drawing.Size(134, 18);
            this.srGear1.TabIndex = 6;
            this.srGear1.ValueChangeListener = null;
            // 
            // tabPage16
            // 
            this.tabPage16.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage16.Controls.Add(this.chkSymmetric);
            this.tabPage16.Controls.Add(this.label127);
            this.tabPage16.Controls.Add(this.label126);
            this.tabPage16.Controls.Add(this.lblFToeOffset);
            this.tabPage16.Controls.Add(this.label124);
            this.tabPage16.Controls.Add(this.label123);
            this.tabPage16.Controls.Add(this.lblFThirdSpring);
            this.tabPage16.Controls.Add(this.label121);
            this.tabPage16.Controls.Add(this.label120);
            this.tabPage16.Controls.Add(this.label119);
            this.tabPage16.Controls.Add(this.label118);
            this.tabPage16.Controls.Add(this.label117);
            this.tabPage16.Controls.Add(this.label116);
            this.tabPage16.Controls.Add(this.label115);
            this.tabPage16.Controls.Add(this.label114);
            this.tabPage16.Controls.Add(this.label113);
            this.tabPage16.Controls.Add(this.srFRCaster);
            this.tabPage16.Controls.Add(this.srFRCamber);
            this.tabPage16.Controls.Add(this.srFLCaster);
            this.tabPage16.Controls.Add(this.srFLCamber);
            this.tabPage16.Controls.Add(this.srFToeOffset);
            this.tabPage16.Controls.Add(this.srFToeIn);
            this.tabPage16.Controls.Add(this.srFARB);
            this.tabPage16.Controls.Add(this.srFRRideHeight);
            this.tabPage16.Controls.Add(this.srFRPacker);
            this.tabPage16.Controls.Add(this.srFRFastRebound);
            this.tabPage16.Controls.Add(this.srFRFastBump);
            this.tabPage16.Controls.Add(this.srFRSlowRebound);
            this.tabPage16.Controls.Add(this.srFRSlowBump);
            this.tabPage16.Controls.Add(this.srFRSpring);
            this.tabPage16.Controls.Add(this.srFPacker);
            this.tabPage16.Controls.Add(this.srFFastRebound);
            this.tabPage16.Controls.Add(this.srFFastBump);
            this.tabPage16.Controls.Add(this.srFSlowRebound);
            this.tabPage16.Controls.Add(this.srFSlowBump);
            this.tabPage16.Controls.Add(this.srFSpring);
            this.tabPage16.Controls.Add(this.srFLRideHeight);
            this.tabPage16.Controls.Add(this.srFLPacker);
            this.tabPage16.Controls.Add(this.srFLFastRebound);
            this.tabPage16.Controls.Add(this.srFLFastBump);
            this.tabPage16.Controls.Add(this.srFLSlowRebound);
            this.tabPage16.Controls.Add(this.srFLSlowBump);
            this.tabPage16.Controls.Add(this.srFLSpring);
            this.tabPage16.Location = new System.Drawing.Point(4, 22);
            this.tabPage16.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage16.Size = new System.Drawing.Size(678, 295);
            this.tabPage16.TabIndex = 2;
            this.tabPage16.Text = "Front Suspension";
            // 
            // chkSymmetric
            // 
            this.chkSymmetric.AutoSize = true;
            this.chkSymmetric.Location = new System.Drawing.Point(266, 267);
            this.chkSymmetric.Margin = new System.Windows.Forms.Padding(2);
            this.chkSymmetric.Name = "chkSymmetric";
            this.chkSymmetric.Size = new System.Drawing.Size(75, 17);
            this.chkSymmetric.TabIndex = 48;
            this.chkSymmetric.Text = "Symmetric";
            this.chkSymmetric.UseVisualStyleBackColor = true;
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(44, 48);
            this.label127.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(43, 13);
            this.label127.TabIndex = 47;
            this.label127.Text = "Caster:";
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(37, 24);
            this.label126.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(48, 13);
            this.label126.TabIndex = 46;
            this.label126.Text = "Camber:";
            // 
            // lblFToeOffset
            // 
            this.lblFToeOffset.AutoSize = true;
            this.lblFToeOffset.Location = new System.Drawing.Point(568, 119);
            this.lblFToeOffset.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFToeOffset.Name = "lblFToeOffset";
            this.lblFToeOffset.Size = new System.Drawing.Size(64, 13);
            this.lblFToeOffset.TabIndex = 41;
            this.lblFToeOffset.Text = "Toe-Offset:";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Location = new System.Drawing.Point(578, 74);
            this.label124.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(43, 13);
            this.label124.TabIndex = 39;
            this.label124.Text = "Toe-In:";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Location = new System.Drawing.Point(568, 24);
            this.label123.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(67, 13);
            this.label123.TabIndex = 37;
            this.label123.Text = "Anti-Rollbar:";
            // 
            // lblFThirdSpring
            // 
            this.lblFThirdSpring.AutoSize = true;
            this.lblFThirdSpring.Location = new System.Drawing.Point(263, 2);
            this.lblFThirdSpring.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFThirdSpring.Name = "lblFThirdSpring";
            this.lblFThirdSpring.Size = new System.Drawing.Size(61, 13);
            this.lblFThirdSpring.TabIndex = 35;
            this.lblFThirdSpring.Text = "3rd-Spring:";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Location = new System.Drawing.Point(420, 2);
            this.label121.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(36, 13);
            this.label121.TabIndex = 34;
            this.label121.Text = "Right:";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(144, 2);
            this.label120.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(30, 13);
            this.label120.TabIndex = 33;
            this.label120.Text = "Left:";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(20, 215);
            this.label119.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(66, 13);
            this.label119.TabIndex = 18;
            this.label119.Text = "Ride Height:";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(38, 191);
            this.label118.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(48, 13);
            this.label118.TabIndex = 16;
            this.label118.Text = "Packers:";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(7, 167);
            this.label117.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(78, 13);
            this.label117.TabIndex = 14;
            this.label117.Text = "Fast Rebound:";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(23, 143);
            this.label116.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(61, 13);
            this.label116.TabIndex = 12;
            this.label116.Text = "Fast Bump:";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(5, 119);
            this.label115.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(79, 13);
            this.label115.TabIndex = 10;
            this.label115.Text = "Slow Rebound:";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(21, 95);
            this.label114.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(62, 13);
            this.label114.TabIndex = 8;
            this.label114.Text = "Slow Bump:";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(44, 71);
            this.label113.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(41, 13);
            this.label113.TabIndex = 6;
            this.label113.Text = "Spring:";
            // 
            // srFRCaster
            // 
            this.srFRCaster.BackColor = System.Drawing.Color.Transparent;
            this.srFRCaster.ChangeEnabled = true;
            this.srFRCaster.Detachable = false;
            this.srFRCaster.Location = new System.Drawing.Point(367, 42);
            this.srFRCaster.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFRCaster.Name = "srFRCaster";
            this.srFRCaster.Size = new System.Drawing.Size(134, 19);
            this.srFRCaster.TabIndex = 45;
            this.srFRCaster.ValueChangeListener = null;
            // 
            // srFRCamber
            // 
            this.srFRCamber.BackColor = System.Drawing.Color.Transparent;
            this.srFRCamber.ChangeEnabled = true;
            this.srFRCamber.Detachable = false;
            this.srFRCamber.Location = new System.Drawing.Point(367, 18);
            this.srFRCamber.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFRCamber.Name = "srFRCamber";
            this.srFRCamber.Size = new System.Drawing.Size(134, 19);
            this.srFRCamber.TabIndex = 44;
            this.srFRCamber.ValueChangeListener = null;
            // 
            // srFLCaster
            // 
            this.srFLCaster.BackColor = System.Drawing.Color.Transparent;
            this.srFLCaster.ChangeEnabled = true;
            this.srFLCaster.Detachable = false;
            this.srFLCaster.Location = new System.Drawing.Point(90, 42);
            this.srFLCaster.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFLCaster.Name = "srFLCaster";
            this.srFLCaster.Size = new System.Drawing.Size(134, 19);
            this.srFLCaster.TabIndex = 43;
            this.srFLCaster.ValueChangeListener = null;
            // 
            // srFLCamber
            // 
            this.srFLCamber.BackColor = System.Drawing.Color.Transparent;
            this.srFLCamber.ChangeEnabled = true;
            this.srFLCamber.Detachable = false;
            this.srFLCamber.Location = new System.Drawing.Point(90, 18);
            this.srFLCamber.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFLCamber.Name = "srFLCamber";
            this.srFLCamber.Size = new System.Drawing.Size(134, 19);
            this.srFLCamber.TabIndex = 42;
            this.srFLCamber.ValueChangeListener = null;
            // 
            // srFToeOffset
            // 
            this.srFToeOffset.BackColor = System.Drawing.Color.Transparent;
            this.srFToeOffset.ChangeEnabled = true;
            this.srFToeOffset.Detachable = false;
            this.srFToeOffset.Location = new System.Drawing.Point(533, 138);
            this.srFToeOffset.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFToeOffset.Name = "srFToeOffset";
            this.srFToeOffset.Size = new System.Drawing.Size(134, 19);
            this.srFToeOffset.TabIndex = 40;
            this.srFToeOffset.ValueChangeListener = null;
            // 
            // srFToeIn
            // 
            this.srFToeIn.BackColor = System.Drawing.Color.Transparent;
            this.srFToeIn.ChangeEnabled = true;
            this.srFToeIn.Detachable = false;
            this.srFToeIn.Location = new System.Drawing.Point(533, 90);
            this.srFToeIn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFToeIn.Name = "srFToeIn";
            this.srFToeIn.Size = new System.Drawing.Size(134, 19);
            this.srFToeIn.TabIndex = 38;
            this.srFToeIn.ValueChangeListener = null;
            // 
            // srFARB
            // 
            this.srFARB.BackColor = System.Drawing.Color.Transparent;
            this.srFARB.ChangeEnabled = true;
            this.srFARB.Detachable = false;
            this.srFARB.Location = new System.Drawing.Point(533, 42);
            this.srFARB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFARB.Name = "srFARB";
            this.srFARB.Size = new System.Drawing.Size(134, 19);
            this.srFARB.TabIndex = 36;
            this.srFARB.ValueChangeListener = null;
            // 
            // srFRRideHeight
            // 
            this.srFRRideHeight.BackColor = System.Drawing.Color.Transparent;
            this.srFRRideHeight.ChangeEnabled = true;
            this.srFRRideHeight.Detachable = false;
            this.srFRRideHeight.Location = new System.Drawing.Point(367, 210);
            this.srFRRideHeight.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFRRideHeight.Name = "srFRRideHeight";
            this.srFRRideHeight.Size = new System.Drawing.Size(134, 22);
            this.srFRRideHeight.TabIndex = 32;
            this.srFRRideHeight.ValueChangeListener = null;
            // 
            // srFRPacker
            // 
            this.srFRPacker.BackColor = System.Drawing.Color.Transparent;
            this.srFRPacker.ChangeEnabled = true;
            this.srFRPacker.Detachable = false;
            this.srFRPacker.Location = new System.Drawing.Point(367, 186);
            this.srFRPacker.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFRPacker.Name = "srFRPacker";
            this.srFRPacker.Size = new System.Drawing.Size(134, 22);
            this.srFRPacker.TabIndex = 31;
            this.srFRPacker.ValueChangeListener = null;
            // 
            // srFRFastRebound
            // 
            this.srFRFastRebound.BackColor = System.Drawing.Color.Transparent;
            this.srFRFastRebound.ChangeEnabled = true;
            this.srFRFastRebound.Detachable = false;
            this.srFRFastRebound.Location = new System.Drawing.Point(367, 162);
            this.srFRFastRebound.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFRFastRebound.Name = "srFRFastRebound";
            this.srFRFastRebound.Size = new System.Drawing.Size(134, 22);
            this.srFRFastRebound.TabIndex = 30;
            this.srFRFastRebound.ValueChangeListener = null;
            // 
            // srFRFastBump
            // 
            this.srFRFastBump.BackColor = System.Drawing.Color.Transparent;
            this.srFRFastBump.ChangeEnabled = true;
            this.srFRFastBump.Detachable = false;
            this.srFRFastBump.Location = new System.Drawing.Point(367, 138);
            this.srFRFastBump.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFRFastBump.Name = "srFRFastBump";
            this.srFRFastBump.Size = new System.Drawing.Size(134, 22);
            this.srFRFastBump.TabIndex = 29;
            this.srFRFastBump.ValueChangeListener = null;
            // 
            // srFRSlowRebound
            // 
            this.srFRSlowRebound.BackColor = System.Drawing.Color.Transparent;
            this.srFRSlowRebound.ChangeEnabled = true;
            this.srFRSlowRebound.Detachable = false;
            this.srFRSlowRebound.Location = new System.Drawing.Point(367, 114);
            this.srFRSlowRebound.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFRSlowRebound.Name = "srFRSlowRebound";
            this.srFRSlowRebound.Size = new System.Drawing.Size(134, 22);
            this.srFRSlowRebound.TabIndex = 28;
            this.srFRSlowRebound.ValueChangeListener = null;
            // 
            // srFRSlowBump
            // 
            this.srFRSlowBump.BackColor = System.Drawing.Color.Transparent;
            this.srFRSlowBump.ChangeEnabled = true;
            this.srFRSlowBump.Detachable = false;
            this.srFRSlowBump.Location = new System.Drawing.Point(367, 90);
            this.srFRSlowBump.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFRSlowBump.Name = "srFRSlowBump";
            this.srFRSlowBump.Size = new System.Drawing.Size(134, 22);
            this.srFRSlowBump.TabIndex = 27;
            this.srFRSlowBump.ValueChangeListener = null;
            // 
            // srFRSpring
            // 
            this.srFRSpring.BackColor = System.Drawing.Color.Transparent;
            this.srFRSpring.ChangeEnabled = true;
            this.srFRSpring.Detachable = false;
            this.srFRSpring.Location = new System.Drawing.Point(367, 66);
            this.srFRSpring.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFRSpring.Name = "srFRSpring";
            this.srFRSpring.Size = new System.Drawing.Size(134, 21);
            this.srFRSpring.TabIndex = 26;
            this.srFRSpring.ValueChangeListener = null;
            // 
            // srFPacker
            // 
            this.srFPacker.BackColor = System.Drawing.Color.Transparent;
            this.srFPacker.ChangeEnabled = true;
            this.srFPacker.Detachable = false;
            this.srFPacker.Location = new System.Drawing.Point(229, 186);
            this.srFPacker.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFPacker.Name = "srFPacker";
            this.srFPacker.Size = new System.Drawing.Size(134, 21);
            this.srFPacker.TabIndex = 24;
            this.srFPacker.ValueChangeListener = null;
            // 
            // srFFastRebound
            // 
            this.srFFastRebound.BackColor = System.Drawing.Color.Transparent;
            this.srFFastRebound.ChangeEnabled = true;
            this.srFFastRebound.Detachable = false;
            this.srFFastRebound.Location = new System.Drawing.Point(229, 162);
            this.srFFastRebound.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFFastRebound.Name = "srFFastRebound";
            this.srFFastRebound.Size = new System.Drawing.Size(134, 21);
            this.srFFastRebound.TabIndex = 23;
            this.srFFastRebound.ValueChangeListener = null;
            // 
            // srFFastBump
            // 
            this.srFFastBump.BackColor = System.Drawing.Color.Transparent;
            this.srFFastBump.ChangeEnabled = true;
            this.srFFastBump.Detachable = false;
            this.srFFastBump.Location = new System.Drawing.Point(229, 138);
            this.srFFastBump.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFFastBump.Name = "srFFastBump";
            this.srFFastBump.Size = new System.Drawing.Size(134, 21);
            this.srFFastBump.TabIndex = 22;
            this.srFFastBump.ValueChangeListener = null;
            // 
            // srFSlowRebound
            // 
            this.srFSlowRebound.BackColor = System.Drawing.Color.Transparent;
            this.srFSlowRebound.ChangeEnabled = true;
            this.srFSlowRebound.Detachable = false;
            this.srFSlowRebound.Location = new System.Drawing.Point(229, 114);
            this.srFSlowRebound.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFSlowRebound.Name = "srFSlowRebound";
            this.srFSlowRebound.Size = new System.Drawing.Size(134, 21);
            this.srFSlowRebound.TabIndex = 21;
            this.srFSlowRebound.ValueChangeListener = null;
            // 
            // srFSlowBump
            // 
            this.srFSlowBump.BackColor = System.Drawing.Color.Transparent;
            this.srFSlowBump.ChangeEnabled = true;
            this.srFSlowBump.Detachable = false;
            this.srFSlowBump.Location = new System.Drawing.Point(229, 90);
            this.srFSlowBump.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFSlowBump.Name = "srFSlowBump";
            this.srFSlowBump.Size = new System.Drawing.Size(134, 21);
            this.srFSlowBump.TabIndex = 20;
            this.srFSlowBump.ValueChangeListener = null;
            // 
            // srFSpring
            // 
            this.srFSpring.BackColor = System.Drawing.Color.Transparent;
            this.srFSpring.ChangeEnabled = true;
            this.srFSpring.Detachable = false;
            this.srFSpring.Location = new System.Drawing.Point(229, 66);
            this.srFSpring.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFSpring.Name = "srFSpring";
            this.srFSpring.Size = new System.Drawing.Size(134, 19);
            this.srFSpring.TabIndex = 19;
            this.srFSpring.ValueChangeListener = null;
            // 
            // srFLRideHeight
            // 
            this.srFLRideHeight.BackColor = System.Drawing.Color.Transparent;
            this.srFLRideHeight.ChangeEnabled = true;
            this.srFLRideHeight.Detachable = false;
            this.srFLRideHeight.Location = new System.Drawing.Point(90, 210);
            this.srFLRideHeight.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFLRideHeight.Name = "srFLRideHeight";
            this.srFLRideHeight.Size = new System.Drawing.Size(134, 19);
            this.srFLRideHeight.TabIndex = 17;
            this.srFLRideHeight.ValueChangeListener = null;
            // 
            // srFLPacker
            // 
            this.srFLPacker.BackColor = System.Drawing.Color.Transparent;
            this.srFLPacker.ChangeEnabled = true;
            this.srFLPacker.Detachable = false;
            this.srFLPacker.Location = new System.Drawing.Point(90, 186);
            this.srFLPacker.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFLPacker.Name = "srFLPacker";
            this.srFLPacker.Size = new System.Drawing.Size(134, 19);
            this.srFLPacker.TabIndex = 15;
            this.srFLPacker.ValueChangeListener = null;
            // 
            // srFLFastRebound
            // 
            this.srFLFastRebound.BackColor = System.Drawing.Color.Transparent;
            this.srFLFastRebound.ChangeEnabled = true;
            this.srFLFastRebound.Detachable = false;
            this.srFLFastRebound.Location = new System.Drawing.Point(90, 162);
            this.srFLFastRebound.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFLFastRebound.Name = "srFLFastRebound";
            this.srFLFastRebound.Size = new System.Drawing.Size(134, 19);
            this.srFLFastRebound.TabIndex = 13;
            this.srFLFastRebound.ValueChangeListener = null;
            // 
            // srFLFastBump
            // 
            this.srFLFastBump.BackColor = System.Drawing.Color.Transparent;
            this.srFLFastBump.ChangeEnabled = true;
            this.srFLFastBump.Detachable = false;
            this.srFLFastBump.Location = new System.Drawing.Point(90, 138);
            this.srFLFastBump.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFLFastBump.Name = "srFLFastBump";
            this.srFLFastBump.Size = new System.Drawing.Size(134, 19);
            this.srFLFastBump.TabIndex = 11;
            this.srFLFastBump.ValueChangeListener = null;
            // 
            // srFLSlowRebound
            // 
            this.srFLSlowRebound.BackColor = System.Drawing.Color.Transparent;
            this.srFLSlowRebound.ChangeEnabled = true;
            this.srFLSlowRebound.Detachable = false;
            this.srFLSlowRebound.Location = new System.Drawing.Point(90, 114);
            this.srFLSlowRebound.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFLSlowRebound.Name = "srFLSlowRebound";
            this.srFLSlowRebound.Size = new System.Drawing.Size(134, 19);
            this.srFLSlowRebound.TabIndex = 9;
            this.srFLSlowRebound.ValueChangeListener = null;
            // 
            // srFLSlowBump
            // 
            this.srFLSlowBump.BackColor = System.Drawing.Color.Transparent;
            this.srFLSlowBump.ChangeEnabled = true;
            this.srFLSlowBump.Detachable = false;
            this.srFLSlowBump.Location = new System.Drawing.Point(90, 90);
            this.srFLSlowBump.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFLSlowBump.Name = "srFLSlowBump";
            this.srFLSlowBump.Size = new System.Drawing.Size(134, 19);
            this.srFLSlowBump.TabIndex = 7;
            this.srFLSlowBump.ValueChangeListener = null;
            // 
            // srFLSpring
            // 
            this.srFLSpring.BackColor = System.Drawing.Color.Transparent;
            this.srFLSpring.ChangeEnabled = true;
            this.srFLSpring.Detachable = false;
            this.srFLSpring.Location = new System.Drawing.Point(90, 66);
            this.srFLSpring.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFLSpring.Name = "srFLSpring";
            this.srFLSpring.Size = new System.Drawing.Size(134, 18);
            this.srFLSpring.TabIndex = 0;
            this.srFLSpring.ValueChangeListener = null;
            // 
            // tabPage17
            // 
            this.tabPage17.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage17.Controls.Add(this.chkSymmetric01);
            this.tabPage17.Controls.Add(this.lblSetupTrackbar);
            this.tabPage17.Controls.Add(this.label125);
            this.tabPage17.Controls.Add(this.lblRToeOffset);
            this.tabPage17.Controls.Add(this.label129);
            this.tabPage17.Controls.Add(this.label130);
            this.tabPage17.Controls.Add(this.lblRThirdSpring);
            this.tabPage17.Controls.Add(this.label132);
            this.tabPage17.Controls.Add(this.label133);
            this.tabPage17.Controls.Add(this.label134);
            this.tabPage17.Controls.Add(this.label135);
            this.tabPage17.Controls.Add(this.label136);
            this.tabPage17.Controls.Add(this.label137);
            this.tabPage17.Controls.Add(this.label138);
            this.tabPage17.Controls.Add(this.label139);
            this.tabPage17.Controls.Add(this.label140);
            this.tabPage17.Controls.Add(this.srRRRideHeight);
            this.tabPage17.Controls.Add(this.srRRPacker);
            this.tabPage17.Controls.Add(this.srRRFastRebound);
            this.tabPage17.Controls.Add(this.srRRFastBump);
            this.tabPage17.Controls.Add(this.srRRSlowRebound);
            this.tabPage17.Controls.Add(this.srRRSlowBump);
            this.tabPage17.Controls.Add(this.srRRCamber);
            this.tabPage17.Controls.Add(this.srRRTrackbar);
            this.tabPage17.Controls.Add(this.srRRSpring);
            this.tabPage17.Controls.Add(this.srRPacker);
            this.tabPage17.Controls.Add(this.srRFastRebound);
            this.tabPage17.Controls.Add(this.srRFastBump);
            this.tabPage17.Controls.Add(this.srRSlowRebound);
            this.tabPage17.Controls.Add(this.srRSlowBump);
            this.tabPage17.Controls.Add(this.srRSpring);
            this.tabPage17.Controls.Add(this.srRLRideHeight);
            this.tabPage17.Controls.Add(this.srRLPacker);
            this.tabPage17.Controls.Add(this.srRLFastRebound);
            this.tabPage17.Controls.Add(this.srRLFastBump);
            this.tabPage17.Controls.Add(this.srRLSlowRebound);
            this.tabPage17.Controls.Add(this.srRLSlowBump);
            this.tabPage17.Controls.Add(this.srRLSpring);
            this.tabPage17.Controls.Add(this.srRLTrackbar);
            this.tabPage17.Controls.Add(this.srRLCamber);
            this.tabPage17.Controls.Add(this.srRToeOffset);
            this.tabPage17.Controls.Add(this.srRToeIn);
            this.tabPage17.Controls.Add(this.srRARB);
            this.tabPage17.Location = new System.Drawing.Point(4, 22);
            this.tabPage17.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage17.Size = new System.Drawing.Size(678, 295);
            this.tabPage17.TabIndex = 3;
            this.tabPage17.Text = "Rear Suspension";
            // 
            // chkSymmetric01
            // 
            this.chkSymmetric01.AutoSize = true;
            this.chkSymmetric01.Location = new System.Drawing.Point(266, 267);
            this.chkSymmetric01.Margin = new System.Windows.Forms.Padding(2);
            this.chkSymmetric01.Name = "chkSymmetric01";
            this.chkSymmetric01.Size = new System.Drawing.Size(75, 17);
            this.chkSymmetric01.TabIndex = 115;
            this.chkSymmetric01.Text = "Symmetric";
            this.chkSymmetric01.UseVisualStyleBackColor = true;
            // 
            // lblSetupTrackbar
            // 
            this.lblSetupTrackbar.AutoSize = true;
            this.lblSetupTrackbar.Location = new System.Drawing.Point(32, 48);
            this.lblSetupTrackbar.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSetupTrackbar.Name = "lblSetupTrackbar";
            this.lblSetupTrackbar.Size = new System.Drawing.Size(53, 13);
            this.lblSetupTrackbar.TabIndex = 89;
            this.lblSetupTrackbar.Text = "Trackbar:";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(37, 24);
            this.label125.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(48, 13);
            this.label125.TabIndex = 88;
            this.label125.Text = "Camber:";
            // 
            // lblRToeOffset
            // 
            this.lblRToeOffset.AutoSize = true;
            this.lblRToeOffset.Location = new System.Drawing.Point(568, 119);
            this.lblRToeOffset.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRToeOffset.Name = "lblRToeOffset";
            this.lblRToeOffset.Size = new System.Drawing.Size(64, 13);
            this.lblRToeOffset.TabIndex = 83;
            this.lblRToeOffset.Text = "Toe-Offset:";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(578, 74);
            this.label129.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(43, 13);
            this.label129.TabIndex = 81;
            this.label129.Text = "Toe-In:";
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(568, 24);
            this.label130.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(67, 13);
            this.label130.TabIndex = 79;
            this.label130.Text = "Anti-Rollbar:";
            // 
            // lblRThirdSpring
            // 
            this.lblRThirdSpring.AutoSize = true;
            this.lblRThirdSpring.Location = new System.Drawing.Point(263, 2);
            this.lblRThirdSpring.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRThirdSpring.Name = "lblRThirdSpring";
            this.lblRThirdSpring.Size = new System.Drawing.Size(61, 13);
            this.lblRThirdSpring.TabIndex = 77;
            this.lblRThirdSpring.Text = "3rd-Spring:";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(420, 2);
            this.label132.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(36, 13);
            this.label132.TabIndex = 76;
            this.label132.Text = "Right:";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(144, 2);
            this.label133.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(30, 13);
            this.label133.TabIndex = 75;
            this.label133.Text = "Left:";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(20, 215);
            this.label134.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(66, 13);
            this.label134.TabIndex = 61;
            this.label134.Text = "Ride Height:";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(38, 191);
            this.label135.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(48, 13);
            this.label135.TabIndex = 59;
            this.label135.Text = "Packers:";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(7, 167);
            this.label136.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(78, 13);
            this.label136.TabIndex = 57;
            this.label136.Text = "Fast Rebound:";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(23, 143);
            this.label137.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(61, 13);
            this.label137.TabIndex = 55;
            this.label137.Text = "Fast Bump:";
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(5, 119);
            this.label138.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(79, 13);
            this.label138.TabIndex = 53;
            this.label138.Text = "Slow Rebound:";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(21, 95);
            this.label139.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(62, 13);
            this.label139.TabIndex = 51;
            this.label139.Text = "Slow Bump:";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(44, 71);
            this.label140.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(41, 13);
            this.label140.TabIndex = 49;
            this.label140.Text = "Spring:";
            // 
            // srRRRideHeight
            // 
            this.srRRRideHeight.BackColor = System.Drawing.Color.Transparent;
            this.srRRRideHeight.ChangeEnabled = true;
            this.srRRRideHeight.Detachable = false;
            this.srRRRideHeight.Location = new System.Drawing.Point(367, 210);
            this.srRRRideHeight.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRRRideHeight.Name = "srRRRideHeight";
            this.srRRRideHeight.Size = new System.Drawing.Size(134, 19);
            this.srRRRideHeight.TabIndex = 114;
            this.srRRRideHeight.ValueChangeListener = null;
            // 
            // srRRPacker
            // 
            this.srRRPacker.BackColor = System.Drawing.Color.Transparent;
            this.srRRPacker.ChangeEnabled = true;
            this.srRRPacker.Detachable = false;
            this.srRRPacker.Location = new System.Drawing.Point(367, 186);
            this.srRRPacker.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRRPacker.Name = "srRRPacker";
            this.srRRPacker.Size = new System.Drawing.Size(134, 19);
            this.srRRPacker.TabIndex = 113;
            this.srRRPacker.ValueChangeListener = null;
            // 
            // srRRFastRebound
            // 
            this.srRRFastRebound.BackColor = System.Drawing.Color.Transparent;
            this.srRRFastRebound.ChangeEnabled = true;
            this.srRRFastRebound.Detachable = false;
            this.srRRFastRebound.Location = new System.Drawing.Point(367, 162);
            this.srRRFastRebound.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRRFastRebound.Name = "srRRFastRebound";
            this.srRRFastRebound.Size = new System.Drawing.Size(134, 19);
            this.srRRFastRebound.TabIndex = 112;
            this.srRRFastRebound.ValueChangeListener = null;
            // 
            // srRRFastBump
            // 
            this.srRRFastBump.BackColor = System.Drawing.Color.Transparent;
            this.srRRFastBump.ChangeEnabled = true;
            this.srRRFastBump.Detachable = false;
            this.srRRFastBump.Location = new System.Drawing.Point(367, 138);
            this.srRRFastBump.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRRFastBump.Name = "srRRFastBump";
            this.srRRFastBump.Size = new System.Drawing.Size(134, 19);
            this.srRRFastBump.TabIndex = 111;
            this.srRRFastBump.ValueChangeListener = null;
            // 
            // srRRSlowRebound
            // 
            this.srRRSlowRebound.BackColor = System.Drawing.Color.Transparent;
            this.srRRSlowRebound.ChangeEnabled = true;
            this.srRRSlowRebound.Detachable = false;
            this.srRRSlowRebound.Location = new System.Drawing.Point(367, 114);
            this.srRRSlowRebound.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRRSlowRebound.Name = "srRRSlowRebound";
            this.srRRSlowRebound.Size = new System.Drawing.Size(134, 19);
            this.srRRSlowRebound.TabIndex = 110;
            this.srRRSlowRebound.ValueChangeListener = null;
            // 
            // srRRSlowBump
            // 
            this.srRRSlowBump.BackColor = System.Drawing.Color.Transparent;
            this.srRRSlowBump.ChangeEnabled = true;
            this.srRRSlowBump.Detachable = false;
            this.srRRSlowBump.Location = new System.Drawing.Point(367, 90);
            this.srRRSlowBump.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRRSlowBump.Name = "srRRSlowBump";
            this.srRRSlowBump.Size = new System.Drawing.Size(134, 19);
            this.srRRSlowBump.TabIndex = 109;
            this.srRRSlowBump.ValueChangeListener = null;
            // 
            // srRRCamber
            // 
            this.srRRCamber.BackColor = System.Drawing.Color.Transparent;
            this.srRRCamber.ChangeEnabled = true;
            this.srRRCamber.Detachable = false;
            this.srRRCamber.Location = new System.Drawing.Point(367, 18);
            this.srRRCamber.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRRCamber.Name = "srRRCamber";
            this.srRRCamber.Size = new System.Drawing.Size(134, 19);
            this.srRRCamber.TabIndex = 108;
            this.srRRCamber.ValueChangeListener = null;
            // 
            // srRRTrackbar
            // 
            this.srRRTrackbar.BackColor = System.Drawing.Color.Transparent;
            this.srRRTrackbar.ChangeEnabled = true;
            this.srRRTrackbar.Detachable = false;
            this.srRRTrackbar.Location = new System.Drawing.Point(367, 42);
            this.srRRTrackbar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRRTrackbar.Name = "srRRTrackbar";
            this.srRRTrackbar.Size = new System.Drawing.Size(134, 19);
            this.srRRTrackbar.TabIndex = 107;
            this.srRRTrackbar.ValueChangeListener = null;
            // 
            // srRRSpring
            // 
            this.srRRSpring.BackColor = System.Drawing.Color.Transparent;
            this.srRRSpring.ChangeEnabled = true;
            this.srRRSpring.Detachable = false;
            this.srRRSpring.Location = new System.Drawing.Point(367, 66);
            this.srRRSpring.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRRSpring.Name = "srRRSpring";
            this.srRRSpring.Size = new System.Drawing.Size(134, 19);
            this.srRRSpring.TabIndex = 106;
            this.srRRSpring.ValueChangeListener = null;
            // 
            // srRPacker
            // 
            this.srRPacker.BackColor = System.Drawing.Color.Transparent;
            this.srRPacker.ChangeEnabled = true;
            this.srRPacker.Detachable = false;
            this.srRPacker.Location = new System.Drawing.Point(229, 186);
            this.srRPacker.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRPacker.Name = "srRPacker";
            this.srRPacker.Size = new System.Drawing.Size(134, 19);
            this.srRPacker.TabIndex = 105;
            this.srRPacker.ValueChangeListener = null;
            // 
            // srRFastRebound
            // 
            this.srRFastRebound.BackColor = System.Drawing.Color.Transparent;
            this.srRFastRebound.ChangeEnabled = true;
            this.srRFastRebound.Detachable = false;
            this.srRFastRebound.Location = new System.Drawing.Point(229, 162);
            this.srRFastRebound.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRFastRebound.Name = "srRFastRebound";
            this.srRFastRebound.Size = new System.Drawing.Size(134, 19);
            this.srRFastRebound.TabIndex = 104;
            this.srRFastRebound.ValueChangeListener = null;
            // 
            // srRFastBump
            // 
            this.srRFastBump.BackColor = System.Drawing.Color.Transparent;
            this.srRFastBump.ChangeEnabled = true;
            this.srRFastBump.Detachable = false;
            this.srRFastBump.Location = new System.Drawing.Point(229, 138);
            this.srRFastBump.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRFastBump.Name = "srRFastBump";
            this.srRFastBump.Size = new System.Drawing.Size(134, 19);
            this.srRFastBump.TabIndex = 103;
            this.srRFastBump.ValueChangeListener = null;
            // 
            // srRSlowRebound
            // 
            this.srRSlowRebound.BackColor = System.Drawing.Color.Transparent;
            this.srRSlowRebound.ChangeEnabled = true;
            this.srRSlowRebound.Detachable = false;
            this.srRSlowRebound.Location = new System.Drawing.Point(229, 114);
            this.srRSlowRebound.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRSlowRebound.Name = "srRSlowRebound";
            this.srRSlowRebound.Size = new System.Drawing.Size(134, 19);
            this.srRSlowRebound.TabIndex = 102;
            this.srRSlowRebound.ValueChangeListener = null;
            // 
            // srRSlowBump
            // 
            this.srRSlowBump.BackColor = System.Drawing.Color.Transparent;
            this.srRSlowBump.ChangeEnabled = true;
            this.srRSlowBump.Detachable = false;
            this.srRSlowBump.Location = new System.Drawing.Point(229, 90);
            this.srRSlowBump.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRSlowBump.Name = "srRSlowBump";
            this.srRSlowBump.Size = new System.Drawing.Size(134, 19);
            this.srRSlowBump.TabIndex = 101;
            this.srRSlowBump.ValueChangeListener = null;
            // 
            // srRSpring
            // 
            this.srRSpring.BackColor = System.Drawing.Color.Transparent;
            this.srRSpring.ChangeEnabled = true;
            this.srRSpring.Detachable = false;
            this.srRSpring.Location = new System.Drawing.Point(229, 66);
            this.srRSpring.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRSpring.Name = "srRSpring";
            this.srRSpring.Size = new System.Drawing.Size(134, 19);
            this.srRSpring.TabIndex = 100;
            this.srRSpring.ValueChangeListener = null;
            // 
            // srRLRideHeight
            // 
            this.srRLRideHeight.BackColor = System.Drawing.Color.Transparent;
            this.srRLRideHeight.ChangeEnabled = true;
            this.srRLRideHeight.Detachable = false;
            this.srRLRideHeight.Location = new System.Drawing.Point(90, 210);
            this.srRLRideHeight.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRLRideHeight.Name = "srRLRideHeight";
            this.srRLRideHeight.Size = new System.Drawing.Size(134, 19);
            this.srRLRideHeight.TabIndex = 99;
            this.srRLRideHeight.ValueChangeListener = null;
            // 
            // srRLPacker
            // 
            this.srRLPacker.BackColor = System.Drawing.Color.Transparent;
            this.srRLPacker.ChangeEnabled = true;
            this.srRLPacker.Detachable = false;
            this.srRLPacker.Location = new System.Drawing.Point(90, 186);
            this.srRLPacker.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRLPacker.Name = "srRLPacker";
            this.srRLPacker.Size = new System.Drawing.Size(134, 19);
            this.srRLPacker.TabIndex = 98;
            this.srRLPacker.ValueChangeListener = null;
            // 
            // srRLFastRebound
            // 
            this.srRLFastRebound.BackColor = System.Drawing.Color.Transparent;
            this.srRLFastRebound.ChangeEnabled = true;
            this.srRLFastRebound.Detachable = false;
            this.srRLFastRebound.Location = new System.Drawing.Point(90, 162);
            this.srRLFastRebound.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRLFastRebound.Name = "srRLFastRebound";
            this.srRLFastRebound.Size = new System.Drawing.Size(134, 19);
            this.srRLFastRebound.TabIndex = 97;
            this.srRLFastRebound.ValueChangeListener = null;
            // 
            // srRLFastBump
            // 
            this.srRLFastBump.BackColor = System.Drawing.Color.Transparent;
            this.srRLFastBump.ChangeEnabled = true;
            this.srRLFastBump.Detachable = false;
            this.srRLFastBump.Location = new System.Drawing.Point(90, 138);
            this.srRLFastBump.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRLFastBump.Name = "srRLFastBump";
            this.srRLFastBump.Size = new System.Drawing.Size(134, 19);
            this.srRLFastBump.TabIndex = 96;
            this.srRLFastBump.ValueChangeListener = null;
            // 
            // srRLSlowRebound
            // 
            this.srRLSlowRebound.BackColor = System.Drawing.Color.Transparent;
            this.srRLSlowRebound.ChangeEnabled = true;
            this.srRLSlowRebound.Detachable = false;
            this.srRLSlowRebound.Location = new System.Drawing.Point(90, 114);
            this.srRLSlowRebound.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRLSlowRebound.Name = "srRLSlowRebound";
            this.srRLSlowRebound.Size = new System.Drawing.Size(134, 19);
            this.srRLSlowRebound.TabIndex = 95;
            this.srRLSlowRebound.ValueChangeListener = null;
            // 
            // srRLSlowBump
            // 
            this.srRLSlowBump.BackColor = System.Drawing.Color.Transparent;
            this.srRLSlowBump.ChangeEnabled = true;
            this.srRLSlowBump.Detachable = false;
            this.srRLSlowBump.Location = new System.Drawing.Point(90, 90);
            this.srRLSlowBump.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRLSlowBump.Name = "srRLSlowBump";
            this.srRLSlowBump.Size = new System.Drawing.Size(134, 19);
            this.srRLSlowBump.TabIndex = 94;
            this.srRLSlowBump.ValueChangeListener = null;
            // 
            // srRLSpring
            // 
            this.srRLSpring.BackColor = System.Drawing.Color.Transparent;
            this.srRLSpring.ChangeEnabled = true;
            this.srRLSpring.Detachable = false;
            this.srRLSpring.Location = new System.Drawing.Point(90, 66);
            this.srRLSpring.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRLSpring.Name = "srRLSpring";
            this.srRLSpring.Size = new System.Drawing.Size(134, 19);
            this.srRLSpring.TabIndex = 93;
            this.srRLSpring.ValueChangeListener = null;
            // 
            // srRLTrackbar
            // 
            this.srRLTrackbar.BackColor = System.Drawing.Color.Transparent;
            this.srRLTrackbar.ChangeEnabled = true;
            this.srRLTrackbar.Detachable = false;
            this.srRLTrackbar.Location = new System.Drawing.Point(90, 42);
            this.srRLTrackbar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRLTrackbar.Name = "srRLTrackbar";
            this.srRLTrackbar.Size = new System.Drawing.Size(134, 19);
            this.srRLTrackbar.TabIndex = 92;
            this.srRLTrackbar.ValueChangeListener = null;
            // 
            // srRLCamber
            // 
            this.srRLCamber.BackColor = System.Drawing.Color.Transparent;
            this.srRLCamber.ChangeEnabled = true;
            this.srRLCamber.Detachable = false;
            this.srRLCamber.Location = new System.Drawing.Point(90, 18);
            this.srRLCamber.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRLCamber.Name = "srRLCamber";
            this.srRLCamber.Size = new System.Drawing.Size(134, 19);
            this.srRLCamber.TabIndex = 91;
            this.srRLCamber.ValueChangeListener = null;
            // 
            // srRToeOffset
            // 
            this.srRToeOffset.BackColor = System.Drawing.Color.Transparent;
            this.srRToeOffset.ChangeEnabled = true;
            this.srRToeOffset.Detachable = false;
            this.srRToeOffset.Location = new System.Drawing.Point(533, 138);
            this.srRToeOffset.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRToeOffset.Name = "srRToeOffset";
            this.srRToeOffset.Size = new System.Drawing.Size(134, 21);
            this.srRToeOffset.TabIndex = 82;
            this.srRToeOffset.ValueChangeListener = null;
            // 
            // srRToeIn
            // 
            this.srRToeIn.BackColor = System.Drawing.Color.Transparent;
            this.srRToeIn.ChangeEnabled = true;
            this.srRToeIn.Detachable = false;
            this.srRToeIn.Location = new System.Drawing.Point(533, 90);
            this.srRToeIn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRToeIn.Name = "srRToeIn";
            this.srRToeIn.Size = new System.Drawing.Size(134, 21);
            this.srRToeIn.TabIndex = 80;
            this.srRToeIn.ValueChangeListener = null;
            // 
            // srRARB
            // 
            this.srRARB.BackColor = System.Drawing.Color.Transparent;
            this.srRARB.ChangeEnabled = true;
            this.srRARB.Detachable = false;
            this.srRARB.Location = new System.Drawing.Point(533, 42);
            this.srRARB.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRARB.Name = "srRARB";
            this.srRARB.Size = new System.Drawing.Size(134, 19);
            this.srRARB.TabIndex = 78;
            this.srRARB.ValueChangeListener = null;
            // 
            // tabPage18
            // 
            this.tabPage18.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage18.Controls.Add(this.gbSpringRubber);
            this.tabPage18.Controls.Add(this.gbChassisAdj);
            this.tabPage18.Controls.Add(this.groupBox3);
            this.tabPage18.Location = new System.Drawing.Point(4, 22);
            this.tabPage18.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage18.Size = new System.Drawing.Size(678, 295);
            this.tabPage18.TabIndex = 4;
            this.tabPage18.Text = "Chassis";
            // 
            // gbSpringRubber
            // 
            this.gbSpringRubber.Controls.Add(this.label145);
            this.gbSpringRubber.Controls.Add(this.label144);
            this.gbSpringRubber.Controls.Add(this.srSpringRubberRL);
            this.gbSpringRubber.Controls.Add(this.srSpringRubberRR);
            this.gbSpringRubber.Controls.Add(this.srSpringRubberFL);
            this.gbSpringRubber.Controls.Add(this.label143);
            this.gbSpringRubber.Controls.Add(this.label142);
            this.gbSpringRubber.Controls.Add(this.srSpringRubberFR);
            this.gbSpringRubber.Location = new System.Drawing.Point(356, 210);
            this.gbSpringRubber.Margin = new System.Windows.Forms.Padding(2);
            this.gbSpringRubber.Name = "gbSpringRubber";
            this.gbSpringRubber.Padding = new System.Windows.Forms.Padding(2);
            this.gbSpringRubber.Size = new System.Drawing.Size(319, 82);
            this.gbSpringRubber.TabIndex = 102;
            this.gbSpringRubber.TabStop = false;
            this.gbSpringRubber.Text = "Spring Rubber";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Location = new System.Drawing.Point(5, 55);
            this.label145.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(34, 13);
            this.label145.TabIndex = 106;
            this.label145.Text = "Rear:";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Location = new System.Drawing.Point(1, 32);
            this.label144.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(37, 13);
            this.label144.TabIndex = 105;
            this.label144.Text = "Front:";
            // 
            // srSpringRubberRL
            // 
            this.srSpringRubberRL.BackColor = System.Drawing.Color.Transparent;
            this.srSpringRubberRL.ChangeEnabled = true;
            this.srSpringRubberRL.Detachable = false;
            this.srSpringRubberRL.Location = new System.Drawing.Point(42, 55);
            this.srSpringRubberRL.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srSpringRubberRL.Name = "srSpringRubberRL";
            this.srSpringRubberRL.Size = new System.Drawing.Size(134, 19);
            this.srSpringRubberRL.TabIndex = 104;
            this.srSpringRubberRL.ValueChangeListener = null;
            // 
            // srSpringRubberRR
            // 
            this.srSpringRubberRR.BackColor = System.Drawing.Color.Transparent;
            this.srSpringRubberRR.ChangeEnabled = true;
            this.srSpringRubberRR.Detachable = false;
            this.srSpringRubberRR.Location = new System.Drawing.Point(181, 55);
            this.srSpringRubberRR.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srSpringRubberRR.Name = "srSpringRubberRR";
            this.srSpringRubberRR.Size = new System.Drawing.Size(134, 19);
            this.srSpringRubberRR.TabIndex = 103;
            this.srSpringRubberRR.ValueChangeListener = null;
            // 
            // srSpringRubberFL
            // 
            this.srSpringRubberFL.BackColor = System.Drawing.Color.Transparent;
            this.srSpringRubberFL.ChangeEnabled = true;
            this.srSpringRubberFL.Detachable = false;
            this.srSpringRubberFL.Location = new System.Drawing.Point(42, 32);
            this.srSpringRubberFL.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srSpringRubberFL.Name = "srSpringRubberFL";
            this.srSpringRubberFL.Size = new System.Drawing.Size(134, 18);
            this.srSpringRubberFL.TabIndex = 102;
            this.srSpringRubberFL.ValueChangeListener = null;
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Location = new System.Drawing.Point(93, 16);
            this.label143.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(30, 13);
            this.label143.TabIndex = 101;
            this.label143.Text = "Left:";
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(232, 16);
            this.label142.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(36, 13);
            this.label142.TabIndex = 100;
            this.label142.Text = "Right:";
            // 
            // srSpringRubberFR
            // 
            this.srSpringRubberFR.BackColor = System.Drawing.Color.Transparent;
            this.srSpringRubberFR.ChangeEnabled = true;
            this.srSpringRubberFR.Detachable = false;
            this.srSpringRubberFR.Location = new System.Drawing.Point(181, 32);
            this.srSpringRubberFR.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srSpringRubberFR.Name = "srSpringRubberFR";
            this.srSpringRubberFR.Size = new System.Drawing.Size(134, 18);
            this.srSpringRubberFR.TabIndex = 0;
            this.srSpringRubberFR.ValueChangeListener = null;
            // 
            // gbChassisAdj
            // 
            this.gbChassisAdj.Controls.Add(this.srChassisAdj00);
            this.gbChassisAdj.Controls.Add(this.lblChassisAdj00);
            this.gbChassisAdj.Location = new System.Drawing.Point(6, 6);
            this.gbChassisAdj.Margin = new System.Windows.Forms.Padding(2);
            this.gbChassisAdj.Name = "gbChassisAdj";
            this.gbChassisAdj.Padding = new System.Windows.Forms.Padding(2);
            this.gbChassisAdj.Size = new System.Drawing.Size(274, 226);
            this.gbChassisAdj.TabIndex = 101;
            this.gbChassisAdj.TabStop = false;
            this.gbChassisAdj.Text = "Chassis Adjustments";
            // 
            // srChassisAdj00
            // 
            this.srChassisAdj00.BackColor = System.Drawing.Color.Transparent;
            this.srChassisAdj00.ChangeEnabled = true;
            this.srChassisAdj00.Detachable = false;
            this.srChassisAdj00.Location = new System.Drawing.Point(131, 16);
            this.srChassisAdj00.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srChassisAdj00.Name = "srChassisAdj00";
            this.srChassisAdj00.Size = new System.Drawing.Size(134, 18);
            this.srChassisAdj00.TabIndex = 101;
            this.srChassisAdj00.ValueChangeListener = null;
            // 
            // lblChassisAdj00
            // 
            this.lblChassisAdj00.AutoSize = true;
            this.lblChassisAdj00.Location = new System.Drawing.Point(5, 16);
            this.lblChassisAdj00.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblChassisAdj00.Name = "lblChassisAdj00";
            this.lblChassisAdj00.Size = new System.Drawing.Size(120, 13);
            this.lblChassisAdj00.TabIndex = 100;
            this.lblChassisAdj00.Text = "Chassis Adjustment 00:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.srWeightLong);
            this.groupBox3.Controls.Add(this.srWeightLateral);
            this.groupBox3.Controls.Add(this.srWedge);
            this.groupBox3.Controls.Add(this.label141);
            this.groupBox3.Controls.Add(this.srWeightHeight);
            this.groupBox3.Controls.Add(this.label131);
            this.groupBox3.Controls.Add(this.label128);
            this.groupBox3.Controls.Add(this.label122);
            this.groupBox3.Location = new System.Drawing.Point(475, 5);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(200, 125);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Weight Distribution";
            // 
            // srWeightLong
            // 
            this.srWeightLong.BackColor = System.Drawing.Color.Transparent;
            this.srWeightLong.ChangeEnabled = true;
            this.srWeightLong.Detachable = false;
            this.srWeightLong.Location = new System.Drawing.Point(62, 39);
            this.srWeightLong.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srWeightLong.Name = "srWeightLong";
            this.srWeightLong.Size = new System.Drawing.Size(125, 21);
            this.srWeightLong.TabIndex = 104;
            this.srWeightLong.ValueChangeListener = null;
            // 
            // srWeightLateral
            // 
            this.srWeightLateral.BackColor = System.Drawing.Color.Transparent;
            this.srWeightLateral.ChangeEnabled = true;
            this.srWeightLateral.Detachable = false;
            this.srWeightLateral.Location = new System.Drawing.Point(62, 14);
            this.srWeightLateral.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srWeightLateral.Name = "srWeightLateral";
            this.srWeightLateral.Size = new System.Drawing.Size(125, 21);
            this.srWeightLateral.TabIndex = 103;
            this.srWeightLateral.ValueChangeListener = null;
            // 
            // srWedge
            // 
            this.srWedge.BackColor = System.Drawing.Color.Transparent;
            this.srWedge.ChangeEnabled = true;
            this.srWedge.Detachable = false;
            this.srWedge.Location = new System.Drawing.Point(62, 89);
            this.srWedge.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srWedge.Name = "srWedge";
            this.srWedge.Size = new System.Drawing.Size(134, 21);
            this.srWedge.TabIndex = 99;
            this.srWedge.ValueChangeListener = null;
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Location = new System.Drawing.Point(11, 95);
            this.label141.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(45, 13);
            this.label141.TabIndex = 98;
            this.label141.Text = "Wedge:";
            // 
            // srWeightHeight
            // 
            this.srWeightHeight.BackColor = System.Drawing.Color.Transparent;
            this.srWeightHeight.ChangeEnabled = true;
            this.srWeightHeight.Detachable = false;
            this.srWeightHeight.Location = new System.Drawing.Point(62, 64);
            this.srWeightHeight.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srWeightHeight.Name = "srWeightHeight";
            this.srWeightHeight.Size = new System.Drawing.Size(134, 21);
            this.srWeightHeight.TabIndex = 97;
            this.srWeightHeight.ValueChangeListener = null;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(12, 70);
            this.label131.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(46, 13);
            this.label131.TabIndex = 96;
            this.label131.Text = "Vertical:";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(22, 44);
            this.label128.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(34, 13);
            this.label128.TabIndex = 94;
            this.label128.Text = "Long:";
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(14, 18);
            this.label122.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(44, 13);
            this.label122.TabIndex = 92;
            this.label122.Text = "Lateral:";
            // 
            // tabPage19
            // 
            this.tabPage19.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage19.Controls.Add(this.srTorqueSplit);
            this.tabPage19.Controls.Add(this.srBrakeBais);
            this.tabPage19.Controls.Add(this.lblTorqueSplit);
            this.tabPage19.Controls.Add(this.srHandBrakePressure);
            this.tabPage19.Controls.Add(this.srSteeringLock);
            this.tabPage19.Controls.Add(this.lblHandBrakePressure);
            this.tabPage19.Controls.Add(this.label165);
            this.tabPage19.Controls.Add(this.label164);
            this.tabPage19.Controls.Add(this.label163);
            this.tabPage19.Controls.Add(this.label157);
            this.tabPage19.Controls.Add(this.label158);
            this.tabPage19.Controls.Add(this.label159);
            this.tabPage19.Controls.Add(this.label160);
            this.tabPage19.Controls.Add(this.label161);
            this.tabPage19.Controls.Add(this.label162);
            this.tabPage19.Controls.Add(this.label156);
            this.tabPage19.Controls.Add(this.label155);
            this.tabPage19.Controls.Add(this.label154);
            this.tabPage19.Controls.Add(this.label153);
            this.tabPage19.Controls.Add(this.label152);
            this.tabPage19.Controls.Add(this.label151);
            this.tabPage19.Controls.Add(this.srBrakePressure);
            this.tabPage19.Controls.Add(this.srRRDisc);
            this.tabPage19.Controls.Add(this.srRRPressure);
            this.tabPage19.Controls.Add(this.srRLDisc);
            this.tabPage19.Controls.Add(this.srRCompound);
            this.tabPage19.Controls.Add(this.srRLPressure);
            this.tabPage19.Controls.Add(this.srFRDisc);
            this.tabPage19.Controls.Add(this.srFRPressure);
            this.tabPage19.Controls.Add(this.srFLDisc);
            this.tabPage19.Controls.Add(this.srFCompound);
            this.tabPage19.Controls.Add(this.srFLPressure);
            this.tabPage19.Location = new System.Drawing.Point(4, 22);
            this.tabPage19.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage19.Size = new System.Drawing.Size(678, 295);
            this.tabPage19.TabIndex = 5;
            this.tabPage19.Text = "Wheels";
            // 
            // srTorqueSplit
            // 
            this.srTorqueSplit.BackColor = System.Drawing.Color.Transparent;
            this.srTorqueSplit.ChangeEnabled = true;
            this.srTorqueSplit.Detachable = false;
            this.srTorqueSplit.Location = new System.Drawing.Point(508, 135);
            this.srTorqueSplit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srTorqueSplit.Name = "srTorqueSplit";
            this.srTorqueSplit.Size = new System.Drawing.Size(125, 18);
            this.srTorqueSplit.TabIndex = 79;
            this.srTorqueSplit.ValueChangeListener = null;
            // 
            // srBrakeBais
            // 
            this.srBrakeBais.BackColor = System.Drawing.Color.Transparent;
            this.srBrakeBais.ChangeEnabled = true;
            this.srBrakeBais.Detachable = false;
            this.srBrakeBais.Location = new System.Drawing.Point(508, 88);
            this.srBrakeBais.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srBrakeBais.Name = "srBrakeBais";
            this.srBrakeBais.Size = new System.Drawing.Size(125, 18);
            this.srBrakeBais.TabIndex = 78;
            this.srBrakeBais.ValueChangeListener = null;
            // 
            // lblTorqueSplit
            // 
            this.lblTorqueSplit.AutoSize = true;
            this.lblTorqueSplit.Location = new System.Drawing.Point(434, 140);
            this.lblTorqueSplit.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTorqueSplit.Name = "lblTorqueSplit";
            this.lblTorqueSplit.Size = new System.Drawing.Size(68, 13);
            this.lblTorqueSplit.TabIndex = 76;
            this.lblTorqueSplit.Text = "Torque Split:";
            // 
            // srHandBrakePressure
            // 
            this.srHandBrakePressure.BackColor = System.Drawing.Color.Transparent;
            this.srHandBrakePressure.ChangeEnabled = true;
            this.srHandBrakePressure.Detachable = false;
            this.srHandBrakePressure.Location = new System.Drawing.Point(508, 111);
            this.srHandBrakePressure.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srHandBrakePressure.Name = "srHandBrakePressure";
            this.srHandBrakePressure.Size = new System.Drawing.Size(134, 18);
            this.srHandBrakePressure.TabIndex = 75;
            this.srHandBrakePressure.ValueChangeListener = null;
            // 
            // srSteeringLock
            // 
            this.srSteeringLock.BackColor = System.Drawing.Color.Transparent;
            this.srSteeringLock.ChangeEnabled = true;
            this.srSteeringLock.Detachable = false;
            this.srSteeringLock.Location = new System.Drawing.Point(508, 42);
            this.srSteeringLock.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srSteeringLock.Name = "srSteeringLock";
            this.srSteeringLock.Size = new System.Drawing.Size(134, 18);
            this.srSteeringLock.TabIndex = 74;
            this.srSteeringLock.ValueChangeListener = null;
            // 
            // lblHandBrakePressure
            // 
            this.lblHandBrakePressure.AutoSize = true;
            this.lblHandBrakePressure.Location = new System.Drawing.Point(395, 116);
            this.lblHandBrakePressure.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblHandBrakePressure.Name = "lblHandBrakePressure";
            this.lblHandBrakePressure.Size = new System.Drawing.Size(108, 13);
            this.lblHandBrakePressure.TabIndex = 73;
            this.lblHandBrakePressure.Text = "Handbrake Pressure:";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.Location = new System.Drawing.Point(426, 46);
            this.label165.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(75, 13);
            this.label165.TabIndex = 72;
            this.label165.Text = "Steering Lock:";
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Location = new System.Drawing.Point(444, 93);
            this.label164.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(60, 13);
            this.label164.TabIndex = 71;
            this.label164.Text = "Brake Bais:";
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Location = new System.Drawing.Point(421, 70);
            this.label163.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(83, 13);
            this.label163.TabIndex = 69;
            this.label163.Text = "Brake Pressure:";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Location = new System.Drawing.Point(53, 102);
            this.label157.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(34, 13);
            this.label157.TabIndex = 67;
            this.label157.Text = "Rear:";
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(275, 102);
            this.label158.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(36, 13);
            this.label158.TabIndex = 65;
            this.label158.Text = "Right:";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Location = new System.Drawing.Point(5, 169);
            this.label159.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(79, 13);
            this.label159.TabIndex = 63;
            this.label159.Text = "Disc Thickness:";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Location = new System.Drawing.Point(34, 122);
            this.label160.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(53, 13);
            this.label160.TabIndex = 60;
            this.label160.Text = "Pressure:";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Location = new System.Drawing.Point(141, 102);
            this.label161.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(30, 13);
            this.label161.TabIndex = 59;
            this.label161.Text = "Left:";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(20, 146);
            this.label162.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(62, 13);
            this.label162.TabIndex = 58;
            this.label162.Text = "Compound:";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Location = new System.Drawing.Point(49, 2);
            this.label156.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(37, 13);
            this.label156.TabIndex = 56;
            this.label156.Text = "Front:";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Location = new System.Drawing.Point(275, 2);
            this.label155.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(36, 13);
            this.label155.TabIndex = 54;
            this.label155.Text = "Right:";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(5, 70);
            this.label154.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(79, 13);
            this.label154.TabIndex = 52;
            this.label154.Text = "Disc Thickness:";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(34, 23);
            this.label153.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(53, 13);
            this.label153.TabIndex = 49;
            this.label153.Text = "Pressure:";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(141, 2);
            this.label152.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(30, 13);
            this.label152.TabIndex = 48;
            this.label152.Text = "Left:";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(20, 46);
            this.label151.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(62, 13);
            this.label151.TabIndex = 47;
            this.label151.Text = "Compound:";
            // 
            // srBrakePressure
            // 
            this.srBrakePressure.BackColor = System.Drawing.Color.Transparent;
            this.srBrakePressure.ChangeEnabled = true;
            this.srBrakePressure.Detachable = false;
            this.srBrakePressure.Location = new System.Drawing.Point(508, 65);
            this.srBrakePressure.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srBrakePressure.Name = "srBrakePressure";
            this.srBrakePressure.Size = new System.Drawing.Size(134, 18);
            this.srBrakePressure.TabIndex = 68;
            this.srBrakePressure.ValueChangeListener = null;
            // 
            // srRRDisc
            // 
            this.srRRDisc.BackColor = System.Drawing.Color.Transparent;
            this.srRRDisc.ChangeEnabled = true;
            this.srRRDisc.Detachable = false;
            this.srRRDisc.Location = new System.Drawing.Point(229, 164);
            this.srRRDisc.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRRDisc.Name = "srRRDisc";
            this.srRRDisc.Size = new System.Drawing.Size(134, 18);
            this.srRRDisc.TabIndex = 66;
            this.srRRDisc.ValueChangeListener = null;
            // 
            // srRRPressure
            // 
            this.srRRPressure.BackColor = System.Drawing.Color.Transparent;
            this.srRRPressure.ChangeEnabled = true;
            this.srRRPressure.Detachable = false;
            this.srRRPressure.Location = new System.Drawing.Point(229, 118);
            this.srRRPressure.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRRPressure.Name = "srRRPressure";
            this.srRRPressure.Size = new System.Drawing.Size(134, 18);
            this.srRRPressure.TabIndex = 64;
            this.srRRPressure.ValueChangeListener = null;
            // 
            // srRLDisc
            // 
            this.srRLDisc.BackColor = System.Drawing.Color.Transparent;
            this.srRLDisc.ChangeEnabled = true;
            this.srRLDisc.Detachable = false;
            this.srRLDisc.Location = new System.Drawing.Point(90, 164);
            this.srRLDisc.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRLDisc.Name = "srRLDisc";
            this.srRLDisc.Size = new System.Drawing.Size(134, 18);
            this.srRLDisc.TabIndex = 62;
            this.srRLDisc.ValueChangeListener = null;
            // 
            // srRCompound
            // 
            this.srRCompound.BackColor = System.Drawing.Color.Transparent;
            this.srRCompound.ChangeEnabled = true;
            this.srRCompound.Detachable = false;
            this.srRCompound.Location = new System.Drawing.Point(90, 141);
            this.srRCompound.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRCompound.Name = "srRCompound";
            this.srRCompound.Size = new System.Drawing.Size(134, 18);
            this.srRCompound.TabIndex = 61;
            this.srRCompound.ValueChangeListener = null;
            // 
            // srRLPressure
            // 
            this.srRLPressure.BackColor = System.Drawing.Color.Transparent;
            this.srRLPressure.ChangeEnabled = true;
            this.srRLPressure.Detachable = false;
            this.srRLPressure.Location = new System.Drawing.Point(90, 118);
            this.srRLPressure.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRLPressure.Name = "srRLPressure";
            this.srRLPressure.Size = new System.Drawing.Size(134, 18);
            this.srRLPressure.TabIndex = 57;
            this.srRLPressure.ValueChangeListener = null;
            // 
            // srFRDisc
            // 
            this.srFRDisc.BackColor = System.Drawing.Color.Transparent;
            this.srFRDisc.ChangeEnabled = true;
            this.srFRDisc.Detachable = false;
            this.srFRDisc.Location = new System.Drawing.Point(229, 65);
            this.srFRDisc.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFRDisc.Name = "srFRDisc";
            this.srFRDisc.Size = new System.Drawing.Size(134, 18);
            this.srFRDisc.TabIndex = 55;
            this.srFRDisc.ValueChangeListener = null;
            // 
            // srFRPressure
            // 
            this.srFRPressure.BackColor = System.Drawing.Color.Transparent;
            this.srFRPressure.ChangeEnabled = true;
            this.srFRPressure.Detachable = false;
            this.srFRPressure.Location = new System.Drawing.Point(229, 18);
            this.srFRPressure.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFRPressure.Name = "srFRPressure";
            this.srFRPressure.Size = new System.Drawing.Size(134, 18);
            this.srFRPressure.TabIndex = 53;
            this.srFRPressure.ValueChangeListener = null;
            // 
            // srFLDisc
            // 
            this.srFLDisc.BackColor = System.Drawing.Color.Transparent;
            this.srFLDisc.ChangeEnabled = true;
            this.srFLDisc.Detachable = false;
            this.srFLDisc.Location = new System.Drawing.Point(90, 65);
            this.srFLDisc.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFLDisc.Name = "srFLDisc";
            this.srFLDisc.Size = new System.Drawing.Size(134, 18);
            this.srFLDisc.TabIndex = 51;
            this.srFLDisc.ValueChangeListener = null;
            // 
            // srFCompound
            // 
            this.srFCompound.BackColor = System.Drawing.Color.Transparent;
            this.srFCompound.ChangeEnabled = true;
            this.srFCompound.Detachable = false;
            this.srFCompound.Location = new System.Drawing.Point(90, 42);
            this.srFCompound.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFCompound.Name = "srFCompound";
            this.srFCompound.Size = new System.Drawing.Size(134, 18);
            this.srFCompound.TabIndex = 50;
            this.srFCompound.ValueChangeListener = null;
            // 
            // srFLPressure
            // 
            this.srFLPressure.BackColor = System.Drawing.Color.Transparent;
            this.srFLPressure.ChangeEnabled = true;
            this.srFLPressure.Detachable = false;
            this.srFLPressure.Location = new System.Drawing.Point(90, 18);
            this.srFLPressure.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFLPressure.Name = "srFLPressure";
            this.srFLPressure.Size = new System.Drawing.Size(134, 18);
            this.srFLPressure.TabIndex = 0;
            this.srFLPressure.ValueChangeListener = null;
            // 
            // tabPage20
            // 
            this.tabPage20.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage20.Controls.Add(this.groupBox9);
            this.tabPage20.Controls.Add(this.gbFender);
            this.tabPage20.Controls.Add(this.groupBox8);
            this.tabPage20.Controls.Add(this.groupBox6);
            this.tabPage20.Location = new System.Drawing.Point(4, 22);
            this.tabPage20.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage20.Size = new System.Drawing.Size(678, 295);
            this.tabPage20.TabIndex = 6;
            this.tabPage20.Text = "Aero";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label183);
            this.groupBox9.Controls.Add(this.label180);
            this.groupBox9.Controls.Add(this.tbAeroSpeed);
            this.groupBox9.Controls.Add(this.label181);
            this.groupBox9.Controls.Add(this.lblAeroDragFactor);
            this.groupBox9.Controls.Add(this.label182);
            this.groupBox9.Controls.Add(this.lblRearLift);
            this.groupBox9.Controls.Add(this.lblFrontDownforce);
            this.groupBox9.Controls.Add(this.lblFrontLift);
            this.groupBox9.Controls.Add(this.lblRearDownforce);
            this.groupBox9.Controls.Add(this.lblAeroDragForce);
            this.groupBox9.Location = new System.Drawing.Point(461, 14);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(208, 109);
            this.groupBox9.TabIndex = 97;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Downforce (Very inaccurate)";
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Location = new System.Drawing.Point(62, 17);
            this.label183.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(77, 13);
            this.label183.TabIndex = 96;
            this.label183.Text = "Speed (kmph):";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Location = new System.Drawing.Point(8, 35);
            this.label180.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(92, 13);
            this.label180.TabIndex = 76;
            this.label180.Text = "Front Downforce:";
            // 
            // tbAeroSpeed
            // 
            this.tbAeroSpeed.Location = new System.Drawing.Point(147, 14);
            this.tbAeroSpeed.Margin = new System.Windows.Forms.Padding(2);
            this.tbAeroSpeed.Name = "tbAeroSpeed";
            this.tbAeroSpeed.Size = new System.Drawing.Size(43, 21);
            this.tbAeroSpeed.TabIndex = 95;
            this.tbAeroSpeed.Text = "200";
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Location = new System.Drawing.Point(12, 59);
            this.label181.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(89, 13);
            this.label181.TabIndex = 87;
            this.label181.Text = "Rear Downforce:";
            // 
            // lblAeroDragFactor
            // 
            this.lblAeroDragFactor.AutoSize = true;
            this.lblAeroDragFactor.Location = new System.Drawing.Point(107, 85);
            this.lblAeroDragFactor.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAeroDragFactor.Name = "lblAeroDragFactor";
            this.lblAeroDragFactor.Size = new System.Drawing.Size(35, 13);
            this.lblAeroDragFactor.TabIndex = 94;
            this.lblAeroDragFactor.Text = "0.000";
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(67, 85);
            this.label182.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(34, 13);
            this.label182.TabIndex = 88;
            this.label182.Text = "Drag:";
            // 
            // lblRearLift
            // 
            this.lblRearLift.AutoSize = true;
            this.lblRearLift.Location = new System.Drawing.Point(107, 59);
            this.lblRearLift.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRearLift.Name = "lblRearLift";
            this.lblRearLift.Size = new System.Drawing.Size(35, 13);
            this.lblRearLift.TabIndex = 93;
            this.lblRearLift.Text = "0.000";
            // 
            // lblFrontDownforce
            // 
            this.lblFrontDownforce.AutoSize = true;
            this.lblFrontDownforce.Location = new System.Drawing.Point(147, 35);
            this.lblFrontDownforce.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFrontDownforce.Name = "lblFrontDownforce";
            this.lblFrontDownforce.Size = new System.Drawing.Size(47, 13);
            this.lblFrontDownforce.TabIndex = 89;
            this.lblFrontDownforce.Text = "00000 N";
            // 
            // lblFrontLift
            // 
            this.lblFrontLift.AutoSize = true;
            this.lblFrontLift.Location = new System.Drawing.Point(107, 35);
            this.lblFrontLift.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFrontLift.Name = "lblFrontLift";
            this.lblFrontLift.Size = new System.Drawing.Size(35, 13);
            this.lblFrontLift.TabIndex = 92;
            this.lblFrontLift.Text = "0.000";
            // 
            // lblRearDownforce
            // 
            this.lblRearDownforce.AutoSize = true;
            this.lblRearDownforce.Location = new System.Drawing.Point(147, 59);
            this.lblRearDownforce.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRearDownforce.Name = "lblRearDownforce";
            this.lblRearDownforce.Size = new System.Drawing.Size(47, 13);
            this.lblRearDownforce.TabIndex = 90;
            this.lblRearDownforce.Text = "00000 N";
            // 
            // lblAeroDragForce
            // 
            this.lblAeroDragForce.AutoSize = true;
            this.lblAeroDragForce.Location = new System.Drawing.Point(147, 85);
            this.lblAeroDragForce.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAeroDragForce.Name = "lblAeroDragForce";
            this.lblAeroDragForce.Size = new System.Drawing.Size(47, 13);
            this.lblAeroDragForce.TabIndex = 91;
            this.lblAeroDragForce.Text = "00000 N";
            // 
            // gbFender
            // 
            this.gbFender.Controls.Add(this.label168);
            this.gbFender.Controls.Add(this.srLFender);
            this.gbFender.Controls.Add(this.srRFender);
            this.gbFender.Controls.Add(this.label169);
            this.gbFender.Location = new System.Drawing.Point(5, 194);
            this.gbFender.Margin = new System.Windows.Forms.Padding(2);
            this.gbFender.Name = "gbFender";
            this.gbFender.Padding = new System.Windows.Forms.Padding(2);
            this.gbFender.Size = new System.Drawing.Size(246, 68);
            this.gbFender.TabIndex = 86;
            this.gbFender.TabStop = false;
            this.gbFender.Text = "Fenders";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Location = new System.Drawing.Point(13, 23);
            this.label168.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(94, 13);
            this.label168.TabIndex = 78;
            this.label168.Text = "Left Fender Flare:";
            // 
            // srLFender
            // 
            this.srLFender.BackColor = System.Drawing.Color.Transparent;
            this.srLFender.ChangeEnabled = true;
            this.srLFender.Detachable = false;
            this.srLFender.Location = new System.Drawing.Point(109, 18);
            this.srLFender.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srLFender.Name = "srLFender";
            this.srLFender.Size = new System.Drawing.Size(134, 18);
            this.srLFender.TabIndex = 76;
            this.srLFender.ValueChangeListener = null;
            // 
            // srRFender
            // 
            this.srRFender.BackColor = System.Drawing.Color.Transparent;
            this.srRFender.ChangeEnabled = true;
            this.srRFender.Detachable = false;
            this.srRFender.Location = new System.Drawing.Point(109, 42);
            this.srRFender.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRFender.Name = "srRFender";
            this.srRFender.Size = new System.Drawing.Size(134, 18);
            this.srRFender.TabIndex = 77;
            this.srRFender.ValueChangeListener = null;
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Location = new System.Drawing.Point(6, 46);
            this.label169.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(100, 13);
            this.label169.TabIndex = 79;
            this.label169.Text = "Right Fender Flare:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label166);
            this.groupBox8.Controls.Add(this.srFWing);
            this.groupBox8.Controls.Add(this.srRWing);
            this.groupBox8.Controls.Add(this.label167);
            this.groupBox8.Location = new System.Drawing.Point(5, 5);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox8.Size = new System.Drawing.Size(217, 68);
            this.groupBox8.TabIndex = 85;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Downforce";
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(9, 23);
            this.label166.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(64, 13);
            this.label166.TabIndex = 72;
            this.label166.Text = "Front Wing:";
            // 
            // srFWing
            // 
            this.srFWing.BackColor = System.Drawing.Color.Transparent;
            this.srFWing.ChangeEnabled = true;
            this.srFWing.Detachable = false;
            this.srFWing.Location = new System.Drawing.Point(79, 18);
            this.srFWing.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srFWing.Name = "srFWing";
            this.srFWing.Size = new System.Drawing.Size(134, 18);
            this.srFWing.TabIndex = 73;
            this.srFWing.ValueChangeListener = null;
            // 
            // srRWing
            // 
            this.srRWing.BackColor = System.Drawing.Color.Transparent;
            this.srRWing.ChangeEnabled = true;
            this.srRWing.Detachable = false;
            this.srRWing.Location = new System.Drawing.Point(79, 42);
            this.srRWing.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRWing.Name = "srRWing";
            this.srRWing.Size = new System.Drawing.Size(134, 18);
            this.srRWing.TabIndex = 74;
            this.srRWing.ValueChangeListener = null;
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Location = new System.Drawing.Point(13, 46);
            this.label167.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(61, 13);
            this.label167.TabIndex = 75;
            this.label167.Text = "Rear Wing:";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.srRadiator);
            this.groupBox6.Controls.Add(this.srBrakeDucts);
            this.groupBox6.Controls.Add(this.label170);
            this.groupBox6.Controls.Add(this.label171);
            this.groupBox6.Location = new System.Drawing.Point(5, 78);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox6.Size = new System.Drawing.Size(217, 68);
            this.groupBox6.TabIndex = 84;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Cooling";
            // 
            // srRadiator
            // 
            this.srRadiator.BackColor = System.Drawing.Color.Transparent;
            this.srRadiator.ChangeEnabled = true;
            this.srRadiator.Detachable = false;
            this.srRadiator.Location = new System.Drawing.Point(79, 18);
            this.srRadiator.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRadiator.Name = "srRadiator";
            this.srRadiator.Size = new System.Drawing.Size(134, 18);
            this.srRadiator.TabIndex = 81;
            this.srRadiator.ValueChangeListener = null;
            // 
            // srBrakeDucts
            // 
            this.srBrakeDucts.BackColor = System.Drawing.Color.Transparent;
            this.srBrakeDucts.ChangeEnabled = true;
            this.srBrakeDucts.Detachable = false;
            this.srBrakeDucts.Location = new System.Drawing.Point(79, 41);
            this.srBrakeDucts.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srBrakeDucts.Name = "srBrakeDucts";
            this.srBrakeDucts.Size = new System.Drawing.Size(134, 18);
            this.srBrakeDucts.TabIndex = 83;
            this.srBrakeDucts.ValueChangeListener = null;
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(23, 22);
            this.label170.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(52, 13);
            this.label170.TabIndex = 80;
            this.label170.Text = "Radiator:";
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Location = new System.Drawing.Point(5, 46);
            this.label171.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(68, 13);
            this.label171.TabIndex = 82;
            this.label171.Text = "Brake Ducts:";
            // 
            // tabPage23
            // 
            this.tabPage23.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tabPage23.Controls.Add(this.gbAssists);
            this.tabPage23.Controls.Add(this.groupBox4);
            this.tabPage23.Location = new System.Drawing.Point(4, 22);
            this.tabPage23.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage23.Name = "tabPage23";
            this.tabPage23.Padding = new System.Windows.Forms.Padding(2);
            this.tabPage23.Size = new System.Drawing.Size(678, 295);
            this.tabPage23.TabIndex = 7;
            this.tabPage23.Text = "Engine";
            // 
            // gbAssists
            // 
            this.gbAssists.Controls.Add(this.srTC);
            this.gbAssists.Controls.Add(this.label176);
            this.gbAssists.Controls.Add(this.label177);
            this.gbAssists.Controls.Add(this.srABS);
            this.gbAssists.Location = new System.Drawing.Point(5, 128);
            this.gbAssists.Margin = new System.Windows.Forms.Padding(2);
            this.gbAssists.Name = "gbAssists";
            this.gbAssists.Padding = new System.Windows.Forms.Padding(2);
            this.gbAssists.Size = new System.Drawing.Size(234, 70);
            this.gbAssists.TabIndex = 104;
            this.gbAssists.TabStop = false;
            this.gbAssists.Text = "Assists";
            // 
            // srTC
            // 
            this.srTC.BackColor = System.Drawing.Color.Transparent;
            this.srTC.ChangeEnabled = true;
            this.srTC.Detachable = false;
            this.srTC.Location = new System.Drawing.Point(91, 18);
            this.srTC.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srTC.Name = "srTC";
            this.srTC.Size = new System.Drawing.Size(134, 19);
            this.srTC.TabIndex = 101;
            this.srTC.ValueChangeListener = null;
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Location = new System.Drawing.Point(62, 24);
            this.label176.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(24, 13);
            this.label176.TabIndex = 100;
            this.label176.Text = "TC:";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.Location = new System.Drawing.Point(57, 48);
            this.label177.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(30, 13);
            this.label177.TabIndex = 103;
            this.label177.Text = "ABS:";
            // 
            // srABS
            // 
            this.srABS.BackColor = System.Drawing.Color.Transparent;
            this.srABS.ChangeEnabled = true;
            this.srABS.Detachable = false;
            this.srABS.Location = new System.Drawing.Point(91, 42);
            this.srABS.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srABS.Name = "srABS";
            this.srABS.Size = new System.Drawing.Size(134, 19);
            this.srABS.TabIndex = 102;
            this.srABS.ValueChangeListener = null;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label172);
            this.groupBox4.Controls.Add(this.label175);
            this.groupBox4.Controls.Add(this.srRevlimit);
            this.groupBox4.Controls.Add(this.label174);
            this.groupBox4.Controls.Add(this.srBoost);
            this.groupBox4.Controls.Add(this.label173);
            this.groupBox4.Controls.Add(this.srEngineMixture);
            this.groupBox4.Controls.Add(this.srEngineBreaking);
            this.groupBox4.Location = new System.Drawing.Point(5, 5);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(234, 118);
            this.groupBox4.TabIndex = 100;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Engine Settings";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Location = new System.Drawing.Point(38, 25);
            this.label172.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(48, 13);
            this.label172.TabIndex = 92;
            this.label172.Text = "Revlimit:";
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Location = new System.Drawing.Point(5, 94);
            this.label175.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(81, 13);
            this.label175.TabIndex = 99;
            this.label175.Text = "Brake Mapping:";
            // 
            // srRevlimit
            // 
            this.srRevlimit.BackColor = System.Drawing.Color.Transparent;
            this.srRevlimit.ChangeEnabled = true;
            this.srRevlimit.Detachable = false;
            this.srRevlimit.Location = new System.Drawing.Point(91, 19);
            this.srRevlimit.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srRevlimit.Name = "srRevlimit";
            this.srRevlimit.Size = new System.Drawing.Size(134, 19);
            this.srRevlimit.TabIndex = 93;
            this.srRevlimit.ValueChangeListener = null;
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(40, 71);
            this.label174.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(47, 13);
            this.label174.TabIndex = 98;
            this.label174.Text = "Mixture:";
            // 
            // srBoost
            // 
            this.srBoost.BackColor = System.Drawing.Color.Transparent;
            this.srBoost.ChangeEnabled = true;
            this.srBoost.Detachable = false;
            this.srBoost.Location = new System.Drawing.Point(91, 42);
            this.srBoost.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srBoost.Name = "srBoost";
            this.srBoost.Size = new System.Drawing.Size(134, 19);
            this.srBoost.TabIndex = 94;
            this.srBoost.ValueChangeListener = null;
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Location = new System.Drawing.Point(48, 48);
            this.label173.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(38, 13);
            this.label173.TabIndex = 97;
            this.label173.Text = "Boost:";
            // 
            // srEngineMixture
            // 
            this.srEngineMixture.BackColor = System.Drawing.Color.Transparent;
            this.srEngineMixture.ChangeEnabled = true;
            this.srEngineMixture.Detachable = false;
            this.srEngineMixture.Location = new System.Drawing.Point(91, 66);
            this.srEngineMixture.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.srEngineMixture.Name = "srEngineMixture";
            this.srEngineMixture.Size = new System.Drawing.Size(134, 19);
            this.srEngineMixture.TabIndex = 95;
            this.srEngineMixture.ValueChangeListener = null;
            // 
            // btnMakeMainWindow
            // 
            this.btnMakeMainWindow.Name = "btnMakeMainWindow";
            this.btnMakeMainWindow.Size = new System.Drawing.Size(125, 20);
            this.btnMakeMainWindow.Text = "Make Main Window";
            this.btnMakeMainWindow.Click += new System.EventHandler(this.btnMakeMainWindow_Click);
            // 
            // CarStat2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(698, 375);
            this.Controls.Add(this.checkShowUpgrades);
            this.Controls.Add(this.grpUpgrades);
            this.Controls.Add(this.tabs);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip2);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(710, 365);
            this.Name = "CarStat2";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CarStat 2.4";
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.caricon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.grpUpgrades.ResumeLayout(false);
            this.grpUpgrades.PerformLayout();
            this.tabPitstops.ResumeLayout(false);
            this.tabControlPitstop.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            this.grpPitcrew.ResumeLayout(false);
            this.grpPitcrew.PerformLayout();
            this.grpDamage.ResumeLayout(false);
            this.grpDamage.PerformLayout();
            this.grpTireChange.ResumeLayout(false);
            this.grpTireChange.PerformLayout();
            this.grpAdjustments.ResumeLayout(false);
            this.grpAdjustments.PerformLayout();
            this.grpRefueling.ResumeLayout(false);
            this.grpRefueling.PerformLayout();
            this.tabPitstopSim.ResumeLayout(false);
            this.tabPitstopSim.PerformLayout();
            this.grpPitResult.ResumeLayout(false);
            this.grpPitResult.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.grpCompound4.ResumeLayout(false);
            this.grpCompound3.ResumeLayout(false);
            this.grpCompound2.ResumeLayout(false);
            this.grpCompound2.PerformLayout();
            this.tabTires2.ResumeLayout(false);
            this.tabTires.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.grpCompound1.ResumeLayout(false);
            this.grpCompound1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.grpThermal1.ResumeLayout(false);
            this.grpThermal1.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.grpThermal2.ResumeLayout(false);
            this.grpThermal2.PerformLayout();
            this.tabBrakes.ResumeLayout(false);
            this.tabBrakes.PerformLayout();
            this.grpRearBrakes.ResumeLayout(false);
            this.grpRearBrakes.PerformLayout();
            this.grpFrontBrakes.ResumeLayout(false);
            this.grpFrontBrakes.PerformLayout();
            this.tabEngine.ResumeLayout(false);
            this.tabEngine.PerformLayout();
            this.tabEngineData.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.tabGeneral.ResumeLayout(false);
            this.tabGeneral.PerformLayout();
            this.tabControlGeneral.ResumeLayout(false);
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabPage13.ResumeLayout(false);
            this.tabPage13.PerformLayout();
            this.tabPage11.ResumeLayout(false);
            this.tabPage11.PerformLayout();
            this.tabPage24.ResumeLayout(false);
            this.tabPage24.PerformLayout();
            this.grpTeam.ResumeLayout(false);
            this.grpTeam.PerformLayout();
            this.tabs.ResumeLayout(false);
            this.tabPage12.ResumeLayout(false);
            this.tabSetupControl.ResumeLayout(false);
            this.tabPage14.ResumeLayout(false);
            this.tabPage14.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage15.ResumeLayout(false);
            this.tabPage15.PerformLayout();
            this.tabControlDriveline.ResumeLayout(false);
            this.tabPage21.ResumeLayout(false);
            this.tabPage22.ResumeLayout(false);
            this.tabPage22.PerformLayout();
            this.tabPage16.ResumeLayout(false);
            this.tabPage16.PerformLayout();
            this.tabPage17.ResumeLayout(false);
            this.tabPage17.PerformLayout();
            this.tabPage18.ResumeLayout(false);
            this.gbSpringRubber.ResumeLayout(false);
            this.gbSpringRubber.PerformLayout();
            this.gbChassisAdj.ResumeLayout(false);
            this.gbChassisAdj.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabPage19.ResumeLayout(false);
            this.tabPage19.PerformLayout();
            this.tabPage20.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.gbFender.ResumeLayout(false);
            this.gbFender.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabPage23.ResumeLayout(false);
            this.gbAssists.ResumeLayout(false);
            this.gbAssists.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openVEHicleFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem openVehicleSelectorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openTBCFileToolStripMenuItem;
        private Misc.MixedCheckBoxesTreeView treeUpgrades;
        private System.Windows.Forms.TextBox txtUpgradeDescription;
        private System.Windows.Forms.GroupBox grpUpgrades;
        private System.Windows.Forms.TabPage tabPitstops;
        private System.Windows.Forms.GroupBox grpAdjustments;
        private System.Windows.Forms.Label lblDriverswap;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label lblSpringRubber;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label lblPressureAdjustment;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label lblTrackbar;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label lblRadiator;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label lblWedge;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label lblRearWing;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label lblFrontWing;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox grpDamage;
        private System.Windows.Forms.Label lblConcurRepair;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label lblDamageDelay;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label lblDamageSusp;
        private System.Windows.Forms.Label lblDamageAero;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label lblConcurrent;
        private System.Windows.Forms.GroupBox grpTireChange;
        private System.Windows.Forms.Label lblDelayTirechange;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label lblChange4tires;
        private System.Windows.Forms.Label lblChange2tires;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label lblConcurrent2;
        private System.Windows.Forms.GroupBox grpRefueling;
        private System.Windows.Forms.Label lblNozzleRemoval;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label lblNozzleInsertion;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label lblMaxFuelDelay;
        private System.Windows.Forms.Label lblFuelFillRate;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox grpPitcrew;
        private System.Windows.Forms.Label lblMaxDelay;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label lblDelayMulti;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label lblPreparation;
        private System.Windows.Forms.Label lblGiveup;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox grpCompound4;
        private System.Windows.Forms.CheckedListBox CheckCompounds4;
        private ZedGraph.ZedGraphControl zedTire2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox grpCompound3;
        private System.Windows.Forms.CheckedListBox CheckCompounds3;
        private ZedGraph.ZedGraphControl zedTire;
        private System.Windows.Forms.PictureBox pictureBox1;
        private ZedGraph.ZedGraphControl zedTireTemps;
        private System.Windows.Forms.GroupBox grpCompound2;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label lblTireTemp;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.CheckedListBox CheckCompounds2;
        private System.Windows.Forms.TabPage tabTires2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox grpCompound1;
        private System.Windows.Forms.CheckedListBox CheckCompounds1;
        private ZedGraph.ZedGraphControl zedWear;
        private System.Windows.Forms.TabPage tabBrakes;
        private System.Windows.Forms.Label lblBrakeBias;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.GroupBox grpRearBrakes;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label lblRearBrakeFail;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label lblRearBrakeThick;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label lblRearBrakeTorque;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblRearBrakes;
        private ZedGraph.ZedGraphControl zedBrakeRear;
        private System.Windows.Forms.GroupBox grpFrontBrakes;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label lblFrontBrakeFail;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label lblFrontBrakeThick;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label lblFrontBrakeTorque;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lblFrontBrakes;
        private ZedGraph.ZedGraphControl zedBrakeFront;
        private System.Windows.Forms.TabPage tabEngine;
        private System.Windows.Forms.Button btnRescale;
        private System.Windows.Forms.Label lblAspiration;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label lblLifetimeVar;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label lblPowerWeight;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lblPower;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lblTorque;
        private System.Windows.Forms.Label lblRevlimit;
        private System.Windows.Forms.Label lblMaxrpm;
        private System.Windows.Forms.Label lblLifetime;
        private System.Windows.Forms.Label lblSafeOil;
        private System.Windows.Forms.Label lblOptimal;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private ZedGraph.ZedGraphControl zedEngine;
        private System.Windows.Forms.TabPage tabGeneral;
        private System.Windows.Forms.CheckBox checkShowUpgrades;
        private System.Windows.Forms.Label lblNumberGears;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label lblWheelRotation;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label lblWeightDistribution;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label lblDrag;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label lblWheels;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lblUpshift;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblStarter;
        private System.Windows.Forms.Label lblLimiter;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblFuel;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lblCG;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.GroupBox grpTeam;
        private System.Windows.Forms.Label lblWins;
        private System.Windows.Forms.Label lblPoles;
        private System.Windows.Forms.Label lblStarts;
        private System.Windows.Forms.Label lblHQ;
        private System.Windows.Forms.Label lblFullTeam;
        private System.Windows.Forms.Label lblFounded;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblManufacturer;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblEngine;
        private System.Windows.Forms.Label lblDriver;
        private System.Windows.Forms.Label lblTeam;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label lblDownshift;
        private System.Windows.Forms.Label label12347;
        private System.Windows.Forms.Label lblConcurrentDriver;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label lblRearTireRim;
        private System.Windows.Forms.Label lblRearTireMass;
        private System.Windows.Forms.Label lblFrontTireMass;
        private System.Windows.Forms.Label lblFrontTireRim;
        private System.Windows.Forms.Label lblTireStartTemp;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.CheckedListBox CheckCompounds5;
        private ZedGraph.ZedGraphControl zedThermal1;
        private System.Windows.Forms.GroupBox grpThermal1;
        private System.Windows.Forms.Label lblDegActivation;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label lblDegHistoryStep;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.TabControl tabTires;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox grpThermal2;
        private System.Windows.Forms.ListBox CheckCompounds6;
        private System.Windows.Forms.Label lblDegActivation2;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label lblDegHistoryStep2;
        private System.Windows.Forms.Label label87;
        private ZedGraph.ZedGraphControl zedThermal2;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.TextBox txtCustomTemp;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label lblInvalidTemp;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label lblClass;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Button btnGetUpgradeFile;
        private System.Windows.Forms.Label lblTurbo;
        private System.Windows.Forms.Label lblPressureChangePossible;
        private System.Windows.Forms.Label label93;
        public System.Windows.Forms.Label labelTire2Mass;
        public System.Windows.Forms.Label labelTire1Mass;
        private System.Windows.Forms.PictureBox caricon;
        internal System.Windows.Forms.Label labelRim2;
        internal System.Windows.Forms.Label labelRim1;
        private System.Windows.Forms.Label lblAntiRoll;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label lblTrackbarFly;
        private System.Windows.Forms.Label label82;
        internal System.Windows.Forms.CheckBox chkEngineMixture;
        internal System.Windows.Forms.ComboBox cbFuelMixture;
        internal System.Windows.Forms.CheckBox chkBoostMapping;
        internal System.Windows.Forms.ComboBox cbBoostMapping;
        internal System.Windows.Forms.RadioButton radbAspTurbo;
        internal System.Windows.Forms.RadioButton radbAspNaturally;
        private System.Windows.Forms.TabControl tabEngineData;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label lblFuelConsumtion;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label lblEngineInertia;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label lblTurboPressure;
        private System.Windows.Forms.Label lblAntiStall;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label lblSemiAuto;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.TabControl tabControlPitstop;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.TabPage tabPitstopSim;
        private System.Windows.Forms.GroupBox grpPitResult;
        private System.Windows.Forms.Label lblPitTotal;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label lblPitMisc;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label lblPitDamage;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label lblPitTires;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label lblPitFuel;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label lblPitstopTime;
        private System.Windows.Forms.CheckBox chkPitStopManualOverwrite;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox chkDriverswap;
        private System.Windows.Forms.CheckBox chkTrackbar;
        private System.Windows.Forms.CheckBox chkSpringRubber;
        private System.Windows.Forms.CheckBox chkWedge;
        private System.Windows.Forms.CheckBox chkTirePressure;
        private System.Windows.Forms.CheckBox chkRadiatorTap;
        private System.Windows.Forms.CheckBox chkRearWing;
        private System.Windows.Forms.CheckBox chkFrontWing;
        private System.Windows.Forms.CheckBox chkSuspDamage;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.CheckBox chkAeroDamage;
        private System.Windows.Forms.ComboBox dropChangeTires;
        private System.Windows.Forms.TextBox txtAmountFuel;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Button btnSimulatePitstop;
        private System.Windows.Forms.ToolStripMenuItem openOptionsMenu;
        private System.Windows.Forms.TabControl tabControlGeneral;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label lblAutoPit;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label lblAutoBlip;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label lblAutoLift;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label lblSCLow;
        private System.Windows.Forms.Label lblSCHigh;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label lblAutoShiftFull;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label lblAutoShiftOne;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label lblTCLow;
        private System.Windows.Forms.Label lblTCHigh;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label lblABSHigh;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label lblABSLow;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label lblFuelMaxRPM;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label lblFuelMaxTorque;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label lblFuelMaxPower;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label lblFrontTrackWidth;
        private System.Windows.Forms.Label lblRightWheelBase;
        private System.Windows.Forms.Label lblRearTrackWidth;
        private System.Windows.Forms.Label lblLeftWheelBase;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDefaultSetup;
        private System.Windows.Forms.Button btnLoadSetup;
        private System.Windows.Forms.Button btnSaveSetup;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label107;
        internal Misc.SetupRow srFuel;
        internal System.Windows.Forms.RichTextBox tbSetupNotes;
        private System.Windows.Forms.Label label109;
        internal Misc.SetupRow srPitStops;
        internal Misc.SetupRow srPitStop1;
        internal Misc.SetupRow srPitStop3;
        internal Misc.SetupRow srPitStop2;
        internal System.Windows.Forms.Label lblPitStop3;
        internal System.Windows.Forms.Label lblPitStop2;
        internal System.Windows.Forms.Label lblPitStop1;
        internal DataObjects.GearSetupRow srGear1;
        private System.Windows.Forms.Label label111;
        internal DataObjects.GearSetupRow srFinalDrive;
        internal DataObjects.GearSetupRow srGear9;
        internal DataObjects.GearSetupRow srGear2;
        internal DataObjects.GearSetupRow srGear3;
        internal DataObjects.GearSetupRow srGear4;
        internal DataObjects.GearSetupRow srGear5;
        internal DataObjects.GearSetupRow srGear6;
        internal DataObjects.GearSetupRow srGear7;
        internal DataObjects.GearSetupRow srGear8;
        internal System.Windows.Forms.Label lblGear1;
        internal System.Windows.Forms.Label lblGear9;
        internal System.Windows.Forms.Label lblGear2;
        internal System.Windows.Forms.Label lblGear3;
        internal System.Windows.Forms.Label lblGear4;
        internal System.Windows.Forms.Label lblGear5;
        internal System.Windows.Forms.Label lblGear6;
        internal System.Windows.Forms.Label lblGear7;
        internal System.Windows.Forms.Label lblGear8;
        private System.Windows.Forms.TabPage tabPage16;
        internal Misc.SetupRow srFLRideHeight;
        internal Misc.SetupRow srFLPacker;
        internal Misc.SetupRow srFLFastRebound;
        internal Misc.SetupRow srFLFastBump;
        internal Misc.SetupRow srFLSlowRebound;
        internal Misc.SetupRow srFLSlowBump;
        internal Misc.SetupRow srFLSpring;
        internal Misc.SetupRow srFRRideHeight;
        internal Misc.SetupRow srFRPacker;
        internal Misc.SetupRow srFRFastRebound;
        internal Misc.SetupRow srFRFastBump;
        internal Misc.SetupRow srFRSlowRebound;
        internal Misc.SetupRow srFRSlowBump;
        internal Misc.SetupRow srFRSpring;
        internal Misc.SetupRow srFPacker;
        internal Misc.SetupRow srFFastRebound;
        internal Misc.SetupRow srFFastBump;
        internal Misc.SetupRow srFSlowRebound;
        internal Misc.SetupRow srFSlowBump;
        internal Misc.SetupRow srFSpring;
        internal System.Windows.Forms.Label lblFThirdSpring;
        internal System.Windows.Forms.Label lblFToeOffset;
        internal Misc.SetupRow srFToeOffset;
        internal Misc.SetupRow srFToeIn;
        internal Misc.SetupRow srFARB;
        internal Misc.SetupRow srFRCaster;
        internal Misc.SetupRow srFRCamber;
        internal Misc.SetupRow srFLCaster;
        internal Misc.SetupRow srFLCamber;
        internal System.Windows.Forms.CheckBox chkSymmetric;
        private System.Windows.Forms.TabPage tabPage17;
        internal System.Windows.Forms.Label lblSetupTrackbar;
        internal System.Windows.Forms.Label lblRToeOffset;
        internal Misc.SetupRow srRToeOffset;
        internal Misc.SetupRow srRToeIn;
        internal Misc.SetupRow srRARB;
        internal System.Windows.Forms.Label lblRThirdSpring;
        internal Misc.SetupRow srRRRideHeight;
        internal Misc.SetupRow srRRPacker;
        internal Misc.SetupRow srRRFastRebound;
        internal Misc.SetupRow srRRFastBump;
        internal Misc.SetupRow srRRSlowRebound;
        internal Misc.SetupRow srRRSlowBump;
        internal Misc.SetupRow srRRCamber;
        internal Misc.SetupRow srRRTrackbar;
        internal Misc.SetupRow srRRSpring;
        internal Misc.SetupRow srRPacker;
        internal Misc.SetupRow srRFastRebound;
        internal Misc.SetupRow srRFastBump;
        internal Misc.SetupRow srRSlowRebound;
        internal Misc.SetupRow srRSlowBump;
        internal Misc.SetupRow srRSpring;
        internal Misc.SetupRow srRLRideHeight;
        internal Misc.SetupRow srRLPacker;
        internal Misc.SetupRow srRLFastRebound;
        internal Misc.SetupRow srRLFastBump;
        internal Misc.SetupRow srRLSlowRebound;
        internal Misc.SetupRow srRLSlowBump;
        internal Misc.SetupRow srRLSpring;
        internal Misc.SetupRow srRLTrackbar;
        internal Misc.SetupRow srRLCamber;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label140;
        internal System.Windows.Forms.CheckBox chkSymmetric01;
        private System.Windows.Forms.TabPage tabPage18;
        private System.Windows.Forms.GroupBox groupBox3;
        internal Misc.SetupRow srWeightHeight;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label122;
        internal Misc.SetupRow srWedge;
        private System.Windows.Forms.Label label141;
        internal System.Windows.Forms.Label lblChassisAdj00;
        internal System.Windows.Forms.GroupBox gbChassisAdj;
        internal Misc.SetupRow srChassisAdj00;
        internal System.Windows.Forms.GroupBox gbSpringRubber;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label144;
        internal Misc.SetupRow srSpringRubberRL;
        internal Misc.SetupRow srSpringRubberRR;
        internal Misc.SetupRow srSpringRubberFL;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label142;
        internal Misc.SetupRow srSpringRubberFR;
        private System.Windows.Forms.TabPage tabPage19;
        private System.Windows.Forms.TabPage tabPage20;
        internal System.Windows.Forms.Label lblGearR;
        internal DataObjects.GearSetupRow srGearR;
        internal ZedGraph.ZedGraphControl zedGearboxGraph;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label147;
        internal Misc.SetupRow srDiffPreload;
        internal Misc.SetupRow srDiffCoast;
        internal Misc.SetupRow srDiffPower;
        internal Misc.SetupRow srDiffPump;
        private System.Windows.Forms.TabControl tabControlDriveline;
        private System.Windows.Forms.TabPage tabPage21;
        private System.Windows.Forms.TabPage tabPage22;
        internal System.Windows.Forms.Button btnRescaleSpeedGraph;
        internal System.Windows.Forms.Label lblTopSpeed;
        private System.Windows.Forms.Label label150;
        internal System.Windows.Forms.TabControl tabSetupControl;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label154;
        internal Misc.SetupRow srFCompound;
        internal Misc.SetupRow srFLPressure;
        internal Misc.SetupRow srFRDisc;
        internal Misc.SetupRow srFRPressure;
        internal Misc.SetupRow srFLDisc;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        internal Misc.SetupRow srRRDisc;
        private System.Windows.Forms.Label label158;
        internal Misc.SetupRow srRRPressure;
        private System.Windows.Forms.Label label159;
        internal Misc.SetupRow srRLDisc;
        internal Misc.SetupRow srRCompound;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Label label162;
        internal Misc.SetupRow srRLPressure;
        private System.Windows.Forms.Label label163;
        internal Misc.SetupRow srBrakePressure;
        private System.Windows.Forms.Label label164;
        internal System.Windows.Forms.CheckBox chkRealValues;
        internal Misc.SetupRow srHandBrakePressure;
        internal Misc.SetupRow srSteeringLock;
        private System.Windows.Forms.Label label165;
        internal System.Windows.Forms.Label lblHandBrakePressure;
        private System.Windows.Forms.Label label166;
        internal Misc.SetupRow srFWing;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.Label label168;
        internal Misc.SetupRow srRFender;
        internal Misc.SetupRow srLFender;
        private System.Windows.Forms.Label label167;
        internal Misc.SetupRow srRWing;
        internal Misc.SetupRow srRadiator;
        private System.Windows.Forms.Label label170;
        internal Misc.SetupRow srBrakeDucts;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.TabPage tabPage23;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label175;
        internal Misc.SetupRow srRevlimit;
        private System.Windows.Forms.Label label174;
        internal Misc.SetupRow srBoost;
        private System.Windows.Forms.Label label173;
        internal Misc.SetupRow srEngineMixture;
        internal Misc.SetupRow srEngineBreaking;
        internal System.Windows.Forms.GroupBox gbFender;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label176;
        internal Misc.SetupRow srTC;
        internal Misc.SetupRow srABS;
        private System.Windows.Forms.Label label177;
        internal System.Windows.Forms.GroupBox gbAssists;
        internal System.Windows.Forms.Label lblTorqueSplit;
        private System.Windows.Forms.Label lblWearTemp;
        private System.Windows.Forms.Label lblWearRPM;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.Label lblAeroDragForce;
        private System.Windows.Forms.Label lblRearDownforce;
        private System.Windows.Forms.Label lblFrontDownforce;
        private System.Windows.Forms.Label lblAeroDragFactor;
        private System.Windows.Forms.Label lblRearLift;
        private System.Windows.Forms.Label lblFrontLift;
        private System.Windows.Forms.Label label183;
        internal System.Windows.Forms.TextBox tbAeroSpeed;
        internal DataObjects.BalanceSetupRow srWeightLateral;
        internal DataObjects.BalanceSetupRow srWeightLong;
        internal DataObjects.BalanceSetupRow srBrakeBais;
        internal DataObjects.BalanceSetupRow srTorqueSplit;
        private System.Windows.Forms.GroupBox groupBox9;
        internal System.Windows.Forms.CheckBox CheckEngineFriction;
        private System.Windows.Forms.TabPage tabPage24;
        private System.Windows.Forms.Label lblUpgradesINI;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label lblEngineINI;
        private System.Windows.Forms.Label lblTBC;
        private System.Windows.Forms.Label lblVEH;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Label lblHDV;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.Label label194;
        private System.Windows.Forms.Label label195;
        private System.Windows.Forms.Label lblChassisINI;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.ToolStripMenuItem openSetupsManager;
        private System.Windows.Forms.ToolStripMenuItem openEngineFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnMakeMainWindow;
    }
}

