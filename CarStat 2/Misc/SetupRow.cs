﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Misc
{
    public partial class SetupRow : UserControl
    {
        protected CarSetupOption setupOption;
        protected int selectedIndex = -1;
        protected bool displayRealValues = false;
        protected string realValuesUnit = "";
        protected SetupRow partner = null;
        protected ValueFactory valueFactory = new StandardFactory();
        
        protected Timer timer;

        public EventHandler ValueChangeListener { get; set; }
        public bool ChangeEnabled { get; set; }
        public bool Detachable { get; set; }

        public SetupRow()
        {
            InitializeComponent();

            ChangeEnabled = true;

            //EventHandlers
            plus.MouseDown += new MouseEventHandler(this.plusDown);
            plus.MouseUp += new MouseEventHandler(this.mouseUp);
            plus.MouseLeave += new EventHandler(this.mouseLeave);

            minus.MouseDown += new MouseEventHandler(this.minusDown);
            minus.MouseUp += new MouseEventHandler(this.mouseUp);
            minus.MouseLeave += new EventHandler(this.mouseLeave);

            Detachable = false;
        }

        #region Interface

        public void setCarSetupOption(CarSetupOption setupOption)
        {
            if (setupOption == null)
            {
                this.Visible = false;
                return;
            }

            this.Visible = true;
            this.setupOption = setupOption;

            if (selectedIndex == -1 || selectedIndex >= setupOption.NumOfSettings)
                setSettingTo(setupOption.DefaultSetting);

            writeValue();
        }

        public void setRealValuesUnit(string unit)
        {
            realValuesUnit = unit;
        }

        public void setSettingTo(int setting)
        {
            if (setupOption == null)
                return;

            if (!ChangeEnabled && setting != setupOption.DefaultSetting)
                goToDefault();

            if (setting < 0)
                setting = 0;

            if (setting >= setupOption.NumOfSettings && setupOption.NumOfSettings > 0)
                setting = setupOption.NumOfSettings - 1;
            else if (setting >= setupOption.NumOfSettings)
                setting = 0;

            selectedIndex = setting;

            if (partner != null && partner.getSetting() != selectedIndex)
                partner.setSettingTo(selectedIndex);

            writeValue();
        }

        public void goToDefault()
        {
            if (setupOption == null)
                return;

            setSettingTo(setupOption.DefaultSetting);
            writeValue();
        }

        public void setDisplayRealValues(bool realValues)
        {
            this.displayRealValues = realValues;

            writeValue();
        }

        public void setValueFactory(ValueFactory valueFactory)
        {
            this.valueFactory = valueFactory;
        }

        public int getSetting()
        {
            return selectedIndex;
        }

        public virtual double getValue()
        {
            if (setupOption == null)
                return 0;

            return setupOption.AllOptions[selectedIndex].Value;
        }

        public virtual double getFactoryValue()
        {
            double value = getValue();
            if (valueFactory != null)
                value = valueFactory.getValue(value);

            return value;
        }

        public ValueFactory getValueFactory()
        {
            return valueFactory;
        }

        public CarSetupOption getCarSetupOption()
        {
            return setupOption;
        }

        public void setPartner(SetupRow setupRow)
        {
            this.partner = setupRow;
        }

        #endregion

        #region Internal

        private void plusDown(object sender, MouseEventArgs e)
        {
            plusAction(this, null);

            timer = new Timer();
            timer.Tick += new EventHandler(plusAction);
            timer.Interval = 200;

            timer.Start();
        }

        private void minusDown(object sender, MouseEventArgs e)
        {
            minusAction(this, null);

            timer = new Timer();
            timer.Tick += new EventHandler(minusAction);
            timer.Interval = 200;

            timer.Start();
        }

        private void mouseUp(object sender, MouseEventArgs e)
        {
            if (timer != null)
            {
                timer.Stop();
                timer.Dispose();
                timer = null;
            }
        }

        //Because mouseleave does not want a MouseEventHandler
        private void mouseLeave(object sender, EventArgs e) { mouseUp(null, null); }

        private void plusAction(object sender, EventArgs e)
        {
            selectedIndex++;
            writeValue();

            if (partner != null)
                partner.setSettingTo(selectedIndex);

            if (timer != null && timer.Interval > 10)
                timer.Interval = (int) (timer.Interval * 0.85);

            if (ValueChangeListener != null)
                ValueChangeListener.Invoke(sender, e);
        }

        private void minusAction(object sender, EventArgs e)
        {
            selectedIndex--;
            writeValue();

            if (partner != null)
                partner.setSettingTo(selectedIndex);

            if (timer != null && timer.Interval > 10)
                timer.Interval = (int)(timer.Interval * 0.85);

            if (ValueChangeListener != null)
                ValueChangeListener.Invoke(sender, e);
        }

        protected virtual void disableOptions()
        {
            if (!ChangeEnabled)
            {
                minus.Enabled = false;
                plus.Enabled = false;
                return;
            }

            //Disableing plus and minus if necessary
            if (selectedIndex == 0)
                minus.Enabled = false;
            else
                minus.Enabled = true;

            if (selectedIndex < (setupOption.NumOfSettings - 1))
                plus.Enabled = true;
            else
                plus.Enabled = false;
        }

        public virtual string getText()
        {
            if (setupOption == null || setupOption.AllOptions == null || setupOption.AllOptions.Length <= selectedIndex || selectedIndex < 0)
                return "";

            if (Detachable && selectedIndex == 0)
                return "Detached";

            CarSetupOption.SetupValue value = setupOption.AllOptions[selectedIndex];

            string text = "";

            if (displayRealValues)
            {
                double number = value.Value;
                if (valueFactory != null)
                    number = valueFactory.getValue(number);

                text = number + " " + realValuesUnit;
            }
            else
            {
                if (value.Unit != null)
                    text = value.Unit;

                text = value.Text + text;
            }

            return text;
        }

        public void writeValue()
        {
            if (setupOption == null)
                return;

            disableOptions();

            if (!plus.Enabled || !minus.Enabled)
                this.mouseUp(null, null);

            valueDisplay.Text = getText();
        }

        #endregion

        //Value Factory
        public interface ValueFactory
        {
            double getValue(double realValue);
        }

        public class StandardFactory : ValueFactory
        {
            public double getValue(double realValue)
            {
                return realValue;
            }
        }

        public class RoundingFactory : ValueFactory
        {
            private int numberOfDecimals;

            public RoundingFactory(int numberOfDecimals)
            {
                this.numberOfDecimals = numberOfDecimals;
            }

            public double getValue(double realValue)
            {
                return Math.Round(realValue, numberOfDecimals);
            }
        }

        public class MultiFactory : ValueFactory
        {
            private int numberOfDecimals;
            private double multiplier;

            public MultiFactory(int numberOfDecimals, double multiplier)
            {
                this.numberOfDecimals = numberOfDecimals;
                this.multiplier = multiplier;
            }

            public double getValue(double realValue)
            {
                return Math.Round(realValue*multiplier, numberOfDecimals);
            }
        }
    }
}
