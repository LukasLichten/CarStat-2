﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Misc
{
    public class VersionNumber
    {
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Patch { get; set; }

        public int Beta { get; set; }

        public VersionNumber(int major, int minor, int patch, int beta)
        {
            this.Major = major;
            this.Minor = minor;
            this.Patch = patch;
            this.Beta = beta;
        }

        public VersionNumber(string versionString)
        {
            versionString = versionString.Trim();

            //beta version. Non beta versions have the highest value
            Beta = 0;
            if (versionString.Contains('-'))
            {
                string value = versionString.Split('-')[1];
                value = value.ToLower().Replace("beta", "").Trim();
                if (value == "")
                {
                    Beta = 1;
                }
                else
                {
                    Beta = Int32.Parse(value);
                }
                versionString = versionString.Split('-')[0].Trim();
            }

            string[] toConvert = versionString.Split('.');

            Major = Int32.Parse(toConvert[0].Trim());
            Minor = Int32.Parse(toConvert[1].Trim());
            Patch = Int32.Parse(toConvert[2].Trim());
        }

        public int compareTo(VersionNumber number)
        {
            if (this.Major != number.Major)
            {
                if (this.Major > number.Major)
                    return 1;
                else
                    return -1;
            }

            if (this.Minor != number.Minor)
            {
                if (this.Minor > number.Minor)
                    return 1;
                else
                    return -1;
            }

            if (this.Patch != number.Patch)
            {
                if (this.Patch > number.Patch)
                    return 1;
                else
                    return -1;
            }

            if (this.Beta != number.Beta)
            {
                if (this.Beta == 0 || this.Beta == -1)
                {
                    if (number.Beta == 0 || number.Beta == -1)
                        return 0; //Both are not a beta
                    else
                        return 1; //"number" is a beta, "this" is not, so "this" is larger
                }
                else if (number.Beta == 0 || number.Beta == -1)
                    return -1; //"this" must be a beta, so is smaller then "number" 
                else
                {
                    //Both are a beta
                    if (this.Beta > number.Beta)
                        return 1;
                    else
                        return -1;
                }
            }

            return 0;
        }

        public override string ToString()
        {
            string text = Major + "." + Minor + "." + Patch;

            if (this.Beta != 0 && this.Beta != -1)
                text = text + "-Beta " + this.Beta;

            return text;
        }
    }
}
